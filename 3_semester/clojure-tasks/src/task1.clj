;1
(defn call-twice [fun param]
    (do
        (fun param)
        (fun param)
        )
    )

;2
(defn print-return [filename]
    (let [contents (slurp filename)]
        (do
            (println contents)
            (contents)
            )
        )
    )

;3
(def cube-anonymous (fn [x] (* x (* x x))))

;4
(defn concat-reverse [a b] (concat (reverse a) (reverse b)))

;5
(defn contains' [a el]
    (if (empty? a)
        (false)
        (let [[h & t] a]
            (if (= h el) (true) (contains' t el))
            )
        )
    )

;6
(defn diff [col1 col2] (doseq [x col1 y col2] (when (not= x y) (println [x, y]))))

;7
(defn repeate [n elem] (if (= n 0) ([]) (cons elem (repeate (- n 1) elem))))
