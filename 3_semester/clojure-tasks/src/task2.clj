;factorial with multimethods
(defmulti factorial identity)
(defmethod factorial 0 [_] 1)
(defmethod factorial :default [n] (* (factorial (- n 1)) n))
(println (factorial 10))

;or macro
(defmacro my-or 
  ([] nil)
  ([x] x)
  ([head & next] `(if ~head true (my-or ~@next)))
  ) 
(my-or (println "lol") (println "azaza") true false)

;let macro
(defmacro my-let [bindings & body] `((fn ~(vec (take-nth 2 bindings)) ~@body) ~@(take-nth 2 (rest bindings))))

;bank accounts
(let [accounts-count 50
      transactions-count 100
      threads-count 20
      accounts (map (fn [_] (ref (rand-int 1000))) (range 0 accounts-count))
      transfer (fn [from-account to-account amount]
                 (when (not= from-account to-account)
                   (let [from-ref (nth accounts from-account)
                         to-ref (nth accounts to-account)]
                     (dosync
                       (when (>= @from-ref amount)
                         (ref-set from-ref (- @from-ref amount))
                         (ref-set to-ref (+ @to-ref amount))
                         ;(println (format "%d transferred from %d to %d" amount from-account to-account))
                         )
                       ;(println (format "Cannot transfer %d from %d:%d to %d:%d" amount from-account @from-ref to-account @to-ref))
                       )
                     )
                   )
                 )
      stats #(let [v (map deref accounts)]
               (println v)
               (println (format "Total money: %d" (reduce + v)))
               )]
  (stats)
  (dorun
    (apply pcalls
      (repeat threads-count #(dotimes [_ transactions-count]
                               (transfer (rand-int accounts-count) (rand-int accounts-count) (rand-int 500)))
        )
      )
    )
  (stats))

