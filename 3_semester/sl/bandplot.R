library(lattice)

bandplot <- function(formula, data, ...) {
  bands <- function(x, y, upper, lower, col, fill, subscripts, ...) {
    order <- order(x)
    upper <- upper[subscripts[order]]
    lower <- lower[subscripts[order]]
    x <- x[order]
    panel.polygon(c(x, rev(x)), c(upper, rev(lower)), col=fill, border = NA, ...)
    panel.lines(rev(x), rev(lower), col = 'red', lty = 'dashed')
    panel.lines(x, upper, col = 'red', lty = 'dashed')
  }

  xyplot(formula, data, panel = function(x, y,...) {
    panel.superpose(x, y, panel.groups = bands, type='b',...)
    panel.xyplot(x, y, type='p', cex=0.2, ...)   
    panel.loess(x, y, ...)
  }, prepanel = function(x,y,upper,lower,...) { 
    p<-prepanel.default.xyplot(x,y,...)
    p$ylim <- c(min(lower), max(upper))
    p
  }, ...)
}

n <- 10
xs <- rnorm(n)
ys <- data.frame(x = xs, y = 5 * xs)
bandplot(y ~ x, data = ys, upper = ys$y + 2, lower = ys$y - 2, groups = (y>0))

ys <- data.frame(x = xs, y = xs + runif(n, -1, 1))
bandplot(y ~ x, data = ys, upper = ys$y + 2, lower = ys$y - 2, groups = (x<0), fill='gray80', col='black')
