    library(lattice)
    library(corrplot)
    library(e1071)

    data <- read.csv('data/Concrete_Data.csv',comment.char = '#')
    names(data)[9] <- 'Strength'

Построим графики зависимостей всего и вся:

    splom(
      ~data,
      data,      
      upper.panel=function(x, y, ...) { panel.xyplot(x, y, ...); panel.loess(x, y, ..., col='red') },    
      lower.panel=function(x, y, ...) { },      
      pscale=0,       
      varname.cex=0.7, 
      xlab = NULL,
      par.settings=simpleTheme(pch='.', cex=2)
    )

![](./Task3Concrete_files/figure-markdown_strict/unnamed-chunk-2-1.png)

Обратим внимание на немонотонность `Strength ~ Age` и
`Strength ~ Water`.

Помотрим на корреляцию:

    corrplot.mixed(cor(data),tl.cex=0.5)

![](./Task3Concrete_files/figure-markdown_strict/unnamed-chunk-3-1.png)

Сразу видна линейная зависимость `Strength` от `Cement` и `Age`, а так
же то, что `Water` коррелирует с `Superplasticizer` и `FineAggregate`.

Вспомним про немонотонность, добавим факторы:

    scatter.smooth(data$Age, data$Strength)

![](./Task3Concrete_files/figure-markdown_strict/unnamed-chunk-4-1.png)

    data$ageFactor <- data$Age < 80

    scatter.smooth(data$Water, data$Strength)

![](./Task3Concrete_files/figure-markdown_strict/unnamed-chunk-5-1.png)

    data$waterFactor <- data$Water < 195

    # вспомогательная функция
    doStuff <- function(formula=Strength ~ .) {
      m <- lm(formula=formula, data=data)
      print(summary(m))
      tc <- tune.control(sampling = "fix",fix = 2/3)
      t <- tune(lm, formula, data=data, tunecontrol=tc)
      print(t)
    }

    doStuff(Strength ~ Cement + BlastFurnaceSlag + FlyAsh + Water + Superplasticizer + FineAggregate + CoarseAggregate + Age)

    ## 
    ## Call:
    ## lm(formula = formula, data = data)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -28.654  -6.302   0.703   6.569  34.450 
    ## 
    ## Coefficients:
    ##                    Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)      -23.331214  26.585504  -0.878 0.380372    
    ## Cement             0.119804   0.008489  14.113  < 2e-16 ***
    ## BlastFurnaceSlag   0.103866   0.010136  10.247  < 2e-16 ***
    ## FlyAsh             0.087934   0.012583   6.988 5.02e-12 ***
    ## Water             -0.149918   0.040177  -3.731 0.000201 ***
    ## Superplasticizer   0.292225   0.093424   3.128 0.001810 ** 
    ## FineAggregate      0.020190   0.010702   1.887 0.059491 .  
    ## CoarseAggregate    0.018086   0.009392   1.926 0.054425 .  
    ## Age                0.114222   0.005427  21.046  < 2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 10.4 on 1021 degrees of freedom
    ## Multiple R-squared:  0.6155, Adjusted R-squared:  0.6125 
    ## F-statistic: 204.3 on 8 and 1021 DF,  p-value: < 2.2e-16
    ## 
    ## 
    ## Error estimation of 'lm' using fixed training/validation set: 121.52

    doStuff(Strength ~ Cement + BlastFurnaceSlag + FlyAsh + Water + Superplasticizer + Age)

    ## 
    ## Call:
    ## lm(formula = formula, data = data)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -28.987  -6.469   0.653   6.547  34.732 
    ## 
    ## Coefficients:
    ##                   Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)      28.992982   4.213202   6.881 1.03e-11 ***
    ## Cement            0.105413   0.004246  24.825  < 2e-16 ***
    ## BlastFurnaceSlag  0.086472   0.004974  17.385  < 2e-16 ***
    ## FlyAsh            0.068660   0.007735   8.877  < 2e-16 ***
    ## Water            -0.218088   0.021129 -10.322  < 2e-16 ***
    ## Superplasticizer  0.240311   0.084567   2.842  0.00458 ** 
    ## Age               0.113492   0.005407  20.988  < 2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 10.41 on 1023 degrees of freedom
    ## Multiple R-squared:  0.614,  Adjusted R-squared:  0.6118 
    ## F-statistic: 271.2 on 6 and 1023 DF,  p-value: < 2.2e-16
    ## 
    ## 
    ## Error estimation of 'lm' using fixed training/validation set: 114.6251

    doStuff(Strength ~ Cement + BlastFurnaceSlag + FlyAsh + Water + Age)

    ## 
    ## Call:
    ## lm(formula = formula, data = data)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -31.780  -6.440   0.763   6.441  33.385 
    ## 
    ## Coefficients:
    ##                   Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)      34.825722   3.692092   9.433   <2e-16 ***
    ## Cement            0.110049   0.003934  27.974   <2e-16 ***
    ## BlastFurnaceSlag  0.092365   0.004536  20.361   <2e-16 ***
    ## FlyAsh            0.079625   0.006727  11.836   <2e-16 ***
    ## Water            -0.254982   0.016728 -15.243   <2e-16 ***
    ## Age               0.114017   0.005423  21.025   <2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 10.45 on 1024 degrees of freedom
    ## Multiple R-squared:  0.611,  Adjusted R-squared:  0.6091 
    ## F-statistic: 321.6 on 5 and 1024 DF,  p-value: < 2.2e-16
    ## 
    ## 
    ## Error estimation of 'lm' using fixed training/validation set: 116.1401

    doStuff(Strength ~ Cement + BlastFurnaceSlag + FlyAsh + Water + log(Age))

    ## 
    ## Call:
    ## lm(formula = formula, data = data)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -22.1457  -4.4504  -0.1221   4.3439  29.4486 
    ## 
    ## Coefficients:
    ##                   Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)      13.246249   2.541682   5.212 2.26e-07 ***
    ## Cement            0.110539   0.002719  40.648  < 2e-16 ***
    ## BlastFurnaceSlag  0.086829   0.003126  27.774  < 2e-16 ***
    ## FlyAsh            0.062713   0.004637  13.523  < 2e-16 ***
    ## Water            -0.252034   0.011350 -22.205  < 2e-16 ***
    ## log(Age)          8.668383   0.191823  45.190  < 2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 7.223 on 1024 degrees of freedom
    ## Multiple R-squared:  0.814,  Adjusted R-squared:  0.8131 
    ## F-statistic: 896.2 on 5 and 1024 DF,  p-value: < 2.2e-16
    ## 
    ## 
    ## Error estimation of 'lm' using fixed training/validation set: 56.72499

    doStuff(Strength ~ Cement + BlastFurnaceSlag + FlyAsh + waterFactor:Water + ageFactor:log(Age))

    ## 
    ## Call:
    ## lm(formula = formula, data = data)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -21.8414  -4.4521  -0.2326   4.4586  28.5889 
    ## 
    ## Coefficients:
    ##                          Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)             13.888154   3.428458   4.051 5.49e-05 ***
    ## Cement                   0.109538   0.002786  39.314  < 2e-16 ***
    ## BlastFurnaceSlag         0.084897   0.003182  26.679  < 2e-16 ***
    ## FlyAsh                   0.058061   0.004884  11.889  < 2e-16 ***
    ## waterFactorFALSE:Water  -0.256654   0.013771 -18.637  < 2e-16 ***
    ## waterFactorTRUE:Water   -0.260561   0.016258 -16.027  < 2e-16 ***
    ## ageFactorFALSE:log(Age)  8.694166   0.191716  45.349  < 2e-16 ***
    ## ageFactorTRUE:log(Age)   9.278837   0.263481  35.216  < 2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 7.186 on 1022 degrees of freedom
    ## Multiple R-squared:  0.8162, Adjusted R-squared:  0.815 
    ## F-statistic: 648.5 on 7 and 1022 DF,  p-value: < 2.2e-16
    ## 
    ## 
    ## Error estimation of 'lm' using fixed training/validation set: 54.90143

    doStuff(Strength ~ Cement + BlastFurnaceSlag + FlyAsh + waterFactor:Water + ageFactor:log(Age) + Cement:FlyAsh)

    ## 
    ## Call:
    ## lm(formula = formula, data = data)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -21.5229  -4.4746  -0.2679   4.2671  28.5978 
    ## 
    ## Coefficients:
    ##                           Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)              1.546e+01  3.468e+00   4.457 9.22e-06 ***
    ## Cement                   1.055e-01  3.169e-03  33.280  < 2e-16 ***
    ## BlastFurnaceSlag         8.444e-02  3.177e-03  26.578  < 2e-16 ***
    ## FlyAsh                   3.236e-02  1.077e-02   3.006  0.00271 ** 
    ## waterFactorFALSE:Water  -2.591e-01  1.376e-02 -18.829  < 2e-16 ***
    ## waterFactorTRUE:Water   -2.629e-01  1.623e-02 -16.196  < 2e-16 ***
    ## ageFactorFALSE:log(Age)  8.742e+00  1.920e-01  45.539  < 2e-16 ***
    ## ageFactorTRUE:log(Age)   9.311e+00  2.630e-01  35.408  < 2e-16 ***
    ## Cement:FlyAsh            1.047e-04  3.912e-05   2.676  0.00758 ** 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 7.164 on 1021 degrees of freedom
    ## Multiple R-squared:  0.8175, Adjusted R-squared:  0.8161 
    ## F-statistic: 571.8 on 8 and 1021 DF,  p-value: < 2.2e-16
    ## 
    ## 
    ## Error estimation of 'lm' using fixed training/validation set: 53.61356

    doStuff(Strength ~ Cement + BlastFurnaceSlag + FlyAsh + waterFactor:Water + ageFactor:log(Age) + Cement:FlyAsh + Cement:BlastFurnaceSlag)

    ## 
    ## Call:
    ## lm(formula = formula, data = data)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -23.3984  -4.3250  -0.2341   4.4855  29.8692 
    ## 
    ## Coefficients:
    ##                           Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)              1.607e+01  3.435e+00   4.678 3.28e-06 ***
    ## Cement                   9.476e-02  3.863e-03  24.531  < 2e-16 ***
    ## BlastFurnaceSlag         4.750e-02  8.400e-03   5.655 2.02e-08 ***
    ## FlyAsh                   2.292e-02  1.084e-02   2.115  0.03471 *  
    ## waterFactorFALSE:Water  -2.446e-01  1.396e-02 -17.527  < 2e-16 ***
    ## waterFactorTRUE:Water   -2.481e-01  1.636e-02 -15.164  < 2e-16 ***
    ## ageFactorFALSE:log(Age)  8.715e+00  1.901e-01  45.854  < 2e-16 ***
    ## ageFactorTRUE:log(Age)   9.324e+00  2.603e-01  35.825  < 2e-16 ***
    ## Cement:FlyAsh            1.353e-04  3.925e-05   3.447  0.00059 ***
    ## Cement:BlastFurnaceSlag  1.411e-04  2.975e-05   4.742 2.42e-06 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 7.09 on 1020 degrees of freedom
    ## Multiple R-squared:  0.8215, Adjusted R-squared:  0.8199 
    ## F-statistic: 521.4 on 9 and 1020 DF,  p-value: < 2.2e-16
    ## 
    ## 
    ## Error estimation of 'lm' using fixed training/validation set: 48.88494

    doStuff(Strength ~ Cement + BlastFurnaceSlag + waterFactor:Water + ageFactor:log(Age) + Cement:FlyAsh + Cement:BlastFurnaceSlag)

    ## 
    ## Call:
    ## lm(formula = formula, data = data)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -23.3183  -4.3887  -0.3301   4.3688  30.1950 
    ## 
    ## Coefficients:
    ##                           Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)              1.889e+01  3.171e+00   5.956 3.56e-09 ***
    ## Cement                   8.957e-02  2.989e-03  29.966  < 2e-16 ***
    ## BlastFurnaceSlag         4.263e-02  8.091e-03   5.269 1.68e-07 ***
    ## waterFactorFALSE:Water  -2.499e-01  1.375e-02 -18.167  < 2e-16 ***
    ## waterFactorTRUE:Water   -2.542e-01  1.613e-02 -15.757  < 2e-16 ***
    ## ageFactorFALSE:log(Age)  8.748e+00  1.897e-01  46.112  < 2e-16 ***
    ## ageFactorTRUE:log(Age)   9.384e+00  2.591e-01  36.217  < 2e-16 ***
    ## Cement:FlyAsh            2.096e-04  1.754e-05  11.951  < 2e-16 ***
    ## Cement:BlastFurnaceSlag  1.526e-04  2.929e-05   5.210 2.28e-07 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 7.102 on 1021 degrees of freedom
    ## Multiple R-squared:  0.8207, Adjusted R-squared:  0.8193 
    ## F-statistic: 584.1 on 8 and 1021 DF,  p-value: < 2.2e-16
    ## 
    ## 
    ## Error estimation of 'lm' using fixed training/validation set: 47.21514
