    data <- read.table('data/GlaucomaMVF.txt', header=TRUE)

    doStuff <- function (kernel, cost=2^(-5:10), ...) {
      ts <- tune.svm(
        Class ~ ., 
        data = data, 
        type = 'C-classification', 
        kernel = kernel, cost=cost, ...)
      print(ts$best.model)
      print(table(actual = data$Class, predicted = predict(ts$best.model)))
      print(ts)
      return <- ts
    }

### Linear

    ts <- doStuff('linear')

    ## 
    ## Call:
    ## best.svm(x = Class ~ ., data = data, cost = cost, type = "C-classification", 
    ##     kernel = kernel)
    ## 
    ## 
    ## Parameters:
    ##    SVM-Type:  C-classification 
    ##  SVM-Kernel:  linear 
    ##        cost:  1 
    ##       gamma:  0.01515152 
    ## 
    ## Number of Support Vectors:  38
    ## 
    ##           predicted
    ## actual     glaucoma normal
    ##   glaucoma       75      0
    ##   normal          2     76
    ## 
    ## Parameter tuning of 'svm':
    ## 
    ## - sampling method: 10-fold cross validation 
    ## 
    ## - best parameters:
    ##  cost
    ##     1
    ## 
    ## - best performance: 0.105

Видно, что данные линейно разделимы.

    xyplot(ts$performances[, 'error'] ~ log(ts$performances[, 'cost']), type='b', col='green4')

![](Task6Glaucoma_files/figure-markdown_strict/unnamed-chunk-1-1.png)

### Polynomial

    ts <- doStuff('polynomial', degree = (1:5))

    ## 
    ## Call:
    ## best.svm(x = Class ~ ., data = data, degree = ..1, cost = cost, 
    ##     type = "C-classification", kernel = kernel)
    ## 
    ## 
    ## Parameters:
    ##    SVM-Type:  C-classification 
    ##  SVM-Kernel:  polynomial 
    ##        cost:  32 
    ##      degree:  1 
    ##       gamma:  0.01515152 
    ##      coef.0:  0 
    ## 
    ## Number of Support Vectors:  43
    ## 
    ##           predicted
    ## actual     glaucoma normal
    ##   glaucoma       75      0
    ##   normal          2     76
    ## 
    ## Parameter tuning of 'svm':
    ## 
    ## - sampling method: 10-fold cross validation 
    ## 
    ## - best parameters:
    ##  degree cost
    ##       1   32
    ## 
    ## - best performance: 0.0975

Лучшая степень - 1, overfitting при степенях выше.

    xyplot(ts$performances[, 'error'] ~ log(ts$performances[, 'cost']), groups = ts$performances[, 'degree'] , type='b', auto.key=list(title='Degree'))

![](Task6Glaucoma_files/figure-markdown_strict/unnamed-chunk-3-1.png)

### Radial

    ts <- doStuff('radial', cost = 2^(-5:7), gamma = (2^(-2:5))/ncol(data))

    ## 
    ## Call:
    ## best.svm(x = Class ~ ., data = data, gamma = ..1, cost = cost, 
    ##     type = "C-classification", kernel = kernel)
    ## 
    ## 
    ## Parameters:
    ##    SVM-Type:  C-classification 
    ##  SVM-Kernel:  radial 
    ##        cost:  32 
    ##       gamma:  0.003731343 
    ## 
    ## Number of Support Vectors:  65
    ## 
    ##           predicted
    ## actual     glaucoma normal
    ##   glaucoma       75      0
    ##   normal          2     76
    ## 
    ## Parameter tuning of 'svm':
    ## 
    ## - sampling method: 10-fold cross validation 
    ## 
    ## - best parameters:
    ##        gamma cost
    ##  0.003731343   32
    ## 
    ## - best performance: 0.1183333

Overfitting c ростом gamma.

    xyplot(ts$performances[, "error"] ~ log(ts$performances[, "cost"]), groups = ts$performances[, "gamma"] , type="b", auto.key=list(title="Gamma", corner=c(1,1)))

![](Task6Glaucoma_files/figure-markdown_strict/unnamed-chunk-5-1.png)
