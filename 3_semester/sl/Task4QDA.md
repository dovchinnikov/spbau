    ## Loading required package: RColorBrewer
    ## 
    ## Attaching package: 'boot'
    ## 
    ## The following object is masked from 'package:lattice':
    ## 
    ##     melanoma

    make.data <- function(data, groups, model, size = nrow(data), groups.name = "Species") {
      ind <- sample(seq(levels(groups)), size = size, replace = TRUE)
      res <- data.frame(name = factor(levels(groups)[ind], levels = levels(groups)))
      names(res) <- groups.name
      mx <- model$means[ind,]
      mx <- mx + rmvnorm(nrow(mx), sigma = model$cov)
      colnames(mx) <- colnames(model$cov)
      res <- cbind(as.data.frame(mx), res)
      rownames(res) <- NULL
      res
    }

    qda.model <- function(data, groups) { 
      means <- aggregate(data, list(groups = groups), mean)
      data <- as.matrix(data) - as.matrix(means[match(groups, means$groups), -1])
      list(cov = cov(data), means = means[, -1, drop = FALSE])
    }

    model <- qda.model(subset(iris, select = -Species), iris$Species)
    res <- make.data(iris, iris$Species, model)
    splom(iris, groups = iris$Species, lower.panel=function(x, y, ...){}, pscale=0, xlab = NULL)

![](Task4QDA_files/figure-markdown_strict/define%20data%20&%20plots-1.png)

    splom(res, groups = res$Species, lower.panel=function(x, y, ...){}, pscale=0, xlab = NULL)

![](Task4QDA_files/figure-markdown_strict/define%20data%20&%20plots-2.png)

    b <- boot(
      iris,
      function(data) tune(
        qda, 
        Species ~ ., 
        data=data, 
        predict.func=function(...) predict(...)$class, 
        tunecontrol=tune.control(sampling="fix", fix=2/3)
      )$best.performance,
      R=999, 
      sim="parametric",
      ran.gen=function(data, mle, ..., size=300) make.data(data, mle$groups, mle$model, ...), 
      mle=list(groups = iris$Species, model = model)
    )

    print(b)

    ## 
    ## PARAMETRIC BOOTSTRAP
    ## 
    ## 
    ## Call:
    ## boot(data = iris, statistic = function(data) tune(qda, Species ~ 
    ##     ., data = data, predict.func = function(...) predict(...)$class, 
    ##     tunecontrol = tune.control(sampling = "fix", fix = 2/3))$best.performance, 
    ##     R = 999, sim = "parametric", ran.gen = function(data, mle, 
    ##         ..., size = 300) make.data(data, mle$groups, mle$model, 
    ##         ...), mle = list(groups = iris$Species, model = model))
    ## 
    ## 
    ## Bootstrap Statistics :
    ##     original     bias    std. error
    ## t1*        0 0.01701702  0.01868178

    plot(b)

![](Task4QDA_files/figure-markdown_strict/do%20stuff-1.png)

    boot.ci(b, type = "perc")

    ## BOOTSTRAP CONFIDENCE INTERVAL CALCULATIONS
    ## Based on 999 bootstrap replicates
    ## 
    ## CALL : 
    ## boot.ci(boot.out = b, type = "perc")
    ## 
    ## Intervals : 
    ## Level     Percentile     
    ## 95%   ( 0.00,  0.06 )  
    ## Calculations and Intervals on Original Scale
