    library(MASS)
    library(e1071)

    generate.data <- function(predictors, observations) {
      data<-data.frame(replicate(predictors + 1, runif(observations)))
      names(data) <- c('y', paste(replicate(predictors, 'x'), seq(1, predictors), sep = ''))
      return <- data
    }

    generate.formula <- function(data) {
      corrs <- sapply(names(data)[0:-1], function(x) abs(cor(data$y, data[[x]])))
      predictors <- names(sort(corrs,decreasing = TRUE))[1:20]
      as.formula(paste('y ~ ', paste(predictors,collapse = ' + ')))
    }

### Обучение и валидация на одних данных

    data <- generate.data(10000, 50)
    f <- generate.formula(data)
    m <- lm(f, data=data)
    summary(m)

    ## 
    ## Call:
    ## lm(formula = f, data = data)
    ## 
    ## Residuals:
    ##       Min        1Q    Median        3Q       Max 
    ## -0.223723 -0.049313 -0.001673  0.054665  0.169275 
    ## 
    ## Coefficients:
    ##              Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)  0.721988   0.166640   4.333 0.000161 ***
    ## x9891       -0.018998   0.093237  -0.204 0.839959    
    ## x331         0.194239   0.078725   2.467 0.019758 *  
    ## x1692       -0.136035   0.076973  -1.767 0.087700 .  
    ## x4214        0.251373   0.079158   3.176 0.003532 ** 
    ## x5503       -0.149349   0.074187  -2.013 0.053465 .  
    ## x7880        0.007363   0.074977   0.098 0.922443    
    ## x9960       -0.118840   0.083621  -1.421 0.165929    
    ## x8186       -0.132077   0.078101  -1.691 0.101538    
    ## x2704       -0.131481   0.072924  -1.803 0.081791 .  
    ## x7699       -0.089176   0.069092  -1.291 0.207005    
    ## x9859       -0.027356   0.083048  -0.329 0.744218    
    ## x7629        0.020668   0.070669   0.292 0.772019    
    ## x3939       -0.003254   0.078138  -0.042 0.967064    
    ## x3247        0.061109   0.076582   0.798 0.431384    
    ## x3539       -0.164045   0.082136  -1.997 0.055257 .  
    ## x2457        0.054252   0.079541   0.682 0.500611    
    ## x3321       -0.093469   0.061608  -1.517 0.140058    
    ## x6726       -0.079954   0.073217  -1.092 0.283815    
    ## x9900        0.045317   0.077303   0.586 0.562263    
    ## x6279        0.086934   0.090886   0.957 0.346719    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.1153 on 29 degrees of freedom
    ## Multiple R-squared:  0.906,  Adjusted R-squared:  0.8411 
    ## F-statistic: 13.97 on 20 and 29 DF,  p-value: 5.885e-10

    tune(lm, f, data=data)

    ## 
    ## Error estimation of 'lm' using 10-fold cross validation: 0.02657036

### Тестовые и тренировочные данные

    data<- generate.data(10000,100)
    data.test <- data[1:50,]
    data.train <- data[51:100,]
    f <- generate.formula(data.train)
    m <- lm(f, data=data.train)
    summary(m)

    ## 
    ## Call:
    ## lm(formula = f, data = data.train)
    ## 
    ## Residuals:
    ##       Min        1Q    Median        3Q       Max 
    ## -0.180673 -0.094343 -0.005427  0.091245  0.226634 
    ## 
    ## Coefficients:
    ##             Estimate Std. Error t value Pr(>|t|)   
    ## (Intercept)  0.52766    0.19105   2.762  0.00987 **
    ## x7976       -0.12470    0.09771  -1.276  0.21201   
    ## x9008        0.12009    0.09914   1.211  0.23559   
    ## x5350       -0.01313    0.09603  -0.137  0.89216   
    ## x1204       -0.15857    0.10980  -1.444  0.15942   
    ## x9436        0.01731    0.10027   0.173  0.86411   
    ## x9730       -0.13909    0.11218  -1.240  0.22494   
    ## x8277       -0.02629    0.09664  -0.272  0.78756   
    ## x8726       -0.21888    0.12174  -1.798  0.08262 . 
    ## x914         0.02713    0.10690   0.254  0.80148   
    ## x4884        0.03243    0.11072   0.293  0.77164   
    ## x3828       -0.02302    0.10507  -0.219  0.82813   
    ## x3076        0.15736    0.09476   1.661  0.10755   
    ## x4337       -0.05632    0.11854  -0.475  0.63825   
    ## x7935       -0.16094    0.09160  -1.757  0.08949 . 
    ## x4499        0.14050    0.10014   1.403  0.17121   
    ## x2440        0.07637    0.10659   0.716  0.47945   
    ## x8615        0.15322    0.09143   1.676  0.10451   
    ## x5408        0.10957    0.09816   1.116  0.27348   
    ## x3418        0.08162    0.08971   0.910  0.37043   
    ## x8655       -0.06873    0.11273  -0.610  0.54682   
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.1427 on 29 degrees of freedom
    ## Multiple R-squared:  0.8426, Adjusted R-squared:  0.7341 
    ## F-statistic: 7.765 on 20 and 29 DF,  p-value: 5.628e-07

    tune(lm, f, data=data.train)

    ## 
    ## Error estimation of 'lm' using 10-fold cross validation: 0.03857104

    tune(lm, f, data=data.test)

    ## 
    ## Error estimation of 'lm' using 10-fold cross validation: 0.1355905

### Честная кросс-валидация

    data <- generate.data(10000, 50)
    f <- generate.formula(data)
    tune(function(formula, data, subset, ...) {
      data <- data[subset,]
      lm(generate.formula(data), data=data)
    }, f, data=data)

    ## 
    ## Error estimation of 'function(formula, data, subset, ...) {''    data <- data[subset, ]''    lm(generate.formula(data), data = data)''}' using 10-fold cross validation: 0.106535
