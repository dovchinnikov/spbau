    ## Loading required package: RColorBrewer

Подготовим данные и посмотрим на графики

    data <- read.table("data/seeds_dataset.txt")
    names(data) <- c("A", "P", "C", "L", "W", "As", "Lg", "Kind")

    idx_ <- sample(nrow(data), size = nrow(data) * 2/3)
    data.train <- data[idx_,]
    data.test <- data[-idx_,]

    splom(~data,data, upper.panel=function(x, y, ...) { panel.xyplot(x, y, ...); panel.loess(x, y, ..., col='red') }, lower.panel=function(x, y, ...) { }, pscale=0, varname.cex=2, xlab = NULL, par.settings=simpleTheme(pch='.', cex=3))

![](Task3Seeds_files/figure-markdown_strict/unnamed-chunk-2-1.png)

    marginal.plot(data,data=data,groups = Kind)

![](Task3Seeds_files/figure-markdown_strict/unnamed-chunk-2-2.png)

    corrplot.mixed(cor(data))

![](Task3Seeds_files/figure-markdown_strict/unnamed-chunk-2-3.png)

### LDA

    doLDAtt <- function(formula = Kind ~ .) {
      t <- lda(Kind ~ ., data = data.train)
      p <- predict(t, data.test)
      print(table(predicted = p$class, actual = data.test$Kind))
      mean(p$class == data.test$Kind)
    }

    doLDA <- function(formula = Kind ~ .) {
      print(lda(formula, data = data))
      print(tune(lda, formula, data = data, predict.func = function(...) as.numeric(predict(...)$class)))
    }

    doLDA()

    ## Call:
    ## lda(formula, data = data)
    ## 
    ## Prior probabilities of groups:
    ##         1         2         3 
    ## 0.3333333 0.3333333 0.3333333 
    ## 
    ## Group means:
    ##          A        P         C        L        W       As       Lg
    ## 1 14.33443 14.29429 0.8800700 5.508057 3.244629 2.667403 5.087214
    ## 2 18.33429 16.13571 0.8835171 6.148029 3.677414 3.644800 6.020600
    ## 3 11.87386 13.24786 0.8494086 5.229514 2.853771 4.788400 5.116400
    ## 
    ## Coefficients of linear discriminants:
    ##            LD1         LD2
    ## A  -0.42377861   4.1953167
    ## P   3.79919995  -8.5057958
    ## C   5.92772810 -86.9823024
    ## L  -5.98819597  -7.8306747
    ## W   0.03704822   0.7141043
    ## As -0.04504722   0.3212538
    ## Lg  3.11807592   6.9138493
    ## 
    ## Proportion of trace:
    ##    LD1    LD2 
    ## 0.6814 0.3186 
    ## 
    ## Error estimation of 'lda' using 10-fold cross validation: 0.1238095

    doLDAtt()

    ##          actual
    ## predicted  1  2  3
    ##         1 18  0  0
    ##         2  0 21  0
    ##         3  1  0 30

    ## [1] 0.9857143

Уберем С, т.к он зависит от P и A

    doLDA(Kind ~ A + P + L + W + As + Lg)

    ## Call:
    ## lda(formula, data = data)
    ## 
    ## Prior probabilities of groups:
    ##         1         2         3 
    ## 0.3333333 0.3333333 0.3333333 
    ## 
    ## Group means:
    ##          A        P        L        W       As       Lg
    ## 1 14.33443 14.29429 5.508057 3.244629 2.667403 5.087214
    ## 2 18.33429 16.13571 6.148029 3.677414 3.644800 6.020600
    ## 3 11.87386 13.24786 5.229514 2.853771 4.788400 5.116400
    ## 
    ## Coefficients of linear discriminants:
    ##            LD1        LD2
    ## A  -0.23521821  1.8715521
    ## P   3.35526953 -2.6654127
    ## L  -6.13889228 -7.2322239
    ## W   0.40054791 -6.0463565
    ## As -0.04404002  0.4014321
    ## Lg  3.20519133  7.3075051
    ## 
    ## Proportion of trace:
    ##    LD1    LD2 
    ## 0.7266 0.2734 
    ## 
    ## Error estimation of 'lda' using 10-fold cross validation: 0.1

Уберем P, т.к он зависит от L и W

    doLDA(Kind ~ A + L + W + As + Lg)

    ## Call:
    ## lda(formula, data = data)
    ## 
    ## Prior probabilities of groups:
    ##         1         2         3 
    ## 0.3333333 0.3333333 0.3333333 
    ## 
    ## Group means:
    ##          A        L        W       As       Lg
    ## 1 14.33443 5.508057 3.244629 2.667403 5.087214
    ## 2 18.33429 6.148029 3.677414 3.644800 6.020600
    ## 3 11.87386 5.229514 2.853771 4.788400 5.116400
    ## 
    ## Coefficients of linear discriminants:
    ##            LD1        LD2
    ## A  -1.28307382 -0.6298049
    ## L   4.55064600  8.6180001
    ## W   1.89215006  4.3049425
    ## As  0.02017685 -0.3990776
    ## Lg -3.32738971 -7.3419728
    ## 
    ## Proportion of trace:
    ##    LD1    LD2 
    ## 0.7235 0.2765 
    ## 
    ## Error estimation of 'lda' using 10-fold cross validation: 0.1

As и С коррелируют

    doLDA(Kind ~ A + L + W + As:C + Lg)

    ## Call:
    ## lda(formula, data = data)
    ## 
    ## Prior probabilities of groups:
    ##         1         2         3 
    ## 0.3333333 0.3333333 0.3333333 
    ## 
    ## Group means:
    ##          A        L        W       Lg     As:C
    ## 1 14.33443 5.508057 3.244629 5.087214 2.348194
    ## 2 18.33429 6.148029 3.677414 6.020600 3.219005
    ## 3 11.87386 5.229514 2.853771 5.116400 4.066249
    ## 
    ## Coefficients of linear discriminants:
    ##              LD1        LD2
    ## A    -1.28255405 -0.6260617
    ## L     4.55443995  8.5397964
    ## W     1.87993361  4.4805752
    ## Lg   -3.32737446 -7.3725553
    ## As:C  0.02363065 -0.4505023
    ## 
    ## Proportion of trace:
    ##    LD1    LD2 
    ## 0.7252 0.2748 
    ## 
    ## Error estimation of 'lda' using 10-fold cross validation: 0.1095238

    doLDAtt(Kind ~ A + L + W + As:C + Lg)

    ##          actual
    ## predicted  1  2  3
    ##         1 18  0  0
    ##         2  0 21  0
    ##         3  1  0 30

    ## [1] 0.9857143

Подготовим данные для naive Bayes и multinom

    data$Kind <- as.factor(data$Kind)

### Naive Bayes

    doNB <- function(formula = Kind ~ .) {
      print(naiveBayes(formula, data = data))
      tune(naiveBayes, formula, data = data)
    }

    doNB()

    ## 
    ## Naive Bayes Classifier for Discrete Predictors
    ## 
    ## Call:
    ## naiveBayes.default(x = X, y = Y, laplace = laplace)
    ## 
    ## A-priori probabilities:
    ## Y
    ##         1         2         3 
    ## 0.3333333 0.3333333 0.3333333 
    ## 
    ## Conditional probabilities:
    ##    A
    ## Y       [,1]      [,2]
    ##   1 14.33443 1.2157036
    ##   2 18.33429 1.4394963
    ##   3 11.87386 0.7230036
    ## 
    ##    P
    ## Y       [,1]      [,2]
    ##   1 14.29429 0.5765831
    ##   2 16.13571 0.6169950
    ##   3 13.24786 0.3401956
    ## 
    ##    C
    ## Y        [,1]       [,2]
    ##   1 0.8800700 0.01619093
    ##   2 0.8835171 0.01550004
    ##   3 0.8494086 0.02175963
    ## 
    ##    L
    ## Y       [,1]      [,2]
    ##   1 5.508057 0.2315080
    ##   2 6.148029 0.2681911
    ##   3 5.229514 0.1380152
    ## 
    ##    W
    ## Y       [,1]      [,2]
    ##   1 3.244629 0.1776155
    ##   2 3.677414 0.1855391
    ##   3 2.853771 0.1475161
    ## 
    ##    As
    ## Y       [,1]     [,2]
    ##   1 2.667403 1.173901
    ##   2 3.644800 1.181868
    ##   3 4.788400 1.336465
    ## 
    ##    Lg
    ## Y       [,1]      [,2]
    ##   1 5.087214 0.2636987
    ##   2 6.020600 0.2539338
    ##   3 5.116400 0.1620683

    ## 
    ## Error estimation of 'naiveBayes' using 10-fold cross validation: 0.09047619

    doNB(Kind ~ A + L + W + As + Lg)

    ## 
    ## Naive Bayes Classifier for Discrete Predictors
    ## 
    ## Call:
    ## naiveBayes.default(x = X, y = Y, laplace = laplace)
    ## 
    ## A-priori probabilities:
    ## Y
    ##         1         2         3 
    ## 0.3333333 0.3333333 0.3333333 
    ## 
    ## Conditional probabilities:
    ##    A
    ## Y       [,1]      [,2]
    ##   1 14.33443 1.2157036
    ##   2 18.33429 1.4394963
    ##   3 11.87386 0.7230036
    ## 
    ##    L
    ## Y       [,1]      [,2]
    ##   1 5.508057 0.2315080
    ##   2 6.148029 0.2681911
    ##   3 5.229514 0.1380152
    ## 
    ##    W
    ## Y       [,1]      [,2]
    ##   1 3.244629 0.1776155
    ##   2 3.677414 0.1855391
    ##   3 2.853771 0.1475161
    ## 
    ##    As
    ## Y       [,1]     [,2]
    ##   1 2.667403 1.173901
    ##   2 3.644800 1.181868
    ##   3 4.788400 1.336465
    ## 
    ##    Lg
    ## Y       [,1]      [,2]
    ##   1 5.087214 0.2636987
    ##   2 6.020600 0.2539338
    ##   3 5.116400 0.1620683

    ## 
    ## Error estimation of 'naiveBayes' using 10-fold cross validation: 0.07619048

### Мультиномиальная регрессия

    doMulti <- function(formula = Kind ~.) {
      multinom(formula, data=data,maxit=2000,trace=FALSE)
      t <- tune(multinom, formula, data = data,maxit=2000, trace=FALSE)
      print(t)
    }

    doMulti()

    ## 
    ## Error estimation of 'multinom' using 10-fold cross validation: 0.04761905

    doMulti(Kind ~ A + L + W + As + Lg)

    ## 
    ## Error estimation of 'multinom' using 10-fold cross validation: 0.02857143

    doMulti(Kind ~ A + L + W + As:C + Lg)

    ## 
    ## Error estimation of 'multinom' using 10-fold cross validation: 0.04761905

Самую меньшую ошибку на всех методах дает `Kind ~ A + L + W + As + Lg`.

### StepAIC

    m<-multinom(Kind ~ A + L + W + As + Lg,data=data,maxit=2000,trace=FALSE)
    f<-stepAIC(m)

    ## Start:  AIC=43.2
    ## Kind ~ A + L + W + As + Lg
    ## 
    ##        Df    AIC
    ## - W     2 40.645
    ## - A     2 42.606
    ## <none>    43.196
    ## - As    2 54.010
    ## - L     2 64.133
    ## - Lg    2 92.781
    ## 
    ## Step:  AIC=40.64
    ## Kind ~ A + L + As + Lg
    ## 
    ##        Df    AIC
    ## <none>    40.645
    ## - As    2 53.717
    ## - A     2 66.380
    ## - L     2 70.802
    ## - Lg    2 95.127

    f<-as.formula(f)
    data$Kind <- as.numeric(data$Kind)
    doLDA(f)

    ## Call:
    ## lda(formula, data = data)
    ## 
    ## Prior probabilities of groups:
    ##         1         2         3 
    ## 0.3333333 0.3333333 0.3333333 
    ## 
    ## Group means:
    ##          A        L       As       Lg
    ## 1 14.33443 5.508057 2.667403 5.087214
    ## 2 18.33429 6.148029 3.644800 6.020600
    ## 3 11.87386 5.229514 4.788400 5.116400
    ## 
    ## Coefficients of linear discriminants:
    ##            LD1        LD2
    ## A  -0.90588897 -0.2209050
    ## L   3.65094457 -6.9511914
    ## As  0.03711207  0.3766571
    ## Lg -3.37666781  7.8700320
    ## 
    ## Proportion of trace:
    ##    LD1    LD2 
    ## 0.7302 0.2698 
    ## 
    ## Error estimation of 'lda' using 10-fold cross validation: 0.1095238

    data$Kind <- as.factor(data$Kind)
    doNB(f)

    ## 
    ## Naive Bayes Classifier for Discrete Predictors
    ## 
    ## Call:
    ## naiveBayes.default(x = X, y = Y, laplace = laplace)
    ## 
    ## A-priori probabilities:
    ## Y
    ##         1         2         3 
    ## 0.3333333 0.3333333 0.3333333 
    ## 
    ## Conditional probabilities:
    ##    A
    ## Y       [,1]      [,2]
    ##   1 14.33443 1.2157036
    ##   2 18.33429 1.4394963
    ##   3 11.87386 0.7230036
    ## 
    ##    L
    ## Y       [,1]      [,2]
    ##   1 5.508057 0.2315080
    ##   2 6.148029 0.2681911
    ##   3 5.229514 0.1380152
    ## 
    ##    As
    ## Y       [,1]     [,2]
    ##   1 2.667403 1.173901
    ##   2 3.644800 1.181868
    ##   3 4.788400 1.336465
    ## 
    ##    Lg
    ## Y       [,1]      [,2]
    ##   1 5.087214 0.2636987
    ##   2 6.020600 0.2539338
    ##   3 5.116400 0.1620683

    ## 
    ## Error estimation of 'naiveBayes' using 10-fold cross validation: 0.07142857

    doMulti(f)

    ## 
    ## Error estimation of 'multinom' using 10-fold cross validation: 0.02857143
