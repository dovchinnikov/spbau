<?xml version="1.0" encoding="UTF-8"?>
<solution name="timer.sandbox" uuid="3b2ca72b-9188-49b2-9271-bdc8d8b76dae" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)</dependency>
    <dependency reexport="false">1ca3c3be-b3a5-4546-a322-96a915c1a92c(timer)</dependency>
  </dependencies>
  <usedLanguages>
    <usedLanguage>f3061a53-9226-4cc5-a443-f952ceaf5816(jetbrains.mps.baseLanguage)</usedLanguage>
    <usedLanguage>fd392034-7849-419d-9071-12563d152375(jetbrains.mps.baseLanguage.closures)</usedLanguage>
    <usedLanguage>1ca3c3be-b3a5-4546-a322-96a915c1a92c(timer)</usedLanguage>
  </usedLanguages>
</solution>

