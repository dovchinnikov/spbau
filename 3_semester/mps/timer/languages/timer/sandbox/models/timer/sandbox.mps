<?xml version="1.0" encoding="UTF-8"?>
<model modelUID="r:449a46c6-138f-4282-b8a8-9f8cbd781c6a(timer.sandbox)">
  <persistence version="8" />
  <language namespace="1ca3c3be-b3a5-4546-a322-96a915c1a92c(timer)" />
  <language namespace="f3061a53-9226-4cc5-a443-f952ceaf5816(jetbrains.mps.baseLanguage)" />
  <language namespace="fd392034-7849-419d-9071-12563d152375(jetbrains.mps.baseLanguage.closures)" />
  <import index="fxg7" modelUID="f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.io(JDK/java.io@java_stub)" version="-1" />
  <import index="tpck" modelUID="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" version="0" implicit="yes" />
  <import index="fzrt" modelUID="r:b0e452d6-d327-4d71-befa-a7ccd1e25ab2(timer.structure)" version="6" implicit="yes" />
  <root type="fzrt.Timer" typeId="fzrt.6179529223881601480" id="2858701949658583626" nodeInfo="ng">
    <property name="name" nameId="tpck.1169194664001" value="KitchenTimer" />
    <property name="direction" nameId="fzrt.6179529223881868815" value="backward" />
    <property name="time" nameId="fzrt.2858701949657769951" value="true" />
    <property name="display" nameId="fzrt.2858701949657769944" value="false" />
    <property name="actions" nameId="fzrt.2858701949658208605" value="false" />
    <property name="type" nameId="fzrt.2858701949657769939" value="analog" />
    <property name="beep" nameId="fzrt.2858701949658779113" value="true" />
  </root>
  <root type="fzrt.Timer" typeId="fzrt.6179529223881601480" id="2858701949658779061" nodeInfo="ng">
    <property name="name" nameId="tpck.1169194664001" value="SportTimer" />
    <property name="direction" nameId="fzrt.6179529223881868815" value="forward" />
    <property name="time" nameId="fzrt.2858701949657769951" value="true" />
    <property name="display" nameId="fzrt.2858701949657769944" value="true" />
  </root>
  <root type="fzrt.Timer" typeId="fzrt.6179529223881601480" id="2858701949658835750" nodeInfo="ng">
    <property name="name" nameId="tpck.1169194664001" value="SoftwareTimer" />
    <property name="direction" nameId="fzrt.6179529223881868815" value="both" />
    <property name="time" nameId="fzrt.2858701949657769951" value="true" />
    <property name="actions" nameId="fzrt.2858701949658208605" value="true" />
  </root>
</model>

