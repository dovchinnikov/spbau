<?xml version="1.0" encoding="UTF-8"?>
<model modelUID="r:b0e452d6-d327-4d71-befa-a7ccd1e25ab2(timer.structure)" version="6">
  <persistence version="8" />
  <language namespace="c72da2b9-7cce-4447-8389-f407dc1158b7(jetbrains.mps.lang.structure)" />
  <devkit namespace="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  <import index="tpee" modelUID="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" version="5" />
  <import index="tpce" modelUID="r:00000000-0000-4000-0000-011c89590292(jetbrains.mps.lang.structure.structure)" version="0" implicit="yes" />
  <import index="tpck" modelUID="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" version="0" implicit="yes" />
  <import index="fzrt" modelUID="r:b0e452d6-d327-4d71-befa-a7ccd1e25ab2(timer.structure)" version="6" implicit="yes" />
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="6179529223881601480" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="Timer" />
    <property name="rootable" nameId="tpce.1096454100552" value="true" />
    <property name="conceptAlias" nameId="tpce.5092175715804935370" value="timer" />
    <property name="conceptShortDescription" nameId="tpce.4628067390765907488" value="Root concept" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="tpck.1133920641626" resolveInfo="BaseConcept" />
    <node role="implements" roleId="tpce.1169129564478" type="tpce.InterfaceConceptReference" typeId="tpce.1169127622168" id="6179529223882165010" nodeInfo="ig">
      <link role="intfc" roleId="tpce.1169127628841" targetNodeId="tpck.1169194658468" resolveInfo="INamedConcept" />
    </node>
    <node role="propertyDeclaration" roleId="tpce.1071489727084" type="tpce.PropertyDeclaration" typeId="tpce.1071489288299" id="6179529223881868815" nodeInfo="ig">
      <property name="name" nameId="tpck.1169194664001" value="direction" />
      <link role="dataType" roleId="tpce.1082985295845" targetNodeId="6179529223881867831" resolveInfo="Direction" />
    </node>
    <node role="propertyDeclaration" roleId="tpce.1071489727084" type="tpce.PropertyDeclaration" typeId="tpce.1071489288299" id="2858701949657769939" nodeInfo="ig">
      <property name="name" nameId="tpck.1169194664001" value="type" />
      <link role="dataType" roleId="tpce.1082985295845" targetNodeId="2858701949657751047" resolveInfo="TimerType" />
    </node>
    <node role="propertyDeclaration" roleId="tpce.1071489727084" type="tpce.PropertyDeclaration" typeId="tpce.1071489288299" id="2858701949657769944" nodeInfo="ig">
      <property name="name" nameId="tpck.1169194664001" value="display" />
      <link role="dataType" roleId="tpce.1082985295845" targetNodeId="tpck.1082983657063" resolveInfo="boolean" />
    </node>
    <node role="propertyDeclaration" roleId="tpce.1071489727084" type="tpce.PropertyDeclaration" typeId="tpce.1071489288299" id="2858701949657769951" nodeInfo="ig">
      <property name="name" nameId="tpck.1169194664001" value="time" />
      <link role="dataType" roleId="tpce.1082985295845" targetNodeId="tpck.1082983657063" resolveInfo="boolean" />
    </node>
    <node role="propertyDeclaration" roleId="tpce.1071489727084" type="tpce.PropertyDeclaration" typeId="tpce.1071489288299" id="2858701949658208605" nodeInfo="ig">
      <property name="name" nameId="tpck.1169194664001" value="actions" />
      <link role="dataType" roleId="tpce.1082985295845" targetNodeId="tpck.1082983657063" resolveInfo="boolean" />
    </node>
    <node role="propertyDeclaration" roleId="tpce.1071489727084" type="tpce.PropertyDeclaration" typeId="tpce.1071489288299" id="2858701949658779113" nodeInfo="ig">
      <property name="name" nameId="tpck.1169194664001" value="beep" />
      <link role="dataType" roleId="tpce.1082985295845" targetNodeId="tpck.1082983657063" resolveInfo="boolean" />
    </node>
  </root>
  <root type="tpce.EnumerationDataTypeDeclaration" typeId="tpce.1082978164219" id="6179529223881867831" nodeInfo="ng">
    <property name="name" nameId="tpck.1169194664001" value="Direction" />
    <property name="hasNoDefaultMember" nameId="tpce.1212080844762" value="true" />
    <link role="memberDataType" roleId="tpce.1083171729157" targetNodeId="tpck.1082983041843" resolveInfo="string" />
    <node role="member" roleId="tpce.1083172003582" type="tpce.EnumerationMemberDeclaration" typeId="tpce.1083171877298" id="6179529223881867832" nodeInfo="ig">
      <property name="externalValue" nameId="tpce.1083923523172" value="FORWARD" />
      <property name="internalValue" nameId="tpce.1083923523171" value="forward" />
    </node>
    <node role="member" roleId="tpce.1083172003582" type="tpce.EnumerationMemberDeclaration" typeId="tpce.1083171877298" id="6179529223881867862" nodeInfo="ig">
      <property name="externalValue" nameId="tpce.1083923523172" value="BACKWARD" />
      <property name="internalValue" nameId="tpce.1083923523171" value="backward" />
    </node>
    <node role="member" roleId="tpce.1083172003582" type="tpce.EnumerationMemberDeclaration" typeId="tpce.1083171877298" id="2858701949658835802" nodeInfo="ig">
      <property name="externalValue" nameId="tpce.1083923523172" value="BOTH" />
      <property name="internalValue" nameId="tpce.1083923523171" value="both" />
    </node>
  </root>
  <root type="tpce.EnumerationDataTypeDeclaration" typeId="tpce.1082978164219" id="2858701949657751047" nodeInfo="ng">
    <property name="name" nameId="tpck.1169194664001" value="TimerType" />
    <link role="memberDataType" roleId="tpce.1083171729157" targetNodeId="tpck.1082983041843" resolveInfo="string" />
    <node role="member" roleId="tpce.1083172003582" type="tpce.EnumerationMemberDeclaration" typeId="tpce.1083171877298" id="2858701949657751048" nodeInfo="ig">
      <property name="internalValue" nameId="tpce.1083923523171" value="electric" />
      <property name="externalValue" nameId="tpce.1083923523172" value="ELECTRIC" />
    </node>
    <node role="member" roleId="tpce.1083172003582" type="tpce.EnumerationMemberDeclaration" typeId="tpce.1083171877298" id="2858701949657769927" nodeInfo="ig">
      <property name="externalValue" nameId="tpce.1083923523172" value="ANALOG" />
      <property name="internalValue" nameId="tpce.1083923523171" value="analog" />
    </node>
  </root>
</model>

