/**
 */
package pw.ovchinnikov.gp.dGraph;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link pw.ovchinnikov.gp.dGraph.Graph#getVertexes <em>Vertexes</em>}</li>
 * </ul>
 * </p>
 *
 * @see pw.ovchinnikov.gp.dGraph.DGraphPackage#getGraph()
 * @model
 * @generated
 */
public interface Graph extends EObject
{
  /**
   * Returns the value of the '<em><b>Vertexes</b></em>' containment reference list.
   * The list contents are of type {@link pw.ovchinnikov.gp.dGraph.Vertex}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vertexes</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vertexes</em>' containment reference list.
   * @see pw.ovchinnikov.gp.dGraph.DGraphPackage#getGraph_Vertexes()
   * @model containment="true"
   * @generated
   */
  EList<Vertex> getVertexes();

} // Graph
