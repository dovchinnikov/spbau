package pw.ovchinnikov.gp.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import pw.ovchinnikov.gp.services.DGraphGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDGraphParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "':'", "';'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalDGraphParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDGraphParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDGraphParser.tokenNames; }
    public String getGrammarFileName() { return "../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g"; }



     	private DGraphGrammarAccess grammarAccess;
     	
        public InternalDGraphParser(TokenStream input, DGraphGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Graph";	
       	}
       	
       	@Override
       	protected DGraphGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleGraph"
    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:67:1: entryRuleGraph returns [EObject current=null] : iv_ruleGraph= ruleGraph EOF ;
    public final EObject entryRuleGraph() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGraph = null;


        try {
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:68:2: (iv_ruleGraph= ruleGraph EOF )
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:69:2: iv_ruleGraph= ruleGraph EOF
            {
             newCompositeNode(grammarAccess.getGraphRule()); 
            pushFollow(FOLLOW_ruleGraph_in_entryRuleGraph75);
            iv_ruleGraph=ruleGraph();

            state._fsp--;

             current =iv_ruleGraph; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGraph85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGraph"


    // $ANTLR start "ruleGraph"
    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:76:1: ruleGraph returns [EObject current=null] : ( (lv_vertexes_0_0= ruleVertex ) )* ;
    public final EObject ruleGraph() throws RecognitionException {
        EObject current = null;

        EObject lv_vertexes_0_0 = null;


         enterRule(); 
            
        try {
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:79:28: ( ( (lv_vertexes_0_0= ruleVertex ) )* )
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:80:1: ( (lv_vertexes_0_0= ruleVertex ) )*
            {
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:80:1: ( (lv_vertexes_0_0= ruleVertex ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:81:1: (lv_vertexes_0_0= ruleVertex )
            	    {
            	    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:81:1: (lv_vertexes_0_0= ruleVertex )
            	    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:82:3: lv_vertexes_0_0= ruleVertex
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getGraphAccess().getVertexesVertexParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleVertex_in_ruleGraph130);
            	    lv_vertexes_0_0=ruleVertex();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getGraphRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vertexes",
            	            		lv_vertexes_0_0, 
            	            		"Vertex");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGraph"


    // $ANTLR start "entryRuleVertex"
    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:106:1: entryRuleVertex returns [EObject current=null] : iv_ruleVertex= ruleVertex EOF ;
    public final EObject entryRuleVertex() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVertex = null;


        try {
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:107:2: (iv_ruleVertex= ruleVertex EOF )
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:108:2: iv_ruleVertex= ruleVertex EOF
            {
             newCompositeNode(grammarAccess.getVertexRule()); 
            pushFollow(FOLLOW_ruleVertex_in_entryRuleVertex166);
            iv_ruleVertex=ruleVertex();

            state._fsp--;

             current =iv_ruleVertex; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVertex176); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVertex"


    // $ANTLR start "ruleVertex"
    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:115:1: ruleVertex returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= ':' ( (otherlv_2= RULE_ID ) )+ )? otherlv_3= ';' ) ;
    public final EObject ruleVertex() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:118:28: ( ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= ':' ( (otherlv_2= RULE_ID ) )+ )? otherlv_3= ';' ) )
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:119:1: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= ':' ( (otherlv_2= RULE_ID ) )+ )? otherlv_3= ';' )
            {
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:119:1: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= ':' ( (otherlv_2= RULE_ID ) )+ )? otherlv_3= ';' )
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:119:2: ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= ':' ( (otherlv_2= RULE_ID ) )+ )? otherlv_3= ';'
            {
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:119:2: ( (lv_name_0_0= RULE_ID ) )
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:120:1: (lv_name_0_0= RULE_ID )
            {
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:120:1: (lv_name_0_0= RULE_ID )
            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:121:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVertex218); 

            			newLeafNode(lv_name_0_0, grammarAccess.getVertexAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVertexRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:137:2: (otherlv_1= ':' ( (otherlv_2= RULE_ID ) )+ )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:137:4: otherlv_1= ':' ( (otherlv_2= RULE_ID ) )+
                    {
                    otherlv_1=(Token)match(input,11,FOLLOW_11_in_ruleVertex236); 

                        	newLeafNode(otherlv_1, grammarAccess.getVertexAccess().getColonKeyword_1_0());
                        
                    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:141:1: ( (otherlv_2= RULE_ID ) )+
                    int cnt2=0;
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==RULE_ID) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:142:1: (otherlv_2= RULE_ID )
                    	    {
                    	    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:142:1: (otherlv_2= RULE_ID )
                    	    // ../task5/src-gen/pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.g:143:3: otherlv_2= RULE_ID
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getVertexRule());
                    	    	        }
                    	            
                    	    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVertex256); 

                    	    		newLeafNode(otherlv_2, grammarAccess.getVertexAccess().getDependenciesVertexCrossReference_1_1_0()); 
                    	    	

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt2 >= 1 ) break loop2;
                                EarlyExitException eee =
                                    new EarlyExitException(2, input);
                                throw eee;
                        }
                        cnt2++;
                    } while (true);


                    }
                    break;

            }

            otherlv_3=(Token)match(input,12,FOLLOW_12_in_ruleVertex271); 

                	newLeafNode(otherlv_3, grammarAccess.getVertexAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVertex"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleGraph_in_entryRuleGraph75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGraph85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVertex_in_ruleGraph130 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleVertex_in_entryRuleVertex166 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVertex176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVertex218 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_11_in_ruleVertex236 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVertex256 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_12_in_ruleVertex271 = new BitSet(new long[]{0x0000000000000002L});

}