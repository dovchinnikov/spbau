package pw.ovchinnikov.gp.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import pw.ovchinnikov.gp.dGraph.DGraphPackage;
import pw.ovchinnikov.gp.dGraph.Graph;
import pw.ovchinnikov.gp.dGraph.Vertex;
import pw.ovchinnikov.gp.services.DGraphGrammarAccess;

@SuppressWarnings("all")
public class DGraphSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private DGraphGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == DGraphPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case DGraphPackage.GRAPH:
				if(context == grammarAccess.getGraphRule()) {
					sequence_Graph(context, (Graph) semanticObject); 
					return; 
				}
				else break;
			case DGraphPackage.VERTEX:
				if(context == grammarAccess.getVertexRule()) {
					sequence_Vertex(context, (Vertex) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     vertexes+=Vertex*
	 */
	protected void sequence_Graph(EObject context, Graph semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID dependencies+=[Vertex|ID]*)
	 */
	protected void sequence_Vertex(EObject context, Vertex semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
