/**
 */
package pw.ovchinnikov.gp.dGraph;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see pw.ovchinnikov.gp.dGraph.DGraphFactory
 * @model kind="package"
 * @generated
 */
public interface DGraphPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "dGraph";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.ovchinnikov.pw/gp/DGraph";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "dGraph";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  DGraphPackage eINSTANCE = pw.ovchinnikov.gp.dGraph.impl.DGraphPackageImpl.init();

  /**
   * The meta object id for the '{@link pw.ovchinnikov.gp.dGraph.impl.GraphImpl <em>Graph</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see pw.ovchinnikov.gp.dGraph.impl.GraphImpl
   * @see pw.ovchinnikov.gp.dGraph.impl.DGraphPackageImpl#getGraph()
   * @generated
   */
  int GRAPH = 0;

  /**
   * The feature id for the '<em><b>Vertexes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPH__VERTEXES = 0;

  /**
   * The number of structural features of the '<em>Graph</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPH_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link pw.ovchinnikov.gp.dGraph.impl.VertexImpl <em>Vertex</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see pw.ovchinnikov.gp.dGraph.impl.VertexImpl
   * @see pw.ovchinnikov.gp.dGraph.impl.DGraphPackageImpl#getVertex()
   * @generated
   */
  int VERTEX = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VERTEX__NAME = 0;

  /**
   * The feature id for the '<em><b>Dependencies</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VERTEX__DEPENDENCIES = 1;

  /**
   * The number of structural features of the '<em>Vertex</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VERTEX_FEATURE_COUNT = 2;


  /**
   * Returns the meta object for class '{@link pw.ovchinnikov.gp.dGraph.Graph <em>Graph</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Graph</em>'.
   * @see pw.ovchinnikov.gp.dGraph.Graph
   * @generated
   */
  EClass getGraph();

  /**
   * Returns the meta object for the containment reference list '{@link pw.ovchinnikov.gp.dGraph.Graph#getVertexes <em>Vertexes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vertexes</em>'.
   * @see pw.ovchinnikov.gp.dGraph.Graph#getVertexes()
   * @see #getGraph()
   * @generated
   */
  EReference getGraph_Vertexes();

  /**
   * Returns the meta object for class '{@link pw.ovchinnikov.gp.dGraph.Vertex <em>Vertex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Vertex</em>'.
   * @see pw.ovchinnikov.gp.dGraph.Vertex
   * @generated
   */
  EClass getVertex();

  /**
   * Returns the meta object for the attribute '{@link pw.ovchinnikov.gp.dGraph.Vertex#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see pw.ovchinnikov.gp.dGraph.Vertex#getName()
   * @see #getVertex()
   * @generated
   */
  EAttribute getVertex_Name();

  /**
   * Returns the meta object for the reference list '{@link pw.ovchinnikov.gp.dGraph.Vertex#getDependencies <em>Dependencies</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Dependencies</em>'.
   * @see pw.ovchinnikov.gp.dGraph.Vertex#getDependencies()
   * @see #getVertex()
   * @generated
   */
  EReference getVertex_Dependencies();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  DGraphFactory getDGraphFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link pw.ovchinnikov.gp.dGraph.impl.GraphImpl <em>Graph</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see pw.ovchinnikov.gp.dGraph.impl.GraphImpl
     * @see pw.ovchinnikov.gp.dGraph.impl.DGraphPackageImpl#getGraph()
     * @generated
     */
    EClass GRAPH = eINSTANCE.getGraph();

    /**
     * The meta object literal for the '<em><b>Vertexes</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GRAPH__VERTEXES = eINSTANCE.getGraph_Vertexes();

    /**
     * The meta object literal for the '{@link pw.ovchinnikov.gp.dGraph.impl.VertexImpl <em>Vertex</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see pw.ovchinnikov.gp.dGraph.impl.VertexImpl
     * @see pw.ovchinnikov.gp.dGraph.impl.DGraphPackageImpl#getVertex()
     * @generated
     */
    EClass VERTEX = eINSTANCE.getVertex();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VERTEX__NAME = eINSTANCE.getVertex_Name();

    /**
     * The meta object literal for the '<em><b>Dependencies</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VERTEX__DEPENDENCIES = eINSTANCE.getVertex_Dependencies();

  }

} //DGraphPackage
