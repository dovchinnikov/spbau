/*
* generated by Xtext
*/
package pw.ovchinnikov.gp.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class DGraphAntlrTokenFileProvider implements IAntlrTokenFileProvider {
	
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("pw/ovchinnikov/gp/parser/antlr/internal/InternalDGraph.tokens");
	}
}
