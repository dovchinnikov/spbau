package pw.ovchinnikov.gp.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import pw.ovchinnikov.gp.services.DGraphGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDGraphParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "';'", "':'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalDGraphParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDGraphParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDGraphParser.tokenNames; }
    public String getGrammarFileName() { return "../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g"; }


     
     	private DGraphGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(DGraphGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleGraph"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:60:1: entryRuleGraph : ruleGraph EOF ;
    public final void entryRuleGraph() throws RecognitionException {
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:61:1: ( ruleGraph EOF )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:62:1: ruleGraph EOF
            {
             before(grammarAccess.getGraphRule()); 
            pushFollow(FOLLOW_ruleGraph_in_entryRuleGraph61);
            ruleGraph();

            state._fsp--;

             after(grammarAccess.getGraphRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGraph68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGraph"


    // $ANTLR start "ruleGraph"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:69:1: ruleGraph : ( ( rule__Graph__VertexesAssignment )* ) ;
    public final void ruleGraph() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:73:2: ( ( ( rule__Graph__VertexesAssignment )* ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:74:1: ( ( rule__Graph__VertexesAssignment )* )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:74:1: ( ( rule__Graph__VertexesAssignment )* )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:75:1: ( rule__Graph__VertexesAssignment )*
            {
             before(grammarAccess.getGraphAccess().getVertexesAssignment()); 
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:76:1: ( rule__Graph__VertexesAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:76:2: rule__Graph__VertexesAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Graph__VertexesAssignment_in_ruleGraph94);
            	    rule__Graph__VertexesAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getGraphAccess().getVertexesAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGraph"


    // $ANTLR start "entryRuleVertex"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:88:1: entryRuleVertex : ruleVertex EOF ;
    public final void entryRuleVertex() throws RecognitionException {
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:89:1: ( ruleVertex EOF )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:90:1: ruleVertex EOF
            {
             before(grammarAccess.getVertexRule()); 
            pushFollow(FOLLOW_ruleVertex_in_entryRuleVertex122);
            ruleVertex();

            state._fsp--;

             after(grammarAccess.getVertexRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVertex129); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVertex"


    // $ANTLR start "ruleVertex"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:97:1: ruleVertex : ( ( rule__Vertex__Group__0 ) ) ;
    public final void ruleVertex() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:101:2: ( ( ( rule__Vertex__Group__0 ) ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:102:1: ( ( rule__Vertex__Group__0 ) )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:102:1: ( ( rule__Vertex__Group__0 ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:103:1: ( rule__Vertex__Group__0 )
            {
             before(grammarAccess.getVertexAccess().getGroup()); 
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:104:1: ( rule__Vertex__Group__0 )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:104:2: rule__Vertex__Group__0
            {
            pushFollow(FOLLOW_rule__Vertex__Group__0_in_ruleVertex155);
            rule__Vertex__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVertexAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVertex"


    // $ANTLR start "rule__Vertex__Group__0"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:118:1: rule__Vertex__Group__0 : rule__Vertex__Group__0__Impl rule__Vertex__Group__1 ;
    public final void rule__Vertex__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:122:1: ( rule__Vertex__Group__0__Impl rule__Vertex__Group__1 )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:123:2: rule__Vertex__Group__0__Impl rule__Vertex__Group__1
            {
            pushFollow(FOLLOW_rule__Vertex__Group__0__Impl_in_rule__Vertex__Group__0189);
            rule__Vertex__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Vertex__Group__1_in_rule__Vertex__Group__0192);
            rule__Vertex__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group__0"


    // $ANTLR start "rule__Vertex__Group__0__Impl"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:130:1: rule__Vertex__Group__0__Impl : ( ( rule__Vertex__NameAssignment_0 ) ) ;
    public final void rule__Vertex__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:134:1: ( ( ( rule__Vertex__NameAssignment_0 ) ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:135:1: ( ( rule__Vertex__NameAssignment_0 ) )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:135:1: ( ( rule__Vertex__NameAssignment_0 ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:136:1: ( rule__Vertex__NameAssignment_0 )
            {
             before(grammarAccess.getVertexAccess().getNameAssignment_0()); 
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:137:1: ( rule__Vertex__NameAssignment_0 )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:137:2: rule__Vertex__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__Vertex__NameAssignment_0_in_rule__Vertex__Group__0__Impl219);
            rule__Vertex__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getVertexAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group__0__Impl"


    // $ANTLR start "rule__Vertex__Group__1"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:147:1: rule__Vertex__Group__1 : rule__Vertex__Group__1__Impl rule__Vertex__Group__2 ;
    public final void rule__Vertex__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:151:1: ( rule__Vertex__Group__1__Impl rule__Vertex__Group__2 )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:152:2: rule__Vertex__Group__1__Impl rule__Vertex__Group__2
            {
            pushFollow(FOLLOW_rule__Vertex__Group__1__Impl_in_rule__Vertex__Group__1249);
            rule__Vertex__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Vertex__Group__2_in_rule__Vertex__Group__1252);
            rule__Vertex__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group__1"


    // $ANTLR start "rule__Vertex__Group__1__Impl"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:159:1: rule__Vertex__Group__1__Impl : ( ( rule__Vertex__Group_1__0 )? ) ;
    public final void rule__Vertex__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:163:1: ( ( ( rule__Vertex__Group_1__0 )? ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:164:1: ( ( rule__Vertex__Group_1__0 )? )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:164:1: ( ( rule__Vertex__Group_1__0 )? )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:165:1: ( rule__Vertex__Group_1__0 )?
            {
             before(grammarAccess.getVertexAccess().getGroup_1()); 
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:166:1: ( rule__Vertex__Group_1__0 )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:166:2: rule__Vertex__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Vertex__Group_1__0_in_rule__Vertex__Group__1__Impl279);
                    rule__Vertex__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVertexAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group__1__Impl"


    // $ANTLR start "rule__Vertex__Group__2"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:176:1: rule__Vertex__Group__2 : rule__Vertex__Group__2__Impl ;
    public final void rule__Vertex__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:180:1: ( rule__Vertex__Group__2__Impl )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:181:2: rule__Vertex__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Vertex__Group__2__Impl_in_rule__Vertex__Group__2310);
            rule__Vertex__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group__2"


    // $ANTLR start "rule__Vertex__Group__2__Impl"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:187:1: rule__Vertex__Group__2__Impl : ( ';' ) ;
    public final void rule__Vertex__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:191:1: ( ( ';' ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:192:1: ( ';' )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:192:1: ( ';' )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:193:1: ';'
            {
             before(grammarAccess.getVertexAccess().getSemicolonKeyword_2()); 
            match(input,11,FOLLOW_11_in_rule__Vertex__Group__2__Impl338); 
             after(grammarAccess.getVertexAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group__2__Impl"


    // $ANTLR start "rule__Vertex__Group_1__0"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:212:1: rule__Vertex__Group_1__0 : rule__Vertex__Group_1__0__Impl rule__Vertex__Group_1__1 ;
    public final void rule__Vertex__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:216:1: ( rule__Vertex__Group_1__0__Impl rule__Vertex__Group_1__1 )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:217:2: rule__Vertex__Group_1__0__Impl rule__Vertex__Group_1__1
            {
            pushFollow(FOLLOW_rule__Vertex__Group_1__0__Impl_in_rule__Vertex__Group_1__0375);
            rule__Vertex__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Vertex__Group_1__1_in_rule__Vertex__Group_1__0378);
            rule__Vertex__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group_1__0"


    // $ANTLR start "rule__Vertex__Group_1__0__Impl"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:224:1: rule__Vertex__Group_1__0__Impl : ( ':' ) ;
    public final void rule__Vertex__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:228:1: ( ( ':' ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:229:1: ( ':' )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:229:1: ( ':' )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:230:1: ':'
            {
             before(grammarAccess.getVertexAccess().getColonKeyword_1_0()); 
            match(input,12,FOLLOW_12_in_rule__Vertex__Group_1__0__Impl406); 
             after(grammarAccess.getVertexAccess().getColonKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group_1__0__Impl"


    // $ANTLR start "rule__Vertex__Group_1__1"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:243:1: rule__Vertex__Group_1__1 : rule__Vertex__Group_1__1__Impl ;
    public final void rule__Vertex__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:247:1: ( rule__Vertex__Group_1__1__Impl )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:248:2: rule__Vertex__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Vertex__Group_1__1__Impl_in_rule__Vertex__Group_1__1437);
            rule__Vertex__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group_1__1"


    // $ANTLR start "rule__Vertex__Group_1__1__Impl"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:254:1: rule__Vertex__Group_1__1__Impl : ( ( ( rule__Vertex__DependenciesAssignment_1_1 ) ) ( ( rule__Vertex__DependenciesAssignment_1_1 )* ) ) ;
    public final void rule__Vertex__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:258:1: ( ( ( ( rule__Vertex__DependenciesAssignment_1_1 ) ) ( ( rule__Vertex__DependenciesAssignment_1_1 )* ) ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:259:1: ( ( ( rule__Vertex__DependenciesAssignment_1_1 ) ) ( ( rule__Vertex__DependenciesAssignment_1_1 )* ) )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:259:1: ( ( ( rule__Vertex__DependenciesAssignment_1_1 ) ) ( ( rule__Vertex__DependenciesAssignment_1_1 )* ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:260:1: ( ( rule__Vertex__DependenciesAssignment_1_1 ) ) ( ( rule__Vertex__DependenciesAssignment_1_1 )* )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:260:1: ( ( rule__Vertex__DependenciesAssignment_1_1 ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:261:1: ( rule__Vertex__DependenciesAssignment_1_1 )
            {
             before(grammarAccess.getVertexAccess().getDependenciesAssignment_1_1()); 
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:262:1: ( rule__Vertex__DependenciesAssignment_1_1 )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:262:2: rule__Vertex__DependenciesAssignment_1_1
            {
            pushFollow(FOLLOW_rule__Vertex__DependenciesAssignment_1_1_in_rule__Vertex__Group_1__1__Impl466);
            rule__Vertex__DependenciesAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getVertexAccess().getDependenciesAssignment_1_1()); 

            }

            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:265:1: ( ( rule__Vertex__DependenciesAssignment_1_1 )* )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:266:1: ( rule__Vertex__DependenciesAssignment_1_1 )*
            {
             before(grammarAccess.getVertexAccess().getDependenciesAssignment_1_1()); 
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:267:1: ( rule__Vertex__DependenciesAssignment_1_1 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:267:2: rule__Vertex__DependenciesAssignment_1_1
            	    {
            	    pushFollow(FOLLOW_rule__Vertex__DependenciesAssignment_1_1_in_rule__Vertex__Group_1__1__Impl478);
            	    rule__Vertex__DependenciesAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getVertexAccess().getDependenciesAssignment_1_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__Group_1__1__Impl"


    // $ANTLR start "rule__Graph__VertexesAssignment"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:283:1: rule__Graph__VertexesAssignment : ( ruleVertex ) ;
    public final void rule__Graph__VertexesAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:287:1: ( ( ruleVertex ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:288:1: ( ruleVertex )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:288:1: ( ruleVertex )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:289:1: ruleVertex
            {
             before(grammarAccess.getGraphAccess().getVertexesVertexParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleVertex_in_rule__Graph__VertexesAssignment520);
            ruleVertex();

            state._fsp--;

             after(grammarAccess.getGraphAccess().getVertexesVertexParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__VertexesAssignment"


    // $ANTLR start "rule__Vertex__NameAssignment_0"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:298:1: rule__Vertex__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Vertex__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:302:1: ( ( RULE_ID ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:303:1: ( RULE_ID )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:303:1: ( RULE_ID )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:304:1: RULE_ID
            {
             before(grammarAccess.getVertexAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Vertex__NameAssignment_0551); 
             after(grammarAccess.getVertexAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__NameAssignment_0"


    // $ANTLR start "rule__Vertex__DependenciesAssignment_1_1"
    // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:313:1: rule__Vertex__DependenciesAssignment_1_1 : ( ( RULE_ID ) ) ;
    public final void rule__Vertex__DependenciesAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:317:1: ( ( ( RULE_ID ) ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:318:1: ( ( RULE_ID ) )
            {
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:318:1: ( ( RULE_ID ) )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:319:1: ( RULE_ID )
            {
             before(grammarAccess.getVertexAccess().getDependenciesVertexCrossReference_1_1_0()); 
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:320:1: ( RULE_ID )
            // ../task5.ui/src-gen/pw/ovchinnikov/gp/ui/contentassist/antlr/internal/InternalDGraph.g:321:1: RULE_ID
            {
             before(grammarAccess.getVertexAccess().getDependenciesVertexIDTerminalRuleCall_1_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Vertex__DependenciesAssignment_1_1586); 
             after(grammarAccess.getVertexAccess().getDependenciesVertexIDTerminalRuleCall_1_1_0_1()); 

            }

             after(grammarAccess.getVertexAccess().getDependenciesVertexCrossReference_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Vertex__DependenciesAssignment_1_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleGraph_in_entryRuleGraph61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGraph68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Graph__VertexesAssignment_in_ruleGraph94 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleVertex_in_entryRuleVertex122 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVertex129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__Group__0_in_ruleVertex155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__Group__0__Impl_in_rule__Vertex__Group__0189 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_rule__Vertex__Group__1_in_rule__Vertex__Group__0192 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__NameAssignment_0_in_rule__Vertex__Group__0__Impl219 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__Group__1__Impl_in_rule__Vertex__Group__1249 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_rule__Vertex__Group__2_in_rule__Vertex__Group__1252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__Group_1__0_in_rule__Vertex__Group__1__Impl279 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__Group__2__Impl_in_rule__Vertex__Group__2310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__Vertex__Group__2__Impl338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__Group_1__0__Impl_in_rule__Vertex__Group_1__0375 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Vertex__Group_1__1_in_rule__Vertex__Group_1__0378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Vertex__Group_1__0__Impl406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__Group_1__1__Impl_in_rule__Vertex__Group_1__1437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Vertex__DependenciesAssignment_1_1_in_rule__Vertex__Group_1__1__Impl466 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__Vertex__DependenciesAssignment_1_1_in_rule__Vertex__Group_1__1__Impl478 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleVertex_in_rule__Graph__VertexesAssignment520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Vertex__NameAssignment_0551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Vertex__DependenciesAssignment_1_1586 = new BitSet(new long[]{0x0000000000000002L});

}