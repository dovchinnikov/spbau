/*
* generated by Xtext
*/
package pw.ovchinnikov.gp.ui.quickfix

//import org.eclipse.xtext.diagnostics.Diagnostic
//
//import org.eclipse.xtext.ui.editor.quickfix.Fix
//import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
//import org.eclipse.xtext.validation.Issue
//import org.eclipse.xtext.ui.editor.model.XtextDocument

/**
 * Custom quickfixes.
 *
 * see http://www.eclipse.org/Xtext/documentation.html#quickfixes
 */
class DGraphQuickfixProvider extends org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider {

//	@Fix(MyDslValidator::INVALID_NAME)
//	def capitalizeName(Issue issue, IssueResolutionAcceptor acceptor) {
//		acceptor.accept(issue, 'Capitalize name', 'Capitalize the name.', 'upcase.png') [
//			context |
//			val xtextDocument = context.xtextDocument
//			val firstLetter = xtextDocument.get(issue.offset, 1)
//			xtextDocument.replace(issue.offset, 1, firstLetter.toUpperCase)
//		]
//	}
	
//	@Fix(Diagnostic.LINKING_DIAGNOSTIC)
//	def createVertex(Issue issue, IssueResolutionAcceptor acceptor) {
//		acceptor.accept(issue,'Create vertex','Create missing vertex','upcase.png')[
//			context|
//			val xtextDocument = context.xtextDocument
//			issue.
//		]
//	}
}
