<?xml version="1.0" encoding="UTF-8"?>
<model modelUID="r:92ef1acf-dd44-42fc-ae94-657d34aaaedc(tracer.structure)">
  <persistence version="8" />
  <language namespace="c72da2b9-7cce-4447-8389-f407dc1158b7(jetbrains.mps.lang.structure)" />
  <devkit namespace="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  <import index="tpee" modelUID="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" version="5" />
  <import index="tpce" modelUID="r:00000000-0000-4000-0000-011c89590292(jetbrains.mps.lang.structure.structure)" version="0" implicit="yes" />
  <import index="tpck" modelUID="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" version="0" implicit="yes" />
  <import index="bkb6" modelUID="r:92ef1acf-dd44-42fc-ae94-657d34aaaedc(tracer.structure)" version="-1" implicit="yes" />
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="5763961319731012481" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="Asterisk" />
    <property name="conceptAlias" nameId="tpce.5092175715804935370" value="*" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="tpck.1133920641626" resolveInfo="BaseConcept" />
  </root>
  <root type="tpce.EnumerationDataTypeDeclaration" typeId="tpce.1082978164219" id="5763961319730350126" nodeInfo="ng">
    <property name="name" nameId="tpck.1169194664001" value="What" />
    <property name="hasNoDefaultMember" nameId="tpce.1212080844762" value="true" />
    <property name="memberIdentifierPolicy" nameId="tpce.1197591154882" value="derive_from_presentation" />
    <property name="noValueText" nameId="tpce.1212087449254" value="what" />
    <link role="memberDataType" roleId="tpce.1083171729157" targetNodeId="tpck.1082983041843" resolveInfo="string" />
    <node role="member" roleId="tpce.1083172003582" type="tpce.EnumerationMemberDeclaration" typeId="tpce.1083171877298" id="5763961319730350127" nodeInfo="ig">
      <property name="externalValue" nameId="tpce.1083923523172" value="METHOD_CALL" />
      <property name="internalValue" nameId="tpce.1083923523171" value="Method call" />
    </node>
    <node role="member" roleId="tpce.1083172003582" type="tpce.EnumerationMemberDeclaration" typeId="tpce.1083171877298" id="5763961319730351316" nodeInfo="ig">
      <property name="externalValue" nameId="tpce.1083923523172" value="VAR_WRITE" />
      <property name="internalValue" nameId="tpce.1083923523171" value="Variable write" />
    </node>
    <node role="member" roleId="tpce.1083172003582" type="tpce.EnumerationMemberDeclaration" typeId="tpce.1083171877298" id="5763961319730353601" nodeInfo="ig">
      <property name="externalValue" nameId="tpce.1083923523172" value="VAR_READ" />
      <property name="internalValue" nameId="tpce.1083923523171" value="Variable read" />
    </node>
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="5763961319730978837" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="WhatMatcher" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="tpck.1133920641626" resolveInfo="BaseConcept" />
    <node role="linkDeclaration" roleId="tpce.1071489727083" type="tpce.LinkDeclaration" typeId="tpce.1071489288298" id="5763961319730980170" nodeInfo="ig">
      <property name="metaClass" nameId="tpce.1071599937831" value="aggregation" />
      <property name="role" nameId="tpce.1071599776563" value="asteriskBegin" />
      <link role="target" roleId="tpce.1071599976176" targetNodeId="5763961319731012481" resolveInfo="Asterisk" />
    </node>
    <node role="linkDeclaration" roleId="tpce.1071489727083" type="tpce.LinkDeclaration" typeId="tpce.1071489288298" id="5763961319730981642" nodeInfo="ig">
      <property name="metaClass" nameId="tpce.1071599937831" value="aggregation" />
      <property name="role" nameId="tpce.1071599776563" value="asteriskEnd" />
      <link role="target" roleId="tpce.1071599976176" targetNodeId="5763961319731012481" resolveInfo="Asterisk" />
    </node>
    <node role="implements" roleId="tpce.1169129564478" type="tpce.InterfaceConceptReference" typeId="tpce.1169127622168" id="5763961319731111112" nodeInfo="ig">
      <link role="intfc" roleId="tpce.1169127628841" targetNodeId="tpck.1169194658468" resolveInfo="INamedConcept" />
    </node>
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="5763961319730013894" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="Tracer" />
    <property name="rootable" nameId="tpce.1096454100552" value="true" />
    <property name="conceptAlias" nameId="tpce.5092175715804935370" value="trace" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="tpck.1133920641626" resolveInfo="BaseConcept" />
    <node role="propertyDeclaration" roleId="tpce.1071489727084" type="tpce.PropertyDeclaration" typeId="tpce.1071489288299" id="5763961319730611289" nodeInfo="ig">
      <property name="name" nameId="tpck.1169194664001" value="format" />
      <link role="dataType" roleId="tpce.1082985295845" targetNodeId="tpck.1082983041843" resolveInfo="string" />
    </node>
    <node role="propertyDeclaration" roleId="tpce.1071489727084" type="tpce.PropertyDeclaration" typeId="tpce.1071489288299" id="5763961319730838914" nodeInfo="ig">
      <property name="name" nameId="tpck.1169194664001" value="what" />
      <link role="dataType" roleId="tpce.1082985295845" targetNodeId="5763961319730350126" resolveInfo="What" />
    </node>
    <node role="linkDeclaration" roleId="tpce.1071489727083" type="tpce.LinkDeclaration" typeId="tpce.1071489288298" id="5763961319730984221" nodeInfo="ig">
      <property name="metaClass" nameId="tpce.1071599937831" value="aggregation" />
      <property name="role" nameId="tpce.1071599776563" value="matcher" />
      <link role="target" roleId="tpce.1071599976176" targetNodeId="5763961319730978837" resolveInfo="WhatMatcher" />
    </node>
    <node role="implements" roleId="tpce.1169129564478" type="tpce.InterfaceConceptReference" typeId="tpce.1169127622168" id="5763961319731659189" nodeInfo="ig">
      <link role="intfc" roleId="tpce.1169127628841" targetNodeId="tpck.1169194658468" resolveInfo="INamedConcept" />
    </node>
  </root>
</model>

