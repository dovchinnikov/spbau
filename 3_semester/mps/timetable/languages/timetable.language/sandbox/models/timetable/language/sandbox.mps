<?xml version="1.0" encoding="UTF-8"?>
<model modelUID="r:e63bcc95-cb3f-4da2-9e28-0a452480ae79(timetable.language.sandbox)">
  <persistence version="8" />
  <language namespace="ddc0a0e2-1cb3-47f7-9a39-27376f4e8624(timetable.language)" />
  <language namespace="ceab5195-25ea-4f22-9b92-103b95ca8c0c(jetbrains.mps.lang.core)" />
  <import index="tpck" modelUID="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" version="0" implicit="yes" />
  <import index="j1i7" modelUID="r:49b96b0e-57eb-42de-ba78-da8dfef00a02(timetable.language.structure)" version="-1" implicit="yes" />
  <root type="j1i7.Timetable" typeId="j1i7.3525383971049934679" id="3525383971050256558" nodeInfo="ng">
    <property name="name" nameId="tpck.1169194664001" value="Monday" />
    <node role="lectures" roleId="j1i7.3525383971050172941" type="j1i7.Lecture" typeId="j1i7.3525383971049956762" id="3525383971050256649" nodeInfo="ng">
      <property name="name" nameId="tpck.1169194664001" value="GenProg" />
      <property name="room" nameId="j1i7.3525383971050173251" value="206" />
      <property name="presenter" nameId="j1i7.3525383971050173267" value="Mihail Vlasiev" />
      <node role="from" roleId="j1i7.3525383971050173125" type="j1i7.Time" typeId="j1i7.3525383971049990457" id="3525383971050256650" nodeInfo="ng">
        <property name="hr" nameId="j1i7.3525383971050173380" value="16" />
        <property name="min" nameId="j1i7.3525383971050173396" value="01" />
      </node>
      <node role="to" roleId="j1i7.3525383971050173155" type="j1i7.Time" typeId="j1i7.3525383971049990457" id="3525383971050256651" nodeInfo="ng">
        <property name="hr" nameId="j1i7.3525383971050173380" value="17" />
        <property name="min" nameId="j1i7.3525383971050173396" value="35" />
      </node>
    </node>
    <node role="lectures" roleId="j1i7.3525383971050172941" type="j1i7.Lecture" typeId="j1i7.3525383971049956762" id="3525383971050298171" nodeInfo="ng">
      <property name="name" nameId="tpck.1169194664001" value="VM" />
      <property name="room" nameId="j1i7.3525383971050173251" value="206" />
      <property name="presenter" nameId="j1i7.3525383971050173267" value="Igotti" />
      <node role="from" roleId="j1i7.3525383971050173125" type="j1i7.Time" typeId="j1i7.3525383971049990457" id="3525383971050298172" nodeInfo="ng">
        <property name="hr" nameId="j1i7.3525383971050173380" value="14" />
        <property name="min" nameId="j1i7.3525383971050173396" value="10" />
      </node>
      <node role="to" roleId="j1i7.3525383971050173155" type="j1i7.Time" typeId="j1i7.3525383971049990457" id="3525383971050298173" nodeInfo="ng">
        <property name="hr" nameId="j1i7.3525383971050173380" value="15" />
        <property name="min" nameId="j1i7.3525383971050173396" value="45" />
      </node>
    </node>
    <node role="lectures" roleId="j1i7.3525383971050172941" type="j1i7.Lecture" typeId="j1i7.3525383971049956762" id="3525383971050298362" nodeInfo="ng">
      <property name="name" nameId="tpck.1169194664001" value="CV" />
      <property name="room" nameId="j1i7.3525383971050173251" value="206" />
      <property name="presenter" nameId="j1i7.3525383971050173267" value="Vakhitov &amp; Gurevich" />
      <node role="from" roleId="j1i7.3525383971050173125" type="j1i7.Time" typeId="j1i7.3525383971049990457" id="3525383971050298363" nodeInfo="ng">
        <property name="hr" nameId="j1i7.3525383971050173380" value="16" />
        <property name="min" nameId="j1i7.3525383971050173396" value="00" />
      </node>
      <node role="to" roleId="j1i7.3525383971050173155" type="j1i7.Time" typeId="j1i7.3525383971049990457" id="3525383971050298364" nodeInfo="ng">
        <property name="hr" nameId="j1i7.3525383971050173380" value="18" />
        <property name="min" nameId="j1i7.3525383971050173396" value="00" />
      </node>
    </node>
  </root>
</model>

