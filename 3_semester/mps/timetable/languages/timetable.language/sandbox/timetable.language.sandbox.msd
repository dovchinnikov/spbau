<?xml version="1.0" encoding="UTF-8"?>
<solution name="timetable.language.sandbox" uuid="96078266-a295-445c-a958-3d2488ca0627" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <usedLanguages>
    <usedLanguage>ceab5195-25ea-4f22-9b92-103b95ca8c0c(jetbrains.mps.lang.core)</usedLanguage>
    <usedLanguage>ddc0a0e2-1cb3-47f7-9a39-27376f4e8624(timetable.language)</usedLanguage>
  </usedLanguages>
</solution>

