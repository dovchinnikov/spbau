#!/bin/bash

rm -f table.txt
echo "|| r w o || blocking || lockfree ||" >> table.txt

exec 3>&1 4>&2

for t in 2 8 16
do
    for p in 3 4 5
    do
        cmd="java pw.ovchinnikov.hp.task2.Runner $t $t $((10**$p))"
        bl_cmd="$cmd blocking"
        lf_cmd="$cmd lockfree"
        echo ${bl_cmd}
        bl=$(TIMEFORMAT="%R"; { time $bl_cmd 1>&3 2>&4; } 2>&1)
        echo ${lf_cmd}
        lf=$(TIMEFORMAT="%R"; { time $lf_cmd 1>&3 2>&4; } 2>&1)        
        echo "|| $t $t 10^$p || $bl || $lf ||" >> table.txt
    done
done

exec 3>&- 4>&-
cat table.txt
