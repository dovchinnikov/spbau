package pw.ovchinnikov.hp.task2;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;

public class ResurrectDemo {
    
    private A a;

    public static class A {
        private ResurrectDemo demo;
        private String data;
        
        public String getData() {
            return data;
        }
        
        public A(ResurrectDemo demo) {
            this.demo = demo;
            StringBuilder buff = new StringBuilder();
            for (long i = 0; i < 50000000; i++) {
                buff.append('a');
            }
            this.data = buff.toString();

        }
        
        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            System.out.println("A.finalize()");
        }
    }
    
    private static class MyPhantomReference<T> extends PhantomReference<T> {

        public MyPhantomReference(T obj, ReferenceQueue<? super T> queue) {
            super(obj, queue);
            Thread thread = new MyPollingThread<>(queue);
            thread.start();
        }


        public void cleanup() {
            System.out.println("cleanup()");
            clear();
        }


        public static class MyPollingThread<T> extends Thread {

            private ReferenceQueue<? super T> referenceQueue;

            public MyPollingThread(ReferenceQueue<? super T> referenceQueue) {
                this.referenceQueue = referenceQueue;
            }
            
            @Override
            public void run() {
                System.out.println("MyPollingThread started");
                Reference<?> ref;
                while ((ref = referenceQueue.poll()) == null) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        throw new RuntimeException("Thread " + getName() + " has been interrupted");
                    }
                }
                if (ref instanceof MyPhantomReference<?>) {
                    ((MyPhantomReference<?>) ref).cleanup();
                }
            }
        }
    }
    
    public static void main(String[] args) throws InterruptedException {
        ResurrectDemo demo = new ResurrectDemo();
        Thread.sleep(20000);
        Reference<A> ref = new MyPhantomReference<>(new A(demo), new ReferenceQueue<>());
        System.out.println("ref = " + ref);
        System.out.println("A = " + ref.get());
        Thread.sleep(10000);
        System.out.println("System.gc()");
        System.gc();
        Thread.sleep(400);
        System.out.println("ref = " + ref);
        System.out.println("A = " + ref.get());
        Thread.sleep(10000);
        System.out.println("System.gc()");
        System.gc();
        Thread.sleep(400);
        Thread.sleep(10000);
        System.out.println("System.gc()");
        System.gc();
        Thread.sleep(10000);
    }

}
