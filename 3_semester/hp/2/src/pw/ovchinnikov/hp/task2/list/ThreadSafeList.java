package pw.ovchinnikov.hp.task2.list;

public interface ThreadSafeList<T> {

    boolean add(int i, T value);

    boolean remove(int i);

    T get(int i);

    static class Pair<A, B> {
        A first;
        B second;

        protected Pair(A first, B second) {
            this.first = first;
            this.second = second;
        }
    }
}
