package pw.ovchinnikov.hp.task2.list;

public class BlockingThreadSafeList<T> implements ThreadSafeList<T> {

    private Node<T> head = new Node<>(Integer.MIN_VALUE, null, new Node<>(Integer.MAX_VALUE, null, null));

    @Override
    public synchronized boolean add(int i, T value) {
        for (Node<T> previous = head, current = previous.next; current != null; previous = current, current = current.next) {
            if (current.key == i) {
                return false;
            } else if (current.key > i) {
                previous.next = new Node<>(i, value, current);
                return true;
            }
        }
        throw new RuntimeException("Should not get here");
    }

    @Override
    public synchronized boolean remove(int i) {
        for (Node<T> previous = head, current = previous.next; current != null; previous = current, current = current.next) {
            if (current.key == i) {
                previous.next = current.next;
                current.next = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized T get(int i) {
        for (Node<T> current = head; current != null; current = current.next) {
            if (current.key == i) {
                return current.value;
            }
        }
        return null;
    }

    @Override
    public synchronized String toString() {
        final StringBuilder b = new StringBuilder("[");
        for (Node<T> current = head; current != null; current = current.next) {
            b.append(current.key).append(":").append(current.value);
            if (current.next != null) {
                b.append(",\n");
            }
        }
        b.append("]");
        return b.toString();
    }


    private static class Node<T> {
        int key;
        T value;
        Node<T> next;

        Node(int key, T value, Node<T> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
