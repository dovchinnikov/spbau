package pw.ovchinnikov.hp.task2;

import pw.ovchinnikov.hp.task2.list.BlockingThreadSafeList;
import pw.ovchinnikov.hp.task2.list.LockFreeTreadSafeList;
import pw.ovchinnikov.hp.task2.list.ThreadSafeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

public class Runner {

    private static enum Mode {
        blocking,
        lockfree
    }

    private static final Random random = ThreadLocalRandom.current();

    private static void doStuff(int readers, int writers, int operations, Mode mode) {
        final ThreadSafeList<Double> list;
        if (mode == Mode.blocking) {
            list = new BlockingThreadSafeList<>();
        } else if (mode == Mode.lockfree) {
            list = new LockFreeTreadSafeList<>();
        } else {
            throw new RuntimeException("Should not get here anyways");
        }
        
        final CountDownLatch latch = new CountDownLatch(readers + writers + 1);

        final Runnable reader = () -> {
            try {
                latch.countDown();
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
            for (int i = 0; i < operations; i++) {
                final int key = random.nextInt();
                list.get(key);
            }
        };

        final Runnable writer = () -> {
            try {
                latch.countDown();
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
            for (int i = 0; i < operations; i++) {
                if (random.nextBoolean()) {
                    final int key = random.nextInt();
                    final Double value = random.nextDouble();
                    list.add(key, value);
                } else {
                    final int key = random.nextInt();
                    list.remove(key);
                }
            }
        };

        final List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < readers; i++) {
            threads.add(new Thread(reader, "Reader " + i));
        }
        for (int i = 0; i < writers; i++) {
            threads.add(new Thread(writer, "Writer " + i));
        }
        threads.forEach(Thread::start);
        latch.countDown();
        for (final Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        try {
            doStuff(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Mode.valueOf(args[3]));
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Need 4 arguments, " + args.length + " provided");
            System.exit(1);
        } catch (NumberFormatException e) {
            System.err.println("Wrong number: " + e);
            System.exit(1);
        } catch (IllegalArgumentException e) {
            System.err.println("blocking & lockfree accepted");
            System.exit(1);
        }
    }
}
