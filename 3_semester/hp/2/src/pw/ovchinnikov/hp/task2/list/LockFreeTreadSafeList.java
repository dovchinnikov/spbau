package pw.ovchinnikov.hp.task2.list;

import java.util.concurrent.atomic.AtomicMarkableReference;

public class LockFreeTreadSafeList<T> implements ThreadSafeList<T> {

    private Node<T> head = new Node<>(Integer.MIN_VALUE, null, new AtomicMarkableReference<>(
            new Node<>(Integer.MAX_VALUE, null, new AtomicMarkableReference<>(null, false)), false
    ));

    @Override
    public boolean add(int i, T value) {
        while (true) {
            final Pair<Node<T>, Node<T>> nodePair = find(i);
            final Node<T> previous = nodePair.first;
            final Node<T> current = nodePair.second;
            if (current.key == i) {
                // key already exists
                return false;
            } else {
                // create new node and link it to 'current'
                final Node<T> node = new Node<>(i, value, new AtomicMarkableReference<>(current, false));
                // now we need link previous to newly created node
                if (previous.next.compareAndSet(current, node, false, false)) {
                    return true;
                }
            }
        }
    }

    @Override
    public boolean remove(int i) {
        while (true) {
            final Pair<Node<T>, Node<T>> nodePair = find(i);
            final Node<T> previous = nodePair.first;
            final Node<T> current = nodePair.second;
            if (current.key == i) {
                // found node to remove (current)
                final Node<T> next = current.next.getReference();
                if (!current.next.attemptMark(next, true)) {
                    // another thread already marked this node for removal
                    continue;
                }
                previous.next.compareAndSet(current, next, false, false);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public T get(int i) {
        final Node<T> current = find(i).second;
        return current.key == i ? current.value : null;
    }

    private Pair<Node<T>, Node<T>> find(int key) {
        outer:
        while (true) {
            Node<T> previous = head, current = previous.next.getReference();
            while (true) {
                final boolean[] mark = {false};
                Node<T> next = current.next.get(mark);
                while (mark[0]) {
                    // got marked node
                    // help another thread to point previous to next, skipping current (marked) node
                    if (!previous.next.compareAndSet(current, next, false, false)) {
                        // something gone wrong, try once more from the beginning
                        continue outer;
                    }
                    current = next;
                    next = current.next.get(mark);
                }
                if (key <= current.key) {
                    return new Pair<>(previous, current);
                }
                previous = current;
                current = next;
            }
        }
    }

    private static class Node<T> {
        int key;
        T value;
        AtomicMarkableReference<Node<T>> next;

        Node(int key, T value, AtomicMarkableReference<Node<T>> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
