# HP

### Requirements

JRE 1.8 or higher (1.6 & 1.7 will probably work but not tested)

### Building

Set `gradlew` script executable bit:

    chmod +x gradlew

To build all tasks:

    ./gradlew

All tasks will be placed to `build/` directory as jars.

### Running

To run a task after building:

    java -jar build/hp-task1.jar

If you have `groovy` installed and available in PATH (e.g. `#!/usr/bin/env groovy`)
you can run tasks as shell scripts:

    cd src/main/groovy/
    chmod +x pw/ovchinnikov/hp/task1/Main.groovy
    ./pw/ovchinnikov/hp/task1/Main.groovy

### Task list

`task1` – Thread pool.
