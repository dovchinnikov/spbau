package pw.ovchinnikov.hp.task1

import java.util.concurrent.Callable
import java.util.concurrent.Future

class ThreadPool {

    private timeout
    private threads = [] as Set<TaskThread>

    /**
     * Thread Pool
     *
     * @param hot number of hot threads
     * @param timeout timeout of non-hot threads, 3000 msec by default
     */
    ThreadPool(hot, timeout = 3000) {
        this.timeout = timeout
        hot.times {
            def t = [timeout: 0] as TaskThread
            t.start()
            threads += t
        }
    }

    def <T> Future<T> execute(Callable<T> action) {
        // clean dead threads, don't know where to place this line of code
        threads.removeIf { !it.alive }
        //
        def task = new Task(action)
        // find first available thread
        def thread = threads.find { t -> t.currentTask.compareAndSet(null, task) }
        // if not found, create new timeout thread and assign new task to it
        if (thread == null) {
            thread = [timeout: timeout] as TaskThread
            thread.currentTask.set(task)
            thread.start()
            threads += thread
        }
        // thread will be waiting until notified
        synchronized (thread.notifier) {
            thread.notifier.notify()
        }
        // return task handle, future
        task
    }

    /**
     * Synchronously waits until all tasks are finished and returns.
     */
    def shutdown() {
        threads*.shutdown()
    }
}


