package pw.ovchinnikov.hp.task1

import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicReference

public class Task<T> implements RunnableFuture<T> {

    private static enum State {
        NEW,
        RUNNING,
        CANCELLED,
        FAILED,
        READY
    }

    private final lock = new Object()
    private final state = new AtomicReference<>(State.NEW)
    private Thread thread
    private final Callable<T> action
    private T result

    public Task(Callable<T> action) {
        this.action = action
    }

    public void setThread(Thread thread) {
        this.thread = thread
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        if (thread != null && mayInterruptIfRunning) {
            state.set(State.CANCELLED)
            thread.interrupt()
            synchronized (lock) {
                lock.notifyAll()
            }
            return true
        } else {
            return false
        }
    }

    @Override
    public boolean isCancelled() { state.get() == State.CANCELLED }

    @Override
    public boolean isDone() { state.get() == State.READY }

    @Override
    public T get() throws InterruptedException, ExecutionException {
        if (state.get() == State.RUNNING) {
            synchronized (lock) {
                // wait until ready or cancel or fail
                lock.wait()
            }
        }
        if (state.get() == State.READY) {
            return result
        } else {
            return null
        }
    }

    @Override
    public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        throw new RuntimeException("Not implemented yet")
    }

    @Override
    public void run() {
        try {
            state.set(State.RUNNING)
            result = action.call()
            state.set(State.READY)
        } catch (InterruptedException e) {
            if (state.get() != State.CANCELLED) {
                throw new RuntimeException(e)
            }
        } catch (Exception ignored) {
            state.set(State.FAILED)
        } finally {
            synchronized (lock) {
                lock.notifyAll()
            }
        }
    }
}
