#!/usr/bin/env groovy
package pw.ovchinnikov.hp.task1

import java.util.concurrent.Callable
import java.util.concurrent.Future

def R = new Random()
def ADD_TASK_PATTERN = ~/^task\s+(\d+)/
def CANCEL_TASK_PATTERN = ~/^cancel\s+(\d+)/

def tp = [10] as ThreadPool
def futures = [:] as Map<Integer, Future>
def i = 0

// change 0.times to 100.times for test
0.times {
    futures += [(++i): tp.execute { ->
        println "${Thread.currentThread().name}: started task"
        Thread.sleep(R.nextInt(2) * 1000)
        println "${Thread.currentThread().name}: finished task"
    }]
}

// main loop
System.in.eachLine { line ->
    switch (line) {
        case 'exit':
            tp.shutdown()
            Thread.sleep(500)
            System.exit(0)
            break
        case ADD_TASK_PATTERN:
            def matcher = line =~ ADD_TASK_PATTERN
            assert matcher.matches()
            def time = matcher.group(1) as long
            futures[++i] = tp.execute({ ->
                println "${Thread.currentThread().name}: started task"
                Thread.sleep(time)
                println "${Thread.currentThread().name}: returning 100500"
                100500
            } as Callable<Integer>)
            println "task added with handle: $i"
            break
        case CANCEL_TASK_PATTERN:
            def matcher = line =~ CANCEL_TASK_PATTERN
            assert matcher.matches()
            def taskId = matcher.group(1) as int
            def f = futures.get(taskId)
            if (f ? f.cancel(true) : null) {
                println "task $taskId was cancelled"
            } else {
                println "task $taskId was not cancelled"
            }
            break
        default: break
    }
}
