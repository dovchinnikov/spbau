package pw.ovchinnikov.hp.task1

import java.util.concurrent.atomic.AtomicReference

class TaskThread extends Thread {

    final currentTask = new AtomicReference<Task>()
    final notifier = new Object()
    def mustDie = false
    def timeout = 0

    @Override
    void run() {
        println "$name: started"
        while (true) {
            try {
                synchronized (notifier) {
                    // waitin' until new task arrives
                    notifier.wait(timeout)
                }
                if (currentTask.compareAndSet(null, null)) {
                    if (timeout != 0) {
                        // this is timeout thread & it ain't got task to continue living
                        break
                    } else {
                        // this is spurious wake up
                        continue
                    }
                }
            } catch (InterruptedException ignored) {
                // should be interrupted only when about to die
                assert mustDie
                break
            }
            def task = currentTask.get()
            // make task aware of running thread
            task.thread = this
            task.run()
            // make task forget about us
            task.thread = null
            // we are ready for new assignments
            currentTask.set(null)
            if (mustDie) break
        }
        println "$name: shutting down ${timeout == 0 ? 'hot' : 'timeout'} thread"
    }

    def shutdown() {
        mustDie = true
        if (currentTask.compareAndSet(null, null)) {
            // interrupt waiting threads
            interrupt()
        } else {
            // should wait until finished, no interrupt
            currentTask.get().get()
        }
    }
}
