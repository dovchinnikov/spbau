#define __CL_ENABLE_EXCEPTIONS

#ifdef __APPLE__
#include <OpenCL/cl.h>
#elif
#include <CL/cl.h>
#endif

#include "cl.hpp"
#include <vector>
#include <fstream>
#include <iostream>
#include <iterator>
#include <iomanip>

#define WRITE_INPUT_TO_STDOUT 0
#define WRITE_OUTPUT_TO_STDOUT 0
#define TEST_1024 0

typedef float element_type;

template<typename T>
void read_matrix(std::istream & in, std::vector<T> & out, const size_t & size) {
    out.resize(size * size);
    for (size_t row = 0; row < size; row++) {
        for (size_t col = 0; col < size; col++) {
            in >> out[row * size + col];
        }
    }
}

template<typename T>
void write_matrix(std::ostream & out, std::vector<T> & in, const size_t & size) {
    for (size_t row = 0; row < size; row++) {
        for (size_t col = 0; col < size; col++) {
            out << std::fixed << std::setprecision(3) << in[row * size + col] << " ";
        }
        out << std::endl;
    }
}


int main() try {
    cl::Context context;
    cl::CommandQueue queue;
    cl::Program program;
    cl::Device device;
    {
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);
        std::vector<cl::Device> devices;
        platforms[0].getDevices(CL_DEVICE_TYPE_GPU, &devices);
        context = cl::Context(devices);
        device = devices[0];
        queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);

        std::ifstream cl_file("convolution.cl");
        std::string cl_string(std::istreambuf_iterator<char>(cl_file), (std::istreambuf_iterator<char>()));
        cl::Program::Sources source(1, std::make_pair(cl_string.c_str(), cl_string.length() + 1));

        program = cl::Program(context, source);
        program.build(devices);
    }

    std::vector<element_type> A, B;
    size_t N, M;
    if (!TEST_1024) {
        std::ifstream input_file("input.txt");
        input_file >> N >> M;
        read_matrix(input_file, A, N);
        read_matrix(input_file, B, M);
    } else {
        N = 1024;
        M = 9;
        A.resize(N * N);
        B.resize(M * M);
        for (auto & e : A) {
            e = 1;
        }
        for (auto & e : B) {
            e = 1;
        }
    }
    if (WRITE_INPUT_TO_STDOUT) {
        write_matrix(std::cout, A, N);
        write_matrix(std::cout, B, M);
    }
    size_t const size_A = sizeof(element_type) * A.size();
    size_t const size_B = sizeof(element_type) * B.size();

    cl::Buffer dev_input_A(context, CL_MEM_READ_ONLY, size_A);
    cl::Buffer dev_input_B(context, CL_MEM_READ_ONLY, size_B);
    cl::Buffer dev_output(context, CL_MEM_WRITE_ONLY, size_A);

    // send stuff to gpu
    queue.enqueueWriteBuffer(dev_input_A, CL_TRUE, 0, size_A, &A[0]);
    queue.enqueueWriteBuffer(dev_input_B, CL_TRUE, 0, size_B, &B[0]);
    queue.finish();

    // gettin' kernel
    cl::Kernel kernel_gmem(program, "gpu_convolution_gmem");
    // gettin' work group size
    int work_group_size = kernel_gmem.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(device); // 512 on my laptop
    // global work shoud be divisible by work group size
    // so we divide and multiply by work group size
    // we should add 1, so global work size will be at least equal to work group size
    int global_work_size = (N * N / work_group_size + 1) * work_group_size;
    cl::KernelFunctor convolution_gmem(
        kernel_gmem,
        queue,
        cl::NullRange,                      // global work offset
        cl::NDRange(global_work_size),      // global work size
        cl::NDRange(work_group_size)        // work group size
    );
    convolution_gmem(dev_input_A, dev_input_B, N, M, dev_output).wait();

    std::ofstream output_file("output.txt");
    std::vector<element_type> output(A.size());
    queue.enqueueReadBuffer(dev_output, CL_TRUE, 0, size_A, &output[0]);
    write_matrix(output_file, output, N);

    if (WRITE_OUTPUT_TO_STDOUT) {
        write_matrix(std::cout, output, N);
    }

    return 0;
} catch (cl::Error e) {
    std::cout << std::endl << e.what() << " : " << e.err() << std::endl;
    return 1;
}



