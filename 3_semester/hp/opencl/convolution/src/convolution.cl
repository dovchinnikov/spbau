__kernel void gpu_convolution_gmem(__global float * A,
                                   __global float * B,
                                   int N,
                                   int M,
                                   __global float * C) {
    int g = get_global_id(0);
    if (g >= N * N) return;
    int i = g / N;
    int j = g % N;
    int HM = (M - 1) / 2;

    float res = 0;
    for (int k = -HM; k <= HM; ++k) {
        for (int l = -HM; l <= HM; ++l) {

            int ai = i + k;
            int aj = j + l;
            if (ai < 0 || ai >= N || aj < 0 || aj >= N) continue;
            float a = A[ai * N + aj];

            int bi = k + HM;
            int bj = l + HM;
            float b = B[bi * M + bj];

            res += a * b;
        }
    }
    C[i * N + j] = res;
}
