#define SWAP_ADD(a,b) { float _tmp = a; a = b; b += _tmp; }

__kernel void up_sweep(__global float *A, int N, unsigned int offset) {
   int g = get_global_id(0); // id of the thread
   if (N / offset > 512) {
        int a = offset * (2 * g + 1) - 1, b = a + offset;
        if (b < N) { A[b] += A[a]; }
   } else {
        while (N / (offset * 2) > 0) {
            barrier(CLK_GLOBAL_MEM_FENCE);
            int a = offset * (2 * g + 1) - 1, b = a + offset;
            if (b < N) { A[b] += A[a]; }
            offset <<= 1;
        }
   }
}

__kernel void down_sweep(__global float *A, int N, unsigned int offset) {
    int g = get_global_id(0);  // id of the thread
    if (g == 0 && offset == N / 2) {
        A[N - 1] = 0;
    }
    if (N / (offset * 2) > 256) {
        int a = offset * (2 * g + 1) - 1, b = a + offset;
        if (b < N) SWAP_ADD(A[a], A[b]);
    } else {
        while (N / (offset * 2) <= 256) {
            barrier(CLK_GLOBAL_MEM_FENCE);
            int a = offset * (2 * g + 1) - 1, b = a + offset;
            if (b < N) SWAP_ADD(A[a], A[b]); 
            offset >>= 1;
        }
    }
}
