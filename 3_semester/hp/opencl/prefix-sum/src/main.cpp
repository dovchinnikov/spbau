#define __CL_ENABLE_EXCEPTIONS

#ifdef __APPLE__
#include <OpenCL/cl.h>
#elif
#include <CL/cl.h>
#endif

#include "cl.hpp"
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>

#define KERNEL_SRC "prefix-sum.cl"
#define DATA_SRC "input.txt"
#define OUT_FILE "output.txt"
#define TEST_1024 0
#define WRITE_INPUT_TO_STDOUT 0
#define WRITE_INTERMEDIATE_TO_STDOUT 0
#define WRITE_OUTPUT_TO_STDOUT 0
#define DEBUG_KERNEL_CALLS 0

typedef float element_type;

template<typename T>
void read_vector(std::istream & in, std::vector<T> & out, const size_t & size) {
    out.resize(size);
    for (auto i = 0; i < size; i++) {
        in >> out[i];
    }
}

template<typename T>
void write_vector(std::ostream & out, std::vector<T> & in) {
    for (auto i = 0; i < in.size(); i++) {
        out << std::fixed << std::setprecision(3) << in[i] << " ";
        //        out << in[i] << (((i + 1) % 128 == 0) ? "\n" : " ");
    }
    out << std::endl;
}

size_t round_up(size_t x) {
    if (x == 0) { return 0; }
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return x + 1;
}

int main() try {
    cl::Context context;
    cl::CommandQueue queue;
    cl::Program program;
    cl::Device device;
    {
        std::vector<cl::Platform> platforms;
        std::vector<cl::Device> devices;
        cl::Platform::get(&platforms);
        platforms[0].getDevices(CL_DEVICE_TYPE_GPU, &devices);
        context = cl::Context(devices);
        device = devices[0];
        queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);

        std::ifstream cl_file(KERNEL_SRC);
        std::string cl_string(std::istreambuf_iterator<char>(cl_file), (std::istreambuf_iterator<char>()));
        cl::Program::Sources source(1, std::make_pair(cl_string.c_str(), cl_string.length() + 1));

        program = cl::Program(context, source);
        program.build(devices);
    }

    std::vector<element_type> A;
    size_t N;
#if TEST_1024
    N = 1024;
    A.resize(N);
    for (auto & e : A) {
        e = 1;
    }
#else
    std::ifstream input_file(DATA_SRC);
    input_file >> N;
    read_vector(input_file, A, N);
#endif

#if WRITE_INPUT_TO_STDOUT
    write_vector(std::cout, A);
#endif

    size_t const total_size = round_up(N); // total work (& buffer) size
    cl::Buffer dev_A(context, CL_MEM_READ_WRITE, total_size * sizeof(element_type));
    queue.enqueueWriteBuffer(dev_A, CL_TRUE, 0, N * sizeof(element_type), &A[0]);

    auto _debug_read_vector = [&queue, N, &dev_A] () {
#if WRITE_INTERMEDIATE_TO_STDOUT
        std::vector<element_type> r(N);
        queue.enqueueReadBuffer(dev_A, CL_TRUE, 0, N * sizeof(element_type), &r[0]);
        queue.finish();
        write_vector(std::cout, r);
#endif
    };

    cl::Kernel up_sweep_kernel(program, "up_sweep");
    for (auto offset = 1; total_size / offset >= 512; offset <<= 1) {
        cl::KernelFunctor(up_sweep_kernel, queue, 0, total_size / offset, 256)(dev_A, total_size, offset);
#if DEBUG_KERNEL_CALLS
        std::cout << "up sweep (global work size: " << total_size / offset << ", offset: " << offset << ")" << std::endl;
#endif
    }
    if (total_size < 512) {
        cl::KernelFunctor(up_sweep_kernel, queue, 0, 256, 256)(dev_A, total_size, 1);
#if DEBUG_KERNEL_CALLS
        std::cout << "up sweep (global work size: " << total_size << ", offset: " << 1 << ")" << std::endl;
#endif
    }
    _debug_read_vector();

    cl::Kernel down_sweep_kernel(program, "down_sweep");
    {
        auto offset = total_size / 2;
        cl::KernelFunctor(down_sweep_kernel, queue, 0, 256, 256)(dev_A, total_size, offset);
#if DEBUG_KERNEL_CALLS
        std::cout << "down sweep (global work size: " << 256 << ", offset: " << offset << ")" << std::endl;
#endif
        _debug_read_vector();
    }

    for (auto offset = total_size / 1024; offset > 0; offset >>= 1) {
        cl::KernelFunctor(down_sweep_kernel, queue, 0, total_size / offset, 256)(dev_A, total_size, offset);
#if DEBUG_KERNEL_CALLS
        std::cout << "down sweep (global work size: " << total_size / offset << ", offset: " << offset << ")" << std::endl;
#endif
    }

    queue.enqueueReadBuffer(dev_A, CL_TRUE, 0, N * sizeof(element_type), &A[0]);
    queue.finish();

    std::ofstream output_file(OUT_FILE);
    write_vector(output_file, A);
#if WRITE_OUTPUT_TO_STDOUT
    write_vector(std::cout, A);
#endif

    return 0;
} catch (cl::Error e) {
    std::cout << std::endl << e.what() << " : " << e.err() << std::endl;
    return 1;
}



