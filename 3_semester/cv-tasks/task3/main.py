#!/usr/bin/env python
# coding=utf-8

import cv2
import numpy as np

WINDOW_NAME = 'Task 3'
SRC = cv2.imread('mandril.bmp', 0)
STATE = {'Kernel size': 50}


def do_stuff():
    img32f = np.float32(SRC)
    fft = cv2.dft(img32f, flags=cv2.DFT_COMPLEX_OUTPUT)
    fft_shifted = np.fft.fftshift(fft)

    h, w = SRC.shape
    cy, cx, k_size = h / 2, w / 2, STATE['Kernel size'] / 2
    fft_shifted[cy - k_size:cy + k_size, cx - k_size:cx + k_size] = 0  # filter high frequencies is here

    inverse_fft = np.fft.ifftshift(fft_shifted)
    inverse = cv2.idft(inverse_fft)

    filtered = cv2.magnitude(inverse[:, :, 0], inverse[:, :, 1])

    print STATE
    cv2.imshow('Task 3 Original image', SRC)
    cv2.imshow(WINDOW_NAME, filtered)
    cv2.imshow('Task 3 Laplacian', cv2.Laplacian(img32f, cv2.CV_32F, None, ksize=5, scale=1))


if __name__ == "__main__":
    def update_param(param):
        def inner(x):
            STATE[param] = x
            do_stuff()

        return inner

    cv2.namedWindow(WINDOW_NAME)
    for (param_name, param_initial_value) in STATE.iteritems():
        cv2.createTrackbar(param_name, WINDOW_NAME, 0, 100, update_param(param_name))
        cv2.setTrackbarPos(param_name, WINDOW_NAME, param_initial_value)

    do_stuff()
    cv2.waitKey(0)
    cv2.destroyAllWindows()