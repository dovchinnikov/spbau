#!/usr/bin/env python
# coding=utf-8
import cv2
import cv2.cv as cv

WINDOW_NAME = 'Task 1'
src = cv2.imread('text.bmp')
state = {'lks': 5, 'gks': 23, 'lsc': 1, 't': 1}


def do_stuff():
    image = src

    gks = state['gks']
    image = cv2.GaussianBlur(src, (gks, gks), 0) \
        if gks is not None else image

    lks, lsc = state['lks'], state['lsc']
    image = cv2.Laplacian(image, cv2.CV_32F, None, ksize=lks, scale=lsc) \
        if lks is not None else image

    if state['t'] == 1:
        r, image = cv2.threshold(image, 10, 255, cv2.THRESH_BINARY)

    print(state)
    cv2.imshow(WINDOW_NAME, image)
    cv2.imwrite("text-out.bmp", image)


def gaussian_kernel_size(x):
    state['gks'] = None if x is 0 else 2 * x + 1
    do_stuff()


def laplacian_kernel_size(x):
    state['lks'] = None if x is 0 else 2 * x + 1
    do_stuff()


def laplacian_scale(x):
    state['lsc'] = x
    do_stuff()


def threshold(x):
    state['t'] = x
    do_stuff()


if __name__ == "__main__":
    cv2.namedWindow(WINDOW_NAME, cv.CV_WINDOW_NORMAL)
    cv2.createTrackbar('Gaussian kernel size', WINDOW_NAME, 0, 20, gaussian_kernel_size)
    cv2.setTrackbarPos('Gaussian kernel size', WINDOW_NAME, (state['gks'] - 1) / 2)
    cv2.createTrackbar('Laplacian kernel size', WINDOW_NAME, 0, 15, laplacian_kernel_size)
    cv2.setTrackbarPos('Laplacian kernel size', WINDOW_NAME, (state['lks'] - 1) / 2)
    cv2.createTrackbar('Laplacian scale', WINDOW_NAME, 0, 10, laplacian_scale)
    cv2.setTrackbarPos('Laplacian scale', WINDOW_NAME, state['lsc'])
    cv2.createTrackbar('Threshold', WINDOW_NAME, 0, 1, threshold)
    cv2.setTrackbarPos('Threshold', WINDOW_NAME, state['t'])
    do_stuff()
    cv2.waitKey(0)
