#!/usr/bin/env python
# coding=utf-8

import cv2
import numpy as np

WINDOW_NAME = 'Task 5 Harris'
cap = cv2.VideoCapture('sequence.mpg')

# # params for ShiTomasi corner detection
feature_params = dict(maxCorners=100,
                      qualityLevel=0.3,
                      minDistance=7,
                      blockSize=7,
                      useHarrisDetector=True)

# Parameters for Lucas-Kanade optical flow
lk_params = dict(winSize=(15, 15),
                 maxLevel=2,
                 criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

r, initial_frame = cap.read()
initial_frame_gray = cv2.cvtColor(initial_frame, cv2.COLOR_BGR2GRAY)
initial_points = cv2.goodFeaturesToTrack(initial_frame_gray, mask=None, **feature_params)

color = np.random.randint(0, 255, (100, 3))

mask = np.zeros_like(initial_frame)
previous_frame_gray = initial_frame_gray
previous_keypoints = initial_points
while True:
    r, frame = cap.read()
    if not r:
        break
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # calculate optical flow
    points, st, err = cv2.calcOpticalFlowPyrLK(previous_frame_gray, frame_gray, previous_keypoints, None, **lk_params)

    # Select good points
    good_points = points[st == 1]
    previous_good_points = previous_keypoints[st == 1]

    # draw the tracks
    for i, (new, old) in enumerate(zip(good_points, previous_good_points)):
        a, b = new.ravel()
        c, d = old.ravel()
        cv2.line(mask, (a, b), (c, d), color[i].tolist(), 2)
        cv2.circle(frame, (a, b), 5, color[i].tolist(), -1)
    img = cv2.add(frame, mask)

    previous_frame_gray = frame_gray.copy()
    previous_keypoints = points.reshape(-1, 1, 2)

    cv2.imshow(WINDOW_NAME, img)
    k = cv2.waitKey()
    if k == 27:
        break

cv2.destroyAllWindows()
cap.release()

