#!/usr/bin/env python
# coding=utf-8

import cv2
import numpy as np

WINDOW_NAME = 'Task 2'
SRC = cv2.imread('source.bmp', 0)
STATE = {'Dilate Y': 6, 'Dilate X': 6, 'Erode X': 5, 'Erode Y': 5}


def do_stuff():
    image = SRC

    # erode
    ex, ey = STATE['Erode X'], STATE['Erode Y']
    image = cv2.erode(image, np.ones((ex + 1, ey + 1), np.uint8), iterations=1)

    # dilate
    dx, dy = STATE['Dilate X'], STATE['Dilate Y']
    image = cv2.dilate(image, np.ones((dx + 1, dy + 1), np.uint8), iterations=1)

    r, th = cv2.threshold(image, 127, 255, 0)
    contours, tree = cv2.findContours(th, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        cv2.rectangle(image, (x, y), (x + w, y + h), (255, 255, 255), 1)

    print STATE
    cv2.imshow(WINDOW_NAME, image)


if __name__ == "__main__":
    def update_param(param):
        def inner(x):
            STATE[param] = x
            do_stuff()

        return inner

    cv2.namedWindow(WINDOW_NAME)
    for (param_name, param_initial_value) in STATE.iteritems():
        cv2.createTrackbar(param_name, WINDOW_NAME, 0, 15, update_param(param_name))
        cv2.setTrackbarPos(param_name, WINDOW_NAME, param_initial_value)

    do_stuff()
    cv2.waitKey(0)
    cv2.destroyAllWindows()