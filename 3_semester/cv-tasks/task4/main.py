#!/usr/bin/env python
# coding=utf-8

import cv2
import numpy as np

WINDOW_NAME = 'Task 4'
SRC = cv2.imread('mandril.bmp', 0)


def draw_matches(img1, kp1, img2, kp2, matches):
    __author__ = 'some guy from stackoverlfow.com'
    """
    My own implementation of cv2.drawMatches as OpenCV 2.4.9
    does not have this function available but it's supported in
    OpenCV 3.0.0

    This function takes in two images with their associated
    keypoints, as well as a list of DMatch data structure (matches)
    that contains which keypoints matched in which images.

    An image will be produced where a montage is shown with
    the first image followed by the second image beside it.

    Keypoints are delineated with circles, while lines are connected
    between matching keypoints.

    img1,img2 - Grayscale images
    kp1,kp2 - Detected list of keypoints through any of the OpenCV keypoint
              detection algorithms
    matches - A list of matches of corresponding keypoints through any
              OpenCV keypoint matching algorithm
    """

    # Create a new output image that concatenates the two images together
    # (a.k.a) a montage
    rows1 = img1.shape[0]
    cols1 = img1.shape[1]
    rows2 = img2.shape[0]
    cols2 = img2.shape[1]

    out = np.zeros((max([rows1, rows2]), cols1 + cols2, 3), dtype='uint8')

    # Place the first image to the left
    out[:rows1, :cols1, :] = np.dstack([img1, img1, img1])

    # Place the next image to the right of it
    out[:rows2, cols1:cols1 + cols2, :] = np.dstack([img2, img2, img2])

    # For each pair of points we have between both images
    # draw circles, then connect a line between them
    for mat in matches:
        # Get the matching keypoints for each of the images
        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx

        # x - columns
        # y - rows
        (x1, y1) = kp1[img1_idx].pt
        (x2, y2) = kp2[img2_idx].pt

        # Draw a small circle at both co-ordinates
        # radius 4
        # colour blue
        # thickness = 1
        cv2.circle(out, (int(x1), int(y1)), 4, (255, 0, 0), 1)
        cv2.circle(out, (int(x2) + cols1, int(y2)), 4, (255, 0, 0), 1)

        # Draw a line in between the two points
        # thickness = 1
        # colour blue
        cv2.line(out, (int(x1), int(y1)), (int(x2) + cols1, int(y2)), (255, 0, 0), 1)
    return out


if __name__ == "__main__":
    original = SRC
    rows, cols = original.shape
    transform_matrix = cv2.getRotationMatrix2D((cols / 2, rows / 2), 45, 0.5)
    transformed = cv2.warpAffine(original, transform_matrix, (cols, rows))

    sift = cv2.SIFT()

    original_keypoints, original_descriptors = sift.detectAndCompute(original, None)
    original_with_keypoints = cv2.drawKeypoints(
        original, original_keypoints
    )

    transformed_keypoints, transformed_descriptors = sift.detectAndCompute(transformed, None)
    transformed_with_keypoints = cv2.drawKeypoints(
        transformed, transformed_keypoints,
        # flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS
    )

    flann_matcher = cv2.FlannBasedMatcher(indexParams=dict(
        algorithm=0,  # FLANN_INDEX_KDTREE
        trees=5
    ), searchParams=dict(
        checks=50
    ))

    # get 2 matches for each keypoint
    matches2 = flann_matcher.knnMatch(original_descriptors, transformed_descriptors, k=2)
    # keep matches that are enough away from next best match. Enough is 0.75
    matches = [pair[0] for i, pair in enumerate(matches2) if pair[0].distance < 0.75 * pair[1].distance]

    def check(original_keypoint, transformed_keypoint):
        original_point = np.append(original_keypoint.pt, 1)
        transformed_original_point = np.array([
            np.dot(original_point, transform_matrix[0, :]),
            np.dot(original_point, transform_matrix[1, :])
        ])
        transformed_point = transformed_keypoint.pt
        delta = np.array(transformed_original_point) - transformed_point
        return delta[0] ** 2 + delta[1] ** 2 < 1

    total_matches = len(matches)
    right_matches = sum([
        1 for match in matches
        if check(original_keypoints[match.queryIdx], transformed_keypoints[match.trainIdx])
    ])
    print "Total: {0}, right: {1}, percent: {2}%".format(
        total_matches, right_matches, 100.0 * right_matches / total_matches
    )

    result = draw_matches(original, original_keypoints, transformed, transformed_keypoints, matches)

    cv2.imshow('Original image', original)
    cv2.imshow('Transformed image', transformed)
    cv2.imshow('Original image with keypoints', original_with_keypoints)
    cv2.imshow('Transformed image with keypoints', transformed_with_keypoints)
    cv2.imshow('Result', result)

    cv2.waitKey(0)
    cv2.destroyAllWindows()


