H=$1
if [ -z $H ]; then
    echo "please specify host"
    exit 1
fi

P=`ping -vq -c 20 -s 65500 $H | tail -n 2`

echo $P
TR=`echo $P | grep -o -e "\d\+" | head -n 2 | awk '{s+=$1} END {print s}'`
SP=`echo $P | grep -o -e "\d\+\.\d\+" | tail -n 3 | head -n 1`

echo "total transmitted:     $TR"
echo "average time:          $SP"
#echo "average speed formula: `echo "($TR * 65508) / $SP"`"
echo "average speed:         `echo "($TR * 65508) / $SP" | bc -l` kBps"


