#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

unsigned short checksum(unsigned short *, int);

int main(int argc, char *argv[]) {

    const char *time_server;
    if (argc == 1) {
        time_server = "github.com";
    } else {
        time_server = argv[1];
    }

    struct hostent *server_host = gethostbyname(time_server);
    if (server_host == 0) {
        herror("gethostbyname");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    memcpy(&server_addr.sin_addr, server_host->h_addr_list[0], server_host->h_length);

    struct icmp *icmp_request = malloc(sizeof(struct icmp));
    {
        time_t now_t                = time(0);
        struct tm *now              = gmtime(&now_t);
        uint32_t origin_time        = (now->tm_sec + now->tm_min * 60 + now->tm_hour * 3600) * 1000;

        icmp_request->icmp_type     = ICMP_TSTAMP;
        icmp_request->icmp_code     = 0;
        icmp_request->icmp_id       = 1;
        icmp_request->icmp_seq      = 2;
        icmp_request->icmp_otime    = htonl(origin_time);
        icmp_request->icmp_cksum    = 0;
        icmp_request->icmp_cksum    = checksum((unsigned short *)icmp_request, sizeof(struct icmp));
    }

    int socket_handle = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (socket_handle == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    uint32_t addrlen = sizeof(struct sockaddr);
    ssize_t bytes_sent = sendto(
                             socket_handle,
                             icmp_request,
                             sizeof(struct icmp),
                             0,
                             (struct sockaddr *)&server_addr,
                             addrlen
                         );
    if (bytes_sent != sizeof(struct icmp)) {
        perror("sendto");
        exit(EXIT_FAILURE);
    }
    printf("%zd bytes sent to %s\n", bytes_sent, inet_ntoa(server_addr.sin_addr));

    {
        long time_sent  = ntohl(icmp_request->icmp_otime) / 1000;
        struct tm *t    = gmtime(&time_sent);
        printf("Origin time sent:\t%s", asctime(t));
    }

    struct ip *ip_reply = malloc(255);
    {
        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(socket_handle, &readset);
        struct timeval timeout = {3, 0};
        int result = select(socket_handle + 1, &readset, NULL, NULL, &timeout);
        if (result > 0) {
            if (FD_ISSET(socket_handle, &readset)) {
                ssize_t bytes_received = recvfrom(
                                             socket_handle,
                                             ip_reply,
                                             sizeof(struct ip) + sizeof(struct icmp),
                                             0,
                                             (struct sockaddr *)&server_addr,
                                             &addrlen
                                         );
                if (bytes_received <= 0) {
                    perror("recvfrom");
                    exit(EXIT_FAILURE);
                }
                printf("%zd bytes received from %s\n", bytes_received, inet_ntoa(server_addr.sin_addr));
            }
        } else {
            printf("Timeout exceeded\n");
            exit(EXIT_FAILURE);
        }
    }

    printf("IP header length:\t%d\n", ip_reply->ip_hl * 4);

    struct icmp *icmp_reply = (struct icmp *)((void *)ip_reply + 4 * ip_reply->ip_hl);
    printf("Length check:\t\t%ld\n", (void *)icmp_reply - (void *)ip_reply);
    if (icmp_reply->icmp_type != ICMP_TSTAMPREPLY) {
        printf("Wrong reply: type: %d, code: %d\n", icmp_reply->icmp_type, icmp_reply->icmp_code);
        exit(EXIT_FAILURE);
    }

    long origin_time = ntohl(icmp_reply->icmp_otime) / 1000;
    long receive_time = ntohl(icmp_reply->icmp_rtime) / 1000;
    long transmit_time = ntohl(icmp_reply->icmp_ttime) / 1000;

    printf("Origin time received:\t%s", asctime(gmtime(&origin_time)));
    printf("Receive time received:\t%s", asctime(gmtime(&receive_time)));
    printf("Transmit time received:\t%s", asctime(gmtime(&transmit_time)));

    long delta = difftime(origin_time, transmit_time);
    int hours = delta / 3600;
    int minutes = (delta -= hours * 3600) / 60;
    int seconds = (delta -= minutes * 60);
    printf("Time differs by %d hours, %d minutes, %d seconds\n", hours, minutes, seconds);

    free(icmp_request);
    free(ip_reply);
    close(socket_handle);
    return 0;
}

unsigned short checksum(unsigned short *addr, int len) {
    register int sum = 0;
    u_short answer = 0;
    register u_short *w = addr;
    register int nleft = len;
    while (nleft > 1) {
        sum += *w++;
        nleft -= 2;
    }
    if (nleft == 1) {
        *(u_char *) (&answer) = *(u_char *) w;
        sum += answer;
    }
    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);
    answer = ~sum;
    return (answer);
}
