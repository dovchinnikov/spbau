-- create schema competition;
-- use competition;

drop table if exists checkin;
drop table if exists game;
drop table if exists participant;
drop table if exists team;
drop table if exists category_checkpoints;
drop table if exists category;
drop table if exists volunteer;
drop table if exists checkpoint;

create table team (
	id int,
	name varchar(64) not null,
	contact_phone varchar(16) not null,
	register_date date not null,
	payment_status bool default false,
	primary key (id)
);

create table participant (
	id int,
	name varchar(64) not null,
	team_id int not null,	
	primary key (id),
	foreign key (team_id) references team(id)
);

create table category (
	id int,
	name varchar(16) not null,
	rules_description varchar(512),
	max_teams int not null,
	primary key (id)
);

create table checkpoint (
	id int,
	address varchar(256) not null,
	description varchar(512) not null,
	primary key (id)
);

create table category_checkpoints (
	id int,
	category_id int not null,
	checkpoint_id int not null,
	checkpoint_order int default 0, -- порядок КП в категории
	primary key (id),
	foreign key (category_id) references category(id),
	foreign key (checkpoint_id) references checkpoint(id)
);

create table volunteer (
	id int,
	name varchar(64) not null,
	checkpoint_id int not null,
	primary key (id),
	foreign key (checkpoint_id) references checkpoint(id)
);

create table game ( -- в принципе можно вынести всю эту таблицу в таблицу team
	id int,
	team_id int not null unique,
	category_id int not null,
	start_time int not null, -- когда команда оплатила взнос и определилась с категорией, ей назначается start_time, т.е. создается строка в таблице game.
	finish_time int,
	primary key (id),
	foreign key (team_id) references team(id),
	foreign key (category_id) references category(id),
	unique(start_time, category_id)
);

create table checkin (
	id int,
	game_id int not null, 
	checkpoint_id int not null,
	checkin_time int not null,
	primary key (id),
	foreign key (game_id) references game(id),
	foreign key (checkpoint_id) references checkpoint(id),
	unique(game_id, checkpoint_id, checkin_time) -- комадна не может быть одновременно на двух чекпоинтах
);

insert into team values(1, "lol", "1231", "2013-11-11", true);
insert into participant values(1, "a", 1);
insert into participant values(2, "b", 1);
insert into participant values(3, "c", 1);
insert into category values(1, "as", "12", 2);
insert into game values(1,1,1,1111, null);

select t.id as team_id, 
	   c.name as category_name, 
	   g.start_time as start_time,
	   (select count(*) from participant p where p.team_id = t.id) -- можно и через group by
  from team t join game g 
				on t.id = g.team_id
			  join category c
				on g.category_id = c.id
 where t.payment_status
 order by start_time;
