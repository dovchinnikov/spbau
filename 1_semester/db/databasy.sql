select c.name, 
	   c.population, 
	   pp.citypop, 
	   (1.0 - pp.citypop * 1.0 / c.population) as koef 
  from country c join (select countrycode, 
                              sum(population) as citypop 
                         from city 
                        group by countrycode) as pp 
	               on c.code = pp.countrycode 
 where citypop > 0 
   and population > 0 
 order by koef desc;

select cn.name, 
       cn.population, 
       (1 - sum(cc.population)*1.0/cn.population) as koef 
  from country cn join city cc on cn.code = cc.countrycode 
 where cn.population > 0 group by cn.name, cn.population
 order by koef desc;

select cn.name, 
       count(cl.language) as count, 
       string_agg(cl.language, ', ') as languages 
  from country cn join countrylanguage cl 
                    on cn.code = cl.countrycode 
 group by cn.name 
 order by count desc;
 
