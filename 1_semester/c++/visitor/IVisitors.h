#ifndef IVISITORS_H_INCLUDED
#define IVISITORS_H_INCLUDED

#include "INodes.h"
#include <vector>
#include <iostream>
#include <functional>

using std::vector;
using std::cout;
using std::multiplies;
using std::plus;
using std::divides;
using std::minus;


class IVisitor {

	public:
		virtual void visit(Number *) = 0;
		virtual void visit(Sum *) = 0;
		virtual void visit(Multiply *) = 0;
		virtual void visit(Subtract *) = 0;
		virtual void visit(Divide *) = 0;
};

class EvalVisitor : public IVisitor {

	public:
		void visit(Number *node) { 
			stack.push_back(node->value());
		}

		void visit(Sum *node) {
			eval<plus<double> >(node, plus<double>());
		}

		void visit(Multiply *node) {
			eval<multiplies<double> >(node, multiplies<double>());
		} 

		void visit(Subtract *node) {
			eval<minus<double> >(node, minus<double>());
		}

		void visit(Divide *node) {
			eval<divides<double> >(node, divides<double>());
		}

		double result() {
			return *(stack.begin());
		}

	private:
		vector<double> stack;
		template <class Eval> void eval(BinaryOperation *node, Eval func) {		
			node->left()->visit(this);
			node->right()->visit(this);
			double r = stack.back();
			stack.pop_back();
			double l = stack.back();
			stack.pop_back();
			stack.push_back(func(l, r));		
		}
};

class PrintVisitor : public IVisitor {

	public:
		void visit(Number *node) { 
			cout << node->value();
		}

		void visit(Sum *node) {
			print(node, '+');
		}

		void visit(Multiply *node) {	
			print(node, '*');
		}
 
		void visit(Subtract *node) {
			print(node, '-');
		}

		void visit(Divide *node) {
			print(node, '/');
		}	

	private:
		void print(BinaryOperation *node, char sign) {		
			cout << "(";
			node->left()->visit(this);
			cout << sign;
			node->right()->visit(this);
			cout << ")";
		}
};

#endif
