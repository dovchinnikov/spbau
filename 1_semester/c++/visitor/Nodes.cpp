#include "INodes.h"
#include "IVisitors.h"

void Number::visit(IVisitor *visitor) {
	visitor->visit(this);
}

void Sum::visit(IVisitor *visitor) {
	visitor->visit(this);
}

void Multiply::visit(IVisitor *visitor) {
	visitor->visit(this);
}

void Subtract::visit(IVisitor *visitor) {
	visitor->visit(this);
}

void Divide::visit(IVisitor *visitor) {
	visitor->visit(this);
}

