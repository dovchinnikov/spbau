#include "INodes.h"
#include "IVisitors.h"
#include <iostream>

using std::cout;
using std::endl;

int main() {
	INode *a1 = new Number(1);
	INode *a2 = new Number(2);
	INode *a3 = new Number(3);
	INode *a4 = new Number(4);
	INode *a5 = new Number(5);
	INode *a6 = new Number(6);

	INode *o1 = new Subtract(a5, a3);
	INode *o2 = new Divide(a6, o1);
	INode *o3 = new Sum(a4, a1);
	INode *root = new Multiply(o3, o2);

	EvalVisitor *v = new EvalVisitor();
	PrintVisitor *pv = new PrintVisitor();
	root->visit(pv);
	root->visit(v);
	cout << " = " << v->result() << endl;
	
	return 0;
}
