#ifndef INODES_H_INCLUDED
#define INODES_H_INCLUDED

class IVisitor;

class INode {
	
	public:
		virtual void visit(IVisitor *) = 0;

};

class Number : public INode {
	
	public:
		Number(double value):v(value) {}

		void visit(IVisitor *);

		double value() {
			return v;
		}

	private:
		double v;
};

class BinaryOperation : public INode {

	public:
		BinaryOperation(INode *left, INode *right) : l(left), r(right) {}

		INode *left() {
			return l;
		}

		INode *right() {
			return r;
		}

	private:	
		INode *l;
		INode *r;
};

class Multiply : public BinaryOperation {
	
	public:
		Multiply(INode *left, INode *right) : BinaryOperation(left, right) {}
		void visit(IVisitor *);
};

class Sum : public BinaryOperation {
	
	public:
		Sum(INode *left, INode *right) : BinaryOperation(left, right) {}
		void visit(IVisitor *);
};

class Subtract : public BinaryOperation {
	
	public:
		Subtract(INode *left, INode *right) : BinaryOperation(left, right) {}
		void visit(IVisitor *);
};

class Divide : public BinaryOperation {
	
	public:
		Divide(INode *left, INode *right) : BinaryOperation(left, right) {}
		void visit(IVisitor *);
};

#endif
