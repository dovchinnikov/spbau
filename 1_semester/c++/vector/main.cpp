#include <iostream>
#include "MyVector.h"

using namespace std;

int main()
{
    MyVector v;
    {
        MyVector inner(5);
        for (int i=0; i < 100; i++){
            inner.add(i*i);
        }
        v = inner;
    }
    cout << v << endl;
    return 0;
}
