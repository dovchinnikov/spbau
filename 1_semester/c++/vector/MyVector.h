#ifndef MYVECTOR_H
#define MYVECTOR_H

#include <cstdlib>
#include <iostream>

using namespace std;
	
class MyVector
{
	friend ostream& ::operator<<(ostream& output, MyVector& v)
	{
		for (int i=0; i < v.m_index; i++)
   		{
   			output << v.m_array[i] << '\t';
   			if (i%8 == 7)
   			{
				output << endl;
			}
   		}
   		return output;
	}	
	
    public:
        MyVector();
        MyVector(size_t);
        MyVector(const MyVector& other);
        ~MyVector();
    
        MyVector& operator=(const MyVector&);

        int get(size_t s);
        void set(size_t, int);
        void add(int);
        size_t size();

    protected:
    private:
        void swap1(MyVector& other);
        void init(size_t);
        int *m_array;
        size_t m_index;
        size_t m_capacity;
};

#endif // MYVECTOR_H
