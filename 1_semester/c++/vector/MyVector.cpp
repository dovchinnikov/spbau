#include "MyVector.h"
#include <algorithm>

int MyVector::get(size_t index)
{
    return m_array[index];
}

void MyVector::set(size_t index, int e)
{
    m_array[index] = e;
}

void MyVector::add(int e)
{
    if (m_index == m_capacity)
    {
        m_capacity*=2;
        int *arr= new int[m_capacity];
        for (size_t i = 0; i<m_index; i++)
        {
            arr[i] = m_array[i];
        }
        delete[] m_array;
        m_array = arr;
    }
    m_array[m_index++] = e;
}

size_t MyVector::size()
{
    return m_index;
}

MyVector::MyVector()
{
    init(32);
}

MyVector::MyVector(size_t size)
{
    init(size);
}

MyVector::~MyVector()
{
    delete[] m_array;
}

MyVector::MyVector(const MyVector& other)
{
    m_index = other.m_index;
    m_capacity = other.m_capacity;
    m_array = new int[m_capacity];
    for (size_t i=0; i< m_index; i++)
    {
        m_array[i] = other.m_array[i];
    }
}

void MyVector::swap1(MyVector& other)
{
    std::swap(m_array, other.m_array);
    std::swap(m_index, other.m_index);
    std::swap(m_capacity, other.m_capacity);
}

MyVector& MyVector::operator = (const MyVector& other)
{
    if (this==&other)
    {
        return *this;
    }
    MyVector v(other);
    v.swap1(*this);
    return *this;
}

void MyVector::init(size_t size)
{
    m_array = new int[size];
    m_index = 0;
    m_capacity = size;
}
