#include <iostream>

using std::ostream;


class String {
	
	friend ostream& operator<<(ostream& output, String& v);

	public:
		String();
		String(const char *);
		String(const String &);
		~String();
		String& operator+(String& other);

	private:
		char *inner;
		void copy(const char *, char *);

};

inline ostream& operator<<(ostream& output, String& v) {
	output << v.inner;	
	return output;
}

