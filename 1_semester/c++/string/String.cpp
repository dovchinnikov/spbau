#ifndef LOL_STRING
#define LOL_STRING

#include "String.h"
#include <cstring>

String::String() {

}

String::String(const char *str) {
	copy(str, inner);
}

String::String(const String &other){
	copy(other.inner, inner);	
}


String::~String() {
	if (inner != NULL) {
		delete inner;
	}
}

String& String::operator+(String& other) {
	String *s = new String();	

	int len1 = strlen(inner);
	int len2 = strlen(other.inner);
	int l = len1 + len2;
	
	s->inner = new char[l];
	
	int i = 0;
	for (; i < len1; i++){
		s->inner[i] = inner[i];
	} 
	for (; i < l; i++) {
		s->inner[i] = other.inner[i - len1];
	}
	
	return *s;
}

void String::copy(const char *from, char *to) {
	int len = strlen(from);
	if (to != NULL) {
		delete to;
	}
	to = new char[len];
	for (int i = 0; i < len; i++) {
		to[i] = from[i];
	}
}

#endif // LOL_STRING
