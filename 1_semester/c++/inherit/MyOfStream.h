#ifndef MYOFSTREAM
#define MYOFSTREAM

#include <iostream>
#include <fstream>
#include "Point.h"

using std::ofstream;

class MyOfStream: 
	public ofstream {

	public:
		MyOfStream(const char *path):
			ofstream(path) {};
		~MyOfStream() {};

		MyOfStream& operator<<(const Point& p) {
			(*(ofstream *)this) << p.x 
				<< ":" 
				<< p.y;
			return *this;
		};

};

#endif
