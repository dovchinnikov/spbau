#ifndef HASHMAP_H
#define HASHMAP_H

#include <vector>
#include <list>
#include <utility>
#include "functions.h"

using std::pair;
using std::make_pair;
using std::vector;
using std::list;

template <typename K, typename V, typename H = Hash<K>, typename E = Equals<K>, int buckets_count = 32>
class HashMap {

    typedef list<pair<K, V> > Bucket;
    typedef typename Bucket::iterator BucketIterator;

    public:
        HashMap() {
            buckets.resize(buckets_count);
        }
        ~HashMap(){}

        bool contains(const K & key) {
            Bucket & bucket = findBucket(key);
            BucketIterator bi = findValue(key, bucket);
            return bi != bucket.end();
        }

        V & operator[](const K & key) {
            Bucket & bucket = findBucket(key);
            BucketIterator bi = findValue(key, bucket);
            if (bi == bucket.end()) {
                bucket.push_back(make_pair(key, V()));
                return bucket.back().second;
            }
            return bi->second;
        }

        void add (const K & key, V value) {
            Bucket & bucket = findBucket(key);
            BucketIterator bi = findValue(key, bucket);

            if (bi == bucket.end()) {
                bucket.push_back(make_pair(key, value));
            } else {
                bi->second = value;
            }
        }

        void erase(const K & key) {
            Bucket & bucket = findBucket(key);
            BucketIterator bi = findValue(key, bucket);
            if (bi != bucket.end()) {
                bucket.erase(bi);
            }
        }

    private:
        Bucket & findBucket(const K & key) {
            return buckets[hash(key) % buckets_count];
        }

        BucketIterator findValue(const K & key, Bucket & bucket) const {
            for (BucketIterator it = bucket.begin(); it != bucket.end(); it++) {
                if (equals(it->first, key)) {
                    return it;
                }
            }
            return bucket.end();
        }

        vector<Bucket> buckets;
        H hash;
        E equals;

};

#endif // HASHMAP_H
