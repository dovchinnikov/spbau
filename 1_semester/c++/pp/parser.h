#ifndef PARSER_H
#define PARSER_H

#include <memory>
#include <stack>
#include <iostream>

#include "lexer.h"
#include "exceptions.h"
#include "ast.h"

using std::stack;

class Parser
{
    public:
        Parser(Lexer & lexer) : lexer(lexer) {}
        ProgramPtr parseFile();

    private:
        Lexer & lexer;

        NodesList parseInstructionBlock(bool root = false);
        NodePtr parseFunctionDefinition();

        NodePtr parseInstruction();
        NodePtr parseAssignment();
        NodePtr parseConditionBlock();
        NodePtr parseCommand();
        NodePtr parseRead();
        NodePtr parseReturns();

        ExpressionPtr parseExpression();
        ExpressionPtr parseFunctionCall();

        bool parseEndOfLine();

};

#endif // PARSER_H
