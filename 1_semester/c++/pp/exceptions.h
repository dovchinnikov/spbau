#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include <stdexcept>
#include <sstream>
#include "common.h"

using std::ostringstream;

static const char * ExceptionMessage[] = {
    "division by zero",
    "undefined variable",
    "undefined function",
    "arguments number mismatch for"
};

enum ExceptionType {
    DIVISION_BY_0,
    UNDEFINED_VAR,
    UNDEFINED_FUNC,
    ARGUMENT_MISMATCH
};

class ParseException : public std::runtime_error {

    public:
        ParseException(size_t line_number)
            : std::runtime_error("Parse exception") , line_number(line_number) {}

        const char *what() throw() {
            ostringstream message;
            message << "line " << line_number << ": syntax error";
            return message.str().c_str();
        }

    private:
        size_t line_number;
};

class RuntimeException : public std::runtime_error {

    public:
        RuntimeException(ExceptionType type, size_t line_number, string name = "")
            : std::runtime_error("Undefined reference exception"), type(type), line_number(line_number), var_name(name) {}

        const char *what() throw() {
            ostringstream message;
            message << "Runtime error (line " << line_number << "): " << ExceptionMessage[type] << " '" << var_name << "'";
            return message.str().c_str();
        }

    private:
        ExceptionType type;
        size_t line_number;
        string var_name;
};

#endif // EXCEPTIONS_H
