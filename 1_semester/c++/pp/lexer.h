#ifndef LEXER_H
#define LEXER_H

#include <string>
#include <fstream>
#include <vector>
#include <memory>
#include <queue>
#include "token.h"

using std::string;
using std::ifstream;
using std::shared_ptr;
using std::queue;
using std::deque;

class Lexer {

    public:
        Lexer(ifstream & input) : char_stream(input), line_counter(1) {}
        ~Lexer() {}

        Token next_token();
        Token look_ahead(size_t count = 1);

        size_t get_line() const {
            return line_counter;
        }

    private:
        string read_while(bool (*condition)(char));
        Token read_next_token();

        ifstream & char_stream;
        size_t line_counter;
        deque<Token> token_queue;
};

#endif // LEXER_H
