#include "parser.h"


BinaryOperationPtr getBinaryOperation(Token t) {

    BinaryOperation *o = 0;
    if (t.type == Token::ARITHMETIC_OPERATOR || t.type == Token::COMPARISON_OPERATOR) {
        BinaryOperator op;
        if (t.text == "+") {
            op = BinaryOperator::O_PLUS;
        } else if (t.text == "-") {
            op = BinaryOperator::O_MINUS;
        } else if (t.text == "*") {
            op = BinaryOperator::O_MULT;
        } else if (t.text == "/") {
            op = BinaryOperator::O_DIV;
        } else if (t.text == ">") {
            op = BinaryOperator::CM_GT;
        } else if (t.text == ">=") {
            op = BinaryOperator::CM_GE;
        } else if (t.text == "<=") {
            op = BinaryOperator::CM_LE;
        } else if (t.text == "<") {
            op = BinaryOperator::CM_LT;
        } else if (t.text == "!=") {
            op = BinaryOperator::CM_NEQ;
        } else if (t.text == "==") {
            op = BinaryOperator::CM_EQ;
        }
        o = new BinaryOperation(op);
        o->line_number = t.line;
    }
    return BinaryOperationPtr(o);
}

ProgramPtr Parser::parseFile() {
    ProgramPtr p(new Program());
    p->body = parseInstructionBlock(true);
    return p;
}

NodesList Parser::parseInstructionBlock(bool root) {
    NodesList body;
    while (lexer.look_ahead().type != Token::_EOF && (root || lexer.look_ahead().type != Token::K_END)) {
        while (lexer.look_ahead().type == Token::CRLF) {
            lexer.next_token();
        }
        if (lexer.look_ahead().type == Token::_EOF || lexer.look_ahead().type == Token::K_END) {
            break;
        }

        NodePtr node;
        if (root) {
            node = parseFunctionDefinition();
        }
        if (!node) node = parseInstruction();

        if (node && parseEndOfLine()) {
            body.push_back(node);
        } else {
            throw ParseException(lexer.get_line());
        }
    }
    return body;
}

NodePtr Parser::parseFunctionDefinition() {
    Token t = lexer.look_ahead();                           // check if "def" keyword coming next
    if (t.type != Token::K_DEF) {
        return 0;
    }
    lexer.next_token();                                     // pass "def" keyword

    Token function_name = lexer.next_token();
    if (function_name.type != Token::NAME) {
        throw ParseException(lexer.get_line());
    }
    if (lexer.next_token().type != Token::LEFT_BRACE) {
        throw ParseException(lexer.get_line());
    }

    vector<string> function_params;
    if (lexer.look_ahead().type == Token::RIGHT_BRACE) {    // if next token if ")" then function has 0 params
        lexer.next_token();                                 // pass ")" token
    } else {
        for(;;) {                                           // getting function parameters
            if (lexer.look_ahead().type == Token::NAME) {
                function_params.push_back(lexer.next_token().text);
            } else {
                throw ParseException(lexer.get_line());
            }
            if (lexer.look_ahead().type == Token::COMMA) {  // if "," then next param should be specified
                lexer.next_token();                         // pass "," token
            } else {                                        // if not "," then no params left
                break;                                      // then break cycle
            }
        }
        if (lexer.next_token().type != Token::RIGHT_BRACE) {
            throw ParseException(lexer.get_line());
        }
    }

    if (lexer.next_token().type != Token::COLON || lexer.next_token().type != Token::CRLF) {
        throw ParseException(lexer.get_line());
    }

    NodesList body = parseInstructionBlock();
    if (lexer.next_token().type != Token::K_END) {
        throw ParseException(lexer.get_line());
    }

    FunctionDefinitionPtr function_definition(new FunctionDefinition());
    function_definition->name = function_name.text;
    function_definition->line_number = function_name.line;
    function_definition->params = function_params;
    function_definition->body = body;
    return function_definition;
}

NodePtr Parser::parseInstruction() {
    NodePtr result;
    result = parseAssignment();
    if (!result) result = parseRead();
    if (!result) result = parseCommand();
    if (!result) result = parseConditionBlock();
    if (!result) result = parseExpression();
    return result;
}

NodePtr Parser::parseAssignment() {
    if (lexer.look_ahead().type != Token::NAME || lexer.look_ahead(2).type != Token::ASSIGNMENT) {
        return 0;
    }

    Token variable_token = lexer.next_token();
    lexer.next_token();
    ExpressionPtr expression = parseExpression();
    if (!expression) {
        throw ParseException(lexer.get_line());
    }

    Assignment * assignment = new Assignment();
    assignment->variable.reset(new Variable(variable_token.text));
    assignment->variable->line_number = variable_token.line;
    assignment->expression = expression;
    return NodePtr(assignment);
}

NodePtr Parser::parseRead() {
    if (lexer.look_ahead().type != Token::K_READ) {
        return 0;
    }
    Token read_token = lexer.next_token();

    if (lexer.look_ahead().type != Token::NAME) {
        throw ParseException(lexer.get_line());
    }

    ReadNode *read_command = new ReadNode();
    read_command->var.reset(new Variable(lexer.next_token().text));
    read_command->line_number = read_token.line;
    return NodePtr(read_command);
}

NodePtr Parser::parseCommand() {

    ExpressionCommand * command;
    if (lexer.look_ahead().type == Token::K_PRINT) {
        command = new PrintNode();
    } else if (lexer.look_ahead().type == Token::K_RETURN) {
        command = new ReturnNode();
    } else {
        return 0;
    }
    command->line_number = lexer.next_token().line;

    command->expr = parseExpression();
    if (!command->expr) {
        delete command;
        throw ParseException(lexer.get_line());
    }

    return NodePtr(command);
}

NodePtr Parser::parseConditionBlock() {

    ConditionBlockPtr block;
    if (lexer.look_ahead().type == Token::K_IF) {
        block.reset(new IfBlock());
    } else if (lexer.look_ahead().type == Token::K_WHILE) {
        block.reset(new WhileBlock());
    } else {
        return 0;
    }
    lexer.next_token();                             //pass keyword

    ExpressionPtr condition = parseExpression();
    if (condition) {
        block->condition = condition;
    } else {
        throw ParseException(lexer.get_line());
    }

    if (lexer.next_token().type != Token::COLON
            && lexer.next_token().type != Token::CRLF) {
        throw ParseException(lexer.get_line());
    }

    block->body = parseInstructionBlock();

    if (lexer.next_token().type != Token::K_END && lexer.next_token().type != Token::CRLF) {
        throw ParseException(lexer.get_line());
    }

    return block;
}

ExpressionPtr Parser::parseExpression() {

    stack<BinaryOperationPtr> operation_stack;  //shunting yard algorithm
    queue<ExpressionPtr> output_queue;

    bool exit_condition = false;
    Token::TokenType last_type = Token::UNDEFINED;

    while (!exit_condition) {
        Token::TokenType type = lexer.look_ahead().type;
        switch (type) {
        case Token::NAME:
        {
            ExpressionPtr expression = parseFunctionCall();
            if (!expression) {
                Token variable_token = lexer.next_token();
                expression.reset(new Variable(variable_token.text));
                expression->line_number = variable_token.line;
            }
            output_queue.push(expression);
        }
            break;
        case Token::NUMBER: {
            ExpressionPtr number(new Number(lexer.next_token().text));
            number->line_number = lexer.get_line();
            output_queue.push(number);
        }
            break;
        case Token::ARITHMETIC_OPERATOR:
        case Token::COMPARISON_OPERATOR: {
            BinaryOperationPtr operation = getBinaryOperation(lexer.next_token());
            if (operation->op == BinaryOperator::O_MINUS
                    && (last_type == Token::UNDEFINED || last_type == Token::LEFT_BRACE || last_type == Token::COMPARISON_OPERATOR)) {
                ExpressionPtr number(new Number("0"));   // dummy left operand
                number->line_number = lexer.get_line();
                output_queue.push(number);
                operation->op = BinaryOperator::U_MINUS; // has higher precedence
            }
            while (!operation_stack.empty() && operation_stack.top() && operation_stack.top()->precedence() >= operation->precedence()) {
                output_queue.push(operation_stack.top());
                operation_stack.pop();
            }
            operation_stack.push(operation);
        }
            break;
        case Token::LEFT_BRACE:
            lexer.next_token();
            operation_stack.push(BinaryOperationPtr()); // push dummy pointer marking brace start
            break;
        case Token::RIGHT_BRACE:
            while (!operation_stack.empty() && operation_stack.top()) {
                output_queue.push(operation_stack.top());
                operation_stack.pop();
            }
            if (operation_stack.empty()) {
                exit_condition = true;
            } else {
                lexer.next_token();
                operation_stack.pop();
            }
            break;
        default:
            exit_condition = true;
            break;
        }
        last_type = type;
    }
    while (operation_stack.size() > 0) {
        output_queue.push(operation_stack.top());
        operation_stack.pop();
    }
    // end of shunting yard

    // start building expression subtree
    stack<ExpressionPtr> operand_stack;
    while (output_queue.size() > 0) {
        ExpressionPtr expr = output_queue.front();
        output_queue.pop();

        switch (expr->node_type()) {
        case FUNCTION_CALL:
        case VARIABLE:
        case NUMBER:
            operand_stack.push(expr);
            break;
        case BINARY_OPERATOR: {
            BinaryOperation * operation = (BinaryOperation *)expr.get();
            operation->right = operand_stack.top();
            operand_stack.pop();
            operation->left = operand_stack.top();
            operand_stack.pop();
            operand_stack.push(expr);
        }

        default:
            break;
        }
    }

    if (operand_stack.empty()) {
        return 0;
    } else {
        return operand_stack.top();
    }
}

ExpressionPtr Parser::parseFunctionCall() {
    if (lexer.look_ahead().type != Token::NAME || lexer.look_ahead(2).type != Token::LEFT_BRACE) {
        return 0;
    }
    Token function_name = lexer.next_token();
    lexer.next_token();                                         // pass left brace

    vector<ExpressionPtr> params;
    if (lexer.look_ahead().type == Token::RIGHT_BRACE) {        // if next token if ")" then function has 0 params
        lexer.next_token();                                     // pass ")" token
    } else {
        for(;;) {                                               // getting function parameters
            ExpressionPtr expr = parseExpression();
            if (expr) {
                params.push_back(expr);
            } else {
                throw ParseException(lexer.get_line());
            }
            if (lexer.look_ahead().type == Token::COMMA) {      // if "," then next param should be specified
                lexer.next_token();                             // pass "," token
            } else {                                            // if not "," then no params left
                break;                                          // then break cycle
            }
        }
        if (lexer.next_token().type != Token::RIGHT_BRACE) {
            throw ParseException(lexer.get_line());
        }
    }

    FunctionCall * function_call = new FunctionCall;
    function_call->name = function_name.text;
    function_call->line_number = function_name.line;
    function_call->params = params;
    return ExpressionPtr(function_call);
}


bool Parser::parseEndOfLine() {
    return lexer.next_token().type == Token::CRLF || lexer.next_token().type == Token::_EOF;
}
