#ifndef VISITOR_H
#define VISITOR_H

#include "ast.h"

class Visitor
{
public:
    Visitor() {}
    virtual ~Visitor() {}

    virtual void visit(Program &) = 0;
    virtual void visit(FunctionDefinition &) = 0;

    virtual void visit(Number &) = 0;
    virtual void visit(Variable &) = 0;
    virtual void visit(FunctionCall &) = 0;
    virtual void visit(Assignment &) = 0;
    virtual void visit(BinaryOperation &) = 0;

    virtual void visit(ReadNode &) = 0;
    virtual void visit(PrintNode &) = 0;
    virtual void visit(ReturnNode &) = 0;

    virtual void visit(IfBlock &) = 0;
    virtual void visit(WhileBlock &) = 0;
};

#endif // VISITOR_H
