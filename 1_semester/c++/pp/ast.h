#ifndef AST_H
#define AST_H

#include <memory>
#include <vector>
#include <map>
#include "common.h"
#include "exceptions.h"
#include "token.h"

using std::shared_ptr;
using std::vector;
using std::map;

struct Node;
struct FunctionDefinition;
struct BlockNode;
struct ConditionBlock;
struct Program;
struct Expression;
struct Variable;
struct BinaryOperation;

typedef shared_ptr<Node>                    NodePtr;
typedef vector<NodePtr>                     NodesList;
typedef shared_ptr<FunctionDefinition>      FunctionDefinitionPtr;
typedef map<string, NodePtr>  FunctionMap;
typedef shared_ptr<BlockNode>               BlockNodePtr;
typedef shared_ptr<ConditionBlock>          ConditionBlockPtr;
typedef shared_ptr<Program>                 ProgramPtr;
typedef shared_ptr<Expression>              ExpressionPtr;
typedef shared_ptr<Variable>                VariablePtr;
typedef shared_ptr<BinaryOperation>         BinaryOperationPtr;


enum NodeType {
    FUNCTIONN_DEFINITION,
    VARIABLE,
    NUMBER,
    FUNCTION_CALL,
    BINARY_OPERATOR,
    GENERAL_NODE
};

struct Node {
    virtual ~Node(){}
    virtual void visit(VisitorPtr) = 0;
    virtual NodeType node_type(){
        return GENERAL_NODE;
    }
    size_t line_number;
};

struct ReadNode : Node {
    VariablePtr var;
    void visit(VisitorPtr);
};

struct ExpressionCommand : Node {
    ExpressionPtr expr;
};

struct PrintNode : ExpressionCommand {
    void visit(VisitorPtr);
};

struct ReturnNode : ExpressionCommand {
    void visit(VisitorPtr);
};

struct BlockNode : Node {
    BlockNode(NodesList const & body) : body(body) {}
    NodesList body;
};

struct FunctionDefinition : BlockNode {

    FunctionDefinition(string const & name = "", vector<string> const & params = vector<string>(), NodesList const & body = NodesList())
        : BlockNode(body), name(name), params(params) {}
    void visit(VisitorPtr);
    NodeType node_type() {
        return FUNCTIONN_DEFINITION;
    }
    string name;
    vector<string> params;
};

struct ConditionBlock : BlockNode {

    ConditionBlock(ExpressionPtr condition = 0, NodesList const & body = NodesList())
        : BlockNode(body), condition(condition) {}
    ExpressionPtr condition;
};

struct WhileBlock : ConditionBlock {
    void visit(VisitorPtr);
};

struct IfBlock : ConditionBlock {
    void visit(VisitorPtr);
};

struct Program : BlockNode {

    Program(NodesList const & body = NodesList())
        : BlockNode(body) {}
    void visit(VisitorPtr);
};

struct Assignment : Node {

    Assignment(VariablePtr l_value = 0, ExpressionPtr expression = 0)
        : variable(l_value), expression(expression) {}
    void visit(VisitorPtr);
    VariablePtr variable;
    ExpressionPtr expression;
};


struct Expression : Node {};

struct Number : Expression {

    Number(string const & number) {
        val = StringToNumber<long long>(number);
    }
    void visit(VisitorPtr);
    NodeType node_type() {
        return NUMBER;
    }
    long long val;
};

struct Variable : Expression {

    Variable(string const & var)
        : name(var) {}
    void visit(VisitorPtr);
    NodeType node_type() {
        return VARIABLE;
    }
    string name;
};

struct FunctionCall : Expression {

    void visit(VisitorPtr);
    NodeType node_type() {
        return FUNCTION_CALL;
    }
    string name;
    vector<ExpressionPtr> params;
};

enum BinaryOperator {
    O_PLUS,         // +
    O_MINUS,        // -
    O_DIV,          // /
    O_MULT,         // *
    CM_EQ,          // ==
    CM_NEQ,         // !=
    CM_LT,          // <
    CM_LE,          // <=
    CM_GT,          // >
    CM_GE,          // >=
    U_MINUS         // so-called "unary" minus
};

struct BinaryOperation : Expression {

    BinaryOperation(BinaryOperator op, ExpressionPtr left = 0, ExpressionPtr right = 0)
        : op(op), left(left), right(right) {}

    void visit(VisitorPtr);
    NodeType node_type() {
        return BINARY_OPERATOR;
    }
    int precedence();
    ValueType apply(ValueType const & left, ValueType const & right);

    BinaryOperator op;
    ExpressionPtr left;
    ExpressionPtr right;
};

#endif // AST_H
