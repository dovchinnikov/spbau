#ifndef EVALVISITOR_H
#define EVALVISITOR_H

#include <map>
#include <string>
#include <iostream>
#include <stack>
#include <vector>
#include <memory>
#include "visitor.h"
#include "exceptions.h"

using std::map;
using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::stack;
using std::vector;
using std::shared_ptr;

typedef map<string, long long> VarMap;
typedef vector<VarMap> VarStack;
typedef shared_ptr<long long> ValRef;

class EvalVisitor : public Visitor {

    public:
        EvalVisitor() {}

        void visit(Program &);
        void visit(FunctionDefinition &);

        void visit(Number &);
        void visit(Variable &);
        void visit(FunctionCall &);
        void visit(Assignment &);
        void visit(BinaryOperation &);

        void visit(ReadNode &);
        void visit(PrintNode &);
        void visit(ReturnNode &);

        void visit(IfBlock &);
        void visit(WhileBlock &);

    private:
        ValRef variable_lookup(string name);
        void set_variable(string name, long long value);

        FunctionMap function_map;
        VarStack variable_map_stack;
        stack<ValueType> operand_stack;
        stack<ValueType> return_stack;
        vector<ExpressionPtr> function_call_params;
};

#endif // EVALVISITOR_H
