#include "lexer.h"

bool isWhiteSpace(char c) {
    return c == ' ' || c == '\t';
}

bool isDigit(char c) {
    return c >= '0' && c <= '9';
}

bool isString(char c) {
    static bool first_char = true;
    bool result = first_char ?
                ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) :
                ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '_');
    first_char = result ? 0 : 1;
    return result;
}

bool isCRLF(char c) {
    return c == '\n';
}

bool isNotCRLF(char c) {
    return !isCRLF(c);
}

string Lexer::read_while(bool (*condition)(char)){
    char c = char_stream.peek();

    string token;
    while (condition(c)) {
        token += char_stream.get();
        c = char_stream.peek();
    }
    return token;
}

Token Lexer::next_token() {
    if (token_queue.size() > 0) {
        Token t = token_queue.front();
        token_queue.pop_front();
        return t;
    }
    return read_next_token();
}

Token Lexer::look_ahead(size_t count) {
    while (token_queue.size() < count) {
        token_queue.push_back(read_next_token());
    }
    return token_queue[count - 1];
}


Token Lexer::read_next_token() {
    Token::TokenType type = Token::UNDEFINED;
    string token;

    read_while(isWhiteSpace);            // skip whitespaces

    token = read_while(isString);        // keyword or variable(function) name
    if (token.length() > 0) {
        if (token == "end") {
            type = Token::K_END;
        } else if (token == "if") {
            type = Token::K_IF;
        } else if (token == "def") {
            type = Token::K_DEF;
        } else if (token == "while") {
            type = Token::K_WHILE;
        } else if (token == "print") {
            type = Token::K_PRINT;
        } else if (token == "read") {
            type = Token::K_READ;
        } else if (token == "return") {
            type = Token::K_RETURN;
        } else {
            type = Token::NAME;
        }
    }

    if (type == Token::UNDEFINED) {
        token = read_while(isCRLF);
        if (token.length() > 0) {
            line_counter += token.length();
            type = Token::CRLF;
        }
    }

    if (type == Token::UNDEFINED) {             // it was not keyword or name
        token = read_while(isDigit);            // number
        if (token.length() > 0) {
            type = Token::NUMBER;
        }
    }

    if (type == Token::UNDEFINED) {             // it was not keyword or name or number
        char c = char_stream.get();
        token = c;

        switch (c) {
            case '#':
                read_while(isNotCRLF);          // skip until end of line
                return read_next_token();       // return next token recursively
            case '=':
                if (char_stream.peek() == '=') {
                    token += char_stream.get();
                    type = Token::COMPARISON_OPERATOR;
                } else {
                    type = Token::ASSIGNMENT;
                }
                break;
            case '<':
            case '>':
                if (char_stream.peek() == '=') {
                    token += char_stream.get();
                }
                type = Token::COMPARISON_OPERATOR;
                break;
            case '!':
                if (char_stream.peek() == '=') {
                    token += char_stream.get();
                    type = Token::COMPARISON_OPERATOR;
                }
                break;
            case '(':
                type = Token::LEFT_BRACE;
                break;
            case ')':
                type = Token::RIGHT_BRACE;
                break;
            case ':':
                type = Token::COLON;
                break;
            case ',':
                type = Token::COMMA;
                break;
            case '+':
            case '-':
            case '*':
            case '/':
                type = Token::ARITHMETIC_OPERATOR;
                break;
            case EOF:
                type = Token::_EOF;
                break;
            default:
                type = Token::UNDEFINED;
        }
    }

    return Token(type, token, line_counter);
}
