#include "evalvisitor.h"

void EvalVisitor::visit(Program & p) {
    variable_map_stack.push_back(VarMap());

    for (NodesList::iterator it = p.body.begin(); it != p.body.end();) {
        if ((*it)->node_type() == FUNCTIONN_DEFINITION) {
            string function_name = ((FunctionDefinition *)it->get())->name;
            function_map[function_name] = *it;
            it = p.body.erase(it);
        } else {
            it++;
        }
    }

    for (NodesList::iterator it = p.body.begin(); it != p.body.end(); ++it) {
        (*it)->visit(this);
    }
}


void EvalVisitor::visit(FunctionDefinition & function) {

    VarMap locals;
    int i = 0;
    for (vector<ExpressionPtr>::iterator it = function_call_params.begin(); it != function_call_params.end(); ++it, ++i) {
        (*it)->visit(this);
        locals[function.params[i]] = operand_stack.top();
        operand_stack.pop();
    }
    variable_map_stack.push_back(locals);

    size_t return_stack_size = return_stack.size();
    for (NodesList::iterator it = function.body.begin(); return_stack_size == return_stack.size() && it != function.body.end(); ++it) {
        (*it)->visit(this);
    }

    if (return_stack_size == return_stack.size()) {
        operand_stack.push(0);
    } else {
        operand_stack.push(return_stack.top());
        return_stack.pop();
    }

    variable_map_stack.pop_back();
}

void EvalVisitor::visit(Number & number) {
    operand_stack.push(number.val);
}

void EvalVisitor::visit(Variable & variable) {
    ValRef val = variable_lookup(variable.name);
    if (val) {
        operand_stack.push(*val);
    } else {
        throw RuntimeException(UNDEFINED_VAR, variable.line_number, variable.name);
    }
}

void EvalVisitor::visit(FunctionCall & function_call) {
    NodePtr function_definition_node = function_map[function_call.name];
    if (!function_definition_node) {
        throw RuntimeException(UNDEFINED_FUNC, function_call.line_number, function_call.name);
    }
    FunctionDefinition *function_definition = (FunctionDefinition *)function_definition_node.get();
    if (function_definition->params.size() != function_call.params.size()) {
        throw RuntimeException(ARGUMENT_MISMATCH, function_call.line_number, function_call.name);
    }
    function_call_params = function_call.params;
    function_definition_node->visit(this);
}

void EvalVisitor::visit(Assignment & assignment) {
    assignment.expression->visit(this);
    set_variable(assignment.variable->name, operand_stack.top());
    operand_stack.pop();
}

void EvalVisitor::visit(BinaryOperation & op) {
    op.left->visit(this);
    op.right->visit(this);
    long long right = operand_stack.top();
    operand_stack.pop();
    long long left = operand_stack.top();
    operand_stack.pop();
    operand_stack.push(op.apply(left, right));
}

void EvalVisitor::visit(ReadNode & command) {
    long long new_value;
    cin >> new_value;
    set_variable(command.var->name, new_value);
}

void EvalVisitor::visit(PrintNode & command) {
    command.expr->visit(this);
    cout << operand_stack.top() << endl;
    operand_stack.pop();
}

void EvalVisitor::visit(ReturnNode & return_) {
    return_.expr->visit(this);
    return_stack.push(operand_stack.top());
    operand_stack.pop();
}

void EvalVisitor::visit(IfBlock & block) {
    block.condition->visit(this);
    if (operand_stack.top() == 1) {
        for (NodesList::iterator it = block.body.begin(); it != block.body.end(); it++) {
            (*it)->visit(this);
        }
    }
    operand_stack.pop();
}

void EvalVisitor::visit(WhileBlock & block){
    block.condition->visit(this);
    while(operand_stack.top() == 1) {
        for (NodesList::iterator it = block.body.begin(); it != block.body.end(); it++) {
            (*it)->visit(this);
        }
        operand_stack.pop();
        block.condition->visit(this);
    }
    operand_stack.pop();
}

ValRef EvalVisitor::variable_lookup(string name) {
    ValRef value;
    for (VarStack::reverse_iterator it = variable_map_stack.rbegin(); it != variable_map_stack.rend(); ++it) {
        if (it->find(name) != it->end()){
            value.reset(new long long((*it)[name]));
            break;
        }
    }
    return value;
}

void EvalVisitor::set_variable(string name, long long value) {
    variable_map_stack.back()[name] = value;
}
