#include <iostream>
#include <string>
#include <memory.h>

#include "debug.h"
#include "common.h"
#include "lexer.h"
#include "parser.h"
#include "evalvisitor.h"

int main(const int argc, const char * argv[]) {

    if (argc < 2) {
        cout << "Please specify filename" << endl;
        return 0;
    }

    ifstream source_file(argv[1]);
    if (!source_file.good()) {
        cout << "Cannot open " << argv[1] << endl;
        return 0;
    }

    try {
        Lexer lexer(source_file);
        Parser parser(lexer);
        ProgramPtr program = parser.parseFile();
        std::unique_ptr<Visitor> visitor(new EvalVisitor()); // to free resources in case of exception
        program->visit(visitor.get());
    } catch (ParseException e) {
        cout << e.what() << endl;
    } catch (RuntimeException e) {
        cout << e.what() << endl;
    }

    return 0;
}
