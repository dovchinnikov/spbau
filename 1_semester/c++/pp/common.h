#ifndef COMMON_H
#define COMMON_H

#include <sstream>
#include "token.h"

using std::stringstream;

class Visitor;
typedef Visitor *VisitorPtr;
typedef long long ValueType;

template <typename T>
inline T StringToNumber (const string & text) {
    stringstream ss(text);
    T result;
    return ss >> result ? result : 0;
}

#endif // COMMON_H
