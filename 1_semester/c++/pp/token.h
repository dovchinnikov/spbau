#ifndef TOKEN_H
#define TOKEN_H

#include <string>

using std::string;

struct Token {

    enum TokenType {
        NUMBER,
        NAME,

        K_DEF,          // keywords
        K_IF,           //
        K_WHILE,        //
        K_END,          //
        K_RETURN,       //
        K_PRINT,        //
        K_READ,         //
        COLON,          // :
        COMMA,          // ,
        ASSIGNMENT,     // =
        COMPARISON_OPERATOR,
        ARITHMETIC_OPERATOR,
        LEFT_BRACE,     // (
        RIGHT_BRACE,    // )
        CRLF,           // \n
        UNDEFINED,
        _EOF
    };

    TokenType type;
    string text;
    size_t line;

    Token(TokenType type = UNDEFINED, string text = "", size_t line = 0)
        : type(type), text(text), line(line) {}

};

#endif // TOKEN_H
