#ifndef DEBUG_H
#define DEBUG_H

#include <iostream>
#include "token.h"
#include "ast.h"

using std::ostream;
using std::endl;

inline ostream& operator<<(ostream& _out, const Token &token) {
    _out << token.type << ":\t" << token.text << endl;
    return _out;
}
/*
inline ostream & operator<<(ostream & out, FunctionMap & map) {
    out << "Map size: " << map.size() << endl;
    for (FunctionMap::iterator it = map.begin(); it != map.end(); ++it) {
        out << it->first << " has " << it->second->params.size() << " params" << endl;
    }
    return out;
}
*/

#endif // DEBUG_H
