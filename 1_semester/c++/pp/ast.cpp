#include "ast.h"
#include "visitor.h"

void Program::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

// Expression derivatives
void Number::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void Variable::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void FunctionCall::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void Assignment::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void BinaryOperation::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

int BinaryOperation::precedence() {
    switch(op) {
        case O_PLUS:
        case O_MINUS:
            return 3;
        case O_DIV:
        case O_MULT:
            return 4;
        case CM_EQ:
        case CM_NEQ:
            return 0;
        case CM_LT:
        case CM_LE:
        case CM_GT:
        case CM_GE:
            return 2;
        case U_MINUS:
            return 6;
        default:
            return 2;
    }
}

ValueType BinaryOperation::apply(const ValueType &left, const ValueType &right) {
    switch(op) {
        case O_PLUS:
            return left + right;
        case O_MINUS:
        case U_MINUS:
            return left - right;
        case O_DIV:
            if (right == 0) {
                throw RuntimeException(DIVISION_BY_0, line_number);
            }
            return left / right;
        case O_MULT:
            return left * right;
        case CM_EQ:
            return left == right;
        case CM_NEQ:
            return left != right;
        case CM_LT:
            return left < right;
        case CM_LE:
            return left <= right;
        case CM_GT:
            return left > right;
        case CM_GE:
            return left >= right;
        default:
            return left;
    }
}

//Nodes
void FunctionDefinition::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void PrintNode::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void ReadNode::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void ReturnNode::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void IfBlock::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}

void WhileBlock::visit(VisitorPtr visitor) {
    visitor->visit(*this);
}
