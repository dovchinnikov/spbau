#include <iostream>
#include <expat.h>
#include <fstream>
using namespace std;
#define BUF_SIZE 4096
#include <cstring>

#include <vector>

const string TAG_ITEM("item");
const string TAG_TITLE("title");
const string TAG_DESCRIPTION("description");

struct Item {
    string title;
    string description;
};

typedef string Item::* ItemField;

struct UserData {
    vector<Item *> items;
    Item *currentItem;
    ItemField *currentTag;
};

template<typename T> ostream& operator<<(ostream & out, vector<T> & v) {
    for (typename vector<T>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << " ";
    }
    return out;
}

void onElementStart(void *userData, const XML_Char *name, const XML_Char **atts){
    string tagName(name);
    UserData *data = (UserData *)userData;

    if (tagName == TAG_ITEM) {
        data->currentItem = new Item();
    } else if (tagName == TAG_TITLE) {
        data->currentTag = (ItemField*)(& &Item::title);
    } if (tagName == TAG_DESCRIPTION) {
        data->currentTag = &Item::description;
    }
}

void onTagBody(void *userData, const XML_Char *s, int len) {
    UserData *data = (UserData *)userData;
    string body(s, len);
    if (data->currentItem && data->currentTag) {
        data->currentItem->*(data->currentTag) = body;
    }
}

void onElementEnd(void *userData, const XML_Char *name) {
    UserData *data = (UserData *)userData;
    string tagName(name);
    if (tagName == TAG_ITEM) {
        data->items.push_back(data->currentItem);
        data->currentItem = NULL;
    }
}

int main()
{
    XML_Parser parser = XML_ParserCreate(NULL);
    XML_SetElementHandler(parser, &onElementStart, &onElementEnd);
    XML_SetCharacterDataHandler(parser, &onTagBody);
    UserData data;
    XML_SetUserData(parser, &data);
    char *buffer = (char *)XML_GetBuffer(parser, BUF_SIZE);
    ifstream filestream("rss.xml");
    while (filestream.good()){
        filestream.read(buffer, BUF_SIZE);
        XML_ParseBuffer(parser, BUF_SIZE, !filestream.good());
    }
    Item *a = new Item();
    a->description = "asda";
    a->title = "sdfgs";
    data.items.push_back(a);
//    data.items.push_back(&(Item){string("dsa"),string("dsa")});
    cout << data.items << endl;
    return 0;
}

