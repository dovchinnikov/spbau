#include "encoder.h"

void resetInputSream(ifstream & stream) {
    stream.clear();
    stream.seekg(0, ios::beg);
}

long long *getFrequencies(ifstream & input) {
    resetInputSream(input);

    long long *freqs = new long long[ALL_BYTES]();
    while (true) {
        byte b = (byte)input.get();
        if (input.good()) {
            freqs[b]++;
        } else {
            break;
        }
    }

    return freqs;
}

Node *buildTree(long long *freqs) {
    Node * root = 0;

    priority_queue<Node *, vector<Node *>, NodeComparator> q;
    for (int i = 0; i < ALL_BYTES; i++) {
        long long freq = freqs[i];
        if (freq) {
            q.push(new Node(i, freq));
        }
    }

    if (q.size() == 1) {
        Node *a = q.top();
        q.pop();
        Node *c = new Node();
        c->left = a;
        q.push(c);
    }

    while (q.size() > 1) {
        Node *a = q.top();
        q.pop();
        Node *b = q.top();
        q.pop();
        Node *c = new Node(a, b);
        q.push(c);
    }

    if (q.size() == 1) {
        root = q.top();
    } else {
        root = 0;
    }

    return root;
}

void buildLengths(const Node *root, const int level, LengthBytesMap & mp) {
    if (!root) {
        return;
    }

    buildLengths(root->left, level + 1, mp);
    buildLengths(root->right, level + 1, mp);

    if (!root->right && !root->left) {
        if (mp.find(level) == mp.end()) {
            mp[level] = vector<byte>();
        }
        mp[level].push_back(root->vbyte);
    }
}

long long getFilesize(ifstream & file) {
    resetInputSream(file);
    file.seekg(0, ios::end);
    return file.tellg();
}

void writeHeader(ofstream & out_stream, Code *code_map, long long original_filesize) {
    byte len = 0;
    for (int c = 0; c < ALL_BYTES; c++) {
        len = code_map[c].len;
        out_stream << len;
    }
    out_stream.write((const char *)&original_filesize, sizeof(original_filesize));
}

void writeData(ifstream & input_stream, ofstream & out_stream, Code *code_map) {

    resetInputSream(input_stream);

    byte buf            = 0;
    int offset          = 0;
    byte last_read_byte = 0;

    while (true) {
        last_read_byte = (byte)input_stream.get();
        if (!input_stream.good()) {
            break;
        }

        Code code = code_map[last_read_byte];
        for (int i = 0; i < code.len; i++) {
            buf = (buf & ~(1 << offset)) | (code[i] << offset);
            offset++;
            if (offset == CHAR_BIT) {
                out_stream << *(const char *)&buf;
                buf = 0;
                offset = 0;
            }
        }
    }

    if (offset > 0) {
        out_stream << *(const char *)&buf;
    }
}

void Encoder::encode(ifstream & input_stream, ofstream & out_stream) {

    long long *freqs = getFrequencies(input_stream);

    Node *root = buildTree(freqs);
    delete[] freqs;

    LengthBytesMap *lbm = new LengthBytesMap();
    buildLengths(root, 0, *lbm);
    delete root;

    Code * code_map = Coder::getCodeMap(*lbm);
    delete lbm;

    long long filesize = getFilesize(input_stream);

    writeHeader(out_stream, code_map, filesize);
    writeData(input_stream, out_stream, code_map);

    delete code_map;
}


