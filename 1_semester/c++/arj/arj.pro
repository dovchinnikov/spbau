TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    treedecoder.cpp \
    encoder.cpp \
    decoder.cpp \
    coder.cpp

HEADERS += \
    treedecoder.h \
    encoder.h \
    decoder.h \
    common.h \
    coder.h

