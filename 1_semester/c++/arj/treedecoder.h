#ifndef TREEDECODER_H
#define TREEDECODER_H

#include "common.h"

class TreeDecoder {

    public:
        TreeDecoder(Node *tree):root(tree), current(tree) {}
        void passBit(bool);
        bool needMoreBits();
        byte getByte();

    private:
        Node *root;
        Node *current;
};

#endif // TREEDECODER_H
