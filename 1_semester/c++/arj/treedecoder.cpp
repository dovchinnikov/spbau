#include "treedecoder.h"

void TreeDecoder::passBit(bool bit) {
    if (bit) {
        current = current->right;
    } else {
        current = current->left;
    }
}

bool TreeDecoder::needMoreBits() {
    return (current->left != 0) && (current->right != 0);
}

byte TreeDecoder::getByte() {
    byte result = current->vbyte;
    current = root;
    return result;
}
