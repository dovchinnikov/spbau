#ifndef CODER_H
#define CODER_H

#include <algorithm>
#include "common.h"

using std::sort;

class Coder {

    public:
        static Code *getCodeMap(LengthBytesMap &);
        static Node *buildTree(LengthBytesMap &);
};

#endif // CODER_H
