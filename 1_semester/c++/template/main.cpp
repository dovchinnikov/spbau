#include <iostream>
#include "myvector.h"

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    MyVector<int> bools;
    bools.push_back(true);
    bools.push_back(false);
    bools.push_back(true);
    bools.push_back(true);
    bools.push_back(true);
    bools.push_back(false);
    bools.push_back(true);
    bools.push_back(true);
    cout << bools << endl;
    bools[6] = false;
    cout << bools << endl;
    bools.pop_back();
    cout << bools << endl;
    bools.pop_back();
    cout << bools << endl;
    bools.push_back(true);
    cout << bools << endl;
    bools.push_back(false);
    cout << bools << endl;
    bools[bools.size() -1 ] = true;
    cout << bools << endl;

    return 0;
}

