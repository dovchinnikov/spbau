#ifndef MYVECTOR_H
#define MYVECTOR_H

typedef unsigned char CONTAINTER_TYPE;
#define BITS sizeof(CONTAINTER_TYPE)*8

#include <iostream>
#include <vector>

using std::ostream;
using std::vector;

template<class T> class MyVector {};

template <>
class MyVector<bool>
{
    struct Internal {
        int index;
        MyVector<bool> ref;

        Internal(int index, MyVector<bool> & v) : index(index), ref(v) {
        }

        operator bool() {
            return ref.internal_container[index/BITS] & (1 << index % BITS);
        }

        void operator=(const bool value) {
            int element_number = index / BITS;
            int bit_number = index % BITS;
            ref.internal_container[element_number] =
                    (ref.internal_container[element_number] & ~(1 << bit_number)) | (value << bit_number);
        }
    };

    public:
        MyVector(): internal_container(vector<CONTAINTER_TYPE>()), offset(0) {
        }

        ~MyVector() {
            delete[] internal_container;
        }

        Internal operator[] (const int index) {
            return Internal(index, this);
        }

        void push_back(const bool value){
            Internal(offset++, this) = value;
        }

        void pop_back() {
            offset--;
        }

        int size() {
            return offset;
        }

    private:
        vector<CONTAINTER_TYPE> internal_container;
        int offset;

};


template <typename T> ostream & operator <<(ostream & out_, MyVector<T> &v){
    for (int i = 0; i < v.size(); i++) {
        out_ << v[i];
    }
    return out_;
}

#endif // MYVECTOR_H
