#include <cstdio>
#include <iostream>

/*function mergeSort(array) {

	function merge(a, b){
		var c = [];
		var i=0, j=0;
		while (i < a.length && j < b.length){
			c.push(a[i] < b[j] ? a[i++] : b[j++]);
		}
		while (i < a.length){
			c.push(a[i++]);
		}
		while (j < b.length){
			c.push(b[j++]);
		}
		return c;
	}

	if (array.length < 2){
		return array;
	} else {
		return merge(mergeSort(array.splice(0, array.length / 2)), mergeSort(array));
	}
}*/

using namespace std;

void printArray(int *a, int *end) {
	for (int *i = a; i != end; ++i){
		cout << *i << ' ';
	}
	cout << '\n';
}

int *merge(int *startA, int *endA, int *startB, int *endB, int *out) {
	int *p = startA,
        *q = startB,
        *o = out;
	while (p != endA && q != endB) {
		*(o++) = (*p < *q)? *(p++) : *(q++);
	}
	if (p == endA) {
		while (q != endB) {
			*(o++) = *(q++);
		}
	} else if (q == endB) {
		while (p != endA) {
			*(o++) = *(p++);
		}
	}

    p = startA;
	o = out;

	while (p != endB) {
        *(p++) = *(o++);
	}
	return startA;
}

int *mergeSort(int *start, int *end, int *out) {
//	cout <<  "Elements count: " << end - start << endl;
	if (end - start < 2) {
		return start;
	} else {
		int shift = (end - start)/2;
		int *half = start + shift;
		int *halfOut = out + shift;
//		cout << "Shift: " << shift << endl;
		return merge(mergeSort(start, half, out),
						half,
						mergeSort(half, end, halfOut),
						end,
						out);
	}
}

int main() {
	int a[100];
	int out[100];
	int count = 0;

	int var;
	cin >> var;
	while (count < 100 && var != 0) {
		*(a+(count++)) = var;
		cin >> var;
	};
	printArray(a, a + count);

	mergeSort(a, a + count, out);
	printArray(out, out + count);

    cin >> var;
	return 0;
}
