#include <iostream>
#include <ctime>

#define N 20000

using namespace std;

//  N-1 memory allocations
int **a1(int n) {
    int **a = new int *[n];
    for (int **p = a; p != a + n; p++) {
        *p = new int[n];
        for (int *pp = *p; pp!= *p + n; pp++) {
            *pp = 0;
        }
    }
    return a;
}

//  1 memory allocation
int *a2(int n){
    int *a = new int[n*n];
    for (int *p = a; p != a + n*n; p++) {
        *p = 0;
    }
    return a;
}

//  2 memory allocations
int **a3(int n){
    int **a = new int *[n];
    int *values = new int[n*n];
    for (int **p = a; p != a + n; p++) {
        *p = values + n*(p - a) ;
    }
    for (int *p = values; p != values + n*n; p++) {
        *p = 0;
    }
    return a;
}

int main() {
    clock_t t = clock();
    a1(N);
    cout << "1: " << clock() - t << endl;
    t = clock();
    a2(N);
    cout << "2: " << clock() - t << endl;
    t = clock();
    a3(N);
    cout << "3: " << clock() - t << endl;
    return 0;
}
