#include <cstdio>

void foo()
{
	printf("OWNED\n");
}

int bar()
{
	void *p = (void *)&foo;
	void *m[10];
	for (int i = 0; i<12; i++)
	{
		m[i]=p;
	}
	return 1;
}

int main()
{
	bar();
	return 0;
}
