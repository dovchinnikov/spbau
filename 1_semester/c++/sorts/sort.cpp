#include <cstdio>
#include <iostream>

using namespace std;

void printArray(int *a, int *end) {
	for (int *i = a; i != end; ++i){
		cout << *i << ' ';
	}
	cout << '\n';
}

void sort(int *a, int *end) {
	for (int *i = end; i != a; --i) {
		for (int *j = a; j != i; ++j) {
			if (*j > *(j+1)) {
				int tmp = *j;
				*j = *(j+1);
				*(j+1) = tmp;
			}
		}
	}
}

int main() {
	int a[100];
	int count = 0; 
	int var;
	
	cin >> var;
	while (count < 100 && var != 0) {		
		*(a+(count++)) = var;	
		cin >> var;
	};		
	printArray(a, a + count);
	
	sort(a, a + count);
	printArray(a, a + count);

	return 0;
}
