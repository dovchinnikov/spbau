#include <cstdio>
#include <iostream>

/*function mergeSort(array) {

	function merge(a, b){
		var c = [];
		var i=0, j=0;
		while (i < a.length && j < b.length){
			c.push(a[i] < b[j] ? a[i++] : b[j++]);
		}
		while (i < a.length){
			c.push(a[i++]);
		}
		while (j < b.length){
			c.push(b[j++]);
		}
		return c;
	}

	if (array.length < 2){
		return array;
	} else {
		return merge(mergeSort(array.splice(0, array.length / 2)), mergeSort(array));
	}
}*/

using namespace std;

void printArray(int *a, int len)
{
    int *t = a;
    while (t != a+len)
    {
        cout << *t << ' ';
    }
    cout << '\n';
}

int *merge(int *a, int lenA, int *b, int lenB, int *out)
{
    int *p = a,
         *q = b,
          *o = out,
           *endA = a + lenA,
           *endB = b + lenB;
    while (p != endA && q != endB)
    {
        *(o++) = (*p < *q)? *(p++) : *(q++);
    }
    while (q != endB)
    {
        *(o++) = *(q++);
    }
    while (p != endA)
    {
        *(o++) = *(p++);
    }

    p = a;
    o = out;
    while (p != endB)
    {
        *(p++) = *(o++);
    }
    return a;
}

int *mergeSort(int *start, int len, int *out)
{
    if (len < 2)
    {
        return start;
    }
    else
    {
        int shift = len/2;
        int *half = start + shift;
        int *halfOut = out + shift;
        return merge(mergeSort(start, shift, out),
                     shift,
                     mergeSort(start + shift, len - shift, out + shift),
                     len - shift,
                     out);
    }
}

int main()
{
    int a[100];
    int out[100];
    int count = 0;

    int var;
    cin >> var;
    while (count < 100 && var != 0)
    {
        *(a+(count++)) = var;
        cin >> var;
    };
    printArray(a, count);

    mergeSort(a, count, out);
    printArray(out, count);

    return 0;
}
