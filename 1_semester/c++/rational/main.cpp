#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>

#include "Rational.h"

using std::ifstream;
using std::ios;
using std::vector;
using std::cout;
using std::endl;
using std::map;
using std::sort;
using std::ostream;
using std::streamsize;



int main(int len, const char* argv[]) {
	
	Rational r1(10, 13);
	cout << r1 << endl;
	Rational r2(15, 25);
	cout << r2 << endl;
	r1 += r2;
	cout << r1 << endl;
	
	return 0;
}
