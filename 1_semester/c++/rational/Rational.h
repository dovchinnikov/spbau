#include <iostream>

using std::cout;
using std::endl;
using std::ostream;

class Rational {

	friend ostream & operator <<(ostream & _cout, Rational & r);
	public:
		Rational(Rational const & r) {
			num = r.num;
			denum = r.denum;
		}
		Rational(int x, int y): num(x), denum(y) {
			reduce();
		}
		~Rational(){};

		Rational & operator=(Rational &r) {
			return *(new Rational(r));
		}
		Rational & operator+=(Rational const & r) {
			num = (num * r.denum + denum * r.num);
			denum *= r.denum;
			reduce();
			return *this;
		}
		Rational & operator*=(Rational const & r) {
			num *= r.num;
			denum *= denum;
			reduce();
			return *this;
		}
		Rational operator-() const {
			return Rational(-num, denum);
		}
		Rational & operator-=(Rational const & r) {
			return (*this) += -r;
		}


	private:
		int num;
		int denum;
		void reduce() {
			int delim = gcd(num, denum);
			num /= delim;
			denum /= delim;
		}
		int gcd(int a, int b) {
			a = (a>0) ? a: -a;
			b = (b>0) ? b: -b;		
			while (a*b > 0)	{
				if (a > b) {
					a%=b;
				} else {
					b%=a;
				}
			}
			return a+b;
		}

};

ostream & operator<<(ostream & _cout, Rational & r) {
	_cout << r.num << "/" << r.denum;
}

Rational operator+(Rational a, Rational const & b) {
	return a += b;
}

Rational & operator*=(Rational & r, int a) {
	Rational n(a, 1);
	return r *= n;
}

