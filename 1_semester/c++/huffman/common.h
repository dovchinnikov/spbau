#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <vector>
#include <map>
#include <limits.h>

#define ALL_BYTES UCHAR_MAX + 1

using std::cout;
using std::ostream;
using std::endl;
using std::vector;
using std::map;

struct Code {
    long long code;
    int len;
    bool operator[](int index) const {
        return (index < len) ? code & (1 << (len - index - 1)) : 0;
    }
};

typedef unsigned char byte;
typedef map<int, Code>              LenCodeMap;
typedef map<int, vector<byte> >     LengthBytesMap;

struct Node {
    Node():left(0), right(0) {}
    Node(byte c, long long weight) : vbyte(c), weight(weight), left(0), right(0) {}
    Node(Node *a, Node *b) : vbyte(0), weight(a->weight + b->weight), left(a), right(b) {}
    ~Node() {
        delete left;
        delete right;
    }

    byte vbyte;
    long long weight;
    Node *left;
    Node *right;
};

struct NodeComparator {
    bool operator()( Node *first, Node *second ) const {
        return first->weight > second->weight;
    }
};


//vector output
template<typename T> ostream& operator<<(ostream& _out, const vector<T>& v) {
    for (typename vector<T>::const_iterator it = v.begin(); it != v.end(); it++) {
        _out << *it << " ";
    }
    return _out;
}

//map output
template<typename K, typename V> inline ostream& operator<<(ostream& _out, const map<K, V>& mp) {
    for (typename map<K, V>::const_iterator it = mp.begin(); it != mp.end(); it++) {
        _out << (it -> first) << " => " << (it -> second) << endl;
    }
    return _out;
}

//Code output
inline ostream& operator<<(ostream& _out, const Code &code) {
    _out << "(" << code.len << "," << code.code << ") = ";
    for (int i = 0; i < code.len; i++){
        _out << (code[i]);
    }
    return _out;
}

#endif // COMMON_H
