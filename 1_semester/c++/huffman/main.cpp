#include <string>

#include "common.h"
#include "decoder.h"
#include "encoder.h"

using std::string;

enum Operation {
    Unspecified,
    Compress,
    Decompress
};

int main(const int args, char * argv[]) {

    char *filename = 0;
    char *out_filename = 0;
    Operation operation = Unspecified;

    int processed = 0;
    while (++processed < args) {
        string current(argv[processed]);
        if (current == "-c") {
            operation = Compress;
        } else if (current == "-d") {
            operation = Decompress;
        } else if (current == "-i") {
            processed++;
            filename = argv[processed];
        } else if (current == "-o") {
            processed++;
            out_filename = argv[processed];
        } else {
            cout << "shi' ma'faka" << endl;
        }
    }

    if (filename == 0) {
        cout << "Please specify input filename" << endl;
    }
    if (out_filename == 0) {
        cout << "PLease specify output filename" << endl;
    }
    if (operation == Unspecified) {
        cout << "Please tell me what to do" << endl;
    }
    if (!filename || !out_filename || !operation) {
        return 0;
    }

    cout << (operation == Compress ? "Compressing " : "Decompressing ")
         << filename << " to " << out_filename << endl;

    ifstream input_stream(filename, ios::binary);
    ofstream out_stream(out_filename, ios::binary);

    if (operation == Compress) {
        Encoder::encode(input_stream, out_stream);
    } else {
        Decoder::decode(input_stream, out_stream);
    }

    input_stream.close();
    out_stream.close();

    return 0;
}

