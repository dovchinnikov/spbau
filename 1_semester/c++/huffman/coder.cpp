#include "coder.h"

LenCodeMap *getStartCodes(LengthBytesMap & lcm) {
    LenCodeMap *startcodes = new LenCodeMap();

    if (lcm.size() > 0) {
        int min_len = lcm.begin()->first;
        int max_len = lcm.rbegin()->first;

        (*startcodes)[max_len] = (Code){0, max_len};
        for (int len = max_len; len > min_len; len--) {
            long long code = (*startcodes)[len].code + lcm[len].size();
            code = code >> 1;
            (*startcodes)[len - 1] = (Code){code, len - 1};
        }
    }

    return startcodes;
}

Code *Coder::getCodeMap(LengthBytesMap & lcm) {
    LenCodeMap *start_codes = getStartCodes(lcm);
    Code *cc = new Code[ALL_BYTES];
    std::fill(cc, cc + ALL_BYTES, (Code){0,0});

    for (LengthBytesMap::iterator it = lcm.begin(); it != lcm.end(); it++) {
        int len = it->first;
        vector<byte> chars = it->second;
        sort(chars.begin(), chars.end());
        for (uint i = 0; i < chars.size(); i++) {
            byte b = chars[i];
            long long code = (*start_codes)[len].code + i;
            cc[b] = (Code){code, len};
        }
    }

    delete start_codes;
    return cc;
}

Node *Coder::buildTree(LengthBytesMap & lcm) {

    Code *code_map = getCodeMap(lcm);
    Node * root = new Node();
    for (int i = 0; i < ALL_BYTES; i++) {
        int len = code_map[i].len;
        if (len) {
            Node *current = root;
            for (int j = 0; j < len; j++) {
                if (code_map[i][j]) {
                    if (!current->right) {
                        current->right = new Node();
                    }
                    current = current->right;
                } else {
                    if (!current->left) {
                        current->left = new Node();
                    }
                    current = current->left;
                }
            }
            current->vbyte = i;
        }
    }

    delete code_map;
    return root;
}
