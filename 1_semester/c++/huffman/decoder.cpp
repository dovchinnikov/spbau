#include "decoder.h"

LengthBytesMap *readLengthBytesMap(ifstream & input_stream) {
    LengthBytesMap *lbm = new LengthBytesMap();

    byte len = 0;
    for (int c = 0; c < ALL_BYTES; c++) {
        len = input_stream.get();
        if (len) {
            if (lbm->find(len) == lbm->end()) {
                (*lbm)[len] = vector<byte>();
            }
            (*lbm)[len].push_back((byte)c);
        }
    }

    return lbm;
}

long long readFileSize(ifstream & input_stream) {
    long long filesize;
    input_stream.read((char *)&filesize, sizeof(long long));
    return filesize;
}

void readData(ifstream & input_stream, ofstream & out_stream, TreeDecoder & tree_decoder, long long filesize) {

    byte        last_byte_read      = 0;
    int         last_bit_read_index = 0;
    long long   decoded_chars       = 0;

    while (decoded_chars != filesize) {
        if (last_bit_read_index == 0) {
            last_byte_read = input_stream.get();
            if (!input_stream.good()) {
                return;
            }
        }
        while (tree_decoder.needMoreBits() && last_bit_read_index != CHAR_BIT) {
            bool c_bit = last_byte_read & (1 << last_bit_read_index);
            last_bit_read_index++;
            tree_decoder.passBit(c_bit);
        }
        if (!tree_decoder.needMoreBits()) {
            byte b = tree_decoder.getByte();
            out_stream << *(const char *)&b;
            decoded_chars++;
        }
        if (last_bit_read_index == CHAR_BIT) {
            last_bit_read_index = 0;
        }
    }
}

void Decoder::decode(ifstream & input_stream, ofstream & out_stream) {

    LengthBytesMap *lbm = readLengthBytesMap(input_stream);
    Node *code_tree = Coder::buildTree(*lbm);
    TreeDecoder tree_decoder(code_tree);

    delete lbm;

    long long original_filesize = readFileSize(input_stream);
    readData(input_stream, out_stream, tree_decoder, original_filesize);

    delete code_tree;
}
