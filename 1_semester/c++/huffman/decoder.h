#ifndef DECODER_H
#define DECODER_H

#include <fstream>
#include "common.h"
#include "coder.h"
#include "treedecoder.h"

using std::ifstream;
using std::ofstream;
using std::ios;

class Decoder {

    public:
        static void decode(ifstream &, ofstream &);

};

#endif // DECODER_H
