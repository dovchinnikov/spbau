#ifndef ENCODER_H
#define ENCODER_H

#include <fstream>
#include <queue>
#include "common.h"
#include "coder.h"

using std::ifstream;
using std::ofstream;
using std::ios;
using std::priority_queue;

class Encoder {

    public:
        static void encode(ifstream &, ofstream &);

};

#endif // ENCODER_H
