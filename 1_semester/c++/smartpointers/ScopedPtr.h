
template<class T> class ScopedPtr { 
    
    public:
        ScopedPtr(T *object) : object(object) {}
        ScopedPtr() {
            object = new T();
        }
        ~ScopedPtr() {
            if (object) {
                delete object;
            }
        }

        T & operator*() {
            return *object;
        }   
        
        T & operator*() const {
            return *object;
        }

        bool operator<(ScopedPtr<T> & other) {
            return object < other.object;
        }
        void operator=(T *obj){
            if (object) {
                delete object;
            }
            object = obj;
        }

        T* operator->() {
            return object;
        }
        
    private:
        T *object;
};
