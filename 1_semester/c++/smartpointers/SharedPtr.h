#include <iostream>

using std::cout;
using std::endl;

template<class T> struct Holder {
    Holder(): value(new T()), counter(1){}
    Holder(T * value): value(value), counter(1){}
    ~Holder() {
        delete value;
    }

    void add() { 
        counter++;
    }
    void release() {    
        if (!--counter){ 
            cout << "removing instance" << endl;
            delete this; 
        }
    }

    int counter;
    T *value;
};

template<class T> class SharedPtr { 
    
    public:
        SharedPtr(): holder(new Holder<T>()) {}
        SharedPtr(T *object) : holder(new Holder<T>(object)) {}
        ~SharedPtr() {
            holder->release();
        }

        T & operator*() {
            return *(holder->value);
        }   
        
        const T & operator*() const {
            return *(holder->value);
        }

        bool operator<(SharedPtr<T> & other) {
            return holder->value < other.holder->value;
        }
            
        void operator=(T *value) {
            holder->release();
            holder = new Holder<T>(value);
        }
        void operator=(SharedPtr<T> & other) {
            holder = other.holder;
            holder->add();
        }

        T * operator->() {
            return holder->value;
        }
        
    private:
        Holder<T> *holder;
};
