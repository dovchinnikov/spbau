#include <iostream>
#include "ScopedPtr.h"
#include "SharedPtr.h"
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::ostream;


template<typename T> ostream & operator<<(ostream & _out, const vector<T> & v) {
    for (typename vector<T>::const_iterator it = v.begin(); it != v.end(); it++) {
        _out << *it << " ";
    }
    return _out;
}

int main() {

    ScopedPtr<int> sp1 = new int(100500);
    ScopedPtr<int> sp2;
    ScopedPtr<char> sp3         = new char(65);
    ScopedPtr<vector<int> > spv = new vector<int>(5);
    spv->push_back(100);
    spv->erase(spv->begin());

    cout << *sp2 << endl;
    cout << *sp3 << endl;
    cout << *spv << endl;
    cout << (sp1 < sp2) << endl;
    cout << (*sp1 < *sp2) << endl;
    cout << endl;

    SharedPtr<vector<int> > shp;
    {
        SharedPtr<vector<int> > shp2 = shp;
        shp2->push_back(1);
        shp2->push_back(10);
        shp2->push_back(30);
        shp2->push_back(11);
        cout << "shp: " << *shp << endl;
        shp = 0;
        cout << "shp removed" << endl;
    }


    return 0;
}
