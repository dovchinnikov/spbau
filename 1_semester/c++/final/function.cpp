
template <class R, class P> 
struct F {

    typedef R (*function_type)(P);
    
    struct FunctionHolder {
        FunctionHolder() {}
        FunctionHolder(function_type function) : function(function) {}

        virtual R operator()(P param){
            return function(param);
        }

        function_type function;
    };

    template <class T>
    struct FunctorHolder : FunctionHolder {
        FunctorHolder(T functor) : functor(functor) {}

        R operator()(P param) {
            return functor(param);
        }
        T functor;
    };

    F (function_type function) {
        f = FunctionHolder(function);
    }   

    template <class T> F (T functor) {
        f = FunctorHolder<T>(functor);
    }
    
    FunctionHolder f;
    R operator()(P param) {
        return f(param);
    }
};

int foo(int a) {
    return a;
}

int main() {
    F<int, int> f(foo);
    f(1);
}
