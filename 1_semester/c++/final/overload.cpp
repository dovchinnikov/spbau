#include<iostream>
using namespace std;

struct A {
    void write (char const * s) {
        cout << s << endl;
    }  
};

struct B : A {
    void write(int i) {}
    void write(float i) {}
    using A::write;
};

int main(){
    B b;
    b.write("hello world!");
    return 0;
}
