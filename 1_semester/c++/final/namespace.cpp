

namespace A {
    int a() {
        return 1;
    }
};

namespace B {
    
    int b() {
        using namespace A;
        return c() + 1;
    }
}

namespace A {
    int c() {
        return 100500;
    }
}

#include <iostream>
using std::cout;
using std::endl;
int main() {
    cout << B::b() << endl;
}
