#ifndef PPSTREAM
#define PPSTREAM

#include "IStream.h"

class PPStream : public IStream {

	public:
		PPStream(IStream & s): stream(s){ };
		~PPStream(){};
		void write(const int);
		void write(const char *);

		PPStream& operator<<(const int &);
		PPStream& operator<<(const char *);
	private:	
		IStream &stream;
};



#endif
