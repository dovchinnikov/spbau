#ifndef ISTREAM
#define ISTREAM

class IStream {
	public:
		virtual void write(const int value) = 0;
		virtual void write(const char *value) = 0;
		virtual IStream & operator<<(const int&) = 0;
		virtual IStream & operator<<(const char *) = 0;
};

#endif
