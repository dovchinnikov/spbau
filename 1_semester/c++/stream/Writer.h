#ifndef WRITER
#define WRITER

#include "IStream.h"
#include <cstdio>

class Writer : public IStream {
	
	public:
		Writer(const char *);
		~Writer();
		void write(const int);
		void write(const char *);
		Writer& operator<<(const int &);
		Writer& operator<<(const char *);

	private:
		Writer(const Writer &);
		Writer& operator=(const Writer &);
		FILE *f;

};

#endif
