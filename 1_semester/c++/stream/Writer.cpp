#include "Writer.h"
#include <cstdio>

Writer::Writer(const char *filename){
	f = fopen(filename, "w");
}

Writer::~Writer(){
	fclose(f);
}

void Writer::write(const int number){
	fprintf(f, "%d", number);
}

void Writer::write(const char *string){
	fprintf(f, "%s", string);
}

Writer& Writer::operator<<(const char *msg){
	write(msg);
	return *this;
}

Writer& Writer::operator<<(const int &number){
	write(number);
	return *this;
}
