#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

struct multiplier {
    long long number;
    long long power;
};

int main() {
    long long n;
    cin >> n;
    
    vector<multiplier> result;
    long long delimiter = 2;

    while (n > 1 && delimiter*delimiter <= n) {
        long long power = 0;
        while (n % delimiter == 0) {
            n /= delimiter;
            power++;
        }
        if (power) {
            result.push_back((multiplier){delimiter, power});
        }        
        delimiter++;
    }

    if (n > 1) {
        result.push_back((multiplier){n, 1});
    }
   
    cout << result.size() << endl;
    for (int i=0; i<result.size(); i++){
        cout << result[i].number << " " << result[i].power << endl;
    }
    
    return 0;
}
