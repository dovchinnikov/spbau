#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

vector<vector<int> > graph;
vector<vector<int> > rgraph;
bool *done;
vector<int> order;

void dfs1 (int v) {
    if (done[v]) return;
    done[v] = true;
    for (size_t i = 0; i < (graph)[v].size(); i++)
        dfs1((graph)[v][i]);
    order.push_back(v);
}

void dfs2 (int v, vector<int> & component) {
    if (done[v]) return;
    done[v] = true;
    component.push_back(v);
    for (size_t i = 0; i < rgraph[v].size(); i++)
        dfs2(rgraph[v][i], component);
}

int main() {
    int v;
    int e;
    cin >> v >> e;

    graph.assign(v, vector<int>());
    rgraph.assign(v, vector<int>());
    done = new bool[v];
    std::fill(done, done + v, 0);

    for (int i = 0; i < e; i++) {
        int from;
        int to;
        cin >> from >> to;
        (graph)[from - 1].push_back(to - 1);
        (rgraph)[to - 1].push_back(from - 1);
    }

    for (int i = 0; i < v; i++){
        dfs1(i);
    }
    std::fill(done, done + v, 0);

    int *vertex_component_number = new int[v];
    std::fill(vertex_component_number, vertex_component_number + v, -1);
    int components_count = 0;

    for (int i = 0; i < v; i++) {
        int c = order[v - i - 1];
        vector<int> component;
        dfs2(c, component);
        if (component.size() > 0) {
            for (int k = 0; k < component.size(); k++){
                vertex_component_number[component[k]] = components_count;
            }
            components_count++;
        }
    }

    vector< std::set<int> > condensated(components_count);
    for (int vertex = 0; vertex < v; vertex++) {
        int component_number = vertex_component_number[vertex];
        for (int i = 0; i < graph[vertex].size(); i++){
            int to_component_number = vertex_component_number[graph[vertex][i]];
            if (component_number != to_component_number) {
                condensated[component_number].insert(to_component_number);
            }
        }
    }

/*
    for (int i = 0; i< v; i++) {
        cout << i << " in " << vertex_component_number[i] << " component" << endl;
    }
*/
    int edge_count = 0;
    for (int i = 0; i < condensated.size(); i++) {
        edge_count+=condensated[i].size();
//        for (int k = 0; k < condensated[i].size(); k++)
//            cout << "edge from " << i << " to " << k << endl;
    }
    cout << edge_count << endl;
    return 0;
}
