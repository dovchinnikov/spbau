#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::vector;


template<typename T> inline std::ostream& operator<<(std::ostream& _out, const vector<T>& v) {
    for (typename vector<T>::const_iterator it = v.begin(); it != v.end(); it++) {
        _out << *it << " ";
    }
    return _out;
}

struct c {
    int value;
    int abs;
};

bool comparison(const c & a, const c & b) {
    return a.abs == b.abs ? a.value < b.value : a.abs < b.abs;
}


int main() {
    int n;
    int k;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    cin >> k;

    int median = floor((n+1)/2) - 1;
    int st = floor((n-k)/2);
    int end = floor((n+k)/2) - 1;

    nth_element(a.begin(), a.begin() + end, a.end());
    nth_element(a.begin(), a.begin() + median, a.begin() + end);
    nth_element(a.begin(), a.begin() + st, a.begin() + median);

    for (int i = st; i <= end; i++){
        cout << a[i] << " ";
    }
    cout << endl;
//    cout << a << endl;

    vector<c> b(n);
    int median_value = a[median];
 //   cout << "median value: " << median_value << endl;
    for (int i = 0; i < n; i++) {
        b[i] = (c){a[i], (median_value > a[i] ? median_value - a[i] : a[i] - median_value)};
    }

    nth_element(b.begin(), b.begin() + k - 1, b.end(), comparison);
    for (int i = 0; i < k; i++) {
        cout << b[i].value << " ";
    }
    cout << endl;
    return 0;
}

