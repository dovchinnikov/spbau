#include <iostream>
#include <algorithm>
#include <vector>

using std::cin;
using std::cout;
using std::vector;

vector<vector<int> > *outgoing;
int counter = 0;
bool *done;
bool *temp;
int *result;

int stop() {
    cout << -1;
    return -1;
}

int dfs(int cv) {
    if (temp[cv]) return stop();
    if (done[cv]) return 0;
    temp[cv] = 1;
    for (int i = 0; i < (*outgoing)[cv].size(); i++) {
        if (dfs((*outgoing)[cv][i]) == -1) return -1;
    }
    temp[cv] = 0;
    done[cv] = 1;
    result[counter++] = cv;
    return 0;
}

int main() {
    int v;
    int e;
    cin >> v >> e;
    
    outgoing = new vector<vector<int> >(v);
    done = new bool[v];
    std::fill(done, done + v, 0);
    temp = new bool[v];
    std::fill(temp, temp + v, 0);

    result = new int[v];
    for (int i = 0; i < e; i++) {
        int from;
        int to;
        cin >> from >> to;
        (*outgoing)[from - 1].push_back(to - 1);
    }
    
    for (int i = 0; i < v; i++)
        if (dfs(i) == -1) return 0; 
    
    if (counter < v) {
        cout << -1;
    } else
    for (int i = 0; i < v; i++) {
        cout << result[counter - i - 1] + 1 << " ";
    }

    return 0;
}
