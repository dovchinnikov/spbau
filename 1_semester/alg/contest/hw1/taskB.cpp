#include <iostream>
#include <string>
#include <limits>
#include <cassert>

using std::string;
using std::cin;
using std::cout;
using std::ostream;
using std::endl;

template<typename T> struct Node {
		Node *prev;
		Node *next;
		T value;
};

template<typename T> class PointerList
{
    template<T> friend ostream& operator<<(ostream&, const PointerList<T> &);

    public:
        PointerList<T>() {
            head = NULL;
            tail = NULL;
            size = 0;
        }

        ~PointerList() {
            while (head != tail) {
                head = head->next;
                delete head->prev;
            }
            delete head;
        }

        Node<T> *push_back(T value)	{
            Node<T> *node = new Node<T>();
            node->value = value;
            if (size == 0) {
                node->next = node;
                node->prev = node;
                head = node;
                tail = node;
                size = 1;
            } else {
                node->next = head;
                node->prev = tail;
                tail->next = node;
                head->prev = node;
                tail = node;
                size++;
            }
            return node;
        }

        T pop_back() {
            T ret_value = tail->value;
            head->prev = tail->prev;
            tail->prev->next = head;
            delete tail;
            tail = head->prev;
            size--;
            return ret_value;
        }

        Node<T> *remove(Node<T> *node){
            Node<T> *next = node->next;
            node->prev->next = node->next;
            node->next->prev = node->prev;
            if (node == head) {
                head = node->next;
            }
            if (node == tail) {
                tail = tail->prev;
            }
            delete node;
            size--;
            return next;
        }

        bool isEmpty(void) {
            return size == 0;
        }

        bool isNotEmpty(void) {
            return size != 0;
        }

        const int getSize(void) {
            return this->size;
        }

        const T operator[](const int index) {
            if (index < 0)
                return NULL;
            if (index < size) {
                Node<T> *curr = head;
                for (int i = 0; i < index; i++)
                    curr = curr->next;
                return curr->value;
            } else {
                return NULL;
            }
        }

        void operator+=(T value) {
            push_back(value);
        }

        Node<T> *head;
        Node<T> *tail;

    private:
        int size;
};

template<typename T> ostream& operator<<(ostream& cout_, const PointerList<T> &list)
{
    Node<T> *c = list.head;
    do {
        cout_ << c->value;
        c = c->next;
    } while (c != list.head);

    return cout_;
};

int main() {
    int n;
    cin >> n;

    string children_names[n];
    PointerList<int> children_list;
    Node<int> *children_array[n];

    {
        string token;
        for (int i = 0; i < n; i++) {
            cin >> token;
            children_names[i] = token;
            children_array[i] = children_list.push_back(i);
        }
    };

    {
        int index;
        for (int c = 0; c < n - 3; c++) {
            cin >> index;
            assert(1 <= index && index <= n);
            index--;
            cout << (children_names[children_array[index]->prev->value])
                << " "
                << (children_names[children_array[index]->next->value])
                << endl;
            children_list.remove(children_array[index]);
        }
    };

    return 0;
}
