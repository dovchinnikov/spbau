#include <iostream>
#include <cstdlib>

using std::cin;
using std::cout;
using std::endl;


int countIndex(int *array, int sum) {
    int median = 0;
    int index = -1;
    double half = (double)sum / 2;
    do {
        median += array[++index];
    } while (median < half);
    
    return index;
}

long long countSteps(int *array, int size, int index) {
    long long steps = 0;
    for (int i = 0; i < size; i++) {
        steps += abs(index - i) * array[i] * 2;
    }
    return steps;
}


int main() {

    int m;
    int n;
    cin >> m >> n;

    int *h = new int[m]();
    int *v = new int[n]();
    int sum = 0;
    
    {   //processing input
        int next;
        for (int i = 0; i < m; i++) {
            for (int k = 0; k < n; k++) {
                cin >> next;
                h[i] += next;
                v[k] += next;
                sum += next;
            }
        }
    }

    int h_index = countIndex(h, sum);
    int v_index = countIndex(v, sum);
    long long steps = countSteps(h, m, h_index) + countSteps(v, n, v_index) + 2 * sum;

    cout << ++h_index << " " << ++v_index << endl;
    cout << steps << endl;

    delete[] h;
    delete[] v;
    return 0;
}
