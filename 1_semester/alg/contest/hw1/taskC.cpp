#include <iostream>
#include <vector>
#include <sstream>

using std::cout;
using std::cin;
using std::ostream;
using std::string;
using std::getline;
using std::istringstream;

template<typename T> struct Node {
		Node *prev;
		Node *next;
		T value;
};

template<typename T> class PointerList
{
    template<T> friend ostream& operator<<(ostream&, const PointerList<T> &);

    public:
        PointerList<T>() {
            head = NULL;
            tail = NULL;
            size = 0;
        }

        ~PointerList() {
            while (head != tail) {
                head = head->next;
                delete head->prev;
            }
            delete head;
        }

        void push_back(T value)	{
            Node<T> *node = new Node<T>();
            node->value = value;
            if (size == 0) {
                node->next = node;
                node->prev = node;
                head = node;
                tail = node;
                size = 1;
            } else {
                node->next = head;
                node->prev = tail;
                tail->next = node;
                head->prev = node;
                tail = node;
                size++;
            }
        }

        T pop_back() {
            T ret_value = tail->value;
            head->prev = tail->prev;
            tail->prev->next = head;
            delete tail;
            tail = head->prev;
            size--;
            return ret_value;
        }
    /*
        void remove(T value) {
            Node<T> *curr = head;
            do {
                if (curr->value == value) {
                    curr->prev->next = curr->next;
                    curr->next->prev = curr->prev;
                    delete curr;
                    size--;
                    break;
                } else {
                    curr = curr->next;
                }
            } while (curr!=head);
        }
    */
        Node<T> *remove(Node<T> *node){
            Node<T> *next = node->next;
            node->prev->next = node->next;
            node->next->prev = node->prev;
            if (node == head) {
                head = node->next;
            }
            if (node == tail) {
                tail = tail->prev;
            }
            delete node;
            size--;
            return next;
        }

        bool isEmpty(void) {
            return size == 0;
        }

        bool isNotEmpty(void) {
            return size != 0;
        }

        const int getSize(void) {
            return this->size;
        }

        const T operator[](const int index) {
            if (index < 0)
                return NULL;
            if (index < size) {
                Node<T> *curr = head;
                for (int i = 0; i < index; i++)
                    curr = curr->next;
                return curr->value;
            } else {
                return NULL;
            }
        }

        void operator+=(T value) {
            push_back(value);
        }

        Node<T> *head;
        Node<T> *tail;

    private:
        int size;
};

template<typename T> ostream& operator<<(ostream& cout_, const PointerList<T> &list)
{
    Node<T> *c = list.head;
    do {
        cout_ << c->value;
        c = c->next;
    } while (c != list.head);

    return cout_;
}



int main() {

    PointerList<char> number;
    int k;

    string line;
    getline(cin, line);
    istringstream iss(line);

    char digit;
    while (iss >> digit) {
        if (digit == '0' || digit == '1' || digit == '2' || digit == '3' || digit == '4'
                || digit == '5' || digit == '6' || digit == '7' || digit == '8' || digit == '9') {
            number.push_back(digit);
        } else {
            break;
        }
    }

    cin >> k;

    Node<char> *current = number.head;

    while (k > 0 && current != number.tail) {
        if (current->value < current->next->value) {
            current = number.remove(current);
            k--;
            if (current->prev && current != number.head) {
                current = current->prev;
            }
        } else {
            current = current->next;
        }
    }
    while (k > 0) {
        number.pop_back();
        k--;
    }

    cout << number;

	return 0;
}
