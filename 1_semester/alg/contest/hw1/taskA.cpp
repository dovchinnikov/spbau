#include <iostream>
#include <algorithm>
#include <vector>

using std::sort;
using std::cout;
using std::cin;
using std::vector;

int main() {
	
	int n;
	cin >> n;
	char *r = new char[n];
	
	for (int i = 0; i < n; i++) {
		cin >> r[i];
	}
	
	vector<char> chars (r, r+n);
	sort(chars.begin(), chars.end());
	
	for (int i = 0; i < n; i++) {
		if (chars[i] > '0') {
			char t = chars[0];
			chars[0] = chars[i];
			chars[i] = t;
			break;
		}
	} 
	
	for (vector<char>::iterator it = chars.begin(); it != chars.end(); ++it) {
		cout << *it;
	}
	
	return 0;	
}
