#include <iostream>
#include <vector>
#include <limits.h>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::ostream;

struct Node {
    int value;
    vector<int> children;
};

template<class T> ostream& operator<<(ostream& out, vector<T> & v) {
    for (typename vector<T>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << " ";
    }
    return out;
}

ostream & operator<<(ostream& out, Node & node){
    out << "{" << node.value << ", " << node.children << "}" << endl;
    return out;
}

int dfs(vector<Node> & tree, int v) {
    Node node = tree[v];
    vector<int> children = node.children;

    if (children.size() == 0) {
        return node.value;
    } else {
        int max = INT_MIN;
        int sum = 0;

        for (size_t i = 0; i < children.size(); i++){
            int result = dfs(tree, children[i]);
            sum += result;
            if (result > max) {
                max = result;
            }
        }

        sum += tree[v].value;
        if (tree[v].value > max) {
            max = tree[v].value;
        }
        return (sum > max ? sum: max);
    }
}

int main() {
    int n;
    cin >> n;

    vector<Node> tree(n);
    cin >> tree[0].value;
    for (int i = 1; i < n; i++) {
        int parent;
        cin >> parent;
        parent--;
        cin >> tree[i].value;
        tree[parent].children.push_back(i);
    }

    cout << dfs(tree, 0) << endl;

    return 0;
}
