#include <iostream>
#include <vector>
#include <limits.h>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::ostream;

typedef vector<bool> vbool;
typedef vector<char> vchar;

template<class T> ostream& operator<<(ostream& out, vector<T> & v) {
    for (typename vector<T>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << " ";
    }
    return out;
}

ostream& operator<<(ostream& out, vector<vchar> & v) {
    for (vector<vchar>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << endl;
    }
    return out;
}


ostream& operator<<(ostream& out, vector<char> & v) {
    for (vector<char>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it;
    }
    return out;
}

ostream& operator<<(ostream& out, vector<vbool> & v) {
    for (vector<vbool>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << endl;
    }
    return out;
}


int main() {

    vector<char> result_string;
    vector<int> result_prev;
    int result_count = INT_MAX;
    int result_shift = 0;

    int n;
    cin >> n;

    vector<vchar> strings(n, vchar(n));
    for (int i = 0; i < n; i++) {
        cin >> strings[0][i];
    }

    for (int i = 1; i < n; i++) {
        for (int j = 0; j < n; j++) {
            strings[i][j] = strings[0][(i+j+n)%n];
        }
    }

//    cout << strings << endl;

    for (int shift = 0; shift < n; shift++){
        vector<char> s = strings[shift];
        vector<vbool> d(n, vbool(n));
        for (int i = 0; i < n; i++) {
            d[i][i] = true;
        }
        for (int i = 0; i < n - 1; i++) {
            d[i][i+1] = s[i] == s[i+1];
        }
        for (int l = 2; l < n; l++) {
            for (int i = l; i < n; i++) {
                d[i-l][i] = d[i-l+1][i-1] && s[i] == s[i-l];
            }
        }

        vector<int> m(n+1,INT_MAX); // = new int[n+1];
        m[0] = 0;
        m[1] = 1;
        vector<int> prev(n);
        for (int i = 2; i <= n; i++) {      //i=[2..n]
            for (int j = 1; j <= i; j++) {  //j=[1..i]
                if (d[j-1][i-1]) {          // s[j..i] - palindrome
                    int c = m[j-1] + 1;     //
                    if (c < m[i]) {
                        m[i] = c;
                        prev[i-1] = j-1;
                    }
                }
            }
        }

        if (m[n] < result_count){
            result_count = m[n];
            result_shift = shift;
            result_string = s;
            result_prev = prev;
        }

//        cout << s << endl;
//        cout << d << endl;
    //    cout << "maxlen: " << maxlen << endl;
//        cout << m  << endl;
//        cout << prev << endl;
    }

    cout << result_shift << " " << result_count << endl;
//    cout << result_string << endl;
//    cout << result_prev << endl;
    int current = result_prev[n-1];
    while(current != 0) {
        result_string.insert(result_string.begin()+current, 1, ' ');
        current = result_prev[current-1];
    }
    cout << result_string << endl;
    return 0;
}

