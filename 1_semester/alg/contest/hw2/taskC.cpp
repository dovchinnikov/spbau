#include <iostream>
#include <vector>
#include <limits.h>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::ostream;

template<class T> ostream& operator<<(ostream& out, vector<T> & v) {
    for (typename vector<T>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << " ";
    }
    return out;
}

typedef vector<long long> vlong;

int main() {
    long long mod = 1000000009;

    int n;
    int A;
    cin >> n >> A;

    vector<long long> set(n+1);
    for (int i = 1; i <= n; i++) {
        cin >> set[i];
    }

    vector<vlong> count(A+1, vlong(n+1, 0));
    count[0].assign(n+1, 1);
    for (int a = 1; a <= A; a++) {
        for (int i = 1; i <= n; i++) {
            if (set[i] > a) {
                count[a][i] = count[a][i-1] % mod;
            } else {
                count[a][i] = count[a-set[i]][i] + count[a][i-1] % mod;
            }
        }
//        cout << count[a] << endl;
    }

    cout << count[A][n] % mod << endl;

    return 0;
}
