#include <iostream>
#include <vector>
#include <stack>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::ostream;
using std::stack;

typedef vector<long long> vlong;

template<class T> ostream& operator<<(ostream& out, vector<T> & v) {
    for (typename vector<T>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << " ";
    }
    return out;
}

template<class T> ostream& operator<<(ostream& out, stack<T> & st) {
    while (st.size() > 0) {
        out << st.top() << " ";
        st.pop();
    }
    return out;
}

int main() {

    int A;
    int n;
    cin >> A >> n;

    vector<int> set(n+1);
    for (int i = 1; i <= n; i++) {
        cin >> set[i];
    }

    vector<vlong> result(A+1, vlong(n+1, 0));
    result[0].assign(n+1, 1);
    for (int a = 1; a <= A; a++) {
        for (int i = 1; i <= n; i++) {
            result[a][i] = result[a][i-1];
            if (set[i] <= a) {
                result[a][i] += result[a-set[i]][i-1];
            }
        }
    }

    int count = result[A][n];
    if (count == 0) {
        cout << 0 << endl;
    } else if (count > 1) {
        cout << -1 << endl;
    } else if (count == 1) {
        int a = A;
        int i = n;
        stack<int> st;
        while (i > 0) {
            if (a >= set[i] && result[a-set[i]][i-1] == 1) {
                a-=set[i];
            } else {
                st.push(i);
            }
            i--;
        }
        cout << st << endl;
    }

    return 0;
}
