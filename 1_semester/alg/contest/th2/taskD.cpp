#include <iostream>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;

void heapifyUp(int *heap, int size) {
    int pos = size - 1;
    while (pos > 0) {
        int parent = ceil(pos / 2.0)-1;
        if (heap[pos] > heap[parent]) {
            int tmp = heap[pos];
            heap[pos] = heap[parent];
            heap[parent] = tmp;
            pos = parent;
        } else {
            break;
        }
    }
}

void buildHeap(int *heap, int n)
{
    heap[0] = 1;
    int i = 2;
    while (i <= n) {
        heap[i-2] = i;
        heapifyUp(heap, i-1);
        heap[i-1] = 1;
        i++;
    }
}

void setValue(int *heap, int i, int & v, int n) {
    if (i < n) {
        heap[i] = v--;
        setValue(heap, 2*i + 1, v, n);
        setValue(heap, 2*i + 2, v, n);
    }
}

int heapifyDown(int *arr, int i, int n) {
    int c = 0;

    int left = 2*i + 1;
    int right = 2*i + 2;
    int nextIndex = -1;
    if (left < n) {
        if (right < n) {
            nextIndex = arr[left] > arr[right] ? left: right;
        } else {
            nextIndex = left;
        }
    }

    if (nextIndex > -1 && arr[i] < arr[nextIndex]) {
        int tmp = arr[i];
        arr[i] = arr[nextIndex];
        arr[nextIndex] = tmp;
        c++;
        c += heapifyDown(arr, nextIndex, n);
    }

    return c;
}

int heapSort(int *arr, int size) {
    int c = 0;
    while (size > 1) {
        int tmp = arr[0];
        arr[0] = arr[size-1];
        arr[size-1] = tmp;
        size--;
        c += heapifyDown(arr, 0, size);
    }
    return c;
}

int main() {
    int n;
    cin >> n;

    int *heap = new int[n];
    int *a = new int[n];

    buildHeap(heap , n);
    for(int i = 0; i < n; i++) {
        a[i] = heap[i];
    }
    int counter = heapSort(a, n);
    cout << counter << endl;
    for(int i = 0; i < n; i++) {
        cout << heap[i] << " ";
    }
    cout << endl;
    return 0;
}
