#include <iostream>
#include <vector>
#include <set>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::set;
using std::min;

struct Edge {
    int to;
    int index;
};

vector<Edge>    *graph;
bool            *visited;
int             timer = 0;
int             *time_in;
int             *fup;
set<int>        result;

void dfs (int vertex, int parent = -1) {
    visited[vertex] = true;
    time_in[vertex] = fup[vertex] = timer++;
    for (size_t i = 0; i < graph[vertex].size(); i++) {
        int to = graph[vertex][i].to;
        if (to == parent) {
            continue;
        }
        if (visited[to]) {
            fup[vertex] = min(fup[vertex], time_in[to]);
        } else {
            dfs(to, vertex);
            fup[vertex] = min(fup[vertex], fup[to]);
            if (fup[to] > time_in[vertex]) {
                result.insert(graph[vertex][i].index);
            }
        }
    }
}

int main() {
    int v;
    int e;
    cin >> v >> e;
    visited = new bool[v]();
    time_in = new int[v]();
    fup = new int[v]();
    graph = new vector<Edge>[v];

    for (int i = 0; i < e; i++) {
        int from;
        int to;
        cin >> from >> to;
        from--;
        to--;
        graph[from].push_back((Edge){to, i});
        graph[to].push_back((Edge){from, i});
    }

    for (int i = 0; i < v; i++) {
        if (!visited[i]) {
            dfs(i);
        }
    }
    cout << result.size() << endl;
    for (set<int>::iterator it = result.begin(); it!=result.end(); it++){
        cout << *it + 1 << " ";
    }
    cout << endl;
    return 0;
}

