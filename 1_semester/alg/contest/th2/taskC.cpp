#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::fill;
using std::upper_bound;
using std::max;
using std::ostream;
using std::reverse;

ostream& operator<<(ostream& out, vector<int> & v) {
    for (vector<int>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << " ";
    }
    return out;
}

int main() {
    int n;
    cin >> n;
    vector<int> values(n);
    for (int i = 0; i < n; i++) {
        cin >> values[i];
    }
    vector<int> last_element(n + 2);
    vector<int> last_index(n + 1);
    vector<int> previous_index(n);

    std::fill(last_element.begin(), last_element.end(), INT_MAX);
    last_element[0] = INT_MIN;
    std::fill(last_index.begin(), last_index.end(), -1);
    std::fill(previous_index.begin(), previous_index.end(), -1);

    for (int i = 0; i < n; i++) {
        int j = int (upper_bound(last_element.begin(), last_element.end(), values[i]) - last_element.begin());
        if (last_element[j-1] < values[i] && values[i] < last_element[j]) {
            last_element[j] = values[i];
            last_index[j] = i;
            previous_index[i] = last_index[j-1];
        }
    }

//    cout << values << endl;
//    cout << last_element << endl;
//    cout << last_index << endl;
//    cout << previous_index << endl;

    int max_len = 0;
    while (last_element[max_len] != INT_MAX) {
        max_len++;
    }
    max_len--;
    cout << max_len << endl;
    vector<int> result;
    int ind = last_index[max_len];
    while(ind!=-1) {
        result.push_back(values[ind]);
        ind = previous_index[ind];
    }
    reverse(result.begin(), result.end());
    cout << result << endl;
    return 0;
}
