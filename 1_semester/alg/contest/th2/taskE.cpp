#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::ostream;
using std::priority_queue;

struct Request {
    int start;
    int end;
    int index;
};

struct Room {
    int no;
    int end;
    bool operator<(const Room &other) const{  
        return end > other.end;
    }
};

template<typename T> ostream& operator<<(ostream & out, vector<T> & v) {
    for (typename vector<T>::iterator it = v.begin(); it!=v.end(); it++) {
        cout << *it << " ";
    }
    return out;
}

ostream& operator<<(ostream & out, Request & r){
    out << r.start << " " << r.end << endl;
    return out;
}

int request_comparator_start(const Request & a, const Request & b) {
    return a.start < b.start;
}


int main() {
    int n;
    cin >> n;
    
    vector<Request> requests(n);
     
    for (int i = 0; i < n; i++) {
        int s;
        int e;
        cin >> s >> e;
        requests[i] = (Request){s, e, i};
    }

    std::sort(requests.begin(), requests.end(), request_comparator_start);
    int room_counter = 0;
    priority_queue<Room> reserved;
    vector<int> result(n);
    for (int i = 0; i < n; i++) {
        Request req = requests[i];
        if (reserved.size() == 0 || reserved.top().end > req.start) {
            Room room;
            room.no = ++room_counter;
            room.end = req.end;
            reserved.push(room);
            result[req.index] = room.no;
        } else if (reserved.top().end <= req.start) {
            Room room = reserved.top();
            reserved.pop();
            room.end = req.end;
            reserved.push(room);
            result[req.index] = room.no;
        }
    }
   
//    cout << requests <<  endl;
    cout << room_counter << endl;
    cout << result << endl;
    
    return 0;
}
