APP=C

g++ -C task$APP.cpp -o $APP


(cat <<END
10
1 2 3 4 5 6 7 8 9 10
END
) | ./$APP

echo ""

(cat <<END
11
7 9 10 1 5 2 3 6 2 8 4
END
) | ./$APP

echo ""

(cat <<END
4
4 2 3 1
END
) | ./$APP

echo ""

(cat <<END
1
3
END
) | ./$APP

echo ""

(cat <<END
6
1 1 1 1 1 1
END
) | ./$APP

echo ""

(cat <<END
2
1 3
END
) | ./$APP

echo ""

(cat <<END
2
3 2
END
) | ./$APP

echo ""

(cat <<END
15
10 9 8 7 6 5 4 3 2 1 2 3 4 5 6 7 8
END
) | ./$APP

echo ""

