function mergeSort(array) {
	
	function merge(a, b){
		var c = [];
		var i=0, j=0;
		while (i < a.length && j < b.length){
			c.push(a[i] < b[j] ? a[i++] : b[j++]);
		}
		while (i < a.length){
			c.push(a[i++]);
		}
		while (j < b.length){
			c.push(b[j++]);
		}
		return c;
	}
	
	if (array.length < 2){
		return array;
	} else {
		return merge(mergeSort(array.splice(0, array.length / 2)), mergeSort(array));
	}
}