﻿#!/bin/sh
#This script stops tomcat, backups web app and deploys new WAR

BACKUP_ROOT=
APP_CODE=
ENV_CODE=env
CUSTOMER=trunk
TOMCAT=
WAR_NAME=
RESTART=
DEPLOY=1
COPY_NEW_WAR=

#set parameters
while [ "$1" != "" ]; do
    case $1 in
        -b | --backup_dir )		shift
								BACKUP_ROOT=$1
                                ;;
		-e | --environment )	shift
								ENV_CODE=$1
								;;
		-a | --application )	shift
								APP_CODE=$1
								;;
		-t | --tomcat )			shift
								TOMCAT=$1
								;;
		-c | --customer )		shift
								CUSTOMER=$1
								;;
		-w | --war )			shift
								WAR_NAME=$1
								;;	
		-r | --restart )		RESTART=1
								;;
		-d | --deploy-new )		DEPLOY=1
								;;
		-cn | --copy-new-war )	COPY_NEW_WAR=1
								;;								
    esac
    shift
done

#check parameters
EXIT=
if [ ! -d "$BACKUP_ROOT" ]; then
	echo "Backup root folder $BACKUP_ROOT doesn't exist"
	EXIT=1
fi
if [ ! -f "$TOMCAT/bin/catalina.sh" ]; then
	echo "catalina.sh wasn't found at Tomcat directory"
	EXIT=1
fi
if [ "$TOMCAT" = "" ]; then
	echo "Tomcat directory must be specified"
	EXIT=1
fi
if [ "$APP_CODE" = "" ]; then
	echo "Application name must be specified"
	EXIT=1
fi
if [ "$WAR_NAME" = "" ]; then
	echo "WAR name must be specified"
	EXIT=1
fi
if [ "$DEPLOY" = "1" ]; then
	if [ ! -f "$BACKUP_ROOT/war/$WAR_NAME.war" ]; then
		echo "WAR does not exist"
		EXIT=1
	fi
fi
if [ "$EXIT" = "1" ]; then
	exit 1
fi

DIR_NAME=$(date +%Y-%m-%d_%H-%M-%S)"_"$APP_CODE"_"$CUSTOMER"_"$ENV_CODE 
BACKUP_DIR=$BACKUP_ROOT/$DIR_NAME

printf Creating backup directories\n
mkdir $BACKUP_DIR
printf Directory created: $BACKUP_DIR\n

mkdir $BACKUP_DIR/old
mkdir $BACKUP_DIR/new
mkdir $BACKUP_DIR/logs
printf Subfolders created\n

printf Backuping old $WAR_NAME\n
cp $TOMCAT/webapps/$WAR_NAME $BACKUP_DIR/old -R
printf Old $WAR_NAME backuped
 
echo Copying logs: $TOMCAT/logs
cp $TOMCAT/logs/* $BACKUP_DIR/logs -R
echo Logs copied

if [ "$RESTART" = "1" ]; then
	$TOMCAT/bin/catalina.sh stop
	TOMCAT_PID=$(ps -ef | grep $TOMCAT | grep -v grep | grep java | grep -oP '^[0-9a-zA-Z]+\s*\d+' | grep -oP '\d+$')
	if [ "$TOMCAT_PID" = "" ]; then
		echo Tomcat is already dead
	fi
	if [ "$TOMCAT_PID" != "" ]; then
		echo Kill tomcat using PID: $TOMCAT_PID
		kill -9 $TOMCAT_PID
		echo Tomcat died
	fi
fi

echo Removing old files
rm $TOMCAT/webapps/$WAR_NAME* -Rf
echo Old $WAR_NAME removed from tomcat
rm $TOMCAT/logs/* -Rf
echo Logs tomcat folder cleaned
rm $TOMCAT/work/* -Rf
echo Work tomcat folder cleaned
rm $TOMCAT/temp/* -Rf
echo Temp tomcat folder cleaned

if [ "$DEPLOY" = "1" ]; then
	echo Deploying new $WAR_NAME
	if [ "$COPY_NEW_WAR" = "1" ]; then
		cp $BACKUP_ROOT/war/$WAR_NAME.war $BACKUP_DIR/new/$WAR_NAME.war
	fi
	cp $BACKUP_ROOT/war/$WAR_NAME.war $TOMCAT/webapps/$WAR_NAME.war
	echo New $WAR_NAME copied to webapp folder.
fi

if [ "$RESTART" = "1" ]; then
	cd $TOMCAT
	echo Starting Tomcat
	$TOMCAT/bin/catalina.sh start
	echo Tomcat started
fi
exit 0
