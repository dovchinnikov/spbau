#!/bin/bash

CHARS=(0 1 2 3 4 5 6 7 8 9 ｦ ｧ ｨ ｩ ｪ ｫ ｬ ｭ ｮ ｯ ｱ ｲ ｳ ｴ ｵ ｶ ｷ ｸ ｹ ｺ ｻ ｼ ｽ ｾ ｿ ﾀ ﾁ ﾂ ﾃ ﾄ ﾅ ﾆ ﾇ ﾈ ﾉ ﾊ ﾋ ﾌ ﾍ ﾎ ﾏ ﾐ ﾑ ﾒ ﾓ ﾔ ﾕ ﾖ ﾗ ﾘ ﾙ ﾚ ﾛ ﾜ ﾝ)
SIZE=${#CHARS[@]}

ROWS=`tput lines`
COLS=`tput cols`

cleanup() {
	trap '' INT TERM	
	pkill -TERM "matrix.sh"		
	tput reset # && pkill -9 -f "matrix.sh"
	
	#pids=`jobs -p | xargs`
	#kill -9 $pids >/dev/null 2>/dev/null
}

drop() {
	L=$((RANDOM % 10 + 20))
	for ((i=$1; i<$1+$L; i++)); do
		if (( $i < 0 )); then
			continue
		fi
		if (( $i >= $ROWS )); then
			break
		fi
		for ((j=0; j<30; j++)); do
			echo -ne "\E[${i};${2}H█"
			#sleep .0001
			echo -ne "\E[${i};${2}H${CHARS[$((RANDOM % SIZE))]}"	
		done
		sleep .05
	done
	for ((i=$1; i<$1+$L; i++)); do
		if (( $i < 0 )); then
			continue
		fi
		if (( $i >= $ROWS )); then
			break
		fi
		echo -ne "\E[${i};${2}H "
		sleep $((2 / (RANDOM % 5 + 1)))
	done
}


tput setb 0
tput setf 2
tput clear
tput civis

trap cleanup SIGINT

while sleep .01 ; do
	drop $((RANDOM % ROWS - 20)) $((RANDOM % COLS)) & 2>/dev/null
done
