#!/bin/bash
if [ -z $1 ]
	then
	echo "No argument"
	exit 1
fi

arr=
for i in `seq 0 $1` ; do
	if [ 1 -ge $i ]; then
		arr[$i]='1'
	else 
		prev=${arr[$(expr $i - 1)]};
		preprev=${arr[$(expr $i - 2)]};
		arr[$i]=$(expr $prev + $preprev)
	fi 
done
echo ${arr[*]}
