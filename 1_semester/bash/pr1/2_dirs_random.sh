#!/bin/bash
rm input* -R
read n
read m
m=$(expr $m \* 4)

for i in $(seq $n)
do
	dir_name="input$i"
	mkdir $dir_name
	od -An -N$m -td4 < /dev/urandom | cat > "$dir_name/input$i.txt"
done
