#!/bin/bash

echo "   a b c d e f g h"
for i in `seq 8`; do
    printf "$i "
    for j in `seq 8`; do
        c=`expr $i + $j`
        if [[ `expr $c % 2` == '0' ]]; then
            printf "| "
        else
            printf "|\178"
        fi
    done
    echo "|"
done
