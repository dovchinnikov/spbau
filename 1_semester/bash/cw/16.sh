#!/bin/bash

power() {
	num=$1
	pow=$2
	c=1
	result=1
	if((pow==0)); then
		result=1
	fi
	if ((num==0)); then
		result=0
	fi
	if((num>=1&&pow>=1)); then
		while((c<=pow)); do
			result=$((result*num))
			c=$((c + 1))
		done
	fi
	echo $result
}

factorial() {
	c=$1
	f=1
	while [ $c -gt 0 ] ; do
   		f=$((f*c))
   		c=$((c-1))
	done
	echo $f
}

sin () {
	s=0
	x=$1
	for i in `seq 0 10`; do
		a=$((2*i+1))
		p=`power $x $a`
		#echo "$x ^ $a = $p"
		f=`factorial $a`
		#echo "$a! = $f"
		e=`echo $p / $f | bc -l`
		if [[ `expr $i % 2` == '0' ]]; then
			s=`echo $s + $e | bc -l`
		else
			s=`echo $s - $e | bc -l`
		fi
	done
	echo $s
}

sin 2
