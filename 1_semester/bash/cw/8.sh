#!/bin/bash

MASK=
V=0

while [ "$1" != "" ]; do
    case $1 in
        -h | --help )	echo "Usage:	8.sh mask [-v|-h|--help]"
        				echo "-v		for silent mode"
        				exit 0
        				;;
		-v  )			V=1
						;;
		*)				MASK=$1
						;;								
    esac
    shift
done


#echo "$V"
#echo "$MASK"

for file in `find ./ -maxdepth 1 -type f -name "$MASK"`; do
	if [[ $V == "0" ]]; then
		echo "Delete $f? (y/N)"
		read answer
    else
    	answer=y
    fi
	if [ $answer == "y" ]; then
		echo "$file removed"
	else
		echo "$file not removed"
	fi
done
