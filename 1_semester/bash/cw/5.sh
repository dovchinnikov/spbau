#!/bin/bash

out=
for word in $1; do
	if [[ `echo $out | grep -P "\b$word\b" | wc -l` == 0 ]]; then
  		out="$out$word "
	else
		out="$out*$word* "
	fi
done

echo $out
