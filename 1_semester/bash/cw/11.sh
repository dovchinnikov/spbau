#!/bin/bash

D=$1
M=$2
Y=$3

DAYS_OF_WEEK=(`cal | sed -n 2p`)
WEEK=(`cal $2 $3 | grep "\b$1\b"`)
D=

for ((i=0; i<${#WEEK[@]}; i++)); do
	#echo ${WEEK[i]}
	if [[ ${WEEK[i]} == $1 ]]; then
		D=${DAYS_OF_WEEK[i+7-${#WEEK[@]}]}
		break
	fi
done

if [[ -n $D ]]; then
	echo $D
else
	echo "Not found"
fi
