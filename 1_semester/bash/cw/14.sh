#!/bin/bash

chr() {
	printf \\$(printf "%03o" $1)
}

ord() {
	printf "%d" "'$1"
}

while read X ; do 
	for (( i=0; i<${#X}; i++ )); do
		charcode=`ord ${X:$i:1}`
		#echo $charcode
		charcode=$(($charcode + 1))
  		chr $charcode
	done 
	printf "\n"
done

