#!/bin/bash

AUTHOR=$(head -n 1 $1)
COUNT=$(tail -n +2 $1 | head -n 1)
N=$(($RANDOM % $COUNT))

cat $1 | sed -E -n -e "/\* $N/,/\*/p" | sed -E -e 's/^\*.*$//' -e '/^$/d'
printf "\t$AUTHOR\n"
