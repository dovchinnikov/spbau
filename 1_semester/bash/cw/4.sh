#!/bin/bash

NETS=`ifconfig | sed -E -n -e 's/inet addr:([0-9\.]*)(.*)Mask:([0-9\.]*)/\1/p'`

for ip in $NETS; do
	#echo $ip
    part="${ip%.*}"
    #echo $part
    if [[ $part == "127.0.0" ]]; then
    	continue
    fi
    for i in `seq 0 255`; do
        ping -c 1 -t 1 `echo "$part.$i"` >/dev/null 2>/dev/null
        if [[ "$?" == "0" ]]; then
           echo "$part.$i is online"
        fi
    done
done
