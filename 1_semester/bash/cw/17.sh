#!/bin/bash

px=
sx=
nm=
mask=
EXIT=0

while [ "$1" != "" ]; do
    case $1 in
		--prefix )		shift
						px=$1
						;;
		--suffix )		shift
						sx=$1
						;;
		-n | --number )	shift
						nm=$1
						;;
		*)				mask=$1	
						;;							
    esac
    shift
done

if [ "$mask" = "" ]; then
	echo "Mask should be specified"
	EXIT=1
fi

if [ "$px" = "" ]; then
	echo "Prefix should be specified"
	EXIT=1
fi

if [ "$sx" = "" ]; then
	echo "Suffix should be specified"
	EXIT=1
fi

if [ "$nm" = "" ]; then
	nm=0
fi

if [ "$EXIT" = "1" ]; then
	exit 1
fi

echo "mask is $mask" #$px $sx $nm $mask
echo "prefix is $px"
echo "suffix is $sx"
echo "number is $nm"

for file in `find ./ -maxdepth 1 -type f -name "$mask"`; do
	#echo $file
	newname="$px$nm$sx.${file##*.}"
	#echo "newname is $newname"
	mv $file $newname
	nm=$(($nm+1))
done
