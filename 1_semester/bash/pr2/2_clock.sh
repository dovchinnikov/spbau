#!/bin/bash

FORMAT=

case $1 in
	'-d'|'--date')
		FORMAT='%H:%M:%S %D'
		;;
	*)
		FORMAT='%H:%M:%S'
		;;
esac

tput clear
date +"$FORMAT" 

while sleep 1; do
	tput clear
	date +"$FORMAT" 
done
