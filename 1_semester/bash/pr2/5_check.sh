#!/bin/bash

FLAG=1

for file in `find ./ -maxdepth 1 -type f -name "*.c" | sed "s/\.\///"`; do
	for header in `grep -P "#include" $file | grep -Po "(\"|<)(.*)(\"|>)"`; do
		COUNT=`find -maxdepth 1 -name "${header}" | wc -l`
		if [ $COUNT == 0 ] ; then
			FLAG=0
		fi
	done
done

if [ $FLAG == 1 ]; then
	echo All files have matching headers
else
	echo Some of files doesn\'t have matching headers
fi
