#!/bin/bash
if [ -z $1 ];	then
	echo "No argument"
	exit 1
fi

copy=$1
l=${#copy}
for((i=$l-1;i>=0;i--)); do 
	rev="$rev${copy:$i:1}"
done

for file in `find -maxdepth 1 -name "*.$1"`; do
  mv "$file" "${file%.$1}.$rev"
done

