#!/bin/bash
if [[ -z $1 || -z $2 ]];	then
	echo "Please pass arguments"
	exit 1
fi

n=$1
m=$2
 
for i in `seq $n` ; do
	tr -dc A-Za-z0-9 < /dev/urandom | head -c$m
	echo	
done

