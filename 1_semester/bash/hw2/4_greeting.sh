#!/bin/bash
if [ "$(id -u)" != "0" ]; then
	echo Program should be run as root
	exit 1
fi

if [ -e /etc/bash.bashrc.bak ]; then
	rm /etc/bash.bashrc
	mv /etc/bash.bashrc.bak /etc/bash.bashrc
	echo Greeting unistalled!
else
	cp /etc/bash.bashrc /etc/bash.bashrc.bak
	printf 'if [ "$(id -u)" == "0" ]; then
	echo Hi Master of the Universe The Great and The Allmighty! Let me do whatever you want My Lord!
else
	echo Hello $(whoami)!
fi
' >> /etc/bash.bashrc
	echo Greeting installed!
fi

