#!/bin/bash
if [[ -z $1 ]];	then
	echo No arguments
	exit 1
fi

if [[ $1 < 1 ]]; then
	echo Argument must be greater than 0
	exit 0
fi

function fibonacci() {
	case $1 in
		1) echo 1 ;;
		2) echo 1 ;;
		*) 
			let prev=$1-1
			let	preprev=$1-2
			echo $((`fibonacci $prev` + `fibonacci $preprev`))
	esac	
}

fibonacci $1

