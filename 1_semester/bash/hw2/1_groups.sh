#!/bin/bash
if [[ -z $1 ]];	then
	echo No argument
	exit 1
fi
 
grep -P ^$1: /etc/group | cut -d: -f4 | sed '/^$/d' | sed -e 's/,/\n/g' | wc -l

