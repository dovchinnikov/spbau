#!/bin/bash
if [ -z $1 ];	then
	echo "No argument"
	exit 1
fi

function viewArchive() {
	case ${1} in
		*.tar)	tar -tvf $1 ;;
		*.gz)	tar -tvf $1 ;;
		*.bz2)	tar -tvf $1 ;;
		*.zip)	unzip -l $1 ;;			
		*)		echo Extension unsupported 
	esac
}

viewArchive $1

