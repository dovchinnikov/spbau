def generatecombinations(a, k):
    def recursive(arr, c):
        if c == 0 or len(arr) == 0:
            yield []
            return
        if c == 1:
            yield from [[i] for i in arr]
            return
        yield from [[arr[0]] + i for i in recursive(arr[1:], c - 1)]
        yield from recursive(arr[1:], c)

    a.sort()
    return recursive(a, k)


aa = [1, 4, 2, 3, 5, 8, 7]
kk = 3
print(list(generatecombinations(aa, kk)))