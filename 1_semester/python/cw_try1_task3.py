# -*- coding: utf-8 -*-

from difflib import ndiff
from collections import deque


def checkedge(s1, s2):
    diff = list(ndiff(s1, s2))
    pluses = sum([1 for s in diff if s[0] == '+'])
    minuses = sum([1 for s in diff if s[0] == '-'])
    if pluses == 0 and minuses == 1:
        return True
    if pluses == 1 and minuses == 0:
        return True
    if pluses == 1 and minuses == 1:
        signs = [s[0] for s in diff]
        if signs.index('-') + 1 == signs.index('+'):
            return True
    return False


def buildgraph(lst):
    l = len(lst)
    return [[to for to in range(l) if checkedge(lst[fr], lst[to])] for fr in range(l)]


def bfs(g, s, e):
    l = len(g)
    parent = [-1] * l
    done = [False] * l
    queue = deque()
    result = []
    queue.append(s)
    done[s] = True
    while not len(queue) == 0:
        vertex = queue.popleft()
        if vertex == e:
            while not vertex == s:
                result.append(vertex)
                vertex = parent[vertex]
            result.append(s)
            return result
        for adjacent in g[vertex]:
            if not done[adjacent]:
                parent[adjacent] = vertex
                done[adjacent] = True
                queue.append(adjacent)
    return []

dictionary = [u'МАК',
        u'МАТ',
        u'МАГ',
        u'МАЙ',
        u'ЧАЙ',
        u'ПОТ',
        u'ЧАН']
start = u'МИГ'
end = u'ЧАС'
dictionary.append(start)
dictionary.append(end)

graph = buildgraph(dictionary)

path = [dictionary[i] for i in bfs(graph, dictionary.index(start), dictionary.index(end))[::-1]]
if len(path) == 0:
    print 'Impossible'
else:
    for hop in path:
        print hop