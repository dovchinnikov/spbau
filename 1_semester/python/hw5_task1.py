def abstract(f):
    return "abstract"


class Abstract(type):
    def __call__(cls, *args, **kwargs):
        for k, v in cls.__dict__.items():
            if v == "abstract":
                raise NotImplementedError("This class has abstract methods")
        for base in cls.__bases__:
            for k, v in base.__dict__.items():
                if k not in cls.__dict__.keys() and v == "abstract":
                    raise NotImplementedError("This class has abstract methods")
        return super(Abstract, cls).__call__(*args, **kwargs)


class Interface(object, metaclass=Abstract):
    @abstract
    def foo(self): pass


class Interface2(Interface):
    pass


class Implementation(Interface):
    def foo(self): print("It's me!")



b = Implementation()
b.foo()
a = Interface()     #will fail
c = Interface2()    #will fail too