from random import random
from queue import PriorityQueue


def shuffle(it, n):
    q = PriorityQueue()
    count_yielded = 0
    count_got = 0
    for e in it:
        new_index = count_got + random() * (n + 1)
        q.put((new_index, e))
        if count_yielded + n < count_got:
            yield q.get()[1]
            count_yielded += 1
        count_got += 1

    while not q.empty():
        yield q.get()[1]


N = 20
C = 500
a = list(range(C))
b = list(shuffle(a, N))
print(a)
print(b)

m = map(lambda x, y: abs(x - y), a, b)
print(max(m))

