#!/usr/bin/env python


def task1():
    def addAll(number, lst, N):
        if number > N and number != 1:
            return
        if number not in lst:
            lst.append(number)
        addAll(number * 3, lst, N)
        addAll(number * 5, lst, N)

    l = []
    N = 100
    # input("Input N: ")
    addAll(1, l, N)
    l.sort()
    return l


def task3():
    def fib(N):
        lst = [1, 1]
        a = 2
        while a < N:
            lst.append(a)
            i = len(lst)
            a = lst[i - 1] + lst[i - 2]
        return lst

    lst = fib(1000)
    # print(lst)
    s = 0
    for i in lst:
        if i % 2 == 0:
            s += i
    return s


def task5(l):
    return list(set(l))


def task6(l):
    i = 0
    out = []
    while i < len(l):
        out.append(tuple(l[i:i + l[i]]))
        i += l[i]
    return out


print "Task 1: ", task1()
print "Task 2: ", task3()
print "Task 5: ", task5([1, 2, 3, 4, 5, 1, 2, 3, 6, 7, 8, 6, 5, 6, 5, 9, 10])
print "Task 6: ", task6([1, 3, 5, 2, 2, 3, 5, 7, 2])
