def singleton(clz):
    class Singleton(clz):
        _instance = None
        _initialized = False

        def __new__(cls, *args, **kwargs):
            if Singleton._instance is None:
                Singleton._instance = super(Singleton, cls).__new__(cls)
                Singleton._initialized = False
            return Singleton._instance

        def __init__(self, *args, **kwargs):
            if not Singleton._initialized:
                super(Singleton, self).__init__(*args, **kwargs)
                Singleton._initialized = True

    Singleton.__name__ = clz.__name__
    return Singleton


@singleton
class A():
    #def __new__(cls, *args, **kwargs):
    #    print("new")
    #    return super(A, cls).__new__(cls)

    def __init__(self, phrase):
        self.hell = phrase
        print("init")

    def __call__(self, *args, **kwargs):
        print("call")

    def foo(self):
        print("hell " + self.hell)


a = A("yeah")
a()
a.foo()
b = A("god")
print(a is b)