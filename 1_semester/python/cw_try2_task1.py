__author__ = 'dany'


class ClassBase(type):
    def __init__(cls, *args, **kwargs):
        super(ClassBase, cls).__init__(*args, **kwargs)
        filename = cls.__name__ + ".py"
        with open(filename, "r") as file:
            body = file.read()
        fields = {}
        exec(body, {}, fields)
        for k, v in fields.items():
            setattr(cls, k, v)


class A(object, metaclass=ClassBase):
    pass


class B(object, metaclass=ClassBase): #will fail
    pass


a = A()
a.foo()
#B()