__author__ = 'dany'
from queue import PriorityQueue


def knights(start):
    shifts = [(i, j) for i in [-2, -1, 1, 2] for j in [-1, 1, -2, 2] if abs(i) != abs(j)]
    visited = [[False for i in range(9)] for j in range(9)]
    q = PriorityQueue()

    x, y = start
    q.put((0, x, y))
    visited[x][y] = True

    while not q.empty():
        current = q.get()
        path, x, y = current
        yield (x,y)
        cells = [(x + shift[0], y + shift[1]) for shift in shifts]
        for cell in cells:
            nx, ny = cell
            if 0 < nx < 9 and 0 < ny < 9 and not visited[nx][ny]:
                nxt = (path + 1, nx, ny)
                q.put(nxt)
                visited[nx][ny] = True


it = knights((4, 5))
lst = list(it)
print(lst)
print(len(lst))