#!/usr/bin/env python
# -*- coding: utf-8 -*-


def task1(filename):
    from pymorphy import get_morph
    import codecs

    f = codecs.open(filename, 'r', 'cp1251')

    m = get_morph('/opt/pymorphy/ru')
    l = [word.upper() for line in f for word in line.strip().split()]
    print "Total words:\t", len(l)
    w = list(set(l))
    print "Different:\t", len(w)
    s = 0
    for word in w:
        info = m.get_graminfo(word)
        if len(info) == 1:
            if info[0]['class'] == u'С':
                s += l.count(word)
                print 'Nouns count:\t{0}\r'.format(s)
    print 'Nouns count:\t{0}'.format(s)


def task2():
    def primes(N):
        l = list(range(2, N))
        for x in l:
            n = x
            while x * n <= N:
                if x * n in l:
                    l.remove(x * n)
                n += 1
        return l

    def doublePrimes(N):
        ps = primes(N)
        out = []
        for prime in ps:
            for otherPrime in ps[ps.index(prime):]:
                doublePrime = prime * otherPrime
                if doublePrime < N:
                    out.append(tuple([prime, otherPrime, doublePrime]))
                else:
                    break
        return out

    return doublePrimes(100)


def task3():
    class matrix(list):
        def __str__(self):
            out = ""
            if isinstance(self, list):
                out = "(" + "\n ".join(",\t".join(str(col) for col in row) for row in self) + ")"
            else:
                pass
            return out

        def __mul__(self, other):
            if len(self[0]) != len(other):
                raise Exception(
                    "Number of cols in first matrix should be equal with the number of rows in second matrix")

            def mul(a, b):
                return a * b

            # transponed = [[row[i] for row in other] for i in range(len(other))]
            return matrix([[sum(map(mul, row, col)) for col in zip(*other)] for row in self])

    m1 = matrix([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
    m2 = matrix([[1, 2, 3], [1, 3, 2], [3, 1, 2], [3, 2, 1]])

    print "Matrix multiplication"
    print m1
    print m2
    print m1 * m2
    print m2 * m1

# task1('/data/repositories/lt1.txt')
print task2(), "\n"
task3()
