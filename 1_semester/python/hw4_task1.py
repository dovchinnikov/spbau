from math import sin
from math import cos
from math import pi


class Vector():
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return "(x=%d, y=%d, z=%d)" % (self.x, self.y, self.z)


class Matrix(list):
    def __str__(self):
        return "(" + "\n ".join(", ".join(str(col) for col in row) for row in self) + ")"

    def __add__(self, other):
        return Matrix(map(lambda a, b: map(lambda x, y: x + y, a, b), self, other))

    def __mul__(self, other):
        if isinstance(other, int) or isinstance(other, long) or isinstance(other, float):
            return Matrix([[col * other for col in row] for row in self])
        if isinstance(other, Vector):
            return self * Matrix([[other.x], [other.y], [other.z]])
        if isinstance(other, Matrix):
            return Matrix([[sum(map(lambda x, y: x * y, row, col)) for col in zip(*other)] for row in self])
        return "lol"

    def __rmul__(self, other):
        if isinstance(other, Vector):
            return Matrix([[other.x, other.y, other.z]]) * self
        if isinstance(other, (int, float, long)):
            return self * other

    def __invert__(self):
        m = self
        det = m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])
        det -= m[0][1] * (m[1][0] * m[2][2] - m[2][0] * m[1][2])
        det += m[0][2] * (m[1][0] * m[2][1] - m[2][0] * m[1][1])
        if det == 0:
            raise Exception("Cannot invert matrix: Matrix determinant is zero")

        a = [[0] * 3 for i in range(3)]
        v = (m[1][1] * m[2][2] - m[2][1] * m[1][2])
        a[0][0] = v
        a[1][0] = -(m[1][0] * m[2][2] - m[2][0] * m[1][2])
        a[2][0] = (m[1][0] * m[2][1] - m[2][0] * m[1][1])
        a[0][1] = -(m[0][1] * m[2][2] - m[2][1] * m[0][2])
        a[1][1] = (m[0][0] * m[2][2] - m[2][0] * m[0][2])
        a[2][1] = -(m[0][0] * m[2][1] - m[2][0] * m[0][1])
        a[0][2] = (m[0][1] * m[1][2] - m[1][1] * m[0][2])
        a[1][2] = -(m[0][0] * m[1][2] - m[1][0] * m[0][2])
        a[2][2] = (m[0][0] * m[1][1] - m[1][0] * m[0][1])
        return Matrix(a) * (1.0 / det)


class RotateMatrix(Matrix):
    def __init__(self, angle):
        a = (pi * angle) / 180
        m = [[cos(a), -sin(a), 0], [sin(a), cos(a), 0], [0, 0, 1]]
        super(RotateMatrix, self).__init__(m)


v = Vector(3, 2, 1)
m1 = Matrix([[10, 2, 3], [5, 6, 7], [9, 10, 11]])
m2 = Matrix([[1, 2, 3], [1, 3, 2], [3, 1, 2]])

print m1, "\n"
print m2, "\n"
print m1 + m2, "\n"
print m1 * 0.5, "\n"
print 2 * m1, "\n"
print v, "\n"
print m1 * v, "\n"
print v * m1, "\n"
print m1 * m2, "\n"
print m2 * m1, "\n"
print ~m1, "\n"
print (~m1) * m1
print RotateMatrix(4)