#!/usr/bin/env python


import random
import itertools


def task1():
    def merge(A, B):
        nextA = None
        nextB = None
        try:
            nextA = next(A)
        except StopIteration:
            try:
                while True:
                    yield next(B)
            except StopIteration:
                raise
        try:
            nextB = next(B)
        except StopIteration:
            try:
                while True:
                    yield next(A)
            except StopIteration:
                raise
        while True:
            if nextA < nextB:
                yield nextA
                try:
                    nextA = next(A)
                except StopIteration:
                    yield nextB
                    try:
                        while True:
                            yield next(B)
                    except StopIteration:
                        break
            else:
                yield nextB
                try:
                    nextB = next(B)
                except StopIteration:
                    yield nextA
                    try:
                        while True:
                            yield next(A)
                    except StopIteration:
                        break

    def mergeSort(A):
        lst = list(A)
        l = len(lst)
        if l <= 1:
            return iter(lst)
        else:
            return merge(mergeSort(iter(lst[0:l // 2])), mergeSort(iter(lst[l // 2:])))

    class RandomIterator:
        def __init__(self, N):
            self.counter = 0
            self.max = N

        def __iter__(self):
            return self

        def next(self):
            if self.counter < self.max:
                self.counter += 1
                return random.random() * 100
            raise StopIteration()

    # a = [1, 3, 5, 7, 9]
    # b = [2, 4, 6, 8, 10, 11, 12, 13, 14]
    # g = merge(iter([]), iter([]))
    # c = [2, 4, 6, 1, 42, 7, 89, 5, 345, 17, 90, 3, 9, 346]
    # m = mergeSort(iter(c))
    r = RandomIterator(100)
    m = mergeSort(r)
    try:
        while True:
            print next(m)
    except StopIteration:
        pass


def task2_1(N):
    for i in ((0 if i + j < N // 2 or i + j >= N + N // 2 or i - j > N // 2 or j - i > N // 2 else 1
               for j in range(N)) for i in range(N)): print list(i)


def task2_2():
    a = [("fdf", 1), ("zxc", 3), ("rqe", 2), ("xvb", 1), ("dfg", 2),
         ("fss", 2), ("dfh", 3), ("hgg", 1), ("h65y", 3), ("sdf", 1)]
    for i in (iter(sorted(grp, key=lambda c: c[0]))
              for key, grp in itertools.groupby(
            sorted(a, key=lambda c: c[1]),
            lambda c: c[1])):
        print list(i)


task1()
task2_1(31)
task2_2()

