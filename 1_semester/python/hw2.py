#!/usr/bin/env python


def task1():
    chars = [chr(a) for r in [range(0x0391, 0x03a2), range(0x3a3, 0x3aa), range(0x3b1, 0x03ca)] for a in r]
    print(" ".join(chars))


class LongLongLongInteger(object):
    BASE = 100000
    POWER = 0
    while BASE // 10 > 0:
        BASE //= 10
        POWER += 1
    BASE = 10 ** POWER
    PATTERN = "%0{0}d".format(POWER)

    def __normalize(self):
        base = LongLongLongInteger.BASE
        l = len(self.inner)
        for i in range(l):
            if self.inner[i] + 1 > base:
                div = self.inner[i] // base
                self.inner[i] %= base
                if i < l - 1:
                    self.inner[i + 1] += div
                else:
                    self.inner.append(div)

    def __init__(self, number="", inner=None):
        if isinstance(inner, list):
            self.inner = inner
        else:
            power = self.POWER
            number = str(number)[::-1]
            inner = []
            while len(number) > 0:
                inner.append(int(number[0:power][::-1]))
                number = number[power:]
            if len(inner) < 1:
                inner = [0]
            self.inner = inner

    def __add__(self, other):
        def add(a, b):
            if a is None:
                return b
            if b is None:
                return a
            return a + b

        inner = list(map(add, self.inner, other.inner))
        out = LongLongLongInteger(inner=inner)
        out.__normalize()
        return out

    def __str__(self):
        if not self.inner:
            return "0"
        else:
            return str(self.inner[-1]) + "".join(self.PATTERN % digit for digit in self.inner[:-1][::-1])

    def __mul__(self, other):
        lself = len(self.inner)
        lother = len(other.inner)
        inner = [None] * (lself + lother - 1)
        for i in range(lself):
            for j in range(lother):
                if not inner[i + j]:
                    inner[i + j] = 0
                inner[i + j] += self.inner[i] * other.inner[j]
        out = LongLongLongInteger(inner=inner)
        out.__normalize()
        return out


def task2():
    w = LongLongLongInteger("10203047869834752344")
    y = LongLongLongInteger("97516987203467591023134956234785")
    print(w + y)
    print(w * y)


def task3():
    from urllib.request import urlopen
    from xml.etree import ElementTree

    def pprint(elem, level=0):
        tab = "\t" * level

        if elem.text and elem.text.strip():
            print("%s<%s>%s</%s>" % (tab, elem.tag, elem.text, elem.tag))
        else:
            print("%s<%s>" % (tab, elem.tag))
            for child in elem:
                pprint(child, level + 1)
            print("%s</%s>" % (tab, elem.tag))

    # url = "http://bash.im/rss/"
    url = "http://habrahabr.ru/rss/hubs/"
    f = urlopen(url)
    xml = f.read()
    pprint(ElementTree.fromstring(xml))


def task4():
    return 0


task1()
task2()
task3()
