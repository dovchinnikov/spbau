from datetime import datetime
import sys


def meterfactory(program_start):
    def diff(a, b):
        return (b - a).total_seconds() * 1000

    def pmeter(func):
        def wrapper(*args, **kwargs):
            print("%.3fms %s" % (diff(program_start, datetime.now()), func.__name__))
            start = datetime.now()
            func(*args, **kwargs)
            print("+%.3fms" % diff(start, datetime.now()))

        return wrapper

    return pmeter


if __name__ == "__main__":
    f = open(sys.argv[1], "r")
    script = ""
    for line in f.readlines():
        if line[:4] == "def ":
            script += "\n@meter\n"
        script += (line + "\n")
    ps = datetime.now()
    meter = meterfactory(ps)
    f.close()
    exec(script, globals())
