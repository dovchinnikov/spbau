class DeniedKeyException(Exception):
    pass


class AdvancedDict(dict):
    def __init__(self, forbidden=None, *args, **kw):
        super(AdvancedDict, self).__init__(*args, **kw)
        if not forbidden: forbidden = []
        self.__dict__["forbidden"] = forbidden
        self.__dict__["order"] = []

    def __getitem__(self, key):
        k = None
        if isinstance(key, int):
            key = self.order[key]
            if key in self:
                k = key
        if not key in self.forbidden:
            if key in self:
                k = key
            key = key.replace("_", " ")
            if key in self:
                k = key
        if k:
            return super(AdvancedDict, self).__getitem__(k)
        raise KeyError(key)

    def __setitem__(self, key, value):
        if key in self.forbidden:
            raise DeniedKeyException()
        super(AdvancedDict, self).__setitem__(key, value)
        if not key in self.order:
            self.order.append(key)

    def __getattr__(self, key):
        return self[key]

    def __setattr__(self, name, value):
        self[name] = value

    def __delitem__(self, key):
        del self[key]
        self.order.remove(key)


d = AdvancedDict(forbidden=("lal", "trash", "ololo"))
d["foo"] = "bar"
d["hell"] = "yeah"
print d
print d[0]
print d[1]
d["space delimiter"] = "sssss"
print d.space_delimiter
