from datetime import datetime


def spy(func):
    def wrapper(*args, **kwargs):
        wrapper.__log.append((datetime.now(), args, kwargs))
        func(*args, **kwargs)

    wrapper.__log = []
    return wrapper


def bond(func):
    return iter(func.__log)


@spy
def foo(a, b):
    print(a + b)


@spy
def bar(c, d):
    print(c - d)


foo(1, b=3)
bar(4, 9)
foo(b=7, a=1)

for call in bond(bar):
    print(call)

for call in bond(foo):
    print(call)