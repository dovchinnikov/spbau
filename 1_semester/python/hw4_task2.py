class Student():
    def __init__(self, name):
        s = name.split(":")
        if len(s) > 1:
            self._name = s[0]
            self._data = [int(i) for i in s[1].split()]
        else:
            self._name = name
            self._data = []

    def __str__(self):
        return self._name + ": " + " ".join([str(g) for g in self._data])

    def gav(self):
        return sum(self._data) / len(self._data)

    def addGrade(self, grade):
        self._data.append(grade)


class StudentDB():
    def __init__(self, filename):
        f = open(filename, "r")
        next(f)
        self._data = [Student(line) for line in f]
        self._filename = filename

    def add(self, student):
        if isinstance(student, Student):
            self._data.append(student)
        if isinstance(student, str):
            self._data.append(Student(student))

    def remove(self, student):
        if isinstance(student, Student):
            self._data.remove(student)
        if isinstance(student, str):
            for i in range(len(self._data)):
                if self._data[i]._name == student:
                    self._data.remove(self._data[i])
                    break

    def export(self):
        f = open(self._filename, "w")
        f.write("%d\n" % len(self._data))
        map(lambda s: f.write("%s\n" % s), self._data)

    def maxGrade(self):
        return max(self._data, key=lambda st: st.gav())


db = StudentDB("hw4_task2.txt")
s = Student("Dany Dany")
s.addGrade(5)
s.addGrade(5)
s.addGrade(5)
s.addGrade(5)
s.addGrade(5)
db.add(s)
print db.maxGrade()
db.remove("Dany Dany")
print db.maxGrade()
db.export()
