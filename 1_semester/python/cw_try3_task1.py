class OverrideFinalException(Exception):
    pass


def final(f):
    f.__final__ = True
    return f


class Finals(type):
    def __init__(cls, what, *args, **kwargs):
        def check(clz, fname):
            for parent in clz.__bases__:
                check(parent, fname)
                for ke, va in parent.__dict__.items():
                    if ke == fname and hasattr(va, "__final__"):
                        raise OverrideFinalException("Cannot override %s method of class %s in class %s"
                                                     % (fname, parent.__name__, cls.__name__))
            return False

        super().__init__(what)
        print(cls.__bases__)
        for k, v in cls.__dict__.items():
            if callable(v):
                check(cls, k)


class A(object, metaclass=Finals):
    @final
    def foo(self):
        print("I'm final")


class B(A):
    def foo(self):
        pass

    @final
    def bar(self):
        pass


class C(B):
    def foo(self):
        pass

    def bar(self):
        pass