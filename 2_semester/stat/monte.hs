import System.Random
 
f :: Double -> Double
f x = sin (x + exp (-x))

f1 :: Double -> Double
f1 x = (sin x) * exp (-x**2)

f2 :: Double -> Double
f2 x = (sin x)**(-2/3)

main = do
    seed <- newStdGen
    let n = 1000000
    let rs = take n $ randoms seed
    let rs1 = take n $ (randomRs (0, 10) seed :: [Double])
    print $ (sum $ map f rs) / (fromIntegral n)
    print $ (sum $ map f1 rs1 ) / (fromIntegral n)
    print $ (sum $ map f2 rs) / (fromIntegral n)
 
