import System.Random
import Control.Monad
import Control.Applicative 

eta = 10
n = 500
m = 1000

main = do
    mapM_ dostuff [1..n]
    where 
        dostuff n = do    
            dat <- replicateM m $ replicateM n r
            let stats = [[f m | m <-dat ] | f <-[stat1, stat2, stat3 n]]
            let mse = [(sqrt . sum $ map (\t -> (t - eta)^2) stat) / (fromIntegral m) | stat <- stats] 
            putStrLn $ show n ++ ":\t" ++ show mse

r :: IO Double
r = randomRIO (0, eta)

stat1 :: [Double] -> Double
stat1 a = 2 * (sum a) / (fromIntegral $ length a)

stat2 :: [Double] -> Double
stat2 a = maximum a

stat3 :: Int -> [Double] -> Double 
stat3 n a = (fromIntegral $ n + 1) / (fromIntegral n) * stat2 a
