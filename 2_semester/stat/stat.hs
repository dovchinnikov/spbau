module Stat where 

import System.Environment(getArgs)
import Control.Monad (replicateM)
import System.Random.MWC (GenIO)
import Statistics.Distribution (Distribution, ContGen, ContDistr, genContVar, density)
import Math.Statistics (average)
import Data.List (sort)

lns :: [Int] -> [Double]
lns = map (log . fromIntegral)

plotData :: [Int] -> [[Double]] -> [[(Double, Double)]]
plotData ns = map (zip $! lns ns) . map (map log) 
   
compute :: (Distribution d, ContGen d, ContDistr d) => GenIO -> d -> [[Double] -> Double] -> Double -> Int -> Int -> IO [Double]
compute rng distr estimates eta m n  = do 
    dat <- replicateM m $! replicateM n $! genContVar distr rng     -- IO [[Double]]
    let stats = map (\f -> map f dat) estimates
    return [sqrt $ sum [(t - eta)**2 | t <- stat] / fromIntegral m | stat <- stats]


quartilAvg :: [Double] -> Double
quartilAvg list = (average . trim 25) list 

trim :: Int -> [Double] -> [Double] 
trim p list = (take (l * (100 - 2*p) `div` 100) . drop (l * p `div` 100) . sort) list
    where
        l = length list

trimmedMean :: [Double] -> Double
trimmedMean = average . trim 5

winsorizedMean :: [Double] -> Double
winsorizedMean list = average $ (replicate partLen $ minimum trimmed) ++ trimmed ++ (replicate partLen $ maximum trimmed)
    where 
        partLen = (length list) * 5 `div` 100
        trimmed = trim 5 list


cumsum :: [Double] -> [Double]
cumsum = scanl1 (+)

cummean :: [Double] -> [Double] 
cummean l = zipWith (/) (cumsum l) [1..] 

cumvariance :: [Double] -> [Double]
cumvariance l = zipWith (-) (cummean $ map (**2) l) (cummean l)

