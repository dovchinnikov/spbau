import System.Environment(getArgs)
import Control.Monad (replicateM)
import System.Random.MWC (GenIO, createSystemRandom)
import Statistics.Distribution (Distribution, ContGen, ContDistr, genContVar, density)
import Statistics.Distribution.Beta (BetaDistribution, betaDistr)
import Math.Statistics (average)
import Graphics.Gnuplot.Simple
import qualified Stat as S

m = 10
eta = 3.05005
fun x = (sin x)**(-2/3)

main = do
    rng <- createSystemRandom
    dat <- sequence $ map (\d -> mapM (compute rng d m) ns) $! [d1, d2, d3]
    
    plotLists [ Aspect $ Ratio 1
                , XLabel "log_e N"
                , YLabel "log_e RMSE"
                , Title "f(x) = (sin x)^(-2/3), B(1,1)" 
                ] $ S.plotData ns dat
    putStrLn "Plot finished"
    getLine
    where   
        ns = map (ceiling . exp) [1, 1.5 .. 13]
        
    
compute :: (Show d, Distribution d, ContGen d, ContDistr d) => GenIO -> d -> Int -> Int -> IO Double
compute rng d m n  = do 
    putStrLn $ show d ++ "; m=" ++ show m ++ "; n=" ++ show n
    dat <- replicateM m $ replicateM n $ fmap (g d) $ genContVar d rng      -- IO [[ Double ]] 
    stat <- return $! map average $! dat                                           -- [Double]
    return $! sqrt $ sum [(t - eta)**2 | t <- stat ] / fromIntegral m           
    where 
        g d x = fun x / density d x

         
d1 :: BetaDistribution -- uniform
d1 = betaDistr 1 1

d2 :: BetaDistribution
d2 = betaDistr (1/3) 1

d3 :: BetaDistribution
d3 = betaDistr 0.5 0.5


