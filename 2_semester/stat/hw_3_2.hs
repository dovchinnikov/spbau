import System.Random.MWC (createSystemRandom)
import Statistics.Distribution.Uniform (UniformDistribution, uniformDistr)
import Math.Statistics (average, median)
import Graphics.Gnuplot.Simple (plotLists, Attribute (..), Aspect (..))
import Data.List (transpose)
import Stat

m = 100
a = 10

main = do
    rng <- createSystemRandom
    dat <- fmap (plotData ns) $! fmap transpose $! mapM (compute rng d estimates a m) $! ns
    
    _ <- plotLists [ Aspect $ Ratio 1
                    , XLabel "log_e N"
                    , YLabel "log_e RMSE"
                    ] $! dat

    putStrLn "Plot finished"
    getLine
    where 
        d = distr a
        estimates = [ average
                    , median
                    , quartilAvg
                    , trimmedMean
                    , winsorizedMean
                    ]
    
        
ns :: [Int]
ns = map (ceiling . exp) [1, 1.5 .. 10]

distr :: Double -> UniformDistribution
distr a = uniformDistr (a - 1) (a + 1)

