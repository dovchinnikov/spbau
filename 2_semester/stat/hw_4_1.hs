import System.Environment(getArgs)
import Control.Monad (replicateM)
import System.Random.MWC (GenIO, createSystemRandom)
import Statistics.Distribution (Distribution, ContGen, ContDistr, genContVar, density, variance)
import Statistics.Distribution.Beta (BetaDistribution, betaDistr)
import Math.Statistics (average)
import Graphics.Gnuplot.Simple
import qualified Graphics.Gnuplot.Frame as F
import qualified Graphics.Gnuplot.Advanced as A
import qualified Stat as S
import Control.DeepSeq (deepseq, ($!!))

n :: Int
n = 10^3

eta :: Double -- точное значение интеграла
eta = 3.05005   

fun :: Double -> Double
fun x = (sin x)**(-2/3) -- функция

tl :: Double -- уровень доверия
tl = 0.95


main = do
    rng <- createSystemRandom
    dat <- replicateM n $ fmap (g d2) $ genContVar d2 rng -- генерируем выборку n значений
    
    putStrLn $ show $ variance d2
    putStrLn $ show $ last $ S.cumvariance dat    
        
    plotDots [ --Aspect $ Ratio 1
                 XLabel "log_e N"
                , YLabel "log_e RMSE"
                , Title "f(x) = (sin x)^(-2/3), B(1,1)" 
                ] $! zipWith (\i x -> (fromIntegral i, x)) [1 .. n] dat
    putStrLn "Plot finished"
    where   
        ns = map (ceiling . exp) [1, 1.5 .. 13]
        g d x = fun x / density d x
         
d1 :: BetaDistribution -- uniform
d1 = betaDistr 1 1

d2 :: BetaDistribution
d2 = betaDistr (1/3) 1

d3 :: BetaDistribution
d3 = betaDistr 0.5 0.5

