import System.Random
import Control.Monad
import Control.Applicative 
import GSL.Random.Gen 
import GSL.Random.Dist
import Data.List 

n = 500
m = 1000
a = 1

main = do
    r <- rng
    mapM_ (dostuff r) [1,10,100,1000]
    where 
        dostuff r n = do    
            dat <- replicateM m $ replicateM n (getCauchy r a)            
            let sorted = map sort dat
            let stats = [[f m | m <- sorted ] | f <- [median n, stat2 n, stat3 n]]
            let mse = [(sqrt . sum $ map (\t -> t^2) stat) / (fromIntegral m) | stat <- stats] 
            putStrLn $ show n ++ ":\t" ++ show mse

rng :: IO RNG
rng = newRNG $ mt19937

median :: Int -> [Double] -> Double
median len l    | even len = (l !! (middle - 1) + l !! middle ) / 2.0
                | otherwise = l !! middle
    where  
        middle = len `div` 2 
         
stat2 :: Int -> [Double] -> Double
stat2 n (_:[]) = 0
stat2 n (_:_:[]) = 0
stat2 n l = ((sum . tail . init) l) / fromIntegral (n - 2)

stat3 :: Int -> [Double] -> Double 
stat3 n a = avg (n - (10 `percent` n)) $ (take (n - (10 `percent` n)) . drop (5 `percent` n)) a 

avg :: Fractional a => Int -> [a] -> a
avg n l = (sum l) / fromIntegral n

percent p n = floor (fromIntegral n / 100 * p)
