-- 1. fib n вовзращает n-ое число Фибоначчи
--    (1 балл)
fib :: Integer -> Integer
fib n = f n (0, 1)
    where
        f n (a, b) | n == 0 = b
                   | otherwise = f (n-1) (b, a + b)


-- 2a. Написать функцию, возвращающую количество цифр числа.
--     Для целочисленного деления можете использовать функции div и mod.
--    (0.5 балла)
numberOfDigits :: Integer -> Integer
numberOfDigits n | -10 < n && n < 10 = 1 
                 | otherwise = 1 + (numberOfDigits $ div n 10)      

-- 2b. Написать функцию, возвращающую сумму цифр числа.
--    (0.5 балла)
sumOfDigits :: Integer -> Integer
sumOfDigits n | n < 0 = sumOfDigits (-n)
              | n == 0 = 0
              | otherwise = mod n 10 + (sumOfDigits $ div n 10)


-- 3. gcd' возвращает НОД.
--    (1 балл)
gcd' :: Integer -> Integer -> Integer
gcd' a 0 = a
gcd' 0 b = b
gcd' a b | a < 0 = gcd' (-a) b
         | b < 0 = gcd' a (-b)
         | a < b = gcd' a (b-a)
         | a > b = gcd' (a-b) b
         | otherwise = a
        

-- 4. minp p возвращает минимальное по модулю число x такое, что p x == True. Если такого x не существует, minp не завершается.
--    (1 балл)
minp :: (Integer -> Bool) -> Integer
minp p = f p 0
    where 
        f p x | p x || p (-x) = x
              | otherwise = f p (x+1)


-- 5. integral f a b возвращает значение определенного интеграла функции f на отрезке [a,b].
--    Для реализации можете использовать метод трапеций.
--    (2 балла)
integral :: (Double -> Double) -> Double -> Double -> Double
integral f a b | abs (a - b) < delta = 0
               | otherwise = ((f a) + (f $ a + delta)) * delta / 2 + (integral f (a + delta) b)
        where 
            delta = 0.0001

integral' :: (Double -> Double) -> Double -> Double -> Double
integral' f a b = sum traps 
    where delta = (b - a) / 10000
          values = [f x | x <- [a, a + delta .. b]]
          traps = zipWith (\x1 x2 -> (x1 + x2) / 2 * delta) values (tail values)


-- 6. Реализуйте оператор примитивной рекурсии rec, используя функцию (-), укажите тип rec.
--    (1 балл)
rec :: a -> (Integer -> a -> a) -> Integer -> a
rec f _ 0 = f
rec f g n = g (n-1) (rec f g $ n-1)


-- 7. Реализуйте факториал при помощи rec.
--    (1 балл)
facRec :: Integer -> Integer
facRec n = rec 1 (\x y->(x+1)*y) n


-- 8. Реализуйте факториал при помощи fix.
--    (1 балл)
facFix :: Integer -> Integer
facFix = fix (\f n -> if n==0 then 1 else n*(f $ n-1))
    where fix f = f (fix f)

