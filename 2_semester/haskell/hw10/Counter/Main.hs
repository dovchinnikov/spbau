-- (0.5 балла)
import Counter

-- Эти две функции отличаются от обычных тем, что, вызывая tick, они считают сколько шагов заняло их выполнение.
filter' :: (a -> Bool) -> [a] -> Counter [a]
filter' _ [] = return []
filter' p (x:xs) = tick >> fmap (if p x then (:)x else id) (filter' p xs)
    
append :: [a] -> [a] -> Counter [a]
append a [] = return a
append a (x:xs) = tick >> append (a++[x]) xs

-- Реализуйте qsort через filter' и append
qsort :: Ord a => [a] -> Counter [a]
qsort [] = return []
qsort (x:xs) = do
    tick
    l <- filter' ((>) x) xs >>= qsort
    r <- filter' ((<=) x) xs >>= qsort
    append l (x:r)

-- Первый вызов должен занимать большее количество тиков ~ в 2 раза
main = do    
    print $ runCounter $ qsort [1..15]
    print $ runCounter $ qsort [8,4,12,2,6,10,14,1,3,5,7,9,11,13,15]
