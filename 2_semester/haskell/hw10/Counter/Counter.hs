-- (0.5 балла)
module Counter
    ( Counter
    , tick
    , runCounter
    ) where

-- Монада Counter считает количество тиков, т.е. вызовов функции tick
data Counter a = Counter Int a deriving Show

-- Возвращает результат вычислений и количество тиков
runCounter :: Counter a -> (a, Int)
runCounter (Counter i a) = (a,i)

instance Functor Counter where
    fmap f (Counter i x) = Counter i (f x)

instance Monad Counter where
    return = Counter 0
    (Counter i a) >>= f = case f a of Counter k b -> Counter (i+k) b

tick :: Counter ()
tick = Counter 1 ()
