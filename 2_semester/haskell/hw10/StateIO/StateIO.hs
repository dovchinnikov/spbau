-- (1 балл)
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module StateIO
    ( StateIO
    , runStateIO, execStateIO, evalStateIO
    ) where

import Control.Monad.State
import Data.IORef

newtype StateIO s a = StateIO { getStateIO :: IORef s -> IO a }

instance Monad (StateIO s) where
    return a = StateIO (\_-> return a)
--  >>= :: StateIO s a -> (a -> StateIO s b) -> StateIO s b
    (>>=) (StateIO f) g = StateIO (\s -> do 
                                    a <- f s    -- IO a
                                    case g a of StateIO gg -> gg s)

instance MonadState s (StateIO s) where
    get = StateIO readIORef
    put s = StateIO (\r -> writeIORef r s >> return ())

runStateIO :: StateIO s a -> s -> IO (a,s)
runStateIO (StateIO f) s = do 
    ref <- newIORef s
    a <- f ref
    res <- readIORef ref
    return (a,res)

execStateIO :: StateIO s a -> s -> IO s
execStateIO m s = do 
    (a,s) <- runStateIO m s
    return s

evalStateIO :: StateIO s a -> s -> IO a
evalStateIO m s = do 
    (a,s) <- runStateIO m s
    return a
