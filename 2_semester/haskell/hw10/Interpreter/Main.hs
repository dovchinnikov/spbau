-- (1.5 балла)
{-# LANGUAGE TupleSections #-}

import Control.Monad
import qualified Data.Map as M
import Test.HUnit

import Expr
import Eval
import Utils

getInt :: Eval Value -> Eval Integer
getInt m = do 
    v <- m
    case v of 
        (I i) -> return i
        _ -> fail "cannot convert to Integer"   

getBool :: Eval Value -> Eval Bool
getBool m = do
    v <- m
    case v of 
        (B b) -> return b
        _ -> fail "cannot convert to Bool"

if' :: Eval Value -> Eval () -> Maybe (Eval ()) -> Eval ()
if' c t e = do
    b <- getBool c
    if b then t else maybe (return ()) id e
        
evalExpr :: Expr -> Eval Value
evalExpr (Const v)  = return v
evalExpr (Var s)    = getVar s
evalExpr (BinOp op l r) = case op of
    Plus    -> go (+)   getInt  I
    Mul     -> go (*)   getInt  I
    Minus   -> go (-)   getInt  I
    And     -> go (&&)  getBool B
    Or      -> do
        rr <- (getBool $ evalExpr l) `mplus` (getBool $ evalExpr r)
        if rr  
            then return $ B rr
            else liftM B $ getBool $ evalExpr r   
    Less    -> go (<)   getInt  B
    Greater -> go (>)   getInt  B
    Equals  -> go (==)  id      B
    where
        go :: (a -> a -> b) -> (Eval Value -> Eval a) -> (b -> Value)-> Eval Value
        go op get ccc = do
            ll <- get $ evalExpr l
            rr <- get $ evalExpr r
            return $ ccc $ ll `op` rr

evalExpr (UnOp Neg e) = do
    i <- getInt $ evalExpr e
    return $ I $ -i
evalExpr (UnOp Not e) = do
    b <- getBool $ evalExpr e
    return $ B $ not b

evalStatement :: Statement -> Eval ()
evalStatement (Assign var val) = evalExpr val >>= update var
evalStatement (If c t me) = if' (evalExpr c) (evalProgram t) (fmap evalProgram me)
evalStatement a@(While cond body) = if' (evalExpr cond) (evalProgram body >> evalStatement a) Nothing

evalProgram :: Program -> Eval ()
evalProgram [] = return ()
evalProgram (x:xs) = evalStatement x >> evalProgram xs

------------------------------------------------------------------------------------------------
-- tests
------------------------------------------------------------------------------------------------

test1 = not_ (Var "x") ||| Var "y" <<< Const (I 3) &&& Var "z" === Var "y" &&&
    Const (I 5) <<< Var "y" +++ Const (I 7) *** Var "z" +++ Var "y" *** Const (I 3)

test2 = neg (Const $ I 5) +++ neg (Const $ I 3) *** Const (I 2) -.- Const (I 7)

test3 =
    [ "r" $= Const (I 1)
    , While (Var "n" >>> Const (I 0))
        [ "r" $= Var "r" *** Var "n"
        , "n" $= Var "n" -.- Const (I 1)
        ]
    ]

main = fmap (\_ -> ()) $ runTestTT $ test
    [ errorsCount (runEval (evalExpr test1) $ M.fromList [("x",I 3),("y",I 5),("f",I 5)]) ~?= 2
        -- 2 ошибки: неизвестная переменная z и несоответствие типов (в том месте, где вычисляется "!x")
    , let m = M.fromList [("x",B True),("y",I 5),("z",I 5)] in runEval (evalExpr test1) m ~?= (Just (B False), [], m)
    , let m = M.fromList [("x",B True),("y",I 2),("z",I 2)] in runEval (evalExpr test1) m ~?= (Just (B True ), [], m)
    , runEval (evalExpr test2) M.empty ~?= (Just (I (-18)), [], M.empty)
    , runEval (evalProgram test3) (M.fromList [("n",I 5)]) ~?= (Just (), [], M.fromList [("n",I 0),("r",I 120)])
    ]
  where
    errorsCount (_,es,_) = length es
