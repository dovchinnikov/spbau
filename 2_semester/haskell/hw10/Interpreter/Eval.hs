-- (1.5 балла)
module Eval
    ( Eval, runEval
    , Error, Store
    , update, getVar
    ) where

import qualified Data.Map as M
import Data.List
import Control.Monad

import Expr

type Error = String
type Store = M.Map String Value

newtype Eval a = Eval { runEval :: Store -> (Maybe a, [Error], Store) }

instance Monad Eval where
    return x = Eval (\s -> (Just x, [], s))
    Eval m >>= k = Eval (\s -> case m s of 
                                (Nothing, es, ss) -> (Nothing, es, ss)
                                (Just xx, es, ss) -> (\(a, ees, sss) -> (a, es ++ ees, sss)) $ runEval (k xx) ss
                        )
    fail e = Eval (\s -> (Nothing, [e], s))

-- MonadPlus - аналог Alternative для монад
-- mzero - вычисление, которое ничего не делает, сразу завершается неуспехом
-- mplus m1 m2 пытается выполнить m1, если тот завершился неуспехом, выполняет m2
-- Примеры использования этого класса есть в Utils.hs
instance MonadPlus Eval where
    mzero = Eval (\s -> (Nothing, [], s))
    mplus (Eval l) (Eval r) = Eval (\s -> case l s of 
                                        (Nothing, errs, store) -> (\(a, eee, sss)-> (a, errs++eee, sss)) $ r store
                                        res -> res)

update :: String -> Value -> Eval ()
update k v = Eval (\s -> (Just (), [], M.insert k v s))

getVar :: String -> Eval Value
getVar k = Eval (\s -> maybe (Nothing, [k ++ "not found"], s) (\x -> (Just x, [], s)) $ M.lookup k s)

