module Shell
    ( Shell, runShell
    -- Добавьте другие функции для работы с Shell по вкусу
    , getCurrentDirectory, putCurrentDirectory, listFiles
    ) where

import Control.Monad.Trans
import Control.Exception (try, IOException)
import qualified System.Directory as D

data Shell a = Shell { runCommand :: IO a}

instance Functor Shell where
    fmap f (Shell s) = Shell $ fmap f s

instance Monad Shell where
    return = return
    Shell a >>= f = Shell (do
                            res <- a
                            case f res of Shell b -> b)

-- В Shell можно будет выполнять произвольные IO действия, используя liftIO.
instance MonadIO Shell where
    liftIO action = Shell action

runShell :: Shell a -> IO a
runShell (Shell a) = a

getCurrentDirectory :: Shell String
getCurrentDirectory = liftIO D.getCurrentDirectory

putCurrentDirectory :: String -> Shell ()
putCurrentDirectory dir = liftIO $ do 
    r <- try $ D.setCurrentDirectory dir :: IO (Either IOException ())
    either (putStrLn . show) (const $ return ()) r


listFiles :: Shell [String]
listFiles = liftIO (D.getDirectoryContents =<< D.getCurrentDirectory)

