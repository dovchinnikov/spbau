-- Сейчас программа должна уметь следующее: считывает комманды с stdin, выполняет их, не падает (!)

module Main where

import Shell
import System.IO
import Control.Monad
import Control.Monad.Trans
import System.Cmd

-- Для работы со строкой ввода удобно испольовать библиотеку readline.

-- Функция command cmd args выполняет комманду cmd с аргументами args.
-- Есть несколько встроенных комманд: cd, pwd, ... потом еще добавим может быть.
-- Если это не встроенная команда, то пытаемся запустить внешнее приложение.
command :: String -> [String] -> Shell ()
command "pwd" _         = getCurrentDirectory >>= liftIO . putStrLn
command "cd" (dir:_)    = putCurrentDirectory dir
command "ls" _          = listFiles >>= liftIO . mapM_ putStrLn 
command cmd args        = liftIO $ (system $ unwords $ cmd:args) >> return ()

main = do
    putStr "$> " 
    hFlush stdout
    aaa <- fmap words getLine
    case aaa of 
        ["quit"] -> return ()
        [] -> main
        (cmd:args) -> do 
            runShell $ command cmd args
            main
    
