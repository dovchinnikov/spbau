import Network
import System.IO
import Control.Concurrent (forkIO)

main :: IO ()
main = do
    let port = 5555
    sock <- connectTo "localhost" $ PortNumber port
    putStrLn $ "Connected to " ++ show port
    forkIO $ readLoop sock
    writeLoop sock
    
readLoop :: Handle -> IO()
readLoop h = hGetLine h >>= putStrLn >> readLoop h

writeLoop :: Handle -> IO()
writeLoop h = getLine >>= hPutStrLn h >> writeLoop h

