import System.Environment
import System.IO
import Data.List

main = do 
    (inputfile:_) <- getArgs
    file <- readFile inputfile
    putStrLn $ unlines $ map unwords $ transpose $ map words $ lines file
    
