import System.Environment
import Control.Exception
import Control.Monad
import Data.List

main = do
    (pattern:xx) <- getArgs
    go pattern xx
    where
        go :: String-> [String] -> IO ()
        go _ [] = return ()
        go ptrn (x:xs) = do 
            result <- try (readFile x) :: IO (Either IOException String)
            case result of 
                Left ex -> go ptrn xs
                Right strings -> mapM_ (checkMatch ptrn) (lines strings) >> go ptrn xs

        checkMatch :: String -> String -> IO()
        checkMatch pattern s =  if isInfixOf pattern s   
                                then putStrLn s
                                else return()
                
