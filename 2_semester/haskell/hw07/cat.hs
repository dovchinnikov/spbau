import System.Environment
import Control.Exception

main = do
    getArgs >>= go'
    where
        go' :: [String] -> IO()
        go' [] = getLine >>= putStrLn >> go' []
        go' a = go a

        go :: [String] -> IO ()
        go [] = return ()
        go (x:xs) = do 
            result <- try (readFile x) :: IO (Either IOException String)
            case result of 
                Left ex -> go xs
                Right strings -> putStr strings >> go xs

