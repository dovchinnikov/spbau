import Network
import System.IO
import Control.Concurrent (forkIO)

main :: IO ()
main = do
    let port = 5555
    putStrLn $ "Listening on " ++ show port
    listenOn (PortNumber port) >>= sockHandler

sockHandler :: Socket -> IO ()
sockHandler sock = do
    (handle, host, port) <- accept sock
    putStrLn $ "Accepted connection " ++ host ++ ":" ++ show port
    forkIO $ clientLoop handle
    sockHandler sock

clientLoop :: Handle -> IO ()
clientLoop handle = hGetLine handle >>= hPutStrLn handle >> clientLoop handle

