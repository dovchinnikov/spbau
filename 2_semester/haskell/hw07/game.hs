import System.Random
import Control.Exception
main = do 
    number <- randomRIO (1,100)
    putStrLn "Guess wat"
    go 5 number    
    where 
        go :: Int -> Int -> IO()
        go 0 _ = putStrLn "YOU LOSER"
        go tries number = do
            line <- getLine
            parseResult <- try (evaluate (read line :: Int)) :: IO (Either SomeException Int)
            case parseResult of
                Left ex -> putStrLn "Not a number" >> go tries number
                Right t -> case number `compare` t of
                                EQ -> putStrLn "You're goddamn right!"
                                LT -> putStrLn "Number is less" >> go (tries - 1) number
                                GT -> putStrLn "Number is greater" >> go (tries - 1) number

