main = do
    putStrLn "Hello world"
    print $ 1000 <> 2000
    print $ 1000 <> 3000

if' :: Bool -> a -> a -> a
if' cond t e | cond = t
             | otherwise = e 

if'' True t _ = t
if'' False _ e = e

fac' :: (Num a, Eq a) => a -> a
fac' n | n == 0 = 1
       | otherwise = n * fac' (n - 1)

fac'' :: (Num a, Eq a) => a -> a
fac'' 0 = 1
fac'' n = n * fac'' (n - 1)

(<>) :: (Num a, Ord a) => a -> a -> Bool
(<>) a b = if d > 0 then d > bigNumber else -d > bigNumber
            where bigNumber = 1000 
                  d = a - b
sort' :: Ord a => [a] -> [a]
sort' [] = []
sort' (x:xs) = insert x (sort' xs)
    where
        insert x [] = x:[]
        insert x (y:ys) = if x < y then x:y:ys else y: insert x ys

foo :: Num b => (a -> b) -> a -> b
foo f x = let a = f x in a * a 
--foo f x = a * a where a = f x 

fib' :: (Num a, Eq a) => a -> a
fib' 1 = 1
fib' 2 = 1
fib' n = fib' (n-1) + fib' (n-2)

fib'' :: (Num a, Eq a) => a -> a
fib'' n = f n (0, 1)
    where
        f n (a, b) | n == 0 = b
                   | otherwise = f (n-1) (b, a + b)

rec = foldr
