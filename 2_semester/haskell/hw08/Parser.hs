-- Список экспорта менять нельзя!
module Parser
    ( Parser
    , pure, (<$>), (<$), (<*>), (<*), (*>)
    , empty, (<|>)
    , satisfy, eof
    , evalParser
    , parserTest
    ) where

import Control.Applicative
import Test.HUnit

newtype Parser a = Parser { runParser :: String -> Maybe (a, String) }

-- (0.5 балла)
evalParser :: Parser a -> String -> Maybe a
evalParser p str = maybe Nothing (\(a,rest) -> Just a) $ runParser p str 

-- (0.5 балла)
satisfy :: (Char -> Bool) -> Parser Char
satisfy predicate = Parser go
    where 
        go [] = Nothing
        go (x:rest) = if predicate x then Just (x, rest) else Nothing

-- (0.5 балла)
eof :: Parser ()
eof = Parser go
    where go [] = Just ((),[])
          go _ = Nothing

instance Functor Parser where
    fmap f (Parser p) = Parser (\x -> maybe Nothing (\(a,rest) -> Just (f a, rest)) (p x))

instance Applicative Parser where
    -- (0.5 балла)
    pure a = Parser (\x -> Just (a, x))
    -- (1.5 балл)
    Parser f <*> Parser p = Parser (\str -> do 
                                        (a, resta) <- f str
                                        (b, restb) <- p resta
                                        Just (a b, restb))

instance Alternative Parser where
    -- (0.5 балла)
    empty = Parser (\_ -> Nothing)
    -- (0.5 балла)
    Parser f <|> Parser p = Parser (\str -> maybe (p str) (\a -> Just a) $ f str)

parserTest :: (Eq a, Show a) => Parser a -> String -> Maybe (a,String) -> Test
parserTest (Parser p) s e = p s ~?= e

