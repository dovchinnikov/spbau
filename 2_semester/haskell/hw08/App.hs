-- (2 балла)

import Control.Applicative

data Tree a = Node a [Tree a] deriving Show

instance Functor Tree where
    fmap f t = pure f <*> t
    
instance Applicative Tree where
    pure a = Node a $ cycle [pure a]
    (Node f fl) <*> (Node v l) = Node (f v) $ zipWith (<*>) fl l 

