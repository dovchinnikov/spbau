module Combinators
    ( module Parser
    , many, many1
    , char, anyChar, string, oneOf
    , digit, natural, integer
    , spaces
    , try
    , endBy, endBy1
    , sepBy, sepBy1
    , foldr1P, foldl1P
    , between, brackets, parens, braces, angles
    ) where

import Parser
import Data.Char

-- (0.5 балла)
char :: Char -> Parser ()
char c = pure () <$> satisfy (==c) 

-- (0.5 балла)
anyChar :: Parser Char
anyChar = satisfy (\_->True)

-- (0.5 балла)
digit :: Parser Int
digit = (\x -> read [x] :: Int) <$> satisfy isDigit 

-- (0.5 балла)
string :: String -> Parser ()
string [] = pure ()
string (x:xs) = pure (\() -> ()) <$> char x <*> string xs
    
-- (0.5 балла)
oneOf :: String -> Parser Char
oneOf str = satisfy (\x -> x `elem` str)

-- (0.5 балла)
many :: Parser a -> Parser [a]
many p = many1 p <|> pure []

-- (0.5 балла)
many1 :: Parser a -> Parser [a]
many1 p = (:) <$> p <*> many1 p <|> (\x->[x]) <$> p

-- (0.5 балла)
natural :: Parser Integer
natural = toInteger.fromDigits <$> many1 digit 
fromDigits = foldl (\num d -> 10*num + d) 0

-- (0.5 балла)
integer :: Parser Integer
integer = natural <|> (\_ x->(-x)) <$> char '-' <*> natural

-- (0.5 балла)
spaces :: Parser ()
spaces = pure () <$> (many $ char ' ')

-- (0.5 балла)
try :: Parser a -> Parser (Maybe a)
try a = Just <$> a <|> pure Nothing

-- (0.5 балла)
endBy :: Parser a -> Parser b -> Parser [a]
endBy p d = many $ (\a _ -> a) <$> p <*> d

-- (0.5 балла)
endBy1 :: Parser a -> Parser b -> Parser [a]
endBy1 p d = many1 $ (\a _ -> a) <$> p <*> d

-- (0.5 балла)
sepBy :: Parser a -> Parser b -> Parser [a]
sepBy p d = sepBy1 p d <|> pure []

-- (0.5 балла)
sepBy1 :: Parser a -> Parser b -> Parser [a]
sepBy1 p d = (:) <$> p <*> (many $ (\_ a -> a) <$> d <*> p)

-- (0.1 балла)
between :: Parser a -> Parser b -> Parser c -> Parser c
between a b c = a *> c <* b

-- (0.1 балла)
brackets :: Parser a -> Parser a
brackets p = char '[' *> p <* char ']'

-- (0.1 балла)
parens :: Parser a -> Parser a
parens p = char '(' *> p <* char ')'

-- (0.1 балла)
braces :: Parser a -> Parser a
braces p = char '{' *> p <* char '}'

-- (0.1 балла)
angles :: Parser a -> Parser a
angles p = char '<' *> p <* char '>'

-- (1 балл)
foldr1P :: (a -> b -> a -> a) -> Parser a -> Parser b -> Parser a
foldr1P f a b = f <$> a <*> b <*> foldr1P f a b <|> a

-- (1 балл)
foldl1P :: (a -> b -> a -> a) -> Parser a -> Parser b -> Parser a
foldl1P f a b = fld <$> a <*> (many $ (\b a->(b,a)) <$> b <*> a)
    where
        fld acc [] = acc
        fld acc ((b,a):xs) = fld (f acc b a) xs

