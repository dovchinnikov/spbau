import Prelude hiding(lookup)
import Test.HUnit

-- 1. fun четные числа в нечетных позициях (нумеруя с 0) умножает на 2, остальные не изменяет.
--    (0.5 балла)
fun :: [Integer] -> [Integer]
fun = zipWith (\i x -> if odd i && even x then x*2 else x) [0..]

-- 2. Бесконечный список Фибоначчи.
--    (0.5 балла)
fibs :: [Integer]
fibs = map (go (0,1)) [0..] 
    where
        go (_, b) 0 = b
        go (a, b) n = go (b, a + b) $ n-1

-- 3a. shiftL переставляет первый элемент в конец списка. Реализуйте эту функцию так, чтобы она проходила по списку только один раз.
--     (0.5 балла)
shiftL :: [a] -> [a]
shiftL [] = []
shiftL (x:xs) = go x xs
    where go a [] = [a]
          go a (x:xs) = x : go a xs

-- 3b. shiftR переставляет последний элемент в начало. Реализуйте эту функцию так, чтобы она проходила по списку только один раз.
--     (0.5 балла)
shiftR :: [a] -> [a]
shiftR [] = []
shiftR l = fst t : snd t
    where
        t = go (undefined, l)
        go (_, [a]) = (a,[])
        go (_, (x:xs)) = (fst t, x : snd t)
            where 
                t = go (undefined, xs)

-- 4. takeLast n xs возвращает последние n элементов списка xs.
--    (0.5 балла)
takeLast :: Int -> [a] -> [a]
takeLast n = reverse . take n . reverse
  

-- 5. swap i j меняет местами i и j элементы.
--    (1 балл)
swap :: Int -> Int -> [a] -> [a]
swap i j l = if i > len || j > len 
             then l 
             else zipWith (\k x -> if k == i 
                                   then l !! j 
                                   else if k == j 
                                        then l !! i 
                                        else x) [0..] l
    where len = length l


-- 6. Назовем элементы, которые удовлетворяют предикату p хорошими, остальные плохими.
--    Тогда mapl p f xs выбрасывает плохие элементы, а блоки подряд идущих хороших элементов,
--    которые разделяются плохими, отправляет в функцию f и возвращает список результатов.
--    Заметьте, что в функцию f никогда не передаются пустые списки.
--    (1 балл)
mapl :: (a -> Bool) -> ([a] -> b) -> [a] -> [b]
mapl p f l = map f $ toList tmp 
    where 
        tmp = go ([],[]) l
        go a [] = a
        go t@(good, current) (x:xs) = if p x 
                                      then go (good, current ++ [x]) xs 
                                      else go (toList t, []) xs
        toList (a, []) = a 
        toList (a, b) = a ++ [b]

------------------------------------------------------------------------------
-- 7.

data Tree a = Node a [Tree a]

-- (a) Возвращает высоту дерева.
--     (1 балл)
height :: Tree a -> Int
height (Node _ []) = 1
height (Node a c) = 1 + (maximum $ map height c)

-- (b) Возвращает среднее арифметическое значений во всех узлах дерева.
--     Необходимо вычислить эту функцию, выполнив один проход по дереву.
--     (1 балл)
avg :: Tree Int -> Int
avg = div' . go 
    where 
        go (Node a c) = foldl (\(x,y) (z,w) -> (x+z, y+w)) (a, 1) (map go c)
        div' (a,b) = a `div` b

-- (c) Возвращает ширину дерева.
--     Ширина дерева определяется следующим образом:
--     Количество вершин на определенном уровне называется шириной уровня.
--     Ширина дерева - это максимальная ширина уровня по всем уровням.
--     (1 балл)
width :: Tree a -> Int
width = maximum . go 
    where 
        go (Node a []) = [1] 
        go (Node a c) = 1 : foldl (\a b -> zipWith (+) a b) [0,0..] (map go c)

-- tests

(tree1, tree2, tree3) =
    ( b [b [l [b []],
            l [b [],
               l [b [l [],
                     l [],
                     b []],
                  l []]]],
         b [],
         b [],
         l []]
    , b [b [b [],
            b [b [],
               b []]],
         b [b [],
            l [b [],
               b []]],
         l [b []]]
    , b [tree1, tree2]
    )
  where l = Node 500; b = Node 300

(testsHeight, testsAvg, testsWidth) = (
    [ height tree1 ~?= 6
    , height tree2 ~?= 4
    , height tree3 ~?= 7
    ],
    [ avg tree1 ~?= 393
    , avg tree2 ~?= 330
    , avg tree3 ~?= 362
    ],
    [ width tree1 ~?= 4
    , width tree2 ~?= 5
    , width tree3 ~?= 7
    ])

------------------------------------------------------------------------------
-- 8. Реализовать двоичное дерево поиска без балансировки.
--    (4 балла)

data Map k v = Map { 
          key :: k
        , value :: v
        , left :: Map k v
        , right :: Map k v } | Dummy
    deriving Show

-- Первый аргумент - функция, сравнивающая значения типа k.
-- Она вовзращает True, если первое значение меньше второго, иначе False.
lookup :: (k -> k -> Bool) -> k -> Map k v -> Maybe v
lookup _ _ Dummy = Nothing
lookup compare k node   | compare k (key node) = lookup compare k $ left node 
                        | compare (key node) k = lookup compare k $ right node
                        | otherwise = Just $ value node

insert :: (k -> k -> Bool) -> k -> v -> Map k v -> Map k v
insert _ k v Dummy = Map k v Dummy Dummy
insert compare k v node | compare k (key node) = Map (key node) (value node) (insert' $ left node) (right node)
                        | compare (key node) k = Map (key node) (value node) (left node) (insert' $ right node)
                        | otherwise = Map k v (left node) (right node)
    where
        insert' = insert compare k v 

delete :: (k -> k -> Bool) -> k -> Map k v -> Maybe (Map k v)
delete = undefined
{-|
delete _ _ _ Dummy = Nothing
delete compare k node   | compare k (key node) = Map (key node) (value node) (delete' $ left node) (right node)
                        | compare (key node) k = Map (key node) (value node) (left node) (delete' $ right node)
                        | otherwise = Map (fst a) (snd a) (left node)
    where
        delete' = delete compare k v 
        d node  | right node == Dummy = left node
                | left node == Dummy = right node
                | otherwise = Map (fst a) (snd a) (delete fst a)
        a = largest compare $ left node

        
        

largest :: (k -> k -> Bool) -> Map k v -> Maybe (k,v)
largest compare Dummy                           = Nothing
largest compare node    | right node == Dummy   = Just (key node, value node)
                        | otherwise             = largest $ right node
-}


fromList :: (k -> k -> Bool) -> [(k,v)] -> Map k v
fromList _ [] = Dummy
fromList compare ((k,v):xs) = insert compare k v $ fromList compare xs

toList :: Map k v -> [(k,v)]
toList Dummy = []
toList node = (toList $ left node) ++ (key node, value node) : (toList $ right node)

-- tests

sort :: (a -> a -> Bool) -> [a] -> [a]
sort p = map fst . toList . fromList p . map (\x -> (x, ()))

------------------------------------------------------------------------------
-- main

main = fmap (\_ -> ()) $ runTestTT $ test
    $    label "fun"
    [ fun [1,3,6,10,15,21,28,30,60] ~?= [1,3,6,20,15,21,28,60,60]
    , take 11 (fun fibs) ~?= [1,1,2,3,5,16,13,21,34,55,89]
    ] ++ label "fibs"
    [ take 10 fibs ~?= [1,1,2,3,5,8,13,21,34,55]
    , fibs !! 1000 ~?= 70330367711422815821835254877183549770181269836358732742604905087154537118196933579742249494562611733487750449241765991088186363265450223647106012053374121273867339111198139373125598767690091902245245323403501
    ] ++ label "shiftL"
    [ shiftL [1..20] ~?= [2..20] ++ [1]
    , shiftL [] ~?= ([] :: [Bool])
    ] ++ label "shiftR"
    [ shiftR [1..20] ~?= 20:[1..19]
    , shiftR [] ~?= ([] :: [Bool])
    ] ++ label "takeLast"
    [ takeLast 5 [1..20] ~?= [16,17,18,19,20]
    , takeLast 5 [1,2,3] ~?= [1,2,3]
    ] ++ label "swap"
    [ swap 1 2 [3,4,5,6] ~?= [3,5,4,6]
    , swap 2 0 "abcd" ~?= "cbad"
    , swap 100 7 [1..10] ~?= [1..10]
    ] ++ label "mapl"
    [ mapl (\x -> x `mod` 7 /= 3) id [1..20] ~?= [[1,2],[4,5,6,7,8,9],[11,12,13,14,15,16],[18,19,20]]
    , mapl (\x -> elem x [1,4,6,8,9,10,12,14,15,16,18,20]) sum [1..20] ~?= [1,4,6,27,12,45,18,20]
    ] ++ label "height" testsHeight
      ++ label "avg" testsAvg
      ++ label "width" testsWidth
      ++ label "Map" -- можете сами написать тесты на каждую функцию :)
    [ sort (<) [10,24,13,56,35,13,6,23] ~?= [6,10,13,23,24,35,56]
--    , lookup (\x y -> x < y) 10 Dummy ~?= Nothing
    ]
  where
    label :: String -> [Test] -> [Test]
    label l = map (\(i,t) -> TestLabel (l ++ " [" ++ show i ++ "]") t) . zip [1..]

