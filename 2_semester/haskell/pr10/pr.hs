import Control.Monad.State

main = do
    n <- fmap (read) getLine
    putStrLn $ show $ fac n

fac :: Int -> Int 
fac n = snd $ execState (go n) (0,1)
    where 
        go :: Int -> State (Int, Int) ()
        go n = do
            (i,r) <- get
            if i < n then do
                put (i+1, (i+1)*r)
                go n
            else
                return ()
        
