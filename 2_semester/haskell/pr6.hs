data Tree a = Node a [Tree a]

instance Show a => Show (Tree a) where
--    show (Node a c) = show a ++ ":" ++ show c
    show (Node a []) = show a ++ ":{}"
    show (Node a c) = show a ++ ":{" ++ init (foldl (\a b -> a ++ b ++ ",") "" (map show c)) ++ "}"

instance Functor Tree where
--    fmap :: (a -> b) -> Tree a -> Tree b
    fmap f (Node a c) = Node (f a) (map (fmap f) c)

instance Read a => Read (Tree a) where
    readsPrec = undefined
