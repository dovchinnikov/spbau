
main = putStrLn "more?" >> getLine >>= \s -> case s of 
                            "yes" -> main
                            "no" -> return ()
                            _ -> putStrLn "yes or no" >> main


