data Pair a = Pair a a  deriving (Show, Eq)

-- Определите instance так, чтобы он удовлетворял законам монад.
instance Monad Pair where
    return a = Pair a a
 --   (>>=) (Pair a b) f = f b
 --   (>>=) (Pair a b) f = f a
    (>>=) p@(Pair a b) f = case f a of (Pair aa _ ) -> return aa
    
      
k :: Int  -> Pair String
k x = Pair (show x) (show $ x+1)

test :: Pair String
test = return 1 >>= k
test2 :: Pair String 
test2 = k 1

main = do
    
    return ()


