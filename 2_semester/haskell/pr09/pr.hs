import Data.IORef
import Control.Applicative

while :: Monad m => m Bool -> m () -> m ()
while cond body = do
    b <- cond
    if b
        then body >> while cond body
        else return ()

fac :: Int -> IO Int
fac n = do
    i <- newIORef 0
    r <- newIORef 1
    while (readIORef i >>= \i' -> return $ i' < n) $ do 
        r' <- readIORef r
        i' <- readIORef i
        writeIORef i (i' + 1)
        writeIORef r ((i' + 1) * r')
    readIORef r

