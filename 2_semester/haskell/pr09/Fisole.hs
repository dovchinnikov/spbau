import System.IO

newtype Fisole a = Fisole (Handle -> IO a)
   
runFisole :: Fisole a -> FilePath -> IO a 
runFisole (Fisole f) path = withFile path ReadWriteMode f

instance Functor Fisole where
    fmap f (Fisole r) = Fisole (fmap f . r)

instance Monad Fisole where
    return x = Fisole (return x)
    m >>= k = undefined


