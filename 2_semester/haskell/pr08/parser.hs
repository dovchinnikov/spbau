
import Control.Applicative

newtype Parser a = Parser {runParser:: String -> Maybe (a, String)}

evalParser :: Parser a -> String -> Maybe a
evalParser p str = case runParser p str of 
                        Nothing -> Nothing
                        Just (a, rest) -> Just a

satisfy :: (Char -> Bool) -> Parser Char
satisfy predicate = Parser go
    where 
        go [] = Nothing
        go (x:rest) = if predicate x then Just (x, rest) else Nothing

eof :: Parser ()
eof = Parser go
    where go [] = Just ((),[])
          go _ = Nothing

instance Functor Parser where
    fmap = (<$>) 

instance Applicative Parser where
    pure a = Parser (\x -> Just (a, x))
    -- <*> :: Parser (a -> b) -> Parser a -> Parser b
    Parser f <*> Parser p = Parser (\str -> case f str of 
                                                Nothing -> Nothing 
                                                Just (resf, rest) -> case p rest of 
                                                    Nothing -> Nothing
                                                    Just (resg, rest) -> Just (resf resg, rest))

