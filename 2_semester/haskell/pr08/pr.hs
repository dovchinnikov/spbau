import Control.Applicative

data BTree a = Void | Node a (BTree a) (BTree a) deriving Show

instance Functor BTree where
--    fmap f (Node a l r) = Node (f a) (fmap f l) (fmap f r)  
--    fmap _ _  = Void
    fmap f x = pure f <*> x

instance Applicative BTree where
    -- pure :: a -> BTree a
    pure a = Node a (pure a) (pure a)
    -- :: BTree (a -> b) -> BTree a -> BTree b
    Void <*> _ = Void
    _ <*> Void = Void
    (Node f l r) <*> (Node a la ra) = Node (f a) (l <*> la) (r <*> ra)
