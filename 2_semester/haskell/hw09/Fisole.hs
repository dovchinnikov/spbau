-- (3 балла)

-- Список экспорта менять нельзя!
module Fisole
    ( Fisole, runFisole
    , abortF, putCharF, getCharF
    ) where

import System.IO
import Control.Exception

newtype Fisole a = Fisole (Handle -> IO (Either String a))

-- Второй аргумент - имя файла с которым будут работать функции putCharF и getCharF.
-- Если произошли какие-то ошибки ввода-вывода (т.е. исключения бросались), 
-- нужно их поймать и вернуть Left с каким-нибудь сообщением об ошибке.
runFisole :: Fisole a -> FilePath -> IO (Either String a)
runFisole (Fisole f) path = do 
    x <- ttt path f
    case x of 
        Left ex -> return $ Left $ show ex
        Right res -> case res of 
            Left s -> return $ Left s
            Right r -> return $ Right r
   where 
        ttt :: FilePath -> (Handle -> IO a) -> IO (Either IOException a) 
        ttt p f = try (withFile p ReadWriteMode f)

instance Functor Fisole where
--  fmap :: Functor f => (a -> b) -> f a -> f b
    fmap f (Fisole r) = Fisole (\h -> fmap (fmap f) $ r h)

instance Monad Fisole where
--  return :: Monad m => a -> m a
    return x = Fisole (\_ -> return $ Right x)
--  (>>=) :: Monad m => m a -> (a -> m b) -> m b
    (>>=) (Fisole f) h = Fisole (\handle -> do
                x <- f handle
                case x of
                    Left a -> return $ Left a
                    Right a -> do case h a of Fisole b -> b handle)

-- abortF s завершает вычисление, s - сообщение об ошибке.
abortF :: String -> Fisole a
abortF msg = Fisole (\_ -> return $ Left msg)

putCharF :: Char -> Fisole ()
putCharF c = Fisole (\h -> do
                x <- try (hPutChar h c) :: IO (Either IOException ())
                case x of 
                    Left ex -> return $ Left $ show ex
                    Right a -> return $ Right a)

getCharF :: Fisole Char
getCharF = Fisole (\h -> do
                x <- try (hGetChar h) :: IO (Either IOException Char)
                case x of   
                    Left ex -> return $ Left $ show ex
                    Right c -> return $ Right c)
