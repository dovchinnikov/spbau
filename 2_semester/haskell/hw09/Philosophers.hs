-- (4 балла)

import System.Environment
import System.Random
import Control.Monad
import Control.Concurrent.STM
import Control.Concurrent

{-
Решите задачу о философах http://en.wikipedia.org/wiki/Dining_philosophers_problem
Количество философов передается в параметрах командной строки.
Жизненый цикл философа:
a) Философ сообщает (вывод сообщения на экран) о том, что он готов обедать.
b) Ждет пока не освободятся обе вилки.
c) Берет вилки, сообщает об этом, начинает обедать, что занимает рандомное время (от 1 до 3 секунд).
d) Кладет вилки, сообщает об этом, начинает думать, что занимает рандомное время (от 1 до 3 секунд).
e) Возвращается к шагу (a).

Для реализации используйте библиотеку STM.
Вам также понадобятся функции forkIO, threadDelay и randomRIO.
-}

type Fork = TVar Bool

main = do
    n <- fmap (read . head) getArgs
    -- n <- return 5
    forks <- replicateM n (newTVarIO False)
    mapM_ (forkIO . startPh forks n) [1..n-1]
    startPh forks n n
    
startPh :: [Fork] -> Int -> Int -> IO () 
startPh forks n i = start i (forks !! pred i) (forks !! mod i n)
    
start :: Int -> Fork -> Fork -> IO ()
start n left right = forever $ do
    putStrLn $ show n ++ "\tis ready"
    atomically $ do
        l <- readTVar left 
        r <- readTVar right
        if (not l) && (not r) then do
            writeTVar left True
            writeTVar left True
        else 
            retry
    putStrLn $ show n ++ "\tis dining"
    delay
    atomically $ do 
        writeTVar left False
        writeTVar right False
    putStrLn $ show n ++ "\tis thinking"
    delay
    
delay :: IO ()
delay = randomRIO (1, 3*10^6) >>= threadDelay
