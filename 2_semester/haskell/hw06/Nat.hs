module Nat where

-- (1 балл)

data Nat = Zero | Suc Nat

instance Eq Nat where
    (==) Zero Zero = True
    (==) (Suc _) Zero = False
    (==) Zero (Suc _) = False
    (==) (Suc a) (Suc b) = a == b

instance Ord Nat where
    compare Zero Zero = EQ
    compare (Suc _) Zero = GT
    compare Zero (Suc _) = LT
    compare (Suc a) (Suc b)= a `compare` b

instance Num Nat where
    (+) a Zero = a
    (+) Zero b = b
    (+) a (Suc b) = Suc a + b

    (*) _ Zero = Zero
    (*) Zero _ = Zero
    (*) (Suc Zero) b = b
    (*) a (Suc Zero) = a
    (*) (Suc a) (Suc b) = (Suc b) + a * (Suc b)
    
    signum _ = 1
    
    fromInteger 0 = Zero
    fromInteger a = Suc(fromInteger $ a - 1)
    
    negate Zero = Zero
    negate _ = error "Nat: negative value"
    
    abs = id

instance Show Nat where
    show a = show $ go a
        where   
            go (Suc a) = 1 + go a
            go Zero = 0
