module Expr where

import qualified Data.Map as M

-- (3 балла)

data Value = I Integer | B Bool deriving (Eq, Show)
data BinOp = Plus | Mul | Minus | Less | Greater | Equals
data UnOp = Neg | Not
data Expr = BinOp BinOp Expr Expr | UnOp UnOp Expr | Const Value | Var String
data Statement = Assign String Expr | If Expr Program (Maybe Program) | While Expr Program
type Program = [Statement]

infixr 0 $=
($=) = Assign

infix 4 `less`, `greater`
less = BinOp Less
greater = BinOp Greater

infixl 7 `mul`
mul = BinOp Mul

infixl 6 `plus`, `minus`
plus = BinOp Plus
minus = BinOp Minus

infix 1 `else_`
else_ :: (Maybe Program -> Statement) -> Program -> Statement
else_ f e = f (Just e)

type Error = String



eval :: BinOp -> Either [Error] Value -> Either [Error] Value -> Either [Error] Value
eval _ (Left e) (Left ee) = Left $ e ++ ee
eval _ (Left e) _ = Left e
eval _ _ (Left e) = Left e
eval Plus   (Right (I a)) (Right (I b)) = Right $ I $ a + b
eval Minus  (Right (I a)) (Right (I b)) = Right $ I $ a - b
eval Mul    (Right (I a)) (Right (I b)) = Right $ I $ a * b
eval Less   (Right (I a)) (Right (I b)) = Right $ B $ a < b
eval Greater (Right (I a)) (Right (I b)) = Right $ B $ a > b
eval Equals (Right (I a)) (Right (I b)) = Right $ B $ a == b
eval _ _ _ = Left ["type mismatch"]

eval1 :: UnOp -> Either [Error] Value -> Either [Error] Value
eval1 _ (Left e) = Left e
eval1 Neg (Right (I a)) = Right $ I $ -a
eval1 Not (Right (B a)) = Right $ B $ not a
eval1 _ _ = Left ["type mismatch"]


-- evalExpr m e интерпретирует e в контексте m, который сопоставлят имени переменной ее значение.
-- evalExpr возвращает либо успешно вычисленный результат, либо список ошибок.
-- Ошибки бывают двух видов: необъявленная переменная и несоответствие типов.
evalExpr :: M.Map String Value -> Expr -> Either [Error] Value
evalExpr m (BinOp op a b) = eval op (evalExpr m a) (evalExpr m b) 
evalExpr m (UnOp op a) = eval1 op $ evalExpr m a
evalExpr m (Const a) = Right a
evalExpr m (Var v) = maybe (Left ["undefined variable"]) (\x -> Right x) (M.lookup v m)


-- evalStatement m e интерпретирует e в контексте m.
-- evalStatement возвращает либо обновленный контекст, либо список ошибок.
evalStatement :: M.Map String Value -> Statement -> Either [Error] (M.Map String Value)
evalStatement m (Assign var e) = either Left
                                        (\val -> Right $ M.insert var val m) 
                                        (evalExpr m e)
    
evalStatement m (If condition thn els) = either Left evaluated $ evalExpr m condition
    where
        evaluated (B res) = if res 
                            then evalProgram m thn 
                            else case els of 
                                 (Just p) -> evalProgram m p
                                 Nothing -> Right m
        evaluated _ = Left ["type mismatch: boolean expected"] 

evalStatement m w@(While condition program) =  either Left evaluated $ evalExpr m condition
    where
        evaluated (B res) = if res
                            then either Left (\m -> evalStatement m w) (evalProgram m program)
                            else Right m
        evaluated _ = Left ["type mismatch: boolean expected"] 


evalProgram :: M.Map String Value -> Program -> Either [Error] (M.Map String Value)
evalProgram m [] = Right m
evalProgram m (st:rest) = either (\e-> Left e) (\m-> evalProgram m rest) (evalStatement m st)
