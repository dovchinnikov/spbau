module Tree where

-- (2 балла)

data Tree a = Node { value :: a, children :: [Tree a] }

instance Eq a => Eq (Tree a) where
    (==) a b = if value a == (value b)
                    then children a == children b 
                    else False

instance Read a => Read (Tree a) where
    readsPrec _ s = concatMap (\(v,s) -> parseRest v s) (reads s)
        where
            parseRest v str@(':':s) = case parseChildren [] s of
                ((c,r):_) -> [(Node v c, r)]
                _ -> [(Node v [], str)]
            parseRest v s = [(Node v [], s)]
            parseChildren acc (f:s) | f == '{' || f == ',' = case reads s of
                                                            ((t,r):_) -> parseChildren (acc ++ [t]) r
                                                            _ -> []
                                    | f == '}' = [(acc, s)]
                                    | otherwise = []

{-
instance Read a => Read (Tree a) where
    readsPrec _ s = concatMap (\(v,s) -> parseRest v s) (reads s)
      where
        parseRest :: Read a => a -> String -> [(Tree a, String)]
        parseRest v (':':s) = map (\(l,s') -> (Node v l,s')) (reads s)
        parseRest _ _ = []

-- read "1:[2:[4:[],5:[]],3:[]]"
-}

instance Show a => Show (Tree a) where
--    show (Node a c) = show a ++ ":" ++ show c
    show (Node a []) = show a
    show (Node a c) = show a ++ ":{" ++ init (foldl (\a b -> a ++ b ++ ",") "" (map show c)) ++ "}"

instance Functor Tree where
    fmap f tree = Node (f $ value tree) (map (fmap f) (children tree))

