package ru.spbau.ovchinnikov.task2.reader;

import ru.spbau.ovchinnikov.task2.IllegalMessageFormatException;
import ru.spbau.ovchinnikov.task2.Message;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for reading messages from the file.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public class FileMessageReader implements MessageReader {

    private final BufferedReader reader;

    /**
     * Constructs new {@code FileMessageReader}.
     *
     * @param filename filename of the file to read from.
     * @throws java.io.FileNotFoundException if the named file does not exist,
     *                                       is a directory rather than a regular file,
     *                                       or for some other reason cannot be opened for reading
     */
    public FileMessageReader(final String filename) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(filename));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Message readMessage() throws IllegalMessageFormatException {
        try {
            List<String> lines = new ArrayList<>();
            int lineCount = Integer.parseInt(reader.readLine());
            for (int i = 0; i < lineCount; i++) {
                String line = reader.readLine();
                if (line == null) {
                    throw new IllegalMessageFormatException("Cannot read next line", null);
                }
                lines.add(line);
            }
            return new Message(lines);
        } catch (NumberFormatException e) {
            throw new IllegalMessageFormatException("Cannot parse lines count", e);
        } catch (IOException e) {
            throw new IllegalMessageFormatException("Cannot read next line", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasMoreMessages() throws IOException {
        return reader.ready();
    }

    /**
     * Closes underlying input stream.
     *
     * @throws IOException if an I/O error occurs.
     * @see java.io.BufferedReader#close()
     */
    @Override
    public void close() throws IOException {
        reader.close();
    }

}
