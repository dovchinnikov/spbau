package ru.spbau.ovchinnikov.task2.writer;

import ru.spbau.ovchinnikov.task2.Message;

import java.io.IOException;

/**
 * Class for writing {@link ru.spbau.ovchinnikov.task2.Message} objects to the {@code stdout}.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public class ConsoleMessageWriter implements MessageWriter {

    private int messageCounter;

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeMessage(final Message message) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int lineCounter = 0;
        sb.append("Message ").append(++messageCounter).append('\n');
        for (String line : message.getLines()) {
            sb.append(messageCounter).append('.')
                    .append(++lineCounter).append(". ")
                    .append(line).append('\n');
        }
        System.out.print(sb.toString());
    }

    /**
     * Does nothing.<br/>
     * This method is needed for using {@code ConsoleMessageWriter} within try-with-resources.
     *
     * @throws IOException never.
     */
    @Override
    public void close() throws IOException {
        //nothing to close
    }

}
