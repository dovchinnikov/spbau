package ru.spbau.ovchinnikov.task2.writer;

import ru.spbau.ovchinnikov.task2.Message;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class for writing {@link ru.spbau.ovchinnikov.task2.Message} objects to the file.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public class FileMessageWriter implements MessageWriter {

    private final BufferedWriter writer;

    /**
     * Constructs new {@code FileMessageWriter}.
     *
     * @param filename filename of the file to write to.
     * @throws IOException if an I/O error occurs.
     */
    public FileMessageWriter(final String filename) throws IOException {
        this.writer = new BufferedWriter(new FileWriter(filename));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeMessage(final Message message) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append(message.getLines().size()).append('\n');
        for (String line : message.getLines()) {
            sb.append(line).append('\n');
        }
        writer.write(sb.toString());
    }

    /**
     * Closes underlying output stream.
     *
     * @throws IOException if an I/O error occurs.
     * @see java.io.BufferedWriter#close()
     */
    @Override
    public void close() throws IOException {
        writer.close();
    }

}
