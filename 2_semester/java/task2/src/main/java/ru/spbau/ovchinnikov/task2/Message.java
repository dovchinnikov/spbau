package ru.spbau.ovchinnikov.task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class represents one particular message.
 * Every message consists of zero or more lines.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public class Message {

    private List<String> lines;

    /**
     * Constructs new {@code Message} form the other using deep copy.
     * Subsequent modification of the other message
     * does not affect the newly created {@code Message}.
     *
     * @param other {@code Message} to make copy from
     */
    public Message(final Message other) {
        this.lines = new ArrayList<>(other.getLines());
    }

    /**
     * Constructs new {@code Message}.
     * Creates copy of the lines and stores them.
     *
     * @param lines Message lines.
     */
    public Message(final List<String> lines) {
        this.lines = new ArrayList<>(lines);
    }

    /**
     * Copies lines from other {@code Message} and appends them to the current lines.
     *
     * @param other Message to be appended.
     */
    public void append(final Message other) {
        lines.addAll(other.lines);
    }


    /**
     * @return Readonly message lines.
     * @see {@link java.util.Collections#unmodifiableList(java.util.List)}
     */
    public List<String> getLines() {
        return Collections.unmodifiableList(lines);
    }

}
