package ru.spbau.ovchinnikov.task2;

/**
 * An exception class used for signaling run-time format failure
 * while working with the {@link Message} streams.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public class IllegalMessageFormatException extends Exception {

    /**
     * Constructs a new exception with the specified detail message and
     * cause.  <p>Note that the detail message associated with
     * {@code cause} is <i>not</i> automatically incorporated in
     * this exception's detail message.
     *
     * @param message the detail message (which is saved for later retrieval
     *                by the {@link #getMessage()} method).
     * @param cause   the cause (which is saved for later retrieval by the
     *                {@link #getCause()} method).  (A <tt>null</tt> value is
     *                permitted, and indicates that the cause is nonexistent or
     *                unknown.)
     */
    public IllegalMessageFormatException(String message, Throwable cause) {
        super(message, cause);
    }

}
