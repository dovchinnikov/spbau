package ru.spbau.ovchinnikov.task2.writer;

import ru.spbau.ovchinnikov.task2.Message;

import java.io.IOException;

/**
 * Class for writing compressed {@link ru.spbau.ovchinnikov.task2.Message} objects
 * to the underlying {@link ru.spbau.ovchinnikov.task2.writer.MessageWriter}.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public class CompressingMessageWriter implements MessageWriter {

    private final MessageWriter underlyingWriter;
    private Message bufferedMessage;

    /**
     * Constructs new {@code CompressingMessageWriter} with {@code MessageWriter}.
     *
     * @param underlyingWriter {@code MessageWriter} to write compressed messages to.
     */
    public CompressingMessageWriter(MessageWriter underlyingWriter) {
        this.underlyingWriter = underlyingWriter;
    }

    /**
     * Appends every even {@code Message} to the previous {@code Message}
     * and then writes to underlying {@code MessageWriter}.
     *
     * @param message {@code Message} to process.
     * @throws java.io.IOException if an I/O error occurs.
     */
    @Override
    public void writeMessage(final Message message) throws IOException {
        if (bufferedMessage == null) {
            bufferedMessage = new Message(message);
        } else {
            bufferedMessage.append(message);
            underlyingWriter.writeMessage(bufferedMessage);
            bufferedMessage = null;
        }
    }

    /**
     * Flushes the buffer to the underlying stream and closes the underlying stream.
     *
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void close() throws IOException {
        try {
            flush();
        } finally {
            underlyingWriter.close();
        }
    }

    private void flush() throws IOException {
        if (bufferedMessage != null) {
            underlyingWriter.writeMessage(bufferedMessage);
        }
    }

}
