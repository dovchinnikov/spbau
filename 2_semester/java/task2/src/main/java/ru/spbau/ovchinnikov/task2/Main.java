package ru.spbau.ovchinnikov.task2;

import ru.spbau.ovchinnikov.task2.reader.FileMessageReader;
import ru.spbau.ovchinnikov.task2.reader.MessageReader;
import ru.spbau.ovchinnikov.task2.writer.CompressingMessageWriter;
import ru.spbau.ovchinnikov.task2.writer.ConsoleMessageWriter;
import ru.spbau.ovchinnikov.task2.writer.FileMessageWriter;
import ru.spbau.ovchinnikov.task2.writer.MessageWriter;

import java.io.*;

/**
 * Contains application entry point. <br/>
 * For more information please refer to {@link #main(String[])}.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public class Main {

    /**
     * Compresses message files given as command line arguments.
     *
     * @param args Command-line arguments.<br/>
     *             The first command-line argument is interpreted as input filename. <br/>
     *             If the second command-line was passed it is interpreted as output filename.
     *             In other case output is printed to {@code stdout}.
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: input_file_name [output_file_name]");
            return;
        }

        try (MessageReader reader = new FileMessageReader(args[0]);
             MessageWriter writer = new CompressingMessageWriter((args.length > 1)
                     ? new FileMessageWriter(args[1])
                     : new ConsoleMessageWriter())) {
            while (reader.hasMoreMessages()) {
                writer.writeMessage(reader.readMessage());
            }
        } catch (FileNotFoundException e) {
            System.err.println("Couldn't find file: " + args[0]);
        } catch (IOException | IllegalMessageFormatException e) {
            System.err.println(e.getMessage());
        }
    }

}
