package ru.spbau.ovchinnikov.task2.writer;

import ru.spbau.ovchinnikov.task2.Message;

import java.io.Closeable;
import java.io.IOException;

/**
 * Interface for writing {@link ru.spbau.ovchinnikov.task2.Message} objects.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public interface MessageWriter extends Closeable {

    /**
     * Writes next {@code Message}.
     *
     * @param message {@link Message} to write.
     * @throws IOException if an I/O error occurs.
     */
    void writeMessage(final Message message) throws IOException;

}
