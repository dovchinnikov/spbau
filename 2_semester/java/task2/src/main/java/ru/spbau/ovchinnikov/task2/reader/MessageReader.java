package ru.spbau.ovchinnikov.task2.reader;

import ru.spbau.ovchinnikov.task2.IllegalMessageFormatException;
import ru.spbau.ovchinnikov.task2.Message;

import java.io.Closeable;
import java.io.IOException;

/**
 * Interface for reading {@link ru.spbau.ovchinnikov.task2.Message} objects.
 *
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
public interface MessageReader extends Closeable {

    /**
     * Reads next message.
     *
     * @return Newly read message.
     * @throws IllegalMessageFormatException if an format error occurs,
     *                                       i.e. wrong number of lines or leak of the lines,
     *                                       or in case when no more messages is available.
     */
    Message readMessage() throws IllegalMessageFormatException;

    /**
     * Checks if there are more messages.
     *
     * @return {@value true} if there are more data, otherwise {@value false}.
     * @throws IOException if an I/O error occurs.
     */
    boolean hasMoreMessages() throws IOException;

}
