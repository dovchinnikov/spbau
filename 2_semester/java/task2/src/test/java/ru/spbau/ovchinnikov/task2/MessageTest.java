package ru.spbau.ovchinnikov.task2;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Daniil Ovchinnikov
 * @since 3/10/14
 */
public class MessageTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testReadOnlyLines() {
        Message message = new Message(new ArrayList<String>() {{
            add("yo");
            add("mama");
            add("is");
            add("so");
            add("fat");
        }});
        message.getLines().add("bitch");
    }

    @Test
    public void appendTest() {
        List<String> lines1 = new ArrayList<String>() {{
            add("yo");
            add("mama");
            add("is");
            add("so");
            add("fat");
        }};
        Message msg1 = new Message(lines1);
        List<String> lines2 = new ArrayList<String>() {{
            add("mount");
            add("Everest");
            add("tried");
            add("to climb");
            add("her");
            add("big");
        }};
        Message msg2 = new Message(lines2);
        msg1.append(msg2);
        lines1.addAll(lines2);
        Assert.assertTrue(Arrays.equals(lines1.toArray(), msg1.getLines().toArray()));
    }
}
