package ru.spbau.ovchinnikov.task2;

import org.junit.Assert;
import org.junit.Test;
import ru.spbau.ovchinnikov.task2.reader.FileMessageReader;
import ru.spbau.ovchinnikov.task2.reader.MessageReader;

import java.io.IOException;

/**
 * @author Daniil Ovchinnikov
 * @since 3/3/14
 */
@SuppressWarnings("ConstantConditions")
public class FileMessageReaderTest {

    private static final String EMPTY_FILE = "empty.txt";
    private static final String INPUT_FILE = "input.txt";
    private static final String NON_VALID_FILE = "nonvalid.txt";

    @Test
    public void testEmptyFile() throws IOException {
        MessageReader reader = new FileMessageReader(
                getClass().getClassLoader().getResource(EMPTY_FILE).getFile()
        );
        Assert.assertFalse(reader.hasMoreMessages());
    }

    @Test
    public void testLinesCount() throws IllegalMessageFormatException, IOException {
        MessageReader reader = new FileMessageReader(
                getClass().getClassLoader().getResource(INPUT_FILE).getFile()
        );
        Assert.assertTrue(reader.hasMoreMessages());
        Assert.assertEquals(reader.readMessage().getLines().size(), 1);
        Assert.assertEquals(reader.readMessage().getLines().size(), 2);
        Assert.assertEquals(reader.readMessage().getLines().size(), 3);
        Assert.assertEquals(reader.readMessage().getLines().size(), 4);
        Assert.assertEquals(reader.readMessage().getLines().size(), 5);
        Assert.assertFalse(reader.hasMoreMessages());
    }

    @Test(expected = IllegalMessageFormatException.class)
    public void testNonValid() throws IOException, IllegalMessageFormatException {
        MessageReader reader = new FileMessageReader(
                getClass().getClassLoader().getResource(NON_VALID_FILE).getFile()
        );
        Assert.assertTrue(reader.hasMoreMessages());
        reader.readMessage();
    }

}
