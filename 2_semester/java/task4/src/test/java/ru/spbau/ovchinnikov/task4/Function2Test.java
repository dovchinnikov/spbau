package ru.spbau.ovchinnikov.task4;


import org.junit.Test;
import ru.spbau.ovchinnikov.task4.base.Function;
import ru.spbau.ovchinnikov.task4.base.Function2;

import static org.junit.Assert.assertEquals;

/**
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class Function2Test {


    private static final Function2<Integer, Integer, Integer> SUM = new Function2<Integer, Integer, Integer>() {
        @Override
        public Integer apply(Integer arg1, Integer arg2) {
            return arg1 + arg2;
        }
    };
    private static final Function<Integer, Integer> CURRIED_PLUS_2 = SUM.apply(2);
    private static final Function<Integer, Integer> BIND_PLUS_2 = SUM.bind1(2);
    private static final Function<Integer, Integer> BIND_PLUS_3 = SUM.bind2(3);

    @Test
    public void test() {
        assertEquals(Integer.valueOf(4), SUM.apply(2, 2));
    }

    @Test
    public void testApply() {
        for (int i = 0; i < 10; i++) {
            assertEquals(Integer.valueOf(i + 2), CURRIED_PLUS_2.apply(i));
        }
    }

    @Test
    public void testBind1() {
        for (int i = 0; i < 10; i++) {
            assertEquals(Integer.valueOf(i + 2), BIND_PLUS_2.apply(i));
        }
    }

    @Test
    public void testBind2() {
        for (int i = 0; i < 10; i++) {
            assertEquals(Integer.valueOf(i + 3), BIND_PLUS_3.apply(i));
        }
    }

    @Test
    public void testComposition() {
        Function2<String, Integer, Integer> C = SUM.then(FunctionTest.TO_STRING);
        assertEquals("4", C.apply(1, 3));
    }
}
