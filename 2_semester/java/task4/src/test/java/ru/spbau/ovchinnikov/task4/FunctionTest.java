package ru.spbau.ovchinnikov.task4;

import org.junit.Test;
import ru.spbau.ovchinnikov.task4.base.Function;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class FunctionTest {

    private static final List<String> list = Arrays.asList("a", "b", "c", "d");

    public static final Function<String, Integer> TO_STRING = new Function<String, Integer>() {
        @Override
        public String apply(Integer param) {
            return String.valueOf(param);
        }
    };

    private static final Function<Integer, Collection<?>> SIZE = new Function<Integer, Collection<?>>() {
        @Override
        public Integer apply(Collection<?> param) {
            return param.size();
        }
    };

    @Test
    public void test() {
        assertTrue(SIZE.apply(list) == 4);
        assertEquals(TO_STRING.apply(1), "1");
    }

    @Test
    public void testComposition() {
        final Function<String, Collection<?>> C = SIZE.then(TO_STRING);
        assertEquals(TO_STRING.apply(SIZE.apply(list)), C.apply(list));
    }
}
