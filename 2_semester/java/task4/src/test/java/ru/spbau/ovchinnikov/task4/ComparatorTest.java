package ru.spbau.ovchinnikov.task4;

import org.junit.Test;
import ru.spbau.ovchinnikov.task4.base.Function;

import static org.junit.Assert.assertTrue;

/**
 * @author Daniil Ovchinnikov
 * @since 3/28/14
 */
public class ComparatorTest {

    @Test
    public void test() {
        final Comparator<String, Integer> c = new Comparator<>(new Function<Integer, String>() {
            @Override
            public Integer apply(String arg) {
                return arg.length();
            }
        });

        assertTrue(c.compare("a", "b") == 0);
        assertTrue(c.compare("a", "aa") < 0);
        assertTrue(c.compare("aa", "a") > 0);
    }
}
