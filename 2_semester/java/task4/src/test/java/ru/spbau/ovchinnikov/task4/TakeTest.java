package ru.spbau.ovchinnikov.task4;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class TakeTest {

    private static final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    @Test
    public void take10() {
        assertEquals(new Take<Integer>().apply(5).apply(list), Arrays.asList(1, 2, 3, 4, 5));
        assertEquals(new Take<Integer>().apply(5, list), Arrays.asList(1, 2, 3, 4, 5));
    }

    @Test
    public void takeMore() {
        assertEquals(list, new Take<Integer>().apply(100).apply(list));
        assertEquals(list, new Take<Integer>().apply(100, list));
    }
}
