package ru.spbau.ovchinnikov.task4;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Daniil Ovchinnikov
 * @since 3/28/14
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        FunctionTest.class,
        Function2Test.class,
        PredicateTest.class,
        FilterTest.class,
        TakeTest.class,
        MapTest.class,
        FoldrTest.class,
        ComparatorTest.class
})
public class FuncLibSuite {

}
