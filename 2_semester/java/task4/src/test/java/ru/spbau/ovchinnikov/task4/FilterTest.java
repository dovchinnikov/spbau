package ru.spbau.ovchinnikov.task4;

import org.junit.Test;
import ru.spbau.ovchinnikov.task4.base.Predicate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Daniil Ovchinnikov
 * @since 3/19/14
 */
public class FilterTest {

    final List<Integer> testList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    @Test
    public void testTrue() {
        assertEquals(
                testList,
                new Filter<Integer>().apply(Predicate.<Integer>alwaysTrue(), testList)
        );
        assertEquals(
                testList,
                new Filter<Integer>().apply(Predicate.<Integer>notNull(), testList)
        );
    }

    @Test
    public void testFalse() {
        assertEquals(
                Collections.EMPTY_LIST,
                new Filter<Integer>().apply(Predicate.<Integer>alwaysFalse(), testList)
        );
        assertEquals(
                Collections.EMPTY_LIST,
                new Filter<Integer>().apply(Predicate.<Integer>notNull().not(), testList)
        );
    }

    @Test
    public void testPredicates() {
        final Filter<Integer> f = new Filter<>();
        final Predicate<Integer> p = new Predicate<Integer>() {
            @Override
            public Boolean apply(Integer arg) {
                return arg < 6;
            }
        };

        assertEquals(
                Arrays.asList(1, 2, 3, 4, 5),
                f.apply(p, testList)
        );
        assertEquals(
                Arrays.asList(6, 7, 8, 9, 10),
                f.apply(p.not()).apply(testList)
        );
        assertEquals(
                Collections.EMPTY_LIST,
                f.apply(p.and(p.not())).apply(testList)
        );
        assertEquals(
                testList,
                f.apply(p.or(p.not())).apply(testList)
        );
    }
}
