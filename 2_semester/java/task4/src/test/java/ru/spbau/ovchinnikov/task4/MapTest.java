package ru.spbau.ovchinnikov.task4;

import org.junit.Test;
import ru.spbau.ovchinnikov.task4.base.Function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class MapTest {

    @Test
    public void test() {
        assertEquals(
                new Map<String, Integer>().apply(FunctionTest.TO_STRING, Arrays.asList(1, 2, 3, 4, 5, 6, 7)),
                Arrays.asList("1", "2", "3", "4", "5", "6", "7")
        );

    }

    @Test
    public void testComplex() {
        final List<Function<Integer, Integer>> functionList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            final int finalI = i;
            functionList.add(
                    new Function<Integer, Integer>() {
                        @Override
                        public Integer apply(Integer arg) {
                            return arg + finalI;
                        }
                    }
            );
        }
        final Function<Integer, Function<Integer, Integer>> applier = new Function<Integer, Function<Integer, Integer>>() {
            @Override
            public Integer apply(Function<Integer, Integer> arg) {
                return arg.apply(100);
            }
        };
        assertEquals(
                new Map<Integer, Function<Integer, Integer>>()
                        .apply(applier)
                        .apply(functionList),
                Arrays.asList(100, 101, 102, 103, 104, 105, 106, 107, 108, 109)
        );
    }
}
