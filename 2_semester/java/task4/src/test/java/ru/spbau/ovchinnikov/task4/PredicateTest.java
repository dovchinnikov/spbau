package ru.spbau.ovchinnikov.task4;

import org.junit.Test;
import ru.spbau.ovchinnikov.task4.base.Predicate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class PredicateTest {

    @Test
    public void testConstants() {
        assertTrue(Predicate.alwaysTrue().apply(1));
        assertTrue(Predicate.alwaysTrue().apply("lol"));
        assertFalse(Predicate.alwaysFalse().apply(100500));
        assertFalse(Predicate.alwaysFalse().apply("lol"));
    }

    @Test
    public void test() {
        assertTrue(Predicate.notNull().apply("azaza"));
        assertFalse(Predicate.notNull().apply(null));
        assertTrue(Predicate.less(1).apply(-1));
        assertFalse(Predicate.less(1).apply(3));
        assertTrue(Predicate.equals(2).apply(2));
        assertFalse(Predicate.equals(2).apply(3));
    }

    @Test
    public void testLogic() {
        assertFalse(Predicate.alwaysTrue().and(Predicate.alwaysFalse()).apply(new Object()));
        assertFalse(Predicate.alwaysFalse().and(Predicate.alwaysTrue()).apply(new Object()));
        assertTrue(Predicate.alwaysTrue().or(Predicate.alwaysFalse()).apply(new Object()));
        assertTrue(Predicate.alwaysFalse().or(Predicate.alwaysTrue()).apply(new Object()));
        assertTrue(Predicate.alwaysFalse().not().and(Predicate.alwaysTrue()).apply(new Object()));
        assertFalse(Predicate.alwaysTrue().not().and(Predicate.alwaysFalse()).apply(new Object()));
    }

}
