package ru.spbau.ovchinnikov.task4;

import org.junit.Test;
import ru.spbau.ovchinnikov.task4.base.Function;
import ru.spbau.ovchinnikov.task4.base.Function2;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class FoldrTest {

    @Test
    public void testSum() {
        final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        final Function<Integer, Collection<? extends Integer>> sum = new Foldr<Integer, Integer>(0).apply(
                new Function2<Integer, Integer, Integer>() {
                    @Override
                    public Integer apply(Integer arg1, Integer arg2) {
                        return arg1 + arg2;
                    }
                }
        );
        assertEquals(55L, (long) sum.apply(list));
    }


    @Test
    public void testJoin() {
        final List<String> list = Arrays.asList("hello ", "world", "!");
        final Function<StringBuilder, Collection<? extends String>> join =
                new Foldr<StringBuilder, String>(new StringBuilder()).apply(
                        new Function2<StringBuilder, String, StringBuilder>() {
                            @Override
                            public StringBuilder apply(String str, StringBuilder builder) {
                                return builder.append(str);
                            }
                        }
                );
        assertEquals("hello world!", join.apply(list).toString());
    }
}
