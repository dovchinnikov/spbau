package ru.spbau.ovchinnikov.task4;

import ru.spbau.ovchinnikov.task4.base.Function2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Represents {@code take} function.
 * <p/>
 * For more information please refer to {@link #apply(Integer, Collection)}.
 *
 * @param <T> supertype of the collection elements.
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class Take<T> extends Function2<Collection<T>, Integer, Collection<? extends T>> {


    /**
     * Takes first {@code number} of elements from the specified {@code collection}.
     * <p/>
     * If there is a lack of elements if the specified {@code collection}, all available elements will be taken.
     *
     * @param number     number of elements to take.
     * @param collection collection, from where to take.
     * @return new {@code Collection} of elements.
     */
    @Override
    public Collection<T> apply(Integer number, Collection<? extends T> collection) {
        final Collection<T> result = new ArrayList<>();
        final Iterator<? extends T> it = collection.iterator();
        for (int i = 0; i < number && it.hasNext(); i++) {
            result.add(it.next());
        }
        return result;
    }
}
