package ru.spbau.ovchinnikov.task4;

import ru.spbau.ovchinnikov.task4.base.Function2;

import java.util.Collection;

/**
 * Represents {@code foldr} function.
 * <p/>
 * For more information please refer to {@link #apply(Function2, Collection)}.
 *
 * @param <R> type of result and starting value.
 * @param <T> supertype of elements in the collection.
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class Foldr<R, T> extends Function2<R, Function2<R, ? super T, R>, Collection<? extends T>> {

    private final R start;

    /**
     * Creates new {@code Foldr} function object with starting value.
     *
     * @param start starting value
     */
    public Foldr(final R start) {
        this.start = start;
    }

    /**
     * Reduces the {@code collection} using the binary {@code function} and starting value.
     *
     * @param function   function to be applied.
     * @param collection collection to be reduced.
     * @return result of the reducing.
     */
    @Override
    public R apply(final Function2<R, ? super T, R> function, final Collection<? extends T> collection) {
        R res = start;
        for (final T elem : collection) {
            res = function.apply(elem, res);
        }
        return res;
    }
}
