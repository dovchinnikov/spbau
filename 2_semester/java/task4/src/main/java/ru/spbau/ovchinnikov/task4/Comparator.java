package ru.spbau.ovchinnikov.task4;

import ru.spbau.ovchinnikov.task4.base.Function;

/**
 * Class for comparing objects using helper {@link Function}.
 *
 * @param <T> type of objects to be compared.
 * @param <A> return type of helper function.
 * @author Daniil Ovchinnikov
 * @since 3/19/14
 */
public class Comparator<T, A extends Comparable<? super A>> {

    private final Function<A, ? super T> f;

    /**
     * Constructs new {@code Comparator} and stores helper {@link Function} for further use.
     *
     * @param f helper function
     */
    public Comparator(final Function<A, ? super T> f) {
        this.f = f;
    }

    /**
     * Applies helper {@link Function} to the arguments and compares results.
     *
     * @param a first object.
     * @param b second object.
     * @return comparison result.
     */
    public int compare(final T a, final T b) {
        return f.apply(a).compareTo(f.apply(b));
    }
}
