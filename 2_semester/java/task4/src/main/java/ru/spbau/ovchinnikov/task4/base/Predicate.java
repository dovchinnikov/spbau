package ru.spbau.ovchinnikov.task4.base;

/**
 * Represents function of one argument returning {@link Boolean}.
 * <p/>
 * To implement function logic you must override {@link #apply} method.
 *
 * @param <T> type of argument to applied to.
 * @author Daniil Ovchinnikov
 * @since 3/19/14
 */
public abstract class Predicate<T> extends Function<Boolean, T> {

    /**
     * Represents {@link Predicate} that always returns {@code true}.
     *
     * @param <S> type of argument to apply predicate to.
     */
    public static class TruePredicate<S> extends Predicate<S> {

        /**
         * Returns {@code true} no matter of argument.
         *
         * @param arg argument to be applied to.
         * @return {@code true}.
         */
        @Override
        public Boolean apply(S arg) {
            return true;
        }
    }

    /**
     * Represents {@link Predicate} that always returns {@code false}.
     *
     * @param <S> type of argument to apply predicate to.
     */
    public static class FalsePredicate<S> extends Predicate<S> {

        /**
         * Returns {@code false} no matter of argument.
         *
         * @param arg argument to be applied to.
         * @return {@code false}.
         */
        @Override
        public Boolean apply(S arg) {
            return false;
        }
    }

    /**
     * Represents {@link Predicate} that always returns {@code false}.
     *
     * @param <S> type of argument to apply predicate to.
     */
    public static class NotNullPredicate<S> extends Predicate<S> {

        /**
         * Returns {@code true} if arg is not {@code null}, otherwise returns {@code false}.
         *
         * @param arg argument to be applied to.
         * @return {@code true} if arg is not {@code null}, otherwise returns {@code false}.
         */
        @Override
        public Boolean apply(final S arg) {
            return arg != null;
        }
    }

    /**
     * Represents {@link Predicate} that checks if argument is less than specified value.
     *
     * @param <S> type of argument to apply predicate to.
     */
    public static class LessPredicate<S extends Comparable<S>> extends Predicate<S> {

        private final S value;

        /**
         * Constructs new {@code Predicate} and stores specified value.
         *
         * @param value specified value.
         */
        public LessPredicate(S value) {
            this.value = value;
        }

        /**
         * Compares arg with stored value and returns result.
         *
         * @param arg argument to be compared.
         * @return {@code true} if {@code arg} is less than the stored value, otherwise {@code false}.
         * @see {@link java.lang.Comparable#compareTo}
         */
        @Override
        public Boolean apply(final S arg) {
            return value.compareTo(arg) > 0;
        }
    }


    /**
     * Represents {@link Predicate} that checks if argument is equal to specified value.
     *
     * @param <S> type of argument to apply predicate to.
     */
    public static class EqualsPredicate<S extends Comparable<S>> extends Predicate<S> {

        private final S value;

        /**
         * Constructs new {@code Predicate} and stores specified value.
         *
         * @param value specified value.
         */
        public EqualsPredicate(S value) {
            this.value = value;
        }

        /**
         * Compares arg with stored value and returns result.
         *
         * @param arg argument to be compared.
         * @return {@code true} if {@code arg} is equal to the stored value, otherwise {@code false}.
         * @see {@link java.lang.Comparable#compareTo}
         */
        @Override
        public Boolean apply(final S arg) {
            return value.compareTo(arg) == 0;
        }
    }


    /**
     * Creates {@code Predicate} object returning {@code true} no matter of argument.
     *
     * @param <S> type of argument to apply predicate to.
     * @return new {@code Predicate} object returning {@code true} no matter of argument.
     */
    public static <S> TruePredicate<S> alwaysTrue() {
        return new TruePredicate<>();
    }

    /**
     * Creates new {@code Predicate} object returning {@code false} no matter of argument.
     *
     * @param <S> type of argument to apply predicate to.
     * @return new {@code Predicate} object returning {@code false} no matter of argument.
     */
    public static <S> FalsePredicate<S> alwaysFalse() {
        return new FalsePredicate<>();
    }

    /**
     * Creates new {@code Predicate} object checking if argument is not {@code null}.
     *
     * @param <S> type of argument to apply predicate to.
     * @return new {@code Predicate} object checking if argument is not {@code null}.
     */
    public static <S> NotNullPredicate<S> notNull() {
        return new NotNullPredicate<>();
    }

    /**
     * Creates new {@link ru.spbau.ovchinnikov.task4.base.Predicate.EqualsPredicate} with specified value.
     *
     * @param value value to be compared to.
     * @param <S>   type of the value to be compared to.
     * @return new {@link ru.spbau.ovchinnikov.task4.base.Predicate.EqualsPredicate} with specified value.
     */
    public static <S extends Comparable<S>> Predicate<? super S> equals(final S value) {
        return new EqualsPredicate<>(value);
    }

    /**
     * Creates new {@link Predicate.LessPredicate} with specified value.
     *
     * @param value value to be compared to.
     * @param <S>   type of the value to be compared to.
     * @return new {@link Predicate.LessPredicate} with specified value.
     */
    public static <S extends Comparable<S>> Predicate<S> less(final S value) {
        return new LessPredicate<>(value);
    }


    /**
     * If current {@code Predicate} is instance of {@link Predicate.TruePredicate}, instance of {@link Predicate.FalsePredicate} will be returned and vice versa.
     * Otherwise new {@code Predicate} inverting logic of current {@code Predicate} will be created.
     *
     * @return {@code Predicate} representing logic {@code not} operation.
     */
    public Predicate<T> not() {
        if (this instanceof FalsePredicate) {
            return new TruePredicate<>();
        } else if (this instanceof TruePredicate) {
            return new FalsePredicate<>();
        } else {
            return new Predicate<T>() {
                @Override
                public Boolean apply(final T arg) {
                    return !Predicate.this.apply(arg);
                }
            };
        }
    }

    /**
     * Represents logic {@code and} operation with {@code Predicate} passed as parameter.
     *
     * @param other {@code Predicate} to create logic {@code and} with.
     * @return {@code Predicate} representing logic {@code and} operation.
     */
    public Predicate<T> and(final Predicate<T> other) {
        if (this instanceof FalsePredicate || other instanceof TruePredicate) {
            return this;
        } else if (this instanceof TruePredicate || other instanceof FalsePredicate) {
            return other;
        } else {
            return new Predicate<T>() {
                @Override
                public Boolean apply(final T arg) {
                    return Predicate.this.apply(arg) && other.apply(arg);
                }
            };
        }
    }

    /**
     * Represents logic {@code or} operation with {@code Predicate} passed as parameter.
     *
     * @param other {@code Predicate} to create logic {@code or} with.
     * @return {@code Predicate} representing logic {@code or} operation.
     */
    public Predicate<T> or(final Predicate<T> other) {
        if (this instanceof TruePredicate || other instanceof FalsePredicate) {
            return this;
        } else if (this instanceof FalsePredicate || other instanceof TruePredicate) {
            return other;
        } else {
            return new Predicate<T>() {
                @Override
                public Boolean apply(final T arg) {
                    return Predicate.this.apply(arg) || other.apply(arg);
                }
            };
        }
    }
}
