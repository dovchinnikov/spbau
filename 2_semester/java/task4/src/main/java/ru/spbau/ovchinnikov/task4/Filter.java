package ru.spbau.ovchinnikov.task4;

import ru.spbau.ovchinnikov.task4.base.Function2;
import ru.spbau.ovchinnikov.task4.base.Predicate;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents {@code filter} function.
 * <p/>
 * For more information please refer to {@link #apply(Predicate, Collection)}.
 *
 * @param <T> supertype of collection elements.
 * @author Daniil Ovchinnikov
 * @since 3/19/14
 */
public class Filter<T> extends Function2<Collection<T>, Predicate<? super T>, Collection<? extends T>> {

    /**
     * Applies {@code predicate} to each element in the specified {@code collection}.
     * If it returns {@code true} elements will be added to output {@code collection},
     * otherwise element it will be skipped.
     *
     * @param predicate  {@code predicate} to check elements with.
     * @param collection {@code collection} to filter.
     * @return new {@code collection} of elements filtered with {@code predicate}.
     */
    @Override
    public Collection<T> apply(final Predicate<? super T> predicate, final Collection<? extends T> collection) {
        final Collection<T> result = new ArrayList<>();
        for (final T obj : collection) {
            if (predicate.apply(obj)) {
                result.add(obj);
            }
        }
        return result;
    }
}
