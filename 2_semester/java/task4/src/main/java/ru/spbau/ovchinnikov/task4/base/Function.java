package ru.spbau.ovchinnikov.task4.base;

/**
 * Abstract class representing function of one argument.
 * <p/>
 * To implement function logic you must override {@link #apply} method.
 *
 * @param <R> type of return value.
 * @param <P> type of argument to be applied to.
 * @author Daniil Ovchinnikov
 * @since 3/19/14
 */
public abstract class Function<R, P> {
    /**
     * You must implement this method with function logic in your functions.
     *
     * @param arg argument to be applied to.
     * @return function result.
     */
    public abstract R apply(final P arg);

    /**
     * Creates new {@link Function} object representing composition of two functions.
     * <p/>
     * Firstly current function will be applied,
     * then the result will be passed as argument to specified function. <br/>
     * Is equivalent to {@code specifiedFunction.apply(this.apply(arg))}.
     *
     * @param function {@link Function} to be applied to the result of current {@code Function}.
     * @param <S>      return type of specified function
     * @return new {@link Function} object representing composition of two functions.
     */
    public <S> Function<S, P> then(final Function<S, ? super R> function) {
        return new Function<S, P>() {
            @Override
            public S apply(final P arg) {
                return function.apply(Function.this.apply(arg));
            }
        };
    }
}
