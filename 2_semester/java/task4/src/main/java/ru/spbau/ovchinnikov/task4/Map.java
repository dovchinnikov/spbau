package ru.spbau.ovchinnikov.task4;

import ru.spbau.ovchinnikov.task4.base.Function;
import ru.spbau.ovchinnikov.task4.base.Function2;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents {@code map} function.
 * <p/>
 * For more information please refer to {@link #apply(Function, Collection)}.
 *
 * @param <S> type of resulting elements.
 * @param <T> supertype of input elements.
 * @author Daniil Ovchinnikov
 * @since 3/27/14
 */
public class Map<S, T> extends Function2<Collection<S>, Function<? extends S, ? super T>, Collection<? extends T>> {

    /**
     * Applies {@code function} to every element in the specified {@code collection}
     * and returns {@code Collection} of results.
     *
     * @param function   function to be applied.
     * @param collection collection of elements to apply {@code function} to.
     * @return new {@code Collection} of results of applying.
     */
    @Override
    public Collection<S> apply(final Function<? extends S, ? super T> function, final Collection<? extends T> collection) {
        final Collection<S> res = new ArrayList<>();
        for (final T obj : collection) {
            res.add(function.apply(obj));
        }
        return res;
    }
}
