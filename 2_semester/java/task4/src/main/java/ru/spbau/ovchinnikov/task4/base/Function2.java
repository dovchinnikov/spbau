package ru.spbau.ovchinnikov.task4.base;

/**
 * Abstract class representing function of two arguments.
 * <p/>
 * To implement function logic you must override {@link #apply} method.
 *
 * @param <R>  type of return value.
 * @param <P1> type of the first argument to be applied to.
 * @param <P2> type of the second argument to be applied to.
 * @author Daniil Ovchinnikov
 * @since 3/19/14
 */
public abstract class Function2<R, P1, P2> {

    /**
     * You must implement this method with function logic in your functions.
     *
     * @param arg1 first argument to be applied to.
     * @param arg2 second argument to be applied to.
     * @return function result.
     */
    public abstract R apply(final P1 arg1, final P2 arg2);

    /**
     * Creates new {@link ru.spbau.ovchinnikov.task4.base.Function} object representing curried function.<br/>
     * Uses {@link #bind1(P1)}
     *
     * @param arg first argument to be applied to.
     * @return new {@link ru.spbau.ovchinnikov.task4.base.Function} object that can be applied to the second argument.
     * @see #bind1
     */
    public Function<R, P2> apply(final P1 arg) {
        return bind1(arg);
    }

    /**
     * Creates new {@link Function2} object representing composition of two functions.
     * <p/>
     * Firstly current function will be applied,
     * then the result will be passed as argument to specified function. <br/>
     * Is equivalent to {@code specifiedFunction.apply(this.apply(arg1, arg2))}.
     *
     * @param function {@link ru.spbau.ovchinnikov.task4.base.Function}
     *                 to be applied to the result
     *                 of current {@link Function2#apply(P1, P2)}.
     *                 Should have argument type of {@code R}.
     * @param <S>      return type of specified function.
     * @return new {@link Function2} object representing composition of two functions.
     */
    public <S> Function2<S, P1, P2> then(final Function<S, ? super R> function) {
        return new Function2<S, P1, P2>() {
            @Override
            public S apply(final P1 arg1, final P2 arg2) {
                return function.apply(Function2.this.apply(arg1, arg2));
            }
        };
    }

    /**
     * Binds specified argument as first argument of the current function.
     *
     * @param arg1 argument to be bound.
     * @return new {@link ru.spbau.ovchinnikov.task4.base.Function} object
     * that can be applied to the second argument.
     */
    public Function<R, P2> bind1(final P1 arg1) {
        return new Function<R, P2>() {
            @Override
            public R apply(final P2 arg2) {
                return Function2.this.apply(arg1, arg2);
            }
        };
    }

    /**
     * Binds specified argument as second argument of the current function.
     *
     * @param arg2 argument to be bound.
     * @return new {@link ru.spbau.ovchinnikov.task4.base.Function} object
     * that can be applied to the first argument.
     */
    public Function<R, P1> bind2(final P2 arg2) {
        return new Function<R, P1>() {
            @Override
            public R apply(final P1 arg1) {
                return Function2.this.apply(arg1, arg2);
            }
        };
    }
}
