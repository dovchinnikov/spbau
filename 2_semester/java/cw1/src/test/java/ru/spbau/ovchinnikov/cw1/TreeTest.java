package ru.spbau.ovchinnikov.cw1;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Daniil Ovchinnikov
 * @since 3/26/14
 */
public class TreeTest {

    @Test(expected = BinaryTreeInsertionException.class)
    public void nullExceptionTest() throws BinaryTreeInsertionException {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.insert(null);
    }

    @Test(expected = BinaryTreeInsertionException.class)
    public void duplicateKeyExceptionTest() throws BinaryTreeInsertionException {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.insert(1);
        tree.insert(1);
    }

    @Test
    public void test1() throws BinaryTreeInsertionException {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        Assert.assertEquals(2, tree.getHeight());
    }

    @Test
    public void test2() throws BinaryTreeInsertionException {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.insert(3);
        tree.insert(2);
        tree.insert(1);
        tree.insert(4);
        tree.insert(6);
        tree.insert(5);
        Assert.assertEquals(3, tree.getHeight());
    }


    @Test
    public void lookupTest() throws BinaryTreeInsertionException {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.insert(3);
        tree.insert(2);
        tree.insert(1);
        tree.insert(4);
        tree.insert(6);
        tree.insert(5);
        Assert.assertTrue(tree.has(1));
        Assert.assertTrue(tree.has(2));
        Assert.assertTrue(tree.has(5));
        Assert.assertTrue(tree.has(6));
        Assert.assertFalse(tree.has(7));
        Assert.assertFalse(tree.has(null));
    }

    @Test
    public void selectorTest() throws BinaryTreeInsertionException {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.insert(3);
        tree.insert(2);
        tree.insert(1);
        tree.insert(4);
        tree.insert(6);
        tree.insert(5);
        Selector<Integer> selector = tree.getSelector();
        Assert.assertTrue(selector.hasNext());
        selector.next();
        Assert.assertTrue(selector.current() == 1);
        selector.next();
        selector.next();
        selector.next();
        selector.next();
        selector.next();
        Assert.assertFalse(selector.hasNext());
    }


    @Test
    public void reverseSelectorTest() throws BinaryTreeInsertionException {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.insert(3);
        tree.insert(2);
        tree.insert(1);
        tree.insert(4);
        tree.insert(6);
        tree.insert(5);
        Selector<Integer> selector = tree.getReverseSelector();
        Assert.assertTrue(selector.hasNext());
        selector.next();
        Assert.assertTrue(selector.current() == 6);
        selector.next();
        selector.next();
        selector.next();
        Assert.assertTrue(selector.current() == 3);
        selector.next();
        selector.next();
        Assert.assertFalse(selector.hasNext());
    }
}
