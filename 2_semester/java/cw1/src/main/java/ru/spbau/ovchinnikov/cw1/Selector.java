package ru.spbau.ovchinnikov.cw1;

/**
 * @author Daniil Ovchinnikov
 * @since 3/26/14
 */
public interface Selector<T extends Comparable<T>> {

    T current();

    boolean hasNext();

    void next();

    BinaryTree<T> getContainer();

    boolean isForward();
}
