package ru.spbau.ovchinnikov.cw1;

/**
 * @author Daniil Ovchinnikov
 * @since 3/26/14
 */
public class BinaryTreeInsertionException extends Exception {

    public BinaryTreeInsertionException(String message) {
        super(message);
    }
}
