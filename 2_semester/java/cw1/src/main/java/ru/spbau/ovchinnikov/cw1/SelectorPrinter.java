package ru.spbau.ovchinnikov.cw1;

/**
 * @author Daniil Ovchinnikov
 * @since 3/26/14
 */
public class SelectorPrinter {

    public static void print(Selector selector) {
        final Selector s = selector.isForward()
                ? selector.getContainer().getSelector()
                : selector.getContainer().getReverseSelector();
        while (s.hasNext()) {
            s.next();
            System.out.print(s.current() + " ");
        }
    }

}
