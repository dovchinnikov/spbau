package ru.spbau.ovchinnikov.cw1;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniil Ovchinnikov
 * @since 3/26/14
 */
public class BinaryTree<T extends Comparable<T>> {

    private static class Node<T> {

        Node(T value, Node<T> parent) {
            this.parent = parent;
            this.value = value;
        }

        Node<T> parent;
        Node<T> left;
        Node<T> right;
        T value;
    }

    private Node<T> root;
    private int height;

    public BinaryTree() {
        height = 0;
    }


    public int getHeight() {
        return height;
    }

    void insert(final T value) throws BinaryTreeInsertionException {
        if (value == null) {
            throw new BinaryTreeInsertionException("null value");
        }
        if (root == null) {
            root = new Node<>(value, null);
        } else {
            height = insert(value, root, 0);
        }
    }

    public boolean has(final T value) {
        return lookup(value, root);
    }

    public Selector<T> getSelector() {
        return new Selector<T>() {
            private int current;
            private List<T> nodes;

            {
                current = -1;
                nodes = dfs();
            }

            @Override
            public T current() {
                return nodes.get(current);
            }

            @Override
            public boolean hasNext() {
                return current < nodes.size() - 1;
            }

            @Override
            public void next() {
                current++;
            }

            @Override
            public BinaryTree<T> getContainer() {
                return BinaryTree.this;
            }

            @Override
            public boolean isForward() {
                return true;
            }
        };
    }

    public Selector<T> getReverseSelector() {
        return new Selector<T>() {
            private int current;
            private List<T> nodes;

            {
                nodes = dfs();
                current = nodes.size();
            }

            @Override
            public T current() {
                return nodes.get(current);
            }

            @Override
            public boolean hasNext() {
                return current > 0;
            }

            @Override
            public void next() {
                current--;
            }

            @Override
            public BinaryTree<T> getContainer() {
                return BinaryTree.this;
            }

            @Override
            public boolean isForward() {
                return false;
            }
        };
    }



    private int insert(final T value, final Node<T> root, final int level) throws BinaryTreeInsertionException {
        if (value.compareTo(root.value) < 0) {
            if (root.left == null) {
                root.left = new Node<>(value, root);
                return level + 1;
            } else {
                return insert(value, root.left, level + 1);
            }
        } else if (value.compareTo(root.value) > 0) {
            if (root.right == null) {
                root.right = new Node<>(value, root);
                return level + 1;
            } else {
                return insert(value, root.right, level + 1);
            }
        }
        throw new BinaryTreeInsertionException("key already exists");
    }

    private boolean lookup(final T value, final Node<T> root) {
        if (value == null || root == null) {
            return false;
        } else {
            final int compare = value.compareTo(root.value);
            return compare < 0
                    ? lookup(value, root.left)
                    : (compare <= 0 || lookup(value, root.right));
        }
    }

    private List<T> dfs() {
        List<T> res = new ArrayList<>();
        if (root != null) {
            dfs(res, root);
        }
        return res;
    }

    private void dfs(List<T> nodes, Node<T> root) {
        if (root.left != null) {
            dfs(nodes, root.left);
        }
        nodes.add(root.value);
        if (root.right != null) {
            dfs(nodes, root.right);
        }
    }
}
