package ru.spbau.ovchinnikov.cw1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Daniil Ovchinnikov
 * @since 3/26/14
 */
public class Main {

    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("Enter filename");
            return;
        }

        final BinaryTree<Integer> tree = new BinaryTree<>();

        try (BufferedReader r = new BufferedReader(new FileReader(args[0]))) {
            String s;
            while ((s = r.readLine()) != null) {
                String[] numbers = s.split(" ");
                for (String number : numbers) {
                    try {
                        tree.insert(Integer.parseInt(number));
                    } catch (BinaryTreeInsertionException e) {
                        System.err.println(e.getMessage());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Selector<Integer> forward = tree.getSelector();
        SelectorPrinter.print(forward);
        System.out.println();

        final Selector<Integer> reverse = tree.getReverseSelector();
        SelectorPrinter.print(reverse);
        System.out.println();

    }
}
