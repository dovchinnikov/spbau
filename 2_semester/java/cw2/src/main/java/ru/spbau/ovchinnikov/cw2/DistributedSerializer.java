package ru.spbau.ovchinnikov.cw2;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * @author Daniil Ovchinnikov
 * @since 5/21/14
 */
public class DistributedSerializer<T> {

    public static final Integer THREADS_COUNT = 5;
    private static final Map<Class, Class> WRAPPERS = new HashMap<Class, Class>() {{
        put(boolean.class, Boolean.class);
        put(byte.class, Byte.class);
        put(char.class, Character.class);
        put(double.class, Double.class);
        put(float.class, Float.class);
        put(int.class, Integer.class);
        put(long.class, Long.class);
        put(short.class, Short.class);
        put(void.class, Void.class);
    }};

    private final Class<T> clazz;
    private final ExecutorService executor;

    public DistributedSerializer(Class<T> clazz) {
        this.clazz = clazz;
        this.executor = Executors.newFixedThreadPool(THREADS_COUNT);
    }

    void serialize(final T object, final String filename) throws InterruptedException {

        final Future<Void> result = executor.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                Properties p = new Properties();
                for (final PropertyDescriptor propertyDescriptor
                        : Introspector.getBeanInfo(clazz, Object.class).getPropertyDescriptors()) {
                    final Method getter = propertyDescriptor.getReadMethod();
                    if (getter != null) {
                        p.put(propertyDescriptor.getDisplayName(), getter.invoke(object).toString());
                    }
                }
                try (FileOutputStream out = new FileOutputStream(filename)) {
                    p.store(out, null);
                }
                return null;
            }
        });

        try {
            result.get();
        } catch (ExecutionException e) {
            throw (InterruptedException) new InterruptedException("Execution failed").initCause(e);
        }
    }

    T deserialize(final String filename) throws InterruptedException {
        final Future<T> result = executor.submit(new Callable<T>() {
            @Override
            public T call() throws Exception {
                Properties properties = new Properties();
                try (FileInputStream in = new FileInputStream(filename)) {
                    properties.load(in);
                }

                final T object = clazz.newInstance();
                for (final PropertyDescriptor propertyDescriptor
                        : Introspector.getBeanInfo(clazz, Object.class).getPropertyDescriptors()) {
                    final Method setter = propertyDescriptor.getWriteMethod();
                    if (setter != null && properties.containsKey(propertyDescriptor.getDisplayName())) {
                        final Class setterClass = propertyDescriptor.getPropertyType().isPrimitive()
                                ? WRAPPERS.get(propertyDescriptor.getPropertyType())
                                : propertyDescriptor.getPropertyType();

                        final String property = properties.getProperty(propertyDescriptor.getDisplayName());
                        Object value = property;
                        if (setterClass.equals(Character.class)) {
                            value = property.charAt(0);
                        } else if (!setterClass.equals(String.class)) {
                            for (Method parser : setterClass.getDeclaredMethods()) {
                                if (parser.getName().startsWith("parse") && parser.getParameterTypes().length == 1) {
                                    value = parser.invoke(null, property);
                                    break;
                                }
                            }
                        }
                        setter.invoke(object, value);
                    }
                }
                return object;
            }
        });

        try {
            return result.get();
        } catch (ExecutionException e) {
            throw (InterruptedException) new InterruptedException("Execution failed").initCause(e);
        }
    }
}
