package ru.spbau.ovchinnikov.cw2;

/**
 * @author Daniil Ovchinnikov
 * @since 5/21/14
 */
@SuppressWarnings("unused")
public class A {

    private String sss;
    private String lolAzazaString;
    private Float someFloat;
    private Integer someNumber;
    private Double someDoubled;
    private int someInt;
    private double someDouble;
    private char someChar;
    private byte someByte;

    public A() {
    }

    public A(String sss, String lolAzazaString, Float someFloat, Integer someNumber, Double someDoubled, int someInt, double someDouble, char someChar, byte someByte) {
        this.sss = sss;
        this.lolAzazaString = lolAzazaString;
        this.someFloat = someFloat;
        this.someNumber = someNumber;
        this.someDoubled = someDoubled;
        this.someInt = someInt;
        this.someDouble = someDouble;
        this.someChar = someChar;
        this.someByte = someByte;
    }

    public String getSss() {
        return sss;
    }

    public String getLolAzazaString() {
        return lolAzazaString;
    }

    public void setLolAzazaString(String lolAzazaString) {
        this.lolAzazaString = lolAzazaString;
    }

    public Float getSomeFloat() {
        return someFloat;
    }

    public void setSomeFloat(Float someFloat) {
        this.someFloat = someFloat;
    }

    public Integer getSomeNumber() {
        return someNumber;
    }

    public void setSomeNumber(Integer someNumber) {
        this.someNumber = someNumber;
    }

    public Double getSomeDoubled() {
        return someDoubled;
    }

    public void setSomeDoubled(Double someDoubled) {
        this.someDoubled = someDoubled;
    }

    public int getSomeInt() {
        return someInt;
    }

    public void setSomeInt(int someInt) {
        this.someInt = someInt;
    }

    public double getSomeDouble() {
        return someDouble;
    }

    public void setSomeDouble(double someDouble) {
        this.someDouble = someDouble;
    }

    public void setSss(String sss) {
        this.sss = sss;
    }

    public char getSomeChar() {
        return someChar;
    }

    public void setSomeChar(char someChar) {
        this.someChar = someChar;
    }

    public byte getSomeByte() {
        return someByte;
    }

    public void setSomeByte(byte someByte) {
        this.someByte = someByte;
    }

    public void setLol(float a) {
    }

}
