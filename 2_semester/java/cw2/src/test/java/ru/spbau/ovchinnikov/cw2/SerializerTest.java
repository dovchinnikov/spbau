package ru.spbau.ovchinnikov.cw2;

import org.junit.Assert;
import org.junit.Test;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 * @author Daniil Ovchinnikov
 * @since 5/21/14
 */
public class SerializerTest {

    public static final String FILENAME = "file.properties";

    @Test
    public void test() throws Exception {
        A original = new A("hello", "world", 3.1415f, 42, 4815162342d, 100, 500, 'x', (byte) 12);
        DistributedSerializer<A> serializer = new DistributedSerializer<>(A.class);
        serializer.serialize(original, FILENAME);
        A deserialized = serializer.deserialize(FILENAME);
        for (PropertyDescriptor propertyDescriptor
                : Introspector.getBeanInfo(A.class, Object.class).getPropertyDescriptors()) {
            Method getter = propertyDescriptor.getReadMethod();
            if (getter != null && propertyDescriptor.getWriteMethod() != null) {
                Assert.assertEquals(getter.invoke(original), getter.invoke(deserialized));
            }
        }
    }
}
