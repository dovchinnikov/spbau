package ru.spbau.ovchinnikov.task1;

import java.text.ParseException;

/**
 * Contains application entry point. <br/>
 * For more information please refer to {@link #main(String[])}.
 *
 * @author Daniil Ovchinnikov
 * @since 2/23/14
 */
public class Main {

    /**
     * Sums integer numbers passed as command line arguments and prints result.
     * If some argument cannot be parsed "Parse exception" message will be printed.
     *
     * @param args Command line arguments.
     *             Each command line argument can hold several numbers divided by one
     *             or more {@link Character#isWhitespace whitespace} characters.
     */
    public static void main(String[] args) {

        StringBuilder builder = new StringBuilder();
        for (String s : args) {
            builder.append(s).append(' ');
        }
        String input = builder.toString();

        int sum = 0, number = 0;
        boolean negative = false;

        try {
            for (int i = 0; i < input.length(); i++) {
                char c = input.charAt(i);
                if (Character.isWhitespace(c)) {
                    if (negative && number == 0) {
                        throw new ParseException("Parse exception", i);
                    }
                    number *= negative ? -1 : 1;
                    sum += number;
                    number = 0;
                    negative = false;
                } else if (c == '-') {
                    if (negative) {
                        throw new ParseException("Parse exception", i);
                    }
                    negative = true;
                } else if (c >= '0' && c <= '9') {
                    number *= 10;
                    number += c - '0';
                } else {
                    throw new ParseException("Parse exception", i);
                }
            }

            System.out.println(sum);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
    }

}
