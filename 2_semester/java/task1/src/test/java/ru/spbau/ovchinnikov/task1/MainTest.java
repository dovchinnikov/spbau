package ru.spbau.ovchinnikov.task1;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MainTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void testEmptyInput() {
        Main.main(new String[]{});
        Assert.assertEquals("0\n", outContent.toString());
    }

    @Test
    public void test1() {
        Main.main(new String[]{"1", "2", "3"});
        Assert.assertEquals("6\n", outContent.toString());
    }

    @Test
    public void test2() {
        Main.main(new String[]{"1", "2", "-3"});
        Assert.assertEquals("0\n", outContent.toString());
    }

    @Test
    public void test3() {
        Main.main(new String[]{"1 2 3"});
        Assert.assertEquals("6\n", outContent.toString());
    }

    @Test
    public void test4() {
        Main.main(new String[]{"1 2", "3"});
        Assert.assertEquals("6\n", outContent.toString());
    }

    @Test
    public void test5() {
        Main.main(new String[]{"--1-1 2", "3"});
        Assert.assertEquals("Parse exception\n", outContent.toString());
    }

    @Test
    public void test6() {
        Main.main(new String[]{"--1 2", "3"});
        Assert.assertEquals("Parse exception\n", outContent.toString());
    }

    @Test
    public void test7() {
        Main.main(new String[]{"-1 -2 -3 ", "-", "3"});
        Assert.assertEquals("Parse exception\n", outContent.toString());
    }

    @Test
    public void test8() {
        Main.main(new String[]{"-1 -2 -3 ", "s7", "3"});
        Assert.assertEquals("Parse exception\n", outContent.toString());
    }

}
