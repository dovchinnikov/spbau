package ru.spbau.ovchinnikov.task3;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Contains application entry point. <br/>
 * For more information please refer to {@link #main(String[])}.
 *
 * @author Daniil Ovchinnikov
 * @since 3/12/14
 */
public class Main {

    /**
     * Compresses and decompresses files given as command line arguments.
     *
     * @param args command line arguments.
     *             The first command line is action.
     *             <p/>
     *             If action is {@code compress} then
     *             next argument is interpreted as output file name
     *             and the rest of arguments are files to be {@link #compress compressed}.
     *             <p/>
     *             If action is {@code decompress} then
     *             next argument is interpreted as archive name.
     *             Program will {@link #decompress decompress} files and directories from the archive
     *             to the current directory.
     */
    public static void main(String[] args) {

        try {
            if (args.length > 2 && "compress".equals(args[0])) {
                final String[] pathsToProcess = Arrays.copyOfRange(args, 2, args.length);
                compress(args[1], pathsToProcess);
            } else if (args.length > 1 && "decompress".equals(args[0])) {
                decompress(args[1]);
            } else {
                System.out.println("Usage: compress arch_name file1 [file2..] " +
                        "| decompress arch_name");
            }
        } catch (SecurityException | IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Compresses files and non-empty folders to the output file.
     * If one of the files can't be opened it will be skipped.
     *
     * @param archivePath         output file.
     * @param filePathsToCompress paths of files and folders to compress.
     * @throws IOException       if an I/O error occurs.
     * @throws SecurityException if can't write output file.
     * @see #listFiles(java.io.File, java.util.List)
     */
    public static void compress(final String archivePath, final String[] filePathsToCompress) throws IOException, SecurityException {

        final List<File> files = new ArrayList<>();
        for (final String filePath : filePathsToCompress) {
            final File d = new File(filePath);
            listFiles(d, files);
        }

        if (files.size() == 0) {
            System.out.println("Nothing to write");
            return;
        }

        try (final ZipOutputStream zz = new ZipOutputStream(new FileOutputStream(archivePath));
             final DataOutputStream dd = new DataOutputStream(zz)) {

            zz.putNextEntry(new ZipEntry("archive"));

            for (final File file : files) {
                try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file))) {
                    dd.writeUTF(file.getPath());
                    dd.writeLong(file.length());
                    while (bufferedInputStream.available() != 0) {
                        dd.write(bufferedInputStream.read());
                    }
                    System.out.println(file.getCanonicalPath());
                } catch (SecurityException | FileNotFoundException e) {
                    System.err.println("Couldn't open file, will be skipped: "
                            + file.getPath());
                }
            }

            zz.closeEntry();
        }
    }

    /**
     * Decompresses files from archive and restores folder structure
     * relative to current directory.
     *
     * @param archivePath path to archive file to decompress.
     * @throws IOException if an I/O error occurs
     */
    public static void decompress(final String archivePath) throws IOException {

        try (final ZipInputStream zz = new ZipInputStream(new FileInputStream(archivePath));
             final BufferedInputStream bb = new BufferedInputStream(zz);
             final DataInputStream dd = new DataInputStream(bb)) {

            zz.getNextEntry();

            dd.mark(1);
            while (dd.read() != -1) {
                dd.reset();

                final String filePath = dd.readUTF();
                long bytes = dd.readLong();

                try {
                    final File file = new File(filePath).getCanonicalFile();
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }
                    try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                        int bufferSize = 1024;
                        byte[] buf = new byte[bufferSize];
                        int bytesRead;
                        while (bytes > 0) {
                            try {
                                bytesRead = dd.read(buf, 0, bytes < bufferSize ? (int) bytes : bufferSize);
                            } catch (IOException e) {
                                throw new ArchiveIOException(e);
                            }

                            fileOutputStream.write(buf, 0, bytesRead);
                            bytes -= bytesRead;
                        }
                    }
                    System.out.println(file.getPath());
                } catch (SecurityException | IOException e) {
                    System.err.println("Couldn't write file, will be skipped: " + filePath);
                    while (bytes-- > 0) {
                        dd.read();
                    }
                }

                dd.mark(1);
            }
        } catch (ArchiveIOException e) {
            throw e.getCause();
        }
    }

    /**
     * Iterates over the files in the specified folder and its subfolders
     * and adds files to the {@link java.util.List}.
     * <p/>
     * If current root is a file then it will be added to the list.
     * If current root   is a directory then it will recursively iterate over it.
     *
     * @param currentFile {@link java.io.File} root of folder structure to be iterated.
     * @param files       {@link java.util.List} to add files to.
     */
    public static void listFiles(final File currentFile, final List<File> files) {
        if (currentFile == null) {
            return;
        }

        try {
            if (currentFile.isDirectory()) {
                for (final File f : currentFile.listFiles()) {
                    listFiles(f, files);
                }
            } else {
                files.add(currentFile);
            }
        } catch (SecurityException e) {
            System.err.println("Access denied, will be skipped: " + currentFile.getPath());
        }
    }
}
