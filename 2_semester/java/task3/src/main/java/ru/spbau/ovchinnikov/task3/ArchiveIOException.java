package ru.spbau.ovchinnikov.task3;

import java.io.IOException;

/**
 * Exception wrapper to differ archive read/write exceptions from files' exceptions.
 *
 * @author Daniil Ovchinnikov
 * @since 3/20/14
 */
public class ArchiveIOException extends IOException {

    /**
     * Constructs exception with specified cause.
     *
     * @param cause The cause to ba saves for further use.
     * @see IOException#IOException(Throwable)
     */
    public ArchiveIOException(IOException cause) {
        super(cause);
    }

    /**
     * Returns cause of this throwable casted to {@link IOException}.
     * Since cause can be set only once this is safe cast.
     *
     * @return The cause
     */
    @Override
    public synchronized IOException getCause() {
        return (IOException) super.getCause();
    }

}
