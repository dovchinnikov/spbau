import ru.spbau.ovchinnikov.task5.player.Player;

import java.util.Random;

/**
 * @author Daniil Ovchinnikov
 * @since 5/18/14
 */
public class Randosha extends Player {

    Random random = new Random();

    @Override
    public int move() {
        getName();
        getNextVictim();
        getNumber();
//        getPlayersInfo();
        getRoomNumber();
        return random.nextInt(3) - 1;
    }
}
