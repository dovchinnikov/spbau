package ru.spbau.ovchinnikov.task5;

import org.junit.Test;

import java.nio.file.NoSuchFileException;

/**
 * @author Daniil Ovchinnikov
 * @since 5/18/14
 */
public class GameTest {

    @Test(expected = NoSuchFileException.class)
    public void testFailure() throws Exception {
        new Game(10, "/data/repositories/spbau/java/task5/ai/azaza", 10).run();
    }

    @Test
    public void test() throws Exception {
        new Game(10, "/data/repositories/spbau/java/task5/target/classes", 3).run();
    }
}
