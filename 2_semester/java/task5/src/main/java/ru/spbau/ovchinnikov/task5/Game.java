package ru.spbau.ovchinnikov.task5;

import ru.spbau.ovchinnikov.task5.player.Player;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Represents the Game - the set of Tournaments - it is able to run tournaments and process their results.
 *
 * @author Daniil Ovchinnikov
 * @since 5/18/14
 */
public class Game {

    /**
     * Class for holding results of particular tournament.
     * C++-like typedef style.
     */
    public static class TournamentResult extends HashMap<Class<? extends Player>, Integer> {
    }

    private final int fieldSize;
    private final List<Class<? extends Player>> ais;
    private final int tournamentsCount;

    /**
     * Creates new instance of the Game.
     * Loads class files from {@code aiDirectory} and saves for further use.
     *
     * @param fieldSize   size of the field (number of rooms)
     * @param aiDirectory directory with AI classes
     * @param tournaments number of tournaments to run
     * @throws IOException if an IO error occurs
     */
    public Game(int fieldSize, String aiDirectory, int tournaments) throws IOException {
        this.fieldSize = fieldSize;
        this.ais = new ArrayList<>();
        this.tournamentsCount = tournaments;

        final Path aiDirectoryPath = Paths.get(aiDirectory);

        try (final DirectoryStream<Path> ds = Files.newDirectoryStream(aiDirectoryPath, "*.class")) {
            final URLClassLoader cl = new URLClassLoader(new URL[]{
                    aiDirectoryPath.toUri().toURL()
            });
            for (final Path path : ds) {
                try {
                    ais.add(cl.loadClass(path.getFileName().toString().replace(".class", "")).asSubclass(Player.class));
                } catch (ClassNotFoundException | ClassCastException e) {
                    System.out.println(e);
                    // skip if class not found (file might be removed, so we can't do anything)
                    // skip if cannot cast class to Player
                }
            }
        }
    }

    /**
     * Creates runs tournaments with multiple threads,
     * harvests results, process results, builds points table and prints it to {@code stdout}.
     *
     * @throws InterruptedException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void run() throws InterruptedException, InstantiationException, IllegalAccessException {
        final List<Tournament> tournaments = new ArrayList<>();
        for (int i = 0; i < tournamentsCount; i++) {
            tournaments.add(new Tournament(
                    this.fieldSize,
                    this.ais
            ));
        }

        // run asynchronously all tournaments and retrieve the results
        final List<TournamentResult> results = new ArrayList<>();
        for (final Future<TournamentResult> future
                : Executors.newFixedThreadPool(10).invokeAll(tournaments)) {
            try {
                results.add(future.get());
            } catch (InterruptedException | ExecutionException e) {
                System.out.println("Tournament failed: " + e);
            }
        }

        // process results
        final Map<Class<? extends Player>, List<Integer>> table = new HashMap<>();
        for (final TournamentResult result : results) {
            for (Class<? extends Player> c : result.keySet()) {
                if (!table.containsKey(c)) {
                    table.put(c, new ArrayList<Integer>());
                }
                table.get(c).add(result.get(c));
            }
        }

        // compute total points for each player
        for (final List<Integer> points : table.values()) {
            Integer total = 0;
            for (Integer i : points) {
                total += i;
            }
            points.add(total);
        }

        // generate output
        final StringBuilder sb = new StringBuilder();
        for (Class<? extends Player> c : table.keySet()) {
            sb.append(c.getSimpleName());
            for (Integer point : table.get(c)) {
                sb.append(" ").append(point);
            }
            sb.append("\n");
        }

        System.out.println(sb.toString());
    }
}
