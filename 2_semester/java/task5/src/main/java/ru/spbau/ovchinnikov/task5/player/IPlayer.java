package ru.spbau.ovchinnikov.task5.player;

import java.util.List;

/**
 * Player interface.
 *
 * @author Daniil Ovchinnikov
 * @since 5/18/14
 */
public interface IPlayer {

    /**
     * @return next move.
     */
    int move();

    /**
     * @return number of the player.
     */
    int getNumber();

    /**
     * @return number of the room player is in.
     */
    int getRoomNumber();

    /**
     * @return number of victim player.
     */
    int getNextVictim();

    /**
     * @return current info of all players in the field.
     */
    List<PlayerInfo> getPlayersInfo();

    /**
     * @return player name.
     */
    String getName();

}
