package ru.spbau.ovchinnikov.task5.player;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Abstract implementation of the {@link ru.spbau.ovchinnikov.task5.player.IPlayer player} interface.
 *
 * @author Daniil Ovchinnikov
 * @since 5/18/14
 */
public abstract class Player implements IPlayer {

    private int number;
    private PlayerInfo info;
    private Callable<List<PlayerInfo>> playerInfoHandle;

    public final void init(int number, int roomNumber, int nextVictim,
                           final Callable<List<PlayerInfo>> playerInfoHandle) {
        this.number = number;
        this.info = new PlayerInfo(roomNumber, nextVictim);
        this.playerInfoHandle = playerInfoHandle;
    }

    @Override
    public final int getNumber() {
        return this.number;
    }

    @Override
    public final int getRoomNumber() {
        return info.getRoomNumber();
    }

    public final void setRoomNumber(int roomNumber) {
        info = new PlayerInfo(roomNumber, info.getNextVictim());
    }

    @Override
    public final int getNextVictim() {
        return info.getNextVictim();
    }

    public final void setNextVictim(int newVictim) {
        this.info = new PlayerInfo(getRoomNumber(), newVictim);
    }

    @Override
    public final List<PlayerInfo> getPlayersInfo() {
        try {
            return playerInfoHandle.call();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    public final PlayerInfo getInfo() {
        return info;
    }
}
