package ru.spbau.ovchinnikov.task5;

import ru.spbau.ovchinnikov.task5.player.Player;
import ru.spbau.ovchinnikov.task5.player.PlayerInfo;

import java.util.*;
import java.util.concurrent.*;

/**
 * Represents the Tournament - one part of the Game.
 *
 * @author Daniil Ovchinnikov
 * @since 5/18/14
 */
public class Tournament implements Runnable, Callable<Game.TournamentResult> {

    private final int fieldSize;
    private final Map<Integer, Player> players;     // player index -> player
    private final Game.TournamentResult result;     // player class -> tournament points

    private static class Move {
        public final Player player;
        public final int move;

        private Move(Player player, int move) {
            this.player = player;
            this.move = move;
        }
    }

    /**
     * Creates new Tounrament.
     * Instantiates {@code classes}, creates initial point table.
     *
     * @param fieldSize size of the field (number of rooms)
     * @param ais       class instances of the AI
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public Tournament(int fieldSize, List<Class<? extends Player>> ais) throws IllegalAccessException, InstantiationException {
        this.fieldSize = fieldSize;
        this.players = new HashMap<>();
        this.result = new Game.TournamentResult();

        final Callable<List<PlayerInfo>> playerInfoHandler = new Callable<List<PlayerInfo>>() {
            @Override
            public List<PlayerInfo> call() throws Exception {
                final List<PlayerInfo> playerInfoList = new ArrayList<>();
                for (final Player player : players.values()) {
                    playerInfoList.add(player.getInfo());
                }
                return Collections.unmodifiableList(playerInfoList);
            }
        };

        // instantiate players
        Random random = new Random();
        int i = 0, n = ais.size();
        for (final Class<? extends Player> clz : ais) {
            Player player = clz.newInstance();
            player.init(i++, random.nextInt(fieldSize), i % n, playerInfoHandler);
            // store player for easy access by index
            this.players.put(player.getNumber(), player);
            // store player class for count points
            this.result.put(clz, 0);
        }
    }


    /**
     * Runs the tournament.
     * Each player instance operates in its own thread.
     * When exits, {@link #result} table will be fulfilled.
     */
    @Override
    public void run() {
        // store tasks for execution
        // here is the map, so it is easily disable task with player reference
        final Map<Player, Callable<Move>> playerActions = new HashMap<>();
        for (final Player player : players.values()) {
            playerActions.put(player, new Callable<Move>() {
                @Override
                public Move call() throws Exception {
                    return new Move(player, player.move());
                }
            });
        }

        final ExecutorService threadPool = Executors.newFixedThreadPool(10);

        while (players.size() > 1) {
            final Map<Player, Integer> moves = new HashMap<>();
            try {
                // call players for their moves asynchronously
                for (Future<Move> f : threadPool.invokeAll(playerActions.values())) {
                    Move m = f.get();
                    moves.put(m.player, m.move);
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            // process player moves synchronously
            for (Map.Entry<Player, Integer> move : moves.entrySet()) {
                final Player player = move.getKey();
                final int step = move.getValue();

                switch (step) {
                    case -1:
                    case 1:
                        int newRoom = player.getRoomNumber() + step;
                        if (0 <= newRoom && newRoom < fieldSize) {
                            player.setRoomNumber(newRoom);
                        } // do not do anything when wrong room
                        break;
                    case 0:
                        final Player victim = players.get(player.getNextVictim());
                        if (player.getRoomNumber() == victim.getRoomNumber()    // player is in the same cell
                                && moves.get(victim) == 0) {                    // victim is standing

                            player.setNextVictim(victim.getNextVictim());
                            result.put(player.getClass(), result.get(player.getClass()) + 2 + 1);
                            players.remove(victim.getNumber());
                            playerActions.remove(victim);
                        }
                        break;
                    default:
                        throw new RuntimeException("should never get here");
                }
            }
        }
        if (players.size() == 1) {
            for (Player player : players.values()) {
                int points = result.get(player.getClass());
                result.put(player.getClass(), points + 5);
            }
        }
    }

    @Override
    public Game.TournamentResult call() throws Exception {
        run();
        return result;
    }
}
