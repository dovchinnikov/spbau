package ru.spbau.ovchinnikov.task5.player;

/**
 * @author Daniil Ovchinnikov
 * @since 5/18/14
 */
public class PlayerInfo {

    public final int roomNumber;
    public final int nextVictim;

    public PlayerInfo(int roomNumber, int nextVictim) {
        this.roomNumber = roomNumber;
        this.nextVictim = nextVictim;
    }

    int getRoomNumber() {
        return roomNumber;
    }

    int getNextVictim() {
        return nextVictim;
    }
}
