package com.aptu.sd.coffeemachine.shell;

import com.aptu.sd.coffeemachine.machine.DepositTooSmallException;
import com.aptu.sd.coffeemachine.machine.NoSuchProductException;
import com.aptu.sd.coffeemachine.machine.VendingMachine;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static com.aptu.sd.coffeemachine.shell.CommandUtil.assertArgsLength;

/**
 * Created by IntelliJ IDEA.
 * User: andrey
 * Date: 5/23/12, 12:45 AM
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Select implements Command {

    @Override
    public void execute(String[] args, VendingMachine machine) throws CommandParseException {
        assertArgsLength(args, 1);
        String product = args[0];
        try {
            machine.purchaseProduct(product);
            System.out.println("Take your " + product);
        } catch (NoSuchProductException | DepositTooSmallException e) {
            System.out.println(e.getMessage());
        }
    }
}
