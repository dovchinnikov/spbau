package com.aptu.sd.coffeemachine.machine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.List;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
@Component("machineSwitcher")
public class VendingMachineSwitcher implements VendingMachine {

    @Autowired
    private List<VendingMachine> allMachines;

    private VendingMachine current;
    private Iterator<VendingMachine> iterator;

    @PostConstruct
    private void init() {
        iterator = allMachines.iterator();
        current = iterator.next();
    }

    public void switchMachine() {
        if (!iterator.hasNext()) {
            iterator = allMachines.iterator();
        }
        current = iterator.next();
    }

    @Override
    public long getDeposit() {
        return current.getDeposit();
    }

    @Override
    public void deposit(long amount) throws NonPositiveDepositException {
        current.deposit(amount);
    }

    @Override
    public long cancel() {
        return current.cancel();
    }

    @Override
    public void purchaseProduct(String productName) throws NoSuchProductException, DepositTooSmallException {
        current.purchaseProduct(productName);
    }

    @Override
    public long encash() {
        return current.encash();
    }
}
