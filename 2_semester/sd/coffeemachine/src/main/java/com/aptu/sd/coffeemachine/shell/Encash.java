package com.aptu.sd.coffeemachine.shell;

import com.aptu.sd.coffeemachine.machine.VendingMachine;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Encash implements Command {

    @Override
    public void execute(String[] args, VendingMachine machine) throws CommandParseException {
        System.out.println("Encashed: " + machine.encash());
    }
}
