package com.aptu.sd.coffeemachine.springapp;

import com.aptu.sd.coffeemachine.machine.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        "com.aptu.sd.coffeemachine.machine",
        "com.aptu.sd.coffeemachine.shell"
})
public class CoffeeMachineConfig {

    @Bean
    public Product latte() {
        return new Product("latte", 4, 2);
    }

    @Bean
    public Product cappuccino() {
        return new Product("cappuccino", 4, 2);
    }
}
