package com.aptu.sd.coffeemachine.machine;

import org.springframework.stereotype.Component;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
@Component("testMachine")
public class TestMachine implements VendingMachine {

    @Override
    public long getDeposit() {
        return 0;
    }

    @Override
    public void deposit(long amount) throws NonPositiveDepositException {
        System.out.println("I'm test machine");
    }

    @Override
    public long cancel() {
        System.out.println("I'm test machine");
        return 0;
    }

    @Override
    public void purchaseProduct(String productName) throws NoSuchProductException, DepositTooSmallException {
        System.out.println("I'm test machine");
    }

    @Override
    public long encash() {
        System.out.println("I'm test machine");
        return 0;
    }
}
