package com.aptu.sd.coffeemachine.shell;

import com.aptu.sd.coffeemachine.machine.VendingMachine;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by IntelliJ IDEA.
 * User: andrey
 * Date: 5/22/12, 11:41 PM
 */
@Component
public class Shell implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Autowired
    @Qualifier("machineSwitcher")
    private VendingMachine machine;

    public void run() {
        System.out.println("Supported commands [" + StringUtils.join(applicationContext.getBeansOfType(Command.class).keySet(), ",") + "] ");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print(">");
            String line = scanner.nextLine();
            String[] split = line.split("\\s");
            if (split.length > 0) {
                String cmdName = split[0];
                if (!applicationContext.containsBean(cmdName)) {
                    System.out.println("Unknown command: " + cmdName);
                } else {
                    Command command = (Command) applicationContext.getBean(cmdName);
//                    System.out.println(command);
                    String[] args = Arrays.copyOfRange(split, 1, split.length);
                    try {
                        command.execute(args, machine);
                    } catch (CommandParseException e) {
                        System.out.println("Invalid " + cmdName + " arguments: " + StringUtils.join(args, " "));
                    }
                }
            }
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
