package ru.spbau.ovchinnikov.sd.filters;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class MultiplicityFilter extends AbstractPredicateFilter {

    public MultiplicityFilter(IntegerStream underlying, int multiplier) {
        super(underlying, n -> n % multiplier == 0);
    }
}
