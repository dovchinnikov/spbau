package ru.spbau.ovchinnikov.sd.light;

import org.apache.log4j.Logger;
import ru.spbau.ovchinnikov.sd.light.base.FailSafeCompoundLight;

import java.util.Collection;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class Chandelier extends FailSafeCompoundLight<Bulb> {

    private static final Logger LOG = Logger.getLogger(Chandelier.class);

    public Chandelier(Collection<Bulb> lights) {
        super(lights);
    }

    @Override
    public void turnOn() throws LightException {
        LOG.debug("Turning on chandelier");
        super.turnOn();
        LOG.debug("Chandelier turned on");
    }
}
