package ru.spbau.ovchinnikov.sd.light;

import org.apache.log4j.Logger;
import ru.spbau.ovchinnikov.sd.light.base.Light;

import java.util.Random;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class Bulb implements Light {

    private static final Logger LOG = Logger.getLogger(Bulb.class);
    private static final Random RANDOM = new Random();

    @Override
    public void turnOn() throws LightException {
        LOG.debug("Turning on bulb");
        if (RANDOM.nextInt(100) < 5) {
            throw new LightException("Cannot turn on: Bulb has burnt out");
        }
        LOG.debug("Bulb turned on");
    }
}
