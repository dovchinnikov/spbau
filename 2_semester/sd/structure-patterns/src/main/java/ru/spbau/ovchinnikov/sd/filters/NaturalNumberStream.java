package ru.spbau.ovchinnikov.sd.filters;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class NaturalNumberStream implements IntegerStream {

    private int number;

    @Override
    public int getNext() {
        return ++number;
    }
}
