package ru.spbau.ovchinnikov.sd.filters;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class RangeFIlter extends AbstractPredicateFilter {

    protected RangeFIlter(IntegerStream underlying, int start, int end) {
        super(underlying, n -> start <= n && n < end);
    }
}
