package ru.spbau.ovchinnikov.sd.light;

import org.apache.log4j.Logger;
import ru.spbau.ovchinnikov.sd.light.base.CompoundLight;
import ru.spbau.ovchinnikov.sd.light.base.Light;

import java.util.Collection;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class Festoon extends CompoundLight<Bulb> {

    private static final Logger LOG = Logger.getLogger(Festoon.class);

    public Festoon(Collection<Bulb> lights) {
        super(lights);
    }

    @Override
    public void turnOn() throws LightException {
        LOG.debug("Turning on festoon");
        lights.forEach(Light::turnOn);
        LOG.debug("Festoon turned on");
    }
}
