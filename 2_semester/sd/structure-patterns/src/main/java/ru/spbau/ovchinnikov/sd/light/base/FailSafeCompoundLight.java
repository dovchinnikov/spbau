package ru.spbau.ovchinnikov.sd.light.base;

import org.apache.log4j.Logger;
import ru.spbau.ovchinnikov.sd.light.LightException;

import java.util.Collection;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public abstract class FailSafeCompoundLight<T extends Light> extends CompoundLight<T> {

    private static final Logger LOG = Logger.getLogger(FailSafeCompoundLight.class);

    protected FailSafeCompoundLight() {
    }

    protected FailSafeCompoundLight(Collection<T> lights) {
        super(lights);
    }

    @Override
    public void turnOn() throws LightException {
        lights.forEach(c -> {
            try {
                c.turnOn();
            } catch (LightException e) {
                LOG.error(e);
            }
        });
    }
}
