package ru.spbau.ovchinnikov.sd.filters;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class Main {

    public static void main(String[] args) {

        final IntegerStream multiplesOf7And3WithinRangeFrom10To10000 = new MultiplicityFilter(
                new RangeFIlter(
                        new MultiplicityFilter(new NaturalNumberStream(), 3),
                        10, 10000
                ), 7
        );

        // get first 10 numbers from the stream
        for (int i = 0; i < 10; i++) {
            System.out.println(multiplesOf7And3WithinRangeFrom10To10000.getNext());
        }
    }
}
