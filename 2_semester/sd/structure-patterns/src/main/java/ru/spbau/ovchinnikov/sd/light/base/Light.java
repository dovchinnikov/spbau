package ru.spbau.ovchinnikov.sd.light.base;

import ru.spbau.ovchinnikov.sd.light.LightException;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public interface Light {

    void turnOn() throws LightException;

}
