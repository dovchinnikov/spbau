package ru.spbau.ovchinnikov.sd.light.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public abstract class CompoundLight<T extends Light> implements Light {

    protected List<T> lights = new ArrayList<>();

    protected CompoundLight() {
    }

    protected CompoundLight(Collection<T> lights) {
        this.lights.addAll(lights);
    }

    public CompoundLight<T> add(T light) {
        this.lights.add(light);
        return this;
    }
}
