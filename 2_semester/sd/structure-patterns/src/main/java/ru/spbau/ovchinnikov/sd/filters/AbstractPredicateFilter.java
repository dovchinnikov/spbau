package ru.spbau.ovchinnikov.sd.filters;

import java.util.function.Predicate;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public abstract class AbstractPredicateFilter implements IntegerStream {

    private final IntegerStream underlying;
    private final Predicate<Integer> predicate;

    protected AbstractPredicateFilter(IntegerStream underlying, Predicate<Integer> predicate) {
        this.underlying = underlying;
        this.predicate = predicate;
    }

    @Override
    public int getNext() {
        int next;
        do {
            next = underlying.getNext();
        } while (!predicate.test(next));
        return next;
    }
}
