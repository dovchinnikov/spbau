package ru.spbau.ovchinnikov.sd.light;

import org.apache.log4j.Logger;
import ru.spbau.ovchinnikov.sd.light.base.Light;

import java.util.Arrays;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class Main {
    private final static Logger LOG = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        final Light lightSystem = new LightSystem().add(
                new Chandelier(
                        Arrays.asList(new Bulb(), new Bulb(), new Bulb(), new Bulb(), new Bulb())
                )
        ).add(new Bulb()).add(
                new LightSystem(
                        Arrays.asList(
                                new Bulb(),
                                new Bulb(),
                                new Bulb(),
                                new Festoon(
                                        Arrays.asList(new Bulb(), new Bulb(), new Bulb(),
                                                new Bulb(), new Bulb(), new Bulb(), new Bulb())
                                ),
                                new Bulb()
                        )
                )
        ).add(new Bulb()).add(
                new Festoon(
                        Arrays.asList(new Bulb(), new Bulb(), new Bulb(),
                                new Bulb(), new Bulb(), new Bulb())
                )
        );

        int total = 20;
        for (int i = 0; i < total; i++) {
            LOG.info(String.format("Running whole stuff (%d/%d)", i, total));
            lightSystem.turnOn();
        }
    }
}
