package ru.spbau.ovchinnikov.sd.light;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class LightException extends RuntimeException {

    public LightException(String message) {
        super(message);
    }
}
