package ru.spbau.ovchinnikov.sd.light;

import org.apache.log4j.Logger;
import ru.spbau.ovchinnikov.sd.light.base.FailSafeCompoundLight;
import ru.spbau.ovchinnikov.sd.light.base.Light;

import java.util.Collection;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class LightSystem extends FailSafeCompoundLight<Light> {

    private static final Logger LOG = Logger.getLogger(LightSystem.class);

    public LightSystem() {
    }

    public LightSystem(Collection<Light> lights) {
        super(lights);
    }

    @Override
    public void turnOn() throws LightException {
        LOG.debug("Turning on system");
        super.turnOn();
        LOG.debug("System turned on");
    }
}
