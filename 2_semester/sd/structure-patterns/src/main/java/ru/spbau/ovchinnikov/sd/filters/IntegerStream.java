package ru.spbau.ovchinnikov.sd.filters;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public interface IntegerStream {

    int getNext();

}
