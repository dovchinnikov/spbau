package ru.spbau.ovchinnikov.sd.repl.lexer;

import org.junit.Test;
import ru.spbau.ovchinnikov.sd.repl.lexer.base.Lexer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public class LexerTest {


    @Test
    public void test() throws IOException {

        String expr = "100500+2003";
        Lexer lexer = new StringLexer(new ByteArrayInputStream(expr.getBytes()));
        Token t = lexer.nextToken();
        List<Token> tokens = new ArrayList<>();
        while (t.type != TokenType.EOF) {
            tokens.add(t);
            t = lexer.nextToken();
        }
        System.out.println(tokens);
    }

    @Test
    public void test2() throws IOException {
        String expr = " 10 050   0  +   200         3     \t\t s       ";
        Lexer lexer = new StringLexer(new ByteArrayInputStream(expr.getBytes()));
        Token t = lexer.nextToken();
        List<Token> tokens = new ArrayList<>();
        while (t.type != TokenType.EOF) {
            tokens.add(t);
            t = lexer.nextToken();
        }
        System.out.println(tokens);
    }

    @Test
    public void test3() throws IOException {
        String expr = " 23ff23fg +  = ? ++ =";
        Lexer lexer = new StringLexer(new ByteArrayInputStream(expr.getBytes()));
        Token t = lexer.nextToken();
        List<Token> tokens = new ArrayList<>();
        while (t.type != TokenType.EOF) {
            tokens.add(t);
            t = lexer.nextToken();
        }
        System.out.println(tokens);
    }

    @Test
    public void test4() throws IOException {
        String expr = "1 + aaaaaaa ";
        Lexer lexer = new StringLexer(new ByteArrayInputStream(expr.getBytes()));
        Token t = lexer.nextToken();
        List<Token> tokens = new ArrayList<>();
        while (t.type != TokenType.EOF) {
            tokens.add(t);
            t = lexer.nextToken();
        }
        tokens.add(t);
        System.out.println(tokens);
    }
}
