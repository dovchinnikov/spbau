package ru.spbau.ovchinnikov.sd.repl.visitor;

import org.junit.Test;
import ru.spbau.ovchinnikov.sd.repl.ast.Div;
import ru.spbau.ovchinnikov.sd.repl.ast.Mul;
import ru.spbau.ovchinnikov.sd.repl.ast.Num;
import ru.spbau.ovchinnikov.sd.repl.ast.Sum;
import ru.spbau.ovchinnikov.sd.repl.ast.base.INode;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitorAdapter;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class EvaluatorTest {
    @Test
    public void test() {
        //        INode assignment = new Ass(
//                new Var("lol"),
//                new Mul(new Num(11d), new Num(18d))
//                new Var("lol"),
//                var1, exp1);

        INode exp = new Sum(
                new Mul(
                        new Sum(new Num(10.0), new Num(21.0)),
                        new Sum(new Num(22.0), new Num(14.0))
                ),
                new Div(
                        new Sum(new Num(15d), new Num(88d)),
//                        new Var("lol")
                        new Mul(new Num(11d), new Num(18d))
//                        new Num(0d)
                )
        );

        exp.traverse(new ExpVisitorAdapter<Void>() {
            @Override
            public Void visit(Div division) {
                division.right.accept(new ExpVisitorAdapter<Void>() {
                    @Override
                    public Void visit(Num number) {
                        if (number.number.intValue() == 0) {
                            throw new RuntimeException("StaticCheck failed: Divide by zero");
                        }
                        return null;
                    }
                });
                return null;
            }
        });

        Evaluator evaluator = new Evaluator();
        PrettyPrinter printer = new PrettyPrinter();

//        System.out.println(assignment.accept(evaluator).accept(printer));
        System.out.println(exp.accept(evaluator).accept(printer));

    }
}
