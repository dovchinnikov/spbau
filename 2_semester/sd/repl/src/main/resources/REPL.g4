grammar REPL;
//@header {
//package ru.spbau.ovchinnikov.sd.repl.parser;
//}
//options {}
//imports {}
tokens {
    VAR_NAME, NUMBER, PLUS, MINUS, MUL, EQ
}

eval                : statement+ ;
statement           : assignment | exp ;
assignment          : VAR_NAME '=' exp ;

exp                 : sum | minus | term ;
term                : mul | div | factor ;
factor              :  '(' exp ')' | VAR_NAME | NUMBER ;

sum                 : term '+' exp ;
minus               : term '-' exp ;
mul                 : factor '*' term ;
div                 : factor '/' term ;
