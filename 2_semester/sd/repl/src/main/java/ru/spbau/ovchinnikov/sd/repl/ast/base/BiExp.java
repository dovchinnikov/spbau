package ru.spbau.ovchinnikov.sd.repl.ast.base;

import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public abstract class BiExp extends AbstractNode {

    public final INode left;
    public final INode right;

    protected BiExp(INode left, INode right) {
        super(left.getStartPosition(), right.getEndPosition());
        this.left = left;
        this.right = right;
    }

    @Override
    public void traverse(ExpVisitor<Void> visitor) {
        left.traverse(visitor);
        this.accept(visitor);
        right.traverse(visitor);
    }
}
