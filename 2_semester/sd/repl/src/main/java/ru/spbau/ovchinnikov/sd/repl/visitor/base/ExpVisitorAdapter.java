package ru.spbau.ovchinnikov.sd.repl.visitor.base;

import ru.spbau.ovchinnikov.sd.repl.ast.*;
import ru.spbau.ovchinnikov.sd.repl.ast.Err;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public abstract class ExpVisitorAdapter<T> implements ExpVisitor<T> {

    @Override
    public T visit(Num number) {
        return null;
    }

    @Override
    public T visit(Sum sum) {
        return null;
    }

    @Override
    public T visit(Mul multiplication) {
        return null;
    }

    @Override
    public T visit(Div division) {
        return null;
    }

    @Override
    public T visit(Ass assignment) {
        return null;
    }

    @Override
    public T visit(Var variable) {
        return null;
    }

    @Override
    public T visit(Err err) {
        return null;
    }

    @Override
    public T visit(Dummy dummy) {
        return null;
    }
}
