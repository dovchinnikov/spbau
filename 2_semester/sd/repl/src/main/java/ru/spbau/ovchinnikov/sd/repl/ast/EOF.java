package ru.spbau.ovchinnikov.sd.repl.ast;

import ru.spbau.ovchinnikov.sd.repl.ast.base.AbstractNode;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 6/1/14
 */
public class EOF extends AbstractNode {

    public EOF(int start) {
        super(start, start);
    }

    @Override
    public <T> T accept(ExpVisitor<T> visitor) {
        return null;
    }
}
