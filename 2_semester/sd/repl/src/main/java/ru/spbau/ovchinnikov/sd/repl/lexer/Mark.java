package ru.spbau.ovchinnikov.sd.repl.lexer;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public class Mark {

    public final int mark;

    public Mark(int mark) {
        this.mark = mark;
    }
}

