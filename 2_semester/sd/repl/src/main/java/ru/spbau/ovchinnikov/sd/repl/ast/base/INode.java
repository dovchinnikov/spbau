package ru.spbau.ovchinnikov.sd.repl.ast.base;

import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public interface INode {

    public int getStartPosition();

    public int getEndPosition();

    public <T> T accept(ExpVisitor<T> visitor);

    public void traverse(ExpVisitor<Void> visitor);

}
