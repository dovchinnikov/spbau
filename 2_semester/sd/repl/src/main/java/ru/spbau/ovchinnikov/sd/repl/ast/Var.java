package ru.spbau.ovchinnikov.sd.repl.ast;

import ru.spbau.ovchinnikov.sd.repl.ast.base.AbstractNode;
import ru.spbau.ovchinnikov.sd.repl.lexer.Token;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public class Var extends AbstractNode {

    public final String name;

    public Var(Token t) {
        super(t.start, t.end);
        this.name = t.text;
    }

    @Override
    public <T> T accept(final ExpVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Var var = (Var) o;

        return !(name != null ? !name.equals(var.name) : var.name != null);
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
