package ru.spbau.ovchinnikov.sd.repl;

import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoableEdit;
import java.util.List;

/**
 * @author Daniil Ovchinnikov
 * @since 6/2/14
 */
public class EvaluationEdit extends CompoundEdit {

    private boolean replaced = false;

    public EvaluationEdit(List<UndoableEdit> edits) {
        edits.forEach(super::addEdit);
    }

    @Override
    public boolean addEdit(UndoableEdit anEdit) {
        return false;
    }

    @Override
    public boolean replaceEdit(UndoableEdit anEdit) {
        if (!replaced) {
            replaced = super.addEdit(anEdit);
            return replaced;
        }
        return false;
    }

    @Override
    public boolean isSignificant() {
        return true;
    }

    @Override
    public boolean canUndo() {
        return true;
    }
}
