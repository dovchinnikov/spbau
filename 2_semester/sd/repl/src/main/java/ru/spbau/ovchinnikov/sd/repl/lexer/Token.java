package ru.spbau.ovchinnikov.sd.repl.lexer;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public class Token {

    public final int start;
    public final int end;
    public final TokenType type;
    public final String text;

    public Token(int start, int end, TokenType type, String text) {
        this.start = start;
        this.end = end;
        this.type = type;
        this.text = text;
    }

    @Override
    public String toString() {
        return type + "[" + start + "," + end + "]:" + " " + text;
    }
}
