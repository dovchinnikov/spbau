package ru.spbau.ovchinnikov.sd.repl.ast;

import ru.spbau.ovchinnikov.sd.repl.ast.base.AbstractNode;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 6/1/14
 */
public class Err extends AbstractNode {

    private String message;

    public Err(int start, int end) {
        super(start, end);
    }

    @Override
    public <T> T accept(ExpVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public String getMessage() {
        return message;
    }

    public Err setMessage(String message) {
        this.message = message;
        return this;
    }
}
