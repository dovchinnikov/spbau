package ru.spbau.ovchinnikov.sd.repl.ast;

import ru.spbau.ovchinnikov.sd.repl.ast.base.BiExp;
import ru.spbau.ovchinnikov.sd.repl.ast.base.INode;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public class Div extends BiExp {

    public Div(INode left, INode right) {
        super(left, right);
    }

    @Override
    public <T> T accept(final ExpVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
