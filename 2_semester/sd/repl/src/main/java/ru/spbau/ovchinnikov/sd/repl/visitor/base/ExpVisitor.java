package ru.spbau.ovchinnikov.sd.repl.visitor.base;

import ru.spbau.ovchinnikov.sd.repl.ast.*;
import ru.spbau.ovchinnikov.sd.repl.ast.Err;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public interface ExpVisitor<T> {

    T visit(Num number);

    T visit(Sum sum);

    T visit(Mul multiplication);

    T visit(Div division);

    T visit(Ass assignment);

    T visit(Var variable);

    T visit(Err err);

    T visit(Dummy dummy);

}
