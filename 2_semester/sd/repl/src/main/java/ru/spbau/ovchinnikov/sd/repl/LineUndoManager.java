package ru.spbau.ovchinnikov.sd.repl;

import ru.spbau.ovchinnikov.sd.repl.lexer.BufferedLexer;
import ru.spbau.ovchinnikov.sd.repl.parser.Parser;
import ru.spbau.ovchinnikov.sd.repl.visitor.Evaluator;
import ru.spbau.ovchinnikov.sd.repl.visitor.HighlightVisitor;

import javax.swing.event.UndoableEditEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import static ru.spbau.ovchinnikov.sd.repl.ConsoleFilter.userInputStart;

/**
 * @author Daniil Ovchinnikov
 * @since 6/1/14
 */
public class LineUndoManager extends UndoManager {

    private final Evaluator evaluator;
    private final StyledDocument document;

    public LineUndoManager(Evaluator evaluator, StyledDocument document) {
        this.evaluator = evaluator;
        this.document = document;
    }

    @Override
    public void undoableEditHappened(UndoableEditEvent e) {
        super.undoableEditHappened(e);
        highlight();
    }

    @Override
    public synchronized void undo() throws CannotUndoException {
        super.undo();
        highlight();
    }

    public synchronized void undoWholeLine() throws CannotUndoException {
        while (!(editToBeUndone() == null || editToBeUndone() instanceof EvaluationEdit)) {
            super.undo();
        }
        if (canUndo()) {
            undo();
        }
        highlight();
    }

    private void highlight() {
        try {
            document.removeUndoableEditListener(this);

            final int userInputStart = userInputStart(document);
            final String userInput = document.getText(0, document.getLength()).substring(userInputStart);
            if (userInput.isEmpty()) {
                return;
            }

            document.setCharacterAttributes(
                    userInputStart,
                    userInput.length(),
                    document.getStyle(HighlightVisitor.DEFAULT_STYLE_NAME),
                    true
            );
            new Parser(
                    new BufferedLexer(userInput)
            ).parse().traverse(
                    new HighlightVisitor(document, userInputStart, evaluator)
            );
        } catch (BadLocationException e) {
            e.printStackTrace();
        } finally {
            document.addUndoableEditListener(this);
        }
    }
}
