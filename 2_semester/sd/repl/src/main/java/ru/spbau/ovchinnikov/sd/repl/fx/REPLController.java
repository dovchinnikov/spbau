package ru.spbau.ovchinnikov.sd.repl.fx;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import ru.spbau.ovchinnikov.sd.repl.visitor.Evaluator;

import java.net.URL;
import java.util.ResourceBundle;

public class REPLController implements Initializable {

    public static final String GREETING = "Welcome to REPL Console! ";
    public static final String PROMPT = System.lineSeparator() + "> ";

    final Evaluator evaluator;

    public CheckBox evaluatorModeCheckbox;
    public TextArea replConsole;

    public REPLController() {
        evaluator = new Evaluator();
    }

    public void evaluatorModeCheckBoxAction() {
        evaluatorModeCheckbox.isSelected();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        replConsole.setText(GREETING + PROMPT);
        replConsole.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (!keyEvent.getCode().isNavigationKey()) {
                    if (keyEvent.getEventType().equals(KeyEvent.KEY_PRESSED) && keyEvent.getCode() == KeyCode.ENTER) {
                        replConsole.appendText(PROMPT);
                        //TODO
                        keyEvent.consume();
                    } else {
                        int lastReturn = replConsole.getText().lastIndexOf(PROMPT) + PROMPT.length() - 1;
                        if (keyEvent.getCode() == KeyCode.BACK_SPACE) {
                            lastReturn++;
                        }
                        if (replConsole.getAnchor() <= lastReturn || replConsole.getCaretPosition() <= lastReturn) {
                            keyEvent.consume();
                        }
                    }
                }
            }
        });
    }

    public static class Main extends Application {

        @Override
        public void start(Stage primaryStage) throws Exception {
            Parent root = FXMLLoader.load(getClass().getResource("/repl.fxml"));
            primaryStage.setTitle("REPL");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
        }


        public static void main(String[] args) {
            launch(args);
        }
    }
}
