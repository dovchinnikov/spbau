package ru.spbau.ovchinnikov.sd.repl.lexer;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public enum TokenType {
    EOF,
    IDENTIFIER,
    OPERATOR,
    NUMBER,
    UNKNOWN
}
