package ru.spbau.ovchinnikov.sd.repl;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;

import static ru.spbau.ovchinnikov.sd.repl.REPLConsole.GREETING;

/**
 * @author Daniil Ovchinnikov
 * @since 6/1/14
 */
public class ConsoleFilter extends DocumentFilter {

    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
        if (cursorOnLastLine(offset, fb.getDocument())) {
            super.insertString(fb, offset, string, attr);
        }
    }

    @Override
    public void remove(final FilterBypass fb, final int offset, final int length) throws BadLocationException {
        if (cursorOnLastLine(offset, fb.getDocument())) {
            super.remove(fb, offset, length);
        }
    }

    @Override
    public void replace(final FilterBypass fb, final int offset, final int length, final String text, final AttributeSet attrs)
            throws BadLocationException {
        if (cursorOnLastLine(offset, fb.getDocument())) {
            super.replace(fb, offset, length, text, attrs);
        }
    }

    private static boolean cursorOnLastLine(int offset, Document document) throws BadLocationException {
        return offset > userInputStart(document);
    }

    public static int userInputStart(Document document) throws BadLocationException {
        return document.getText(0, document.getLength()).lastIndexOf(GREETING) + GREETING.length() - 1;
    }
}
