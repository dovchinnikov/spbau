package ru.spbau.ovchinnikov.sd.repl.ast;

import ru.spbau.ovchinnikov.sd.repl.ast.base.AbstractNode;
import ru.spbau.ovchinnikov.sd.repl.ast.base.INode;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

import java.util.List;

/**
 * @author Daniil Ovchinnikov
 * @since 6/1/14
 */
public class Dummy extends AbstractNode {

    public final List<INode> children;

    public Dummy(List<INode> children) {
        super(children.get(0).getStartPosition(), children.get(children.size() - 1).getEndPosition());
        this.children = children;
    }

    @Override
    public <T> T accept(ExpVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public void traverse(ExpVisitor<Void> visitor) {
        children.forEach(node -> node.traverse(visitor));
    }
}
