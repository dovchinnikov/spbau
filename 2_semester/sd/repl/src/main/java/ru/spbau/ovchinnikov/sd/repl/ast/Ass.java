package ru.spbau.ovchinnikov.sd.repl.ast;

import ru.spbau.ovchinnikov.sd.repl.ast.base.BiExp;
import ru.spbau.ovchinnikov.sd.repl.ast.base.INode;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public class Ass extends BiExp {

    public final Var var;
    public final INode exp;

    public Ass(Var var, INode exp) {
        super(var, exp);
        this.var = var;
        this.exp = exp;
    }

    @Override
    public <T> T accept(final ExpVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public void traverse(ExpVisitor<Void> visitor) {
        visitor.visit(this);
    }
}
