package ru.spbau.ovchinnikov.sd.repl.lexer;

import ru.spbau.ovchinnikov.sd.repl.lexer.base.Lexer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public class StringLexer implements Lexer {

    private int position;
    private final InputStream input;

    public StringLexer(InputStream input) {
        this.input = input;
    }

    @Override
    public Token nextToken() {
        try {
            input.mark(1);
            int c = input.read();
            position++;

            // skip whitespaces
            while (Character.isWhitespace(c) && c != -1) {
                input.mark(1);
                c = input.read();
                position++;
            }

            int start = position;
            StringBuilder sb = new StringBuilder();

            // try read number
            while (Character.isDigit(c) && c != -1) {
                sb.append(new String(Character.toChars(c)));
                input.mark(1);
                c = input.read();
                position++;
            }
            if (sb.length() > 0) {
                input.reset();
                position--;
                return new Token(start, position, TokenType.NUMBER, sb.toString());
            }

            // try read identifier
            while (Character.isJavaIdentifierPart(c) && c != -1) {
                sb.append(new String(Character.toChars(c)));
                input.mark(1);
                c = input.read();
                position++;
            }
            if (sb.length() > 0) {
                input.reset();
                position--;
                return new Token(start, position, TokenType.IDENTIFIER, sb.toString());
            }

            // try read operators
            if (Arrays.asList('/', '+', '*', '=').contains((char) c)) {
                return new Token(start, position, TokenType.OPERATOR, new String(Character.toChars(c)));
            }

            if (c == -1) {
                return new Token(start, position, TokenType.EOF, "");
            } else {
                return new Token(start, position, TokenType.UNKNOWN, new String(Character.toChars(c)));
            }
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public boolean hasMoreTokens() {
        input.mark(1);
        try {
            int r = input.read();
            input.reset();
            return r != -1;
        } catch (IOException ignored) {
        }
        return false;
    }
}
