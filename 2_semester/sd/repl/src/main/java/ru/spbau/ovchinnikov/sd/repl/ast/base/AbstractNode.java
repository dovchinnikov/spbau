package ru.spbau.ovchinnikov.sd.repl.ast.base;

import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public abstract class AbstractNode implements INode {

    public final int start;
    public final int end;

    protected AbstractNode(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public final int getStartPosition() {
        return start;
    }

    @Override
    public final int getEndPosition() {
        return end;
    }

    @Override
    public void traverse(ExpVisitor<Void> visitor) {
        accept(visitor);
    }
}
