package ru.spbau.ovchinnikov.sd.repl;

import ru.spbau.ovchinnikov.sd.repl.ast.base.INode;
import ru.spbau.ovchinnikov.sd.repl.lexer.BufferedLexer;
import ru.spbau.ovchinnikov.sd.repl.parser.Parser;
import ru.spbau.ovchinnikov.sd.repl.visitor.Evaluator;
import ru.spbau.ovchinnikov.sd.repl.visitor.HighlightVisitor;
import ru.spbau.ovchinnikov.sd.repl.visitor.PrettyPrinter;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import static ru.spbau.ovchinnikov.sd.repl.ConsoleFilter.userInputStart;

public class REPLConsole extends JFrame {

    public static final String GREETING = System.lineSeparator() + "> ";

    private final JCheckBox replMode;
    private final JTextPane console;

    private final DefaultStyledDocument document;
    private final Evaluator evaluator;
    private final PrettyPrinter printer;
    private final LineUndoManager lineUndoManager;

    private REPLConsole() {
        super("REPL Console");

        document = new DefaultStyledDocument();
        evaluator = new Evaluator();
        printer = new PrettyPrinter();
        lineUndoManager = new LineUndoManager(evaluator, document);

        replMode = new JCheckBox("Simplify");
        console = new JTextPane(document);

        replMode.addActionListener(this::replModeOnChanged);
        console.setText("Welcome to REPL Console! " + GREETING);
        console.getKeymap().addActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
                new EvaluationAction()
        );
        console.getKeymap().addActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK),
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (lineUndoManager.canUndo()) {
                            lineUndoManager.undo();
                        }
                    }
                }
        );
        console.getKeymap().addActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK),
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (lineUndoManager.canUndo()) {
                            lineUndoManager.undoWholeLine();
                        }
                    }
                }
        );

        createStyles(document);

        document.setDocumentFilter(new ConsoleFilter());
        document.addUndoableEditListener(lineUndoManager);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        this.add(replMode, "North");
        this.add(console, "Center");
        this.setSize(500, 300);
    }

    private void createStyles(StyledDocument document) {
        Style base = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setFontFamily(base, "Monospace");

        Style def = document.addStyle(HighlightVisitor.DEFAULT_STYLE_NAME, base);
        StyleConstants.setForeground(def, Color.BLACK);

        Style error = document.addStyle(HighlightVisitor.ERROR_STYLE_NAME, base);
        StyleConstants.setForeground(error, Color.RED);
        StyleConstants.setUnderline(error, true);

        Style operand = document.addStyle(HighlightVisitor.OPERAND_STYLE_NAME, base);
        StyleConstants.setForeground(operand, Color.GREEN);
    }

    private void replModeOnChanged(ActionEvent ignored) {
        replMode.setText(replMode.isSelected() ? "Evaluate" : "Simplify");
        evaluator.evaluateMode = replMode.isSelected();
    }

    private class EvaluationAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String text = document.getText(0, document.getLength());
                final int userInputStart = userInputStart(document);
                String userInput = text.substring(userInputStart);
                if (userInput.isEmpty()) {
                    return;
                }

                // parse expression
                INode root = new Parser(new BufferedLexer(userInput)).parse();

                // evaluate expression
                INode evaluation = root.accept(evaluator.reset());

                // build response
                StringBuilder sb = new StringBuilder();
                if (evaluator.errors.size() == 0) {
                    sb.append(System.lineSeparator())
                            .append("Evaluation result from user input: ")
                            .append(evaluation.accept(printer));
                }
                evaluator.errors.forEach(msg ->
                                sb.append(System.lineSeparator()).append(msg)
                );
                sb.append(GREETING);

                document.insertString(document.getLength(), sb.toString(), null);
                console.setCaretPosition(document.getLength());

                lineUndoManager.addEdit(new EvaluationEdit(evaluator.undoableEdits));
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new REPLConsole().setVisible(true);
    }
}
