package ru.spbau.ovchinnikov.sd.repl.visitor;

import ru.spbau.ovchinnikov.sd.repl.ast.*;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitorAdapter;

/**
* @author Daniil Ovchinnikov
* @since 4/16/14
*/
public class PrettyPrinter extends ExpVisitorAdapter<String> {

    @Override
    public String visit(Num number) {
        return String.valueOf(number.number);
    }

    @Override
    public String visit(Div division) {
        return division.left.accept(this) + " / " + division.right.accept(this);
    }

    @Override
    public String visit(Ass assignment) {
        return assignment.var.name + " = " + assignment.exp.accept(this);
    }

    @Override
    public String visit(Var variable) {
        return variable.name;
    }

    @Override
    public String visit(Mul multiplication) {
        return multiplication.left.accept(this) + " * " + multiplication.right.accept(this);
    }

    @Override
    public String visit(Sum exp) {
        return "(" + exp.left.accept(this) + " + " + exp.right.accept(this) + ")";
    }
}
