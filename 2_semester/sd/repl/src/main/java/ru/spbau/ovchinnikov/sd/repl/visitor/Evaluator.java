package ru.spbau.ovchinnikov.sd.repl.visitor;

import ru.spbau.ovchinnikov.sd.repl.ast.*;
import ru.spbau.ovchinnikov.sd.repl.ast.base.INode;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitorAdapter;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import java.util.*;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public class Evaluator extends ExpVisitorAdapter<INode> {

    public final Map<Var, INode> context;
    public final List<String> errors;
    public final Set<Var> visitedVars;
    public final List<UndoableEdit> undoableEdits;

    public boolean evaluateMode;

    public Evaluator() {
        context = new HashMap<>();
        evaluateMode = false;
        errors = new ArrayList<>();
        visitedVars = new HashSet<>();
        undoableEdits = new ArrayList<>();
    }

    public Evaluator reset() {
        errors.clear();
        visitedVars.clear();
        undoableEdits.clear();
        return this;
    }

    @Override
    public INode visit(Num number) {
        return number;
    }

    @Override
    public INode visit(Sum exp) {
        INode l = exp.left.accept(this);
        INode r = exp.right.accept(this);
        return l instanceof Num && r instanceof Num
                ? new Num(((Num) l).number + ((Num) r).number)
                : new Sum(l, r);
    }

    @Override
    public INode visit(Mul multiplication) {
        INode l = multiplication.left.accept(this);
        INode r = multiplication.right.accept(this);
        return l instanceof Num && r instanceof Num
                ? new Num(((Num) l).number * ((Num) r).number)
                : new Mul(l, r);
    }

    @Override
    public INode visit(Div division) {
        INode l = division.left.accept(this);
        INode r = division.right.accept(this);
        return l instanceof Num && r instanceof Num
                ? new Num(((Num) l).number / ((Num) r).number)
                : new Div(l, r);
    }

    @Override
    public INode visit(Ass assignment) {
        visitedVars.add(assignment.var);
        INode exp = assignment.exp.accept(this);
        if (errors.size() == 0) {
            undoableEdits.add(new AbstractUndoableEdit() {
                final Var key = assignment.var;
                final INode value = context.get(key);

                @Override
                public void undo() throws CannotUndoException {
                    if (value == null) {
                        context.remove(key);
                    } else {
                        context.put(key, value);
                    }
                }

                @Override
                public String toString() {
                    return "{key=" + key + ", value=" + value + '}';
                }
            });
            context.put(assignment.var, exp);
        }
        return exp;
    }

    @Override
    public INode visit(Var variable) {
        if (visitedVars.contains(variable)) {
            errors.add(String.format("Error: recursive variable %s is not supported", variable.name));
            return variable;
        } else {
            if (context.containsKey(variable)) {
                return context.get(variable).accept(this);
            } else {
                if (evaluateMode) {
                    errors.add(String.format("Error: %s is undefined", variable.name));
                }
                return variable;
            }
        }
    }

    @Override
    public INode visit(Err err) {
        errors.add(err.getMessage());
        return err;
    }

    @Override
    public INode visit(Dummy dummy) {
        dummy.children.forEach(c -> {
            c.accept(Evaluator.this);
        });
        return dummy;
    }
}
