package ru.spbau.ovchinnikov.sd.repl.lexer;

import ru.spbau.ovchinnikov.sd.repl.lexer.base.LookAheadLexer;

import java.io.ByteArrayInputStream;
import java.util.Stack;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public class BufferedLexer extends StringLexer implements LookAheadLexer {

    private Stack<Token> buffer = new Stack<>();
    private Stack<Token> goneTokens = new Stack<>();
    private int tokenCount;

    public BufferedLexer(String input) {
        super(new ByteArrayInputStream(input.getBytes()));
    }

    @Override
    public Mark mark() {
        return new Mark(tokenCount);
    }

    @Override
    public void returnTo(Mark mark) {
        while (mark.mark < tokenCount) {
            buffer.push(goneTokens.pop());
            tokenCount--;
        }
    }

    @Override
    public Token nextToken() {
        Token t = buffer.size() > 0
                ? buffer.pop()
                : super.nextToken();
        tokenCount++;
        goneTokens.push(t);
        return t;
    }

    @Override
    public int getPosition() {
        return buffer.size() > 0 ? buffer.peek().start - 1 : super.getPosition();
    }

    @Override
    public boolean hasMoreTokens() {
        return buffer.size() > 0 && !buffer.peek().type.equals(TokenType.EOF) || super.hasMoreTokens();
    }
}
