package ru.spbau.ovchinnikov.sd.repl.ast;

import ru.spbau.ovchinnikov.sd.repl.ast.base.AbstractNode;
import ru.spbau.ovchinnikov.sd.repl.lexer.Token;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitor;

/**
 * @author Daniil Ovchinnikov
 * @since 4/16/14
 */
public class Num extends AbstractNode {

    public final Double number;

    public Num(Double number) {
        super(-1, -1);
        this.number = number;
    }

    public Num(Token t) {
        super(t.start, t.end);
        this.number = Double.parseDouble(t.text);
    }

    @Override
    public <T> T accept(ExpVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
