package ru.spbau.ovchinnikov.sd.repl.visitor;

import ru.spbau.ovchinnikov.sd.repl.ast.Ass;
import ru.spbau.ovchinnikov.sd.repl.ast.Err;
import ru.spbau.ovchinnikov.sd.repl.ast.Num;
import ru.spbau.ovchinnikov.sd.repl.ast.Var;
import ru.spbau.ovchinnikov.sd.repl.visitor.base.ExpVisitorAdapter;

import javax.swing.text.StyledDocument;

public class HighlightVisitor extends ExpVisitorAdapter<Void> {

    public static final String DEFAULT_STYLE_NAME = "default";
    public static final String ERROR_STYLE_NAME = "error";
    public static final String OPERAND_STYLE_NAME = "operand";

    private final StyledDocument document;
    private final int offset;
    private final Evaluator evaluator;

    public HighlightVisitor(StyledDocument document, int offset, Evaluator evaluator) {
        this.document = document;
        this.offset = offset;
        this.evaluator = evaluator;
    }

    @Override
    public Void visit(Num number) {
        highlight(number.start, number.end, OPERAND_STYLE_NAME);
        return null;
    }

    @Override
    public Void visit(Var variable) {
        highlight(
                variable.start,
                variable.end,
                evaluator.evaluateMode && !evaluator.context.containsKey(variable)
                        ? ERROR_STYLE_NAME
                        : OPERAND_STYLE_NAME
        );
        return null;
    }

    @Override
    public Void visit(Ass assignment) {
        highlight(
                assignment.var.start,
                assignment.var.end,
                OPERAND_STYLE_NAME
        );
        assignment.exp.traverse(this);
        return null;
    }

    @Override
    public Void visit(Err err) {
        highlight(err.start, err.end, ERROR_STYLE_NAME);
        return null;
    }

    private void highlight(int start, int end, String style) {
        document.setCharacterAttributes(offset + start - 1, end - start + 1, document.getStyle(style), true);
    }
}
