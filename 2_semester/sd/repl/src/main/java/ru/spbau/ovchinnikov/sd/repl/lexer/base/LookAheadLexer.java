package ru.spbau.ovchinnikov.sd.repl.lexer.base;

import ru.spbau.ovchinnikov.sd.repl.lexer.Mark;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public interface LookAheadLexer extends Lexer {

    Mark mark();

    void returnTo(Mark mark);

}
