package ru.spbau.ovchinnikov.sd.repl.parser;

import ru.spbau.ovchinnikov.sd.repl.ast.*;
import ru.spbau.ovchinnikov.sd.repl.ast.base.INode;
import ru.spbau.ovchinnikov.sd.repl.lexer.Mark;
import ru.spbau.ovchinnikov.sd.repl.lexer.Token;
import ru.spbau.ovchinnikov.sd.repl.lexer.TokenType;
import ru.spbau.ovchinnikov.sd.repl.lexer.base.LookAheadLexer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public class Parser {

    private final LookAheadLexer lexer;

    public Parser(LookAheadLexer lexer) {
        this.lexer = lexer;
    }

    public INode parse() {
        INode statement = parseStatement();
        EOF eof = parseEOF();
        if (statement == null) {
            if (eof == null) {
                return skipUntil(TokenType.EOF);
            } else {
                return eof;
            }
        } else {
            if (eof == null) {
                return new Dummy(Arrays.asList(statement, skipUntil(TokenType.EOF)));
            } else {
                return statement;
            }
        }
    }

    private INode parseStatement() {
        Mark m = lexer.mark();
        INode statement = parseAssignment();
        if (statement == null) statement = parseExpression();
        if (statement == null) lexer.returnTo(m);
        return statement;
    }

    private INode parseAssignment() {
        Mark m = lexer.mark();

        Var identifier = parseVariable();
        if (identifier == null) {
            lexer.returnTo(m);
            return null;
        }

        if (!checkNext("=")) {
            lexer.returnTo(m);
            return null;
        }

        INode expression = parseExpression();
        if (expression == null) {
            lexer.returnTo(m);
            return null;
        }

        return new Ass(identifier, expression);
    }

    private INode parseExpression() {
        Mark m = lexer.mark();
        INode node = parseAssignment();
        if (node == null) node = parseSum();
        if (node == null) node = parseTerm();
        if (node == null) lexer.returnTo(m);
        return node;
    }

    private INode parseTerm() {
        Mark m = lexer.mark();
        INode node = parseMul();
        if (node == null) node = parseDiv();
        if (node == null) node = parseFactor();
        if (node == null) lexer.returnTo(m);
        return node;
    }


    private INode parseFactor() {
        Mark m = lexer.mark();
        INode node = parseFactorP();
        if (node == null) node = parseVariable();
        if (node == null) node = parseNumber();
        if (node == null) lexer.returnTo(m);
        return node;
    }

    private INode parseFactorP() {
        Mark m = lexer.mark();

        if (!checkNext("(")) {
            lexer.returnTo(m);
            return null;
        }

        INode expression = parseExpression();
        if (expression == null) {
            lexer.returnTo(m);
            return null;
        }

        if (!checkNext(")")) {
            lexer.returnTo(m);
            return null;
        }

        return expression;
    }

    private Sum parseSum() {
        Mark m = lexer.mark();

        INode term = parseTerm();
        if (term == null) {
            lexer.returnTo(m);
            return null;
        }

        if (!checkNext("+")) {
            lexer.returnTo(m);
            return null;
        }

        INode expression = parseExpression();
        if (expression == null) {
            lexer.returnTo(m);
            return null;
        }

        return new Sum(term, expression);
    }

    private Mul parseMul() {
        Mark m = lexer.mark();

        INode factor = parseFactor();
        if (factor == null) {
            lexer.returnTo(m);
            return null;
        }

        if (!checkNext("*")) {
            lexer.returnTo(m);
            return null;
        }

        INode term = parseExpression();
        if (term == null) {
            lexer.returnTo(m);
            return null;
        }

        return new Mul(factor, term);
    }

    private Div parseDiv() {
        Mark m = lexer.mark();

        INode factor = parseFactor();
        if (factor == null) {
            lexer.returnTo(m);
            return null;
        }

        if (!checkNext("/")) {
            lexer.returnTo(m);
            return null;
        }

        INode term = parseExpression();
        if (term == null) {
            lexer.returnTo(m);
            return null;
        }

        return new Div(factor, term);
    }

    private INode parseNumber() {
        Mark m = lexer.mark();
        Token t = lexer.nextToken();
        if (TokenType.NUMBER.equals(t.type)) {
            return new Num(t);
        } else {
            lexer.returnTo(m);
            return null;
        }
    }

    private Var parseVariable() {
        Mark m = lexer.mark();
        Token t = lexer.nextToken();
        if (TokenType.IDENTIFIER.equals(t.type)) {
            return new Var(t);
        } else {
            lexer.returnTo(m);
            return null;
        }
    }

    private EOF parseEOF() {
        Mark m = lexer.mark();
        Token t = lexer.nextToken();
        if (TokenType.EOF.equals(t.type)) {
            return new EOF(t.start);
        } else {
            lexer.returnTo(m);
            return null;
        }
    }

    private boolean checkNext(String text) {
        Token token = lexer.nextToken();
        return text != null && text.equals(token.text);
    }

    private Err skipUntil(TokenType tokenType) {
        List<Token> skippedTokens = new ArrayList<>();
        Mark m = lexer.mark();
        Token t = lexer.nextToken();
        while (!tokenType.equals(t.type) && !TokenType.EOF.equals(t.type)) {
            skippedTokens.add(t);
            t = lexer.nextToken();
        }
        if (skippedTokens.size() == 0) {
            lexer.returnTo(m);
            return null;
        } else {
            return new Err(skippedTokens.get(0).start, skippedTokens.get(skippedTokens.size() - 1).end).setMessage("Syntax message: unexpected symbols");
        }
    }
}
