package ru.spbau.ovchinnikov.sd.repl.lexer.base;

import ru.spbau.ovchinnikov.sd.repl.lexer.Token;

/**
 * @author Daniil Ovchinnikov
 * @since 5/31/14
 */
public interface Lexer {

    Token nextToken();

    int getPosition();

    boolean hasMoreTokens();
}
