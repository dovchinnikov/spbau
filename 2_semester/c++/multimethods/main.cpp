#include <iostream>
#include "dispatcher.h"
#include "ishape.h"

using namespace std;


struct IShape
{
    IShape(){}
    virtual void visit() = 0;
};

struct Square : IShape {
    void print() {
        cout << "square" << endl;
    }
};

struct Rect : Square {
    void print(){
        cout << "rect" << endl;
    }
};

struct Circle : IShape {
    void print() {
        cout << "circle" << endl;
    }
};

struct Triangle : IShape {
    void print() {
        cout << "triangle" << endl;
    }
};


typedef TypeList<Rect,
                TypeList<Square,
                        TypeList<Circle,
                                    TypeList<Triangle>
        > > > Shapes;

struct NIL {};

template<typename Head, typename Tail = NIL>
struct TypeList {
    typedef Head item;
    typedef Tail next;
};

template<typename T>
struct IDispatcherPart {
    virtual void Dispatch(T &) = 0;
};

template <typename T>
struct inheritDispatcher : IDispatcherPart<typename T::item>, inheritDispatcher<typename T::next> {};

template <>
struct inheritDispatcher<NIL> {};


template<typename T>
struct Dispatcher : T {
    Dispatcher setUnknown(){

    }

};

template<>
struct Dispatcher<int> {

};

template<class T, int arity = 0>
struct FirstStep {

};

template<class T, class T1, int arity>
struct SecondStep {

};

template<class T, class T1, class T2>
struct ThirdStep {

};

template<class A, class B>
struct Logic {
    string doStuff(A a, B b) {
        return a.name() + " " + b.name();
    }
};


string who(IShape &a, IShape &b) {
    return Dispatcher<>().setUnknown(0,a).setUnknown(1,b).getResult();
}

int main()
{  
    Rect a;
    Cicrle b;
    cout << who(a,b) << endl;
    return 0;
}

