#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "suffix_arrays.hpp"
#include "common.hpp"

using std::ifstream;
using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;
using std::set;

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace sa = suffix_arrays;

void print_help(const po::options_description & desc) {
    cout << "Usage: locate --help | [--database FILE] [-p] PATTERN"
         << endl
         << desc
         << endl;
}

int main(const int argc, const char* argv[]) {
    po::options_description desc("Allowed options");
    desc.add_options() ("help,h", "Show help message")
            ("database,d",
             po::value<string>()->required()->default_value("index.db"),
             "Database file")
            ("pattern,p",
             po::value<string>()->required(),
             "Search pattern")
            ;
    po::positional_options_description pdesc;
    pdesc.add("pattern", 1);

    po::variables_map vm;

    try {
        po::store(po::command_line_parser(argc, argv)
                  .options(desc)
                  .positional(pdesc)
                  .run(),
                  vm);
        if (vm.count("help")) {
            print_help(desc);
            return 1;
        }
        po::notify(vm);
    } catch (const std::exception& e) {
        cout << e.what() << endl;
        print_help(desc);
        return 1;
    }

    string input_file_path = vm["database"].as<string>();
    ifstream input_file(input_file_path);
    if (!input_file) {
        std::cerr << "Cannot open \""  << input_file_path << "\" for reading" << endl;
        return 2;
    }

    vector<fs::path> found_paths;
    {
        vector<fs::path> paths;
        sa::suffix_array_with_data index;
        input_file >> paths >> index;

        const string pattern = vm["pattern"].as<string>();

        for (auto path_id : sa::search(index, pattern)) {
            if (fs::exists(paths[path_id])) {
                found_paths.push_back(paths[path_id]);
            }
        }

        std::sort(found_paths.begin(), found_paths.end());
    }

    for (const auto & path : found_paths) {
        cout << path << endl;
    }

    return 0;
}
