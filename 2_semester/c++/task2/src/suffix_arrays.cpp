#include "suffix_arrays.hpp"

#include <algorithm>
#include <cmath>
#include <utility>

#define ALPHABET_SIZE pow(2,(sizeof(char)*8))

namespace suffix_arrays {

    using std::vector;
    using std::string;
    using std::pair;
    using std::make_pair;

    suffix_array build(const string &word) {

        int n = word.size();

        vector<int> p(n, 0), cnt(ALPHABET_SIZE, 0), c(n, 0);

        for (int i = 0; i < n; ++i) {
            ++cnt[word[i]];
        }
        for (int i = 1; i < ALPHABET_SIZE; ++i) {
            cnt[i] += cnt[i-1];
        }
        for (int i = 0; i < n; ++i) {
            p[--cnt[word[i]]] = i;
        }
        c[p[0]] = 0;

        int classes = 1;
        for (int i = 1; i < n; ++i) {
            if (word[p[i]] != word[p[i-1]])  {
                ++classes;
            }
            c[p[i]] = classes - 1;
        }

        vector<int> pn(n, 0), cn(n, 0);
        for (int h = 0; (1<<h) < n; ++h) {
            for (int i = 0; i < n; ++i) {
                pn[i] = p[i] - (1<<h);
                if (pn[i] < 0){
                    pn[i] += n;
                }
            }
            cnt.assign(classes, 0);
            for (int i = 0; i < n; ++i) {
                ++cnt[c[pn[i]]];
            }
            for (int i = 1; i < classes; ++i) {
                cnt[i] += cnt[i-1];
            }
            for (int i = n - 1; i >= 0; --i) {
                p[--cnt[c[pn[i]]]] = pn[i];
            }
            cn[p[0]] = 0;
            classes = 1;
            for (int i = 1; i < n; ++i) {
                int mid1 = (p[i] + (1<<h)) % n,
                        mid2 = (p[i-1] + (1<<h)) % n;
                if (c[p[i]] != c[p[i-1]] || c[mid1] != c[mid2]) {
                    ++classes;
                }
                cn[p[i]] = classes - 1;
            }
            swap(c, cn);
        }

        return p;
    }

    suffix_array_with_data merge(const suffix_array_with_data & left, const suffix_array_with_data & right) {

        suffix_array_with_data result;

        size_t l = 0, r = 0;

        while (l < left.size() && r < right.size()) {
            int compare_result = left[l].first.compare(right[r].first);
            if (compare_result < 0) {
                result.push_back(left[l++]);
            } else if (compare_result == 0) {
                vector<int> path_ids(left[l].second.begin(), left[l].second.end());
                path_ids.insert(path_ids.end(), right[r].second.begin(), right[r].second.end());
                result.push_back(make_pair(left[l].first, path_ids));
                l++;
                r++;
            } else {
                result.push_back(right[r++]);
            }
        }

        std::for_each(left.begin() + l, left.end(), [&result](suffix_array_with_data_entry entry) {
            result.push_back(entry);
        });

        std::for_each(right.begin() + r, right.end(), [&result](suffix_array_with_data_entry entry) {
            result.push_back(entry);
        });

        return result;
    }

    pair<int, int> search(const suffix_array_with_data & index,
                                const pair<int, int> & range,
                                const string & pattern) {
        if (range.second - range.first == 0) {
            return range;
        }
        int middle = (range.first + range.second) / 2;
        string middle_string = index[middle].first.substr(0, pattern.size());
        int compare_result = middle_string.compare(pattern);
        if (compare_result > 0) {
            return search(index, make_pair(range.first, middle), pattern);
        } else if (compare_result < 0) {
            return search(index, make_pair(middle + 1, range.second), pattern);
        } else {
            pair<int, int> result = make_pair(middle, middle + 1);
            for (;;) {
                if (result.first == 0 || result.first == range.first) {
                    break;
                }
                string next = index[result.first - 1].first.substr(0, pattern.size());
                if (next.compare(pattern) == 0) {
                    result.first--;
                } else {
                    break;
                }
            }
            for (;;) {
                if (result.second == range.second) {
                    break;
                }
                string next = index[result.second].first.substr(0, pattern.size());
                if (next.compare(pattern) == 0) {
                    result.second++;
                } else {
                    break;
                }
            }
            return result;
        }
    }

    std::set<int> search(const suffix_array_with_data & index, const std::string & pattern) {
        auto range = search(index, make_pair(0, index.size()), pattern);
        std::set<int> result;
        for (int i = range.first; i < range.second; ++i) {
            auto path_ids = index[i].second;
            result.insert(path_ids.begin(), path_ids.end());
        }
        return result;
    }

}
