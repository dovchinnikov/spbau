#ifndef FS_TRAVERSE_HPP
#define FS_TRAVERSE_HPP

#include <vector>
#include <boost/filesystem.hpp>

#include "suffix_arrays.hpp"

namespace fs_traverse {

    namespace fs = boost::filesystem;
    namespace sa = suffix_arrays;

    auto get_files(const fs::path &) -> std::vector<fs::path>;
    auto build_index(const std::vector<fs::path> &) -> sa::suffix_array_with_data;

}

#endif // FS_TRAVERSE_HPP
