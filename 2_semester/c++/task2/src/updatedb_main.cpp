#include <iostream>
#include <fstream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "fs_traverse.hpp"
#include "common.hpp"

using std::string;
using std::endl;

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace ft = fs_traverse;

void print_help(const po::options_description & desc) {
    std::cout << "Usage: updatedb --help | [--database-root DIR] [--output FILE]"
              << endl
              << desc
              << endl;
}

int main(const int argc, const char *argv[]) {

    po::options_description desc("Allowed options");
    desc.add_options() ("help,h", "Show help message")
            ("database-root,r",
             po::value<string>()->required()->default_value("."),
             "Root catalog for scanning")
            ("output,o",
             po::value<string>()->required()->default_value("index.db"),
             "File to save index to")
            ;
    po::variables_map vm;

    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        if (vm.count("help")) {
            print_help(desc);
            return 1;
        }
        po::notify(vm);
    } catch (const std::exception& e) {
        std::cerr << e.what() << endl;
        print_help(desc);
        return 1;
    }

    string output_file_path = vm["output"].as<string>();
    std::ofstream output_file(output_file_path, std::ios::trunc);
    if (!output_file) {
        std::cerr << "Cannot open \"" << output_file_path << "\" for writing" << endl;
        return 2;
    }

    fs::path root(vm["database-root"].as<string>());
    if (!fs::exists(root)) {
        std::cerr << root << " doesn't exist" << endl;
        return 3;
    } else if (!fs::is_directory(root)){
        std::cerr << root << " is not a directory" << endl;
        return 4;
    }

    auto paths = ft::get_files(root);
    auto file_index = ft::build_index(paths);

    output_file << paths << endl << file_index;

    return 0;
}

