#ifndef REDUCE_HPP
#define REDUCE_HPP

#include <functional>
#include <vector>
#include <future>

template <typename T>
using reduce_function = std::function<T(T,T)>;

template <typename T>
T reduce(typename std::vector<T>::const_iterator from,
         typename std::vector<T>::const_iterator to,
         const reduce_function<T> & f) {

    size_t size = to - from;

    if (size == 1) {
        return *from;
    } else if (size == 2) {
        return f(*from, *(from+1));
    }

    auto future_left = std::async(std::launch::async, reduce<T>, from , from + size / 2, f);
    auto future_right = std::async(std::launch::async, reduce<T>, from + size / 2, to, f);

    return f(future_left.get(), future_right.get());
}

#endif // REDUCE_HPP
