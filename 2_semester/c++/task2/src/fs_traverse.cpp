#include "fs_traverse.hpp"

#define THREADS 5

#include <list>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <string>
#include <algorithm>

#include "reduce.hpp"

using std::string;

auto fs_traverse::get_files(const fs::path & root) -> std::vector<fs::path> {
    std::vector<fs::path> paths;

    std::list<fs::path> directories_queue {fs::canonical(root)};

    std::atomic<int> active_threads(THREADS);
    std::atomic<bool> all_done(false);

    std::mutex paths_mutex;
    std::mutex directories_mutex;

    std::condition_variable got_some_directories;
    std::condition_variable done;

    for (int i = 0; i < THREADS; ++i) {
        std::thread ([&](){
            while(true) {
                std::unique_lock<std::mutex> lock(directories_mutex);

                while (directories_queue.empty()) {
                    active_threads--;
                    done.notify_all();
                    got_some_directories.wait(lock);
                    if (all_done) {
                        return;
                    }
                    active_threads++;
                }

                fs::path p = directories_queue.front();
                directories_queue.pop_front();
                lock.unlock();

                for (fs::directory_iterator it(p), end; it != end; it++) {
                    if (fs::is_directory(it->path()) && !fs::is_symlink(it->path())) {
                        std::lock_guard<std::mutex> _(directories_mutex);
                        directories_queue.push_back(fs::canonical(it->path()));
                        got_some_directories.notify_one();
                    } else {
                        std::lock_guard<std::mutex> _(paths_mutex);
                        paths.push_back(it->path());
                    }
                }
            }
        }).detach();
    }

    std::unique_lock<std::mutex> main_lock(directories_mutex);
    done.wait(main_lock,[&]() -> bool {return active_threads == 0;});
    all_done.store(true);
    got_some_directories.notify_all();

    return paths;
}

auto fs_traverse::build_index(const std::vector<fs::path> & paths) -> sa::suffix_array_with_data {
    sa::suffix_array_with_data file_index;

    int path_id = 0;
    std::vector<sa::suffix_array_with_data> arrays(paths.size());
    std::transform(paths.begin(), paths.end(),
              arrays.begin(), [&path_id] (const fs::path & file_path) {
        const string filename = file_path.filename().generic_string();
        sa::suffix_array s_array = sa::build(filename);
        sa::suffix_array_with_data s_array_w_data(s_array.size());
        std::transform(s_array.begin(), s_array.end(),
                  s_array_w_data.begin(), [path_id, filename](const sa::suffix_array_entry & e) {
            return std::make_pair(filename.substr(e), std::vector<int>({path_id}));
        });
        path_id++;
        return s_array_w_data;
    });

    file_index = reduce<sa::suffix_array_with_data>(
                arrays.begin(), arrays.end(),
                sa::merge
                );

    return file_index;
}
