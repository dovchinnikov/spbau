#ifndef COMMON_HPP
#define COMMON_HPP

#include <iostream>
#include <vector>
#include <string>

template <typename T>
std::ostream & operator<<(std::ostream & out, const std::vector<T> & v) {
    out << v.size() << ' ';
    for (auto it = v.begin(); it != v.end(); ++it) {
        out << *it << ' ';
    }
    return out;
}

template <typename T>
std::istream & operator>>(std::istream & in, std::vector<T> & v) {
    size_t count;
    in >> count;
    for (size_t i = 0; i < count; ++i) {
        T elem;
        in >> elem;
        v.push_back(elem);
    }
    return in;
}

#endif // COMMON_HPP
