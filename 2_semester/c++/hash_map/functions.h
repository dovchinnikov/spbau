#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>

template <typename T> struct Hash;

template <> struct Hash<int> {
    size_t operator() (const int & val) const {
        return val;
    }
};

template <> struct Hash<std::string> {
    size_t operator() (std::string val) {
        size_t hash = 0;
        for (size_t i = 0; i < val.length(); i++){
            hash += val[i] * 100000 / 13 / 11 / 7;
        }
        return hash;
    }
};

template <typename T> struct Equals {
    bool operator() (const T & a, const T & b) const {
       return a == b;
    }
};

#endif // FUNCTIONS_H
