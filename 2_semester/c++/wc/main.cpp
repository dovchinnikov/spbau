#include <iostream>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <iterator>

using namespace std;

template<class II>
void mergeSort(II start, II end) {
    size_t len = end - start;
    if (len < 2) {
        return;
    }
    II middle = start + len / 2;
    if (start != middle) {
        mergeSort(start, middle);
    }
    if (middle != end) {
        mergeSort(middle, end);
    }
    vector<int> res(len);
    merge(start, middle, middle, end, res.begin());
    copy(res.begin(), res.end(), start);
}

int main() {

    unordered_map<string, int> words;

    for_each(istream_iterator<string>(cin), istream_iterator<string>(), [&words](string s) {
        words[s]++;
    });

    for_each(words.begin(), words.end(), [](pair<string, int> entry) {
        cout << entry.first << " " << entry.second << endl;
    });
//------------------------
    vector<int> v = {1,10,2,9,3,8,4,7,5,6};
    mergeSort(v.begin(), v.end());

    for_each(v.begin(), v.end(), [](int a){
        cout << a << endl;
    });

    return 0;
}
