#include <stdio.h>
#include <curl/curl.h>
#include <iostream>
#include <string>
#include <sstream>
#include <utility>
#include <vector>
#include <regex>
#include "tqueue.h"
#include <mutex>
size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{
    std::stringstream& stream = *((std::stringstream*)userp);
    char* char_buf = (char*) buffer;

    for (size_t i = 0; i < size * nmemb; ++i)
        stream << char_buf[i];
    return size * nmemb;
}

std::string GetPage(std::string url)
{
    CURL *curl;
    CURLcode res;
    std::stringstream stream;

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream);

        res = curl_easy_perform(curl);

        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));

        curl_easy_cleanup(curl);
    }

    return stream.str();
}

bool Filter(const std::string& match)
{
    if (match.find('.') != std::string::npos)
        return false;

    if (match.find(':') != std::string::npos)
        return false;

    return true;
}

std::string ToURL(const std::string& match)
{
    return "en.wikipedia.org/wiki/" + match;
}

std::vector<std::string> GetMatches(std::string page)
{
    std::vector<std::string> matches;

    const std::string ref = "<a href=\"/wiki/";

    size_t pos = 0;
    while (pos < page.size() && pos != std::string::npos)
    {
        size_t begin = page.find(ref, pos);
        if (begin == std::string::npos)
            break;

        size_t urlStart = begin + ref.size();
        pos = page.find("\"", urlStart);

        auto match = page.substr(urlStart, pos - urlStart);
        matches.push_back(match);
    }

    return matches;
}

std::vector<std::string> GetURLs(std::string page)
{
    std::vector<std::string> urls;
    auto matches = GetMatches(page);

    for(auto match : matches)
    {
        if(Filter(match))
            urls.push_back(ToURL(match));
    }

    return urls;
}

int pages_counter = 0;

void downloader(TQueue & urls, TQueue & pages) {
    std::unique_lock<std::mutex> lck(urls.mutex);
    while(urls.empty()) {
        urls.cvar.wait(lck);

    }


    auto url = urls.pop();
    lck.unlock();

    auto page = GetPage(url);
    pages.push();
}

//void parser(TQueue & urls, TQueue & pages) {
//    while(pages.empty()) {
//        pages.cvar.wait();
//    }
//    urls.push(GetURLs(pages.pop()));
//}

int main(int argc, char** argv)
{
    if (argc < 2) {
        std::cout << "no base urls specified" << std::endl;
        return 0;
    }
    std::string page = GetPage(argv[1]);

    TQueue urls;
    TQueue pages;
    std::vector<std::thread> threads;

    for (int i = 0; i < 10; i++) {
        threads.push_back(std::thread(downloader, std::ref(urls), std::ref(pages)));
    }
    //    auto urls = GetURLs(page);

    //    for(auto url : urls)
    //        std::cout << url << std::endl;

    return 0;
}
