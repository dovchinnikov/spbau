#ifndef TQUEUE_H
#define TQUEUE_H

#include <queue>
#include <mutex>
#include <thread>
#include <condition_variable>

class TQueue
{
public:

    void push(std::string url) {
        std::lock_guard<std::mutex> locker(mutex);
        urls.push(url);
        cvar.notify_one();
    }

    std::string pop() {
        std::lock_guard<std::mutex> locker(mutex);
        std::string url = urls.back();
        return url;
    }

    bool empty() {
        return urls.empty();
    }

    std::condition_variable cvar;
    std::mutex mutex;
private:
    std::queue<std::string> urls;

};

#endif // TQUEUE_H
