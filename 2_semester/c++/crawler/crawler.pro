TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    simple.cpp \
    tqueue.cpp

QMAKE_CXXFLAGS += -std=c++11

HEADERS += \
    tqueue.h
