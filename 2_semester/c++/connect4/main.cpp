#include <iostream>
#include <sstream>
#include "field.h"

using namespace std;

int main()
{
    Field f;
    int enemy_move;
    {
        string first;
        cin >> first;
        if (first == "Go") {
            cout << f.my_column << endl;
        } else {
            stringstream(first) >> enemy_move;
            cout << f.next_move(enemy_move) << endl;
        }
    }
    while (cin) {
        cin >> enemy_move;
        cout << f.next_move(enemy_move) << endl;
    }
    return 0;
}
