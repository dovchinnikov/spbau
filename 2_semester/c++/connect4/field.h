#ifndef FIELD_H
#define FIELD_H

#define MAGIC_NUMBER 42 // 6x7
#define DEBUG true

#include <algorithm>
#include <bitset>
#include <vector>
#include <set>
#include <queue>

using namespace std;

unsigned long WIN [] = {
    2113665, 4227330, 8454660, 16909320, 33818640,
    67637280, 135274560, 270549120, 541098240,
    1082196480, 2164392960, 4328785920, 8657571840,
    17315143680, 34630287360, 69260574720, 138521149440,
    277042298880, 554084597760, 1108169195520, 2216338391040,
    15, 1920, 245760, 31457280, 4026531840, 515396075520,
    30, 3840, 491520, 62914560, 8053063680, 1030792151040,
    60, 7680, 983040, 125829120, 16106127360, 2061584302080,
    120, 15360, 1966080, 251658240, 32212254720, 4123168604160,
    16843009, 2155905152, 275955859456, 33686018, 4311810304,
    551911718912, 67372036, 8623620608, 1103823437824, 134744072,
    17247241216, 2207646875648, 2130440, 272696320, 34905128960,
    4260880, 545392640, 69810257920, 8521760, 1090785280,
    139620515840, 17043520, 2181570560, 279241031680
};

struct Cell {
    size_t x;
    size_t y;
};

struct Variant {
    size_t col;
    bool causes_threat;
    bool causes_win;
    bool last_enemy_drop;
    bool my_last_drop;

    bool operator <(const Variant & other) const {
        return !operator >(other);
    }

    bool operator >(const Variant & other) const {
        if (causes_threat && other.causes_threat) {
            if (causes_win) {
                return true;
            } else if (other.causes_win) {
                return false;
            } else if (last_enemy_drop) {
                return false;
            } else if (my_last_drop) {
                return true;
            } else if (other.last_enemy_drop) {
                return true;
            } else {
                return !other.my_last_drop;
            }
        } else if (!causes_threat && other.causes_threat) {
            return false;
        } else if (causes_threat && !other.causes_threat) {
            return true;
        } else {
            if (causes_win) {
                return true;
            } else if (other.causes_win) {
                return false;
            } else if (last_enemy_drop) {
                return false;
            } else if (my_last_drop) {
                return true;
            } else if (other.last_enemy_drop) {
                return true;
            } else {
                return !other.my_last_drop;
            }
        }
    }
};

class Field
{
public:

    Field(): white(0), black(0) {
    }

    bool can_drop(const size_t col) {
        size_t bit = col;
        while (bit < MAGIC_NUMBER && (white.test(bit) || black.test(bit))) {
            bit += 7; //next row
        }
        return bit < MAGIC_NUMBER;
    }

    void drop(const size_t col, bool enemy = true) {
        size_t bit = col;
        while (bit < MAGIC_NUMBER && (white.test(bit) || black.test(bit))) {
            bit += 7; //next row
        }
        if (bit >= MAGIC_NUMBER) {
            return;
        }
        if (enemy) {
            black.set(bit);
        } else {
            white.set(bit);
        }
    }

    set<size_t> get_wins(bool enemy = false, int drops = 1) {
        set<size_t> wins;
        for (int i = 0; i < 7; i++) {
            if (!can_drop(i)) {
                continue;
            }
            Field f = *this;
            f.drop(i, enemy);
            if (drops == 2) {
                enemy = !enemy;
                f.drop(i, enemy);
                if (!can_drop(i)) {
                    continue;
                }
            }
            bitset<MAGIC_NUMBER> & curr = enemy ? f.black : f.white;
            for(size_t w = 0; w < 69; w++) {
                if ((curr & bitset<MAGIC_NUMBER>(WIN[w])) == WIN[w]) {
                    wins.insert(i);
                    goto column;
                }
            }
column:
            continue;
        }
        return wins;
    }

    int next_move(const size_t enemy_move) {
        drop(enemy_move);
        int move;

        set<size_t> wins = get_wins();
        if (wins.size() > 0) {
            move = *wins.begin();
        } else {
            set<size_t> threats = get_wins(true);
            if (threats.size() > 0) {
                move = *threats.begin();
            } else {

                set<size_t> causes_threat = get_wins(true, 2);
                set<size_t> causes_win = get_wins(false, 2);
                priority_queue<Variant> choices;
                for (size_t i = 0; i < 7; i++) {
                    if (can_drop(i)) {
                        choices.push({
                                         i,
                                         causes_threat.find(i) != causes_threat.end(),
                                         causes_win.find(i)!= causes_win.end(),
                                         i == enemy_move,
                                         i == my_column
                                     });
                    }
                }
                move = choices.top().col;


//                if (enemy_move == my_column) {
//                    my_column++;
//                    my_column%=7;
//                }
//                for (int i = 0; i < 7 && !can_drop(i); i++) {
//                    my_column++;
//                    my_column%=7;
//                }
//                move = my_column;
            }
        }

        drop(move, false);
        my_column = move;
        return move;
    }

    bitset<MAGIC_NUMBER> white;
    bitset<MAGIC_NUMBER> black;

    size_t my_column = 3;
};
//3, 0, 3, 2, 3, 3, 3, 5, 3, 0, 4, 2, 4, 0, 0, 4, 4, 2, 2, 0, 4, 5, 2, 5, 5, 5, 6, 2, 4, 5, 6, 6, 6, 0, 6, 6, 1, 1
#endif // FIELD_H
