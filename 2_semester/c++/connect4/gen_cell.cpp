#include <bitset>
#include <iostream>
#include <vector>

using namespace std;
typedef bitset<42> FFF;

int main () {

    vector<FFF> result;
    //vertical
    for (size_t y = 0; y < 3; y++) {
        for (size_t x = 0; x < 7; x++) {
            FFF c(0);
            c.set(x + y*7);
            c.set(x+7 + y*7);
            c.set(x+14 +y*7);
            c.set(x+21 + y*7);
            result.push_back(c);
        }
    }

    // horizontal
    for (size_t x = 0; x < 4; x++) {
        for (size_t y = 0; y < 6; y++) {
            FFF c(0);
            c.set(y*7 + x);
            c.set(y*7 + x + 1);
            c.set(y*7 + x + 2);
            c.set(y*7 + x + 3);
            result.push_back(c);
        }
    }

    // diagonal
    for (size_t x = 0; x < 4; x++) {
        for (size_t y = 0; y < 3; y++) {
            FFF c(0);
            c.set(y*7 + x);
            c.set(y*7 + x + 1 + 7);
            c.set(y*7 + x + 2 + 14);
            c.set(y*7 + x + 3 + 21);
            result.push_back(c);
        }
    }

    for (size_t x = 0; x < 4; x++) {
        for (size_t y = 0; y < 3; y++) {
            FFF c(0);
            c.set(y*7 + x + 3);
            c.set(y*7 + x + 2 + 7);
            c.set(y*7 + x + 1 + 14);
            c.set(y*7 + x + 0 + 21);
            result.push_back(c);
        }
    }
    cout << result.size() << endl;
    for (auto it = result.begin(); it != result.end(); it++) {
        cout << it->to_ulong() << ", ";
//        for (int y = 0; y < 6; y++) {
//            for (int x = 0; x < 7; x++) {
//                cout << it->test(x + y*7) << " ";
//            }
//            cout << endl;
//        }

    }

}
