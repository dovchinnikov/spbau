#include <iostream>
#include <algorithm>
#include "tree_iterator.h"

using namespace std;

void print(int i) {
    cout << i << " ";
}

int main()
{
    Node<int> * r = new Node<int>(1);
    r->left = new Node<int>(2);
    r->left->parent = r;
    r->right = new Node<int>(3);
    r->right->parent = r;
    r->left->left = new Node<int>(4);
    r->left->left->parent = r->left;
    r->left->right = new Node<int>(5);
    r->left->right->parent = r->left;
    r->right->left = new Node<int>(6);
    r->right->left->parent = r->right;
    r->right->right = new Node<int>(7);
    r->right->right->parent = r->parent;

    Tree<int> tree(r);

    std::for_each(tree.begin(),tree.end(), print);
    return 0;
}

