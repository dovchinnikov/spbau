#ifndef TREE_ITERATOR_H
#define TREE_ITERATOR_H

#include <map>
#include <iterator>

template <typename K>
struct Node {
    Node * left;
    Node * right;
    Node * parent;
    K data;
    Node (const K & data = K()): data(data) {}
};

template<typename T> class tree_iterator
    : public std::iterator<std::forward_iterator_tag, T>
{

public:

    tree_iterator(Node<T> *root, bool end = false): current(root), end(end) {
        while (current->left){
            current = current->left;
        }
    }

    T& operator*() {
        return current->data;
    }

    T* operator->() {
        return &current->data;
    }

    tree_iterator& operator++() {
        visited[current] = true;
        if (current->left && !visited[current->left]) {
            current = current->left;
        } else if (current->right && !visited[current->right]) {
            current = current->right;
        } else if (current->parent){
            current = current->parent;
        } else {
            end = true;
        }
        return *this;
    }

    tree_iterator operator++(int) {
        tree_iterator temp = *this;
        operator ++();
        return temp;
    }

    bool operator==(const tree_iterator & other) const {
        if (end) {
            return other.end ? true : current == other.current;
        } else {
            return other.end ? false : current == other.current;
        }
    }

    bool operator!=(const tree_iterator & other) const {
        return !operator==(other);
    }

private:
    std::map<Node<T> *, bool> visited;
    Node<T> *current;
    bool end;
};

template <class K = int>
struct Tree {

    typedef tree_iterator<K> iterator;
    Tree(Node<K> *root = 0):root(root) {}

    iterator begin() {
        return iterator(root);
    }

    iterator end() {
        return iterator(root, true);
    }

    Node<K> * root;
};


#endif // TREE_ITERATOR_H
