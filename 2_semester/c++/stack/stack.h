#ifndef STACK_H
#define STACK_H

#define START_MEM_SIZE 10

#include <stdlib.h>

template <class T>
class stack_base {

public:
    stack_base(int n){
        mem = 0;
        mem = malloc(sizeof(T) * n);
    }

    ~stack_base(){
        if (mem) {
            free(mem);
        }
    }
    void *mem;

};

template <class T>
class Stack : private stack_base<T>
{    

public:
    Stack(const int preallocated_memory = START_MEM_SIZE) : stack_base<T>(preallocated_memory), mem_size(preallocated_memory), counter(0) {

    }

    ~Stack(){

    }

    void push (const T & val) {
        if (counter < mem_size) {
            new ((void *) ((T *)this->mem + counter)) T(val);
            ++counter;
        } else {

        }
    }

    void pop() {
        --counter;
    }

    const T & top(){
        return *((T*)this->mem + counter - 1);
    }

    bool empty() {
        return counter == 0;
    }

    size_t size() {
        return counter;
    }

private:
    int mem_size;
    size_t counter;

};

#endif // STACK_H
