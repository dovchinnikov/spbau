#include <iostream>
#include "stack.h"

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    Stack<int> stack;
    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.push(4);
    stack.pop();
    stack.push(5);
    stack.push(6);
    stack.push(7);
    stack.push(8);
    stack.push(9);
    stack.push(10);

    while(!stack.empty()) {
        cout << stack.top() << endl;
        stack.pop();
    }

    return 0;
}

