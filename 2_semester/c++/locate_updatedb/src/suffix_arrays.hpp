#ifndef SUFFIX_ARRAYS_HPP
#define SUFFIX_ARRAYS_HPP

#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <set>

#include "common.hpp"

namespace suffix_arrays {

    typedef int suffix_array_entry;
    typedef std::vector<suffix_array_entry> suffix_array;

    typedef std::pair<std::string, std::vector<int>> suffix_array_with_data_entry;
    typedef std::vector<suffix_array_with_data_entry> suffix_array_with_data;

    suffix_array build(const std::string &);
    suffix_array_with_data merge(const suffix_array_with_data &, const suffix_array_with_data &);
    std::set<int> search(const suffix_array_with_data &, const std::string &);

}

inline std::ostream & operator<<(std::ostream & out, const suffix_arrays::suffix_array_with_data_entry & entry) {
    out << "\"" << entry.first << "\" " << entry.second << ' ';
    return out;
}

inline std::istream & operator>>(std::istream & in, suffix_arrays::suffix_array_with_data & array) {
    size_t count;
    in >> count;
    while (count--) {
        std::string suffix;
        in.ignore(256, '"');
        std::getline(in, suffix, '"');
        std::vector<int> path_ids;
        in >> path_ids;
        array.push_back(std::make_pair(suffix, path_ids));
    }
    return in;
}

#endif // SUFFIX_ARRAYS_HPP
