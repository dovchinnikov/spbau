#include <iostream>
#include <unordered_map>

using namespace std;

int main() {

    unordered_map<string, int> counts;
    string s;
    getline(cin, s);
    while (!s.empty()) {
        counts[s]++;
        getline(cin, s);
    }
    for (unordered_map<string, int>::iterator it = counts.begin(); it != counts.end(); it++) {
        cout << it->first << "\t" << it->second << endl;
    }
    return 0;
}

