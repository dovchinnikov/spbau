#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <queue>

using namespace std;

const int INF = 1000000000;

struct edge {
    int f, t, cap, flow;
};

vector<edge> e;
vector<vector<int>> g;
vector<int> d, ptr;
int s = 0, t;

vector<int> w;
vector<int> r;
vector<vector<int>> games;
int n;

void add_edge(int from, int to, int capacity) {
    edge e1 = { from,   to,     capacity,   0 };
    edge e2 = { to,     from,   0,          0 };
    g[from].push_back(e.size());
    e.push_back(e1);
    g[to].push_back(e.size());
    e.push_back(e2);
}

bool bfs() {
    queue<int> q;
    q.push(s);
    d.assign(n, -1);
    d[s] = 0;
    while (!q.empty() && d[t] == -1) {
        int v = q.front();
        q.pop();
        for (size_t i = 0; i < g[v].size(); i++) {
            int id = g[v][i],
                to = e[id].t;
            if (d[to] == -1 && e[id].flow < e[id].cap) {
                q.push(to);
                d[to] = d[v] + 1;
            }
        }
    }
    return d[t] != -1;
}

int dfs (int v, int flow) {
    if (!flow) {
        return 0;
    }
    if (v == t) {
        return flow;
    }
    for (; ptr[v] < (int)g[v].size(); ++ptr[v]) {
        int id = g[v][ptr[v]],
            to = e[id].t;
        if (d[to] != d[v] + 1)
            continue;
        int pushed = dfs (to, min (flow, e[id].cap - e[id].flow));
        if (pushed) {
            e[id].flow += pushed;
            e[id^1].flow -= pushed;
            return pushed;
        }
    }
    return 0;
}

int dinic() {
    int flow = 0;
    for (;;) {
        if (!bfs())
            break;
        ptr.assign(n, 0);
        while (int pushed = dfs (s, INF)) {
            flow += pushed;
        }
    }
    return flow;
}

int main() {
    cin >> n;

    g.resize(n + 1 + n * n);
    w.resize(n+1);
    for (int i = 1; i <= n; i++) {
        cin >> w[i];
    }

    r.resize(n+1);
    for (int i = 1; i <= n; i++) {
        cin >> r[i];
    }
    games.resize(n + 1);

    for (int i = 1; i <= n; i++) {
        games[i].assign(n+1, 0);
    }

    int expected_flow = 0;
    t = n + 1;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            int a;
            cin >> a;
            if (a > 0 && games[i][j] == 0) {
                add_edge(s, t, a);
                expected_flow += a;
                add_edge(t, i, INF);
                add_edge(t, j, INF);
                games[i][j] = a;
                games[j][i] = a;
                t++;
            }
        }
    }

    add_edge(1, t, INF);
    w[1] += r[1];
    for (int i = 2; i <= n; i++) {
        int cnt = w[1] - w[i];
        if (cnt > 0) {
            add_edge(i, t, cnt);
        }
    }
    n = t + 1;

    bool has_chances = *(max_element(w.begin(), w.end())) == w[1];
    if (!has_chances) {
        cout << "NO" << endl;
        return 0;
    }

    int real_flow = dinic();
    cout << (real_flow >= expected_flow ? "YES" : "NO") << endl;
    return 0;
}
