#include <iostream>

using namespace std;

struct Range {
    Range(int s, int e):start(s),end(e),count(getLength()) {}

    int getLength(){
        return end - start + 1;
    }

    int start;
    int end;
    int count;
};

struct RangeComparator {
    bool operator()(Range a, Range b) {
        return a.start < b.start;
    }
};

struct TreeNode {
    TreeNode *left, *right;
    TreeNode *parent;
    Range data;
    TreeNode(Range init) : left(0), right(0), parent(0), data(init) {}
};

class SplayTree {

    public:
        SplayTree(int n) :root(0) {
            insert(Range(1,n));
        }

        void delete_room(int room) {
            TreeNode *p = find_range_node(room);
            Range r = p->data;
            Range a (r.start, r.start + room - 2);
            Range b (r.start + room, r.end);
            erase(p);
            if (a.getLength() > 0) {
                insert(a);
            }
            if (b.getLength() > 0) {
                insert(b);
            }
        }

        int look_at(int room) {
            TreeNode *p = find_range_node(room);
            return p->data.start + room - 1;
        }
    private:

        TreeNode *root;
        RangeComparator comp;

        void reset_count(TreeNode *x) {
            if (x) {
                x->data.count = (x->left ? x->left->data.count : 0 )
                        + (x->right ? x->right->data.count : 0)
                        + x->data.getLength();
            }
        }

        TreeNode *find_range_node(int & room) {
            TreeNode *p = 0, *current = root;
            while (current) {
                p = current;

                TreeNode *left = current->left;
                int left_count = left ? left->data.count : 0;

                if (room <= left_count) {
                    current = current->left;
                } else if (room <= left_count + current->data.getLength()) {
                    room -= left_count;
                    break;
                } else {
                    room -= left_count;
                    room -= current->data.getLength();
                    current = current->right;
                }
            }
            return p;
        }

        void insert(Range key) {
            TreeNode *p = 0, *z = root;
            while(z) {
                p = z;
                if (comp(z->data, key)) {
                    z = z->right;
                } else {
                    z = z->left;
                }
            }

            z = new TreeNode(key);
            z->parent = p;
            if (!p) {
                root = z;
            } else if (comp( p->data, z->data)) {
                p->right = z;
            } else {
                p->left = z;
            }
            splay(z);
        }

        void erase (TreeNode *z) {
            if (!z) {
                return;
            }

            splay(z);

            if (!z->left){
                if (z->right) {
                    z->right->parent = 0;
                }
                root = z->right;
            } else if (!z->right) {
                if (z->left) {
                    z->left->parent = 0;
                }
                root = z->left;
            } else {
                TreeNode *right = z->right;
                TreeNode *right_min = subtree_minimum(right);
                right->parent = 0;
                splay(right_min);
                if (right_min->left) {
                    cout << "azaza";
                }
                right_min->left = z->left;
                right_min->left->parent = right_min;
                root = right_min;
                reset_count(root);
            }

            delete z;
        }


        void left_rotate(TreeNode *x) {
            TreeNode *y = x->right;
            x->right = y->left;
            if (y->left) {
                y->left->parent = x;
            }
            y->parent = x->parent;
            if (!x->parent) {
                root = y;
            } else if (x == x->parent->left) {
                x->parent->left = y;
            } else {
                x->parent->right = y;
            }
            y->left = x;
            x->parent = y;
            reset_count(x);
            reset_count(y);
        }

        void right_rotate( TreeNode *x ) {
            TreeNode *y = x->left;
            x->left = y->right;
            if (y->right) {
                y->right->parent = x;
            }
            y->parent = x->parent;
            if(!x->parent) {
                root = y;
            } else if (x == x->parent->left) {
                x->parent->left = y;
            } else {
                x->parent->right = y;
            }
            y->right = x;
            x->parent = y;
            reset_count(x);
            reset_count(y);
        }

        void splay(TreeNode *x) {
            while(x->parent) {
                if(!x->parent->parent) {
                    if(x->parent->left == x) {
                        right_rotate(x->parent);
                    } else {
                        left_rotate(x->parent);
                    }
                } else if(x->parent->left == x && x->parent->parent->left == x->parent) {
                    right_rotate( x->parent->parent );
                    right_rotate( x->parent );
                } else if( x->parent->right == x && x->parent->parent->right == x->parent) {
                    left_rotate( x->parent->parent );
                    left_rotate( x->parent );
                } else if( x->parent->left == x && x->parent->parent->right == x->parent ) {
                    right_rotate( x->parent );
                    left_rotate( x->parent );
                } else {
                    left_rotate( x->parent );
                    right_rotate( x->parent );
                }
            }
        }

        TreeNode* subtree_minimum( TreeNode *u ) {
            while (u->left) {
                u = u->left;
            }
            return u;
        }
};


int main()
{
    int n, m, room;
    char action;
    cin >> n >> m;

    SplayTree tree(n);

    for (int i = 0; i < m; i++) {
        cin >> action >> room;
        if (action == 'L') {
            cout << tree.look_at(room) << endl;
        } else if (action == 'D') {
            tree.delete_room(room);
        }
    }
    return 0;
}

