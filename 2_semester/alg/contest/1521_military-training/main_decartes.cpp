#include <iostream>

using namespace std;

struct Range {
     Range(int s, int e) : start(s), end(e), count(getLength()) {}

     size_t getLength() {
         return end - start + 1;
     }

     int start;
     int end;
     size_t count;
};

struct RangeComparator {
    bool operator()(Range a, Range b) {
        return a.start < b.start;
    }
};

struct TreapNode {

    static int level;

    Range range;
    int priority;
    TreapNode *left, *right;

    TreapNode(Range r): range(r), priority(level++), left(0), right(0) {}

};

int TreapNode::level = 0;

struct RangedTreap {

    TreapNode *root;
    size_t _size;
    RangeComparator comp;

    RangedTreap(int n) :root(0),_size(0) {

        insert(Range(1,n));
    }

    size_t size() {
<<<<<<< HEAD
        return root ? root->range.count : 0;
=======
        return _size;
>>>>>>> d432babbd873d51884ccc52efd3951fff28c0644
    }

    void reset_count(TreapNode *x) {
        if (x) {
            x->range.count = (x->left ? x->left->range.count : 0 )
                    + (x->right ? x->right->range.count : 0)
                    + x->range.getLength();
        }
    }

    int delete_kth(size_t order){
        TreapNode *p = find_range_node(order);
        Range r = p->range;
        Range a (r.start, r.start + order - 2);
        Range b (r.start + order, r.end);
        erase(p);
        if (a.getLength() > 0) {
            insert(a);
        }
        if (b.getLength() > 0) {
            insert(b);
        }
        return r.start + order - 1;
    }

    void insert(Range range){
<<<<<<< HEAD
        _size++;
=======
>>>>>>> d432babbd873d51884ccc52efd3951fff28c0644
        TreapNode *new_node = new TreapNode(range);

        if (!root) {
            root = new_node;
            return;
        }

        TreapNode *l = 0, *m = 0, *r = 0;

        split(root, range, l, r);

        m = merge(l, new TreapNode(range));

        root = merge(m, r);
<<<<<<< HEAD

=======
>>>>>>> d432babbd873d51884ccc52efd3951fff28c0644
    }

    TreapNode *merge(TreapNode* a, TreapNode* b){
        if (!a) {
            return b;
        } else if (!b) {
            return a;
        }

        if (a->priority > b->priority) {
            a->right = merge(a->right, b);
<<<<<<< HEAD
            reset_count(a);
            return a;
        } else {
            b->left = merge(a, b->left);
            reset_count(b);
=======
            return a;
        } else {
            b->left = merge(a, b->left);
>>>>>>> d432babbd873d51884ccc52efd3951fff28c0644
            return b;
        }
    }

    void split(TreapNode* rt, Range range, TreapNode* &left, TreapNode* &right){
        if (!rt) {
            left = 0;
            right = 0;
            return;
        }
        if (comp(rt->range, range)){
            right = rt;
            split(right->left, range, left, right->left);
        } else {
            left = rt;
            split(left->right, range, left->right, right);
        }
    }

    TreapNode *find_range_node(size_t & kth) {
        TreapNode *p = 0, *current = root;
        while (current) {
            p = current;

            TreapNode *left = current->left;
            size_t left_count = left ? left->range.count : 0;

            if (kth <= left_count) {
                current = current->left;
            } else if (kth <= left_count + current->range.getLength()) {
                kth -= left_count;
                break;
            } else {
                kth -= left_count;
                kth -= current->range.getLength();
                current = current->right;
            }
        }
        return p;
    }

    void erase(TreapNode *p) {
<<<<<<< HEAD
        TreapNode *a = 0, *b = 0;
        split(root, p->range, a, b);
        if (comp(p->range, root->range)) {
            split(b,a->range,a,b);
        } else {
            split(a,a->range,a,b);
        }
        root = merge(a,b);
        _size--;
        delete p;
=======

>>>>>>> d432babbd873d51884ccc52efd3951fff28c0644
    }
};


int main()
{

    int n, k;
    cin >> n >> k;

    RangedTreap tree(n);
    size_t a = 1;
    while (tree.size() > 0) {
        a += k - 1;
        a %= tree.size();
        a = a ? a : tree.size();
        cout << tree.delete_kth(a) << " ";
    }
    cout << endl;


    return 0;
}
