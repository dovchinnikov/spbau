#include <iostream>
#include <assert.h>

using std::cin;
using std::cout;
using std::endl;

class RangedSplayTree {

private:
    struct Range {
        int start;
        int end;
        int count;

        Range(int s, int e) : start(s), end(e), count(getLength()) {}

        int getLength() const {
            return end - start + 1;
        }
    };

    struct RangeComparator {
        bool operator() (const Range & a, const Range & b) const {
            return a.start < b.start;
        }
    };

    struct TreeNode {
        TreeNode *left, *right;
        TreeNode *parent;
        Range data;
        explicit TreeNode(Range init) : left(0), right(0), parent(0), data(init) {}
        ~TreeNode() {
            delete left;
            delete right;
        }
    private:
        TreeNode(const TreeNode &);
        TreeNode & operator=(const TreeNode &);
    };

public:
    explicit RangedSplayTree(int n) : root(0) {
        insert(Range(1,n));
    }

    ~RangedSplayTree() {
        delete root;
    }

private:
    RangedSplayTree (const RangedSplayTree &);
    RangedSplayTree & operator=(const RangedSplayTree &);

public:
    int delete_kth(int order) {
        TreeNode *p = find_range_node(order);
        if (p) {
            Range r = p->data;
            Range a (r.start, r.start + order - 2);
            Range b (r.start + order, r.end);
            erase(p);
            if (a.getLength() > 0) {
                insert(a);
            }
            if (b.getLength() > 0) {
                insert(b);
            }
            return r.start + order - 1;
        } else {
            return -1;
        }
    }

    int show_kth(int order) const {
        TreeNode *p = find_range_node(order);
        if (p) {
            return p->data.start + order - 1;
        } else {
            return -1;
        }
    }

    size_t size() const {
        return root ? root->data.count : 0;
    }

private:

    void reset_count(TreeNode *x) {
        if (x) {
            x->data.count = (x->left ? x->left->data.count : 0 )
                    + (x->right ? x->right->data.count : 0)
                    + x->data.getLength();
        }
    }

    TreeNode *find_range_node(int & room) const {
        TreeNode *current = root;
        while (current) {
            TreeNode *left = current->left;
            int left_count = left ? left->data.count : 0;

            if (room <= left_count) {
                current = current->left;
            } else if (room <= left_count + current->data.getLength()) {
                room -= left_count;
                return current;
            } else {
                room -= left_count;
                room -= current->data.getLength();
                current = current->right;
            }
        }
        return 0;
    }

    void insert(Range key) {
        TreeNode *p = 0, *z = root;
        while(z) {
            p = z;
            if (comp(z->data, key)) {
                z = z->right;
            } else {
                z = z->left;
            }
        }

        z = new TreeNode(key);
        z->parent = p;
        if (!p) {
            root = z;
        } else if (comp( p->data, z->data)) {
            p->right = z;
        } else {
            p->left = z;
        }
        splay(z);
    }

    void erase (TreeNode *z) {
        if (!z) {
            return;
        }

        splay(z);

        if (!z->left) {
            if (z->right) {
                z->right->parent = 0;
            }
            root = z->right;
        } else if (!z->right) {
            if (z->left) {
                z->left->parent = 0;
            }
            root = z->left;
        } else {
            TreeNode *right = z->right;
            TreeNode *right_min = subtree_minimum(right);
            right->parent = 0;
            splay(right_min);
            assert (right_min->left == NULL);
            right_min->left = z->left;
            right_min->left->parent = right_min;
            root = right_min;
            reset_count(root);
        }

        z->left = 0;
        z->right = 0;
        delete z;
    }


    void left_rotate(TreeNode *x) {
        TreeNode *y = x->right;
        x->right = y->left;
        if (y->left) {
            y->left->parent = x;
        }
        y->parent = x->parent;
        if (!x->parent) {
            root = y;
        } else if (x == x->parent->left) {
            x->parent->left = y;
        } else {
            x->parent->right = y;
        }
        y->left = x;
        x->parent = y;
        reset_count(x);
        reset_count(y);
    }

    void right_rotate(TreeNode *x) {
        TreeNode *y = x->left;
        x->left = y->right;
        if (y->right) {
            y->right->parent = x;
        }
        y->parent = x->parent;
        if(!x->parent) {
            root = y;
        } else if (x == x->parent->left) {
            x->parent->left = y;
        } else {
            x->parent->right = y;
        }
        y->right = x;
        x->parent = y;
        reset_count(x);
        reset_count(y);
    }

    void splay(TreeNode *x) {
        while(x->parent) {
            TreeNode *parent = x->parent;
            if (!parent->parent) {
                if (parent->left == x) {
                    right_rotate(parent);
                } else {
                    left_rotate(parent);
                }
            } else if (parent->left == x && parent->parent->left == parent) {
                right_rotate(parent->parent);
                right_rotate(parent);
            } else if (parent->right == x && parent->parent->right == parent) {
                left_rotate(parent->parent);
                left_rotate(parent);
            } else if (parent->left == x && parent->parent->right == parent) {
                right_rotate(parent);
                left_rotate(x->parent);
            } else {
                left_rotate(parent);
                right_rotate(x->parent);
            }
        }
    }

    TreeNode* subtree_minimum(TreeNode *u) const {
        while (u->left) {
            u = u->left;
        }
        return u;
    }

    TreeNode *root;
    static RangeComparator comp;

};

RangedSplayTree::RangeComparator RangedSplayTree::comp = RangedSplayTree::RangeComparator();

int main() {

    int n, k;
    cin >> n >> k;

    RangedSplayTree tree(n);
    size_t a = 1;

    while (tree.size() > 0) {
        a += k - 1;
        a %= tree.size();
        a = a ? a : tree.size();
        cout << tree.delete_kth(a) << " ";
    }

    cout << endl;
    return 0;
}
