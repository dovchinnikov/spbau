#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

vector<vector<int> > dp;
vector<int> parent;
vector<int> depth;

int getDP(size_t v, size_t i) {

    if (v >= parent.size()) {
        throw "no such a vertex";
    }

    while (v >= dp.size()) {
        dp.push_back(vector<int>());
    }

    if (i == 0) {
        if (dp[v].size() == 0) {
            dp[v].push_back(parent[v]);
        }
    } else {
        if (i >= dp[v].size()) {
            dp[v].push_back(getDP(getDP(v, i - 1), i-1));
        }
    }

    return dp[v][i];
}

int lca(int v, int u) {
    if (v == u) {
        return v;
    }

    if (depth[v] > depth[u]) {
        swap(v, u);                 //from now d[v] <= d[u]
    }

    // go up from u

    for (int i = log2(parent.size()); i >= 0; i--) {
        if (depth[u] - depth[v] >= pow(2, i)) {
            u = getDP(u, i);
        }
    }

    //here vertices are at the same depth, check if they are same
    if (v == u) {
        return v;
    }

    for (int i = log2(parent.size()); i >= 0; i--) {
        if (getDP(v,i) != getDP(u,i)) {
            v = getDP(v, i);
            u = getDP(u, i);
        }
    }
    return parent[v];
}



int main()
{
    int n;
    cin >> n;

    int vertex = 1;

    parent.push_back(0);
    depth.push_back(0);
    dp.push_back(vector<int>());
    dp[0].push_back(0);

    string action;

    while(n--) {
        cin >> action;
        if (action == "add") {
            int p;
            cin >> p;
            p--;
            parent.push_back(p);
            depth.push_back(depth[p] + 1);
            vertex++;
        } else {
           int u,v;
           cin >> u >> v;
           cout << lca(u - 1, v - 1) + 1 << endl;
        }
    }

    return 0;
}

