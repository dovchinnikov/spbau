#include <iostream>
#include <vector>
#include <unordered_set>
using namespace std;


template <typename T>
using matrix = vector<vector<T> >;

struct Simplex {
    unordered_set<size_t> N;
    unordered_set<size_t> B;
    matrix<double> A;
    vector<double> b;
    vector<double> c;
    double v;
};

Simplex pivot(Simplex simplex, size_t l, size_t e) {
//    int m = simplex.A.size();
//    int n = simplex.A[0].size();
//    Simplex result = simplex;
    auto _A(simplex.A);
    auto _b(simplex.b);
    auto _c(simplex.c);
    //Compute the equation for the new basic variable x_e.
    _b[e] = simplex.b[l] / simplex.A[l][e];
    for (size_t j : simplex.B) {
        if (j == e) continue;
        _A[e][j] = simplex.A[l][j] / simplex.A[l][e];
    }
    _A[e][l] = 1 / simplex.A[l][e];

    //Compute the coefficients of the remaining constraints.
    for (size_t i : simplex.B) {
        if (i == l) continue;
        _b[i] = simplex.b[i] - simplex.A[i][e]*_b[e];
        for (size_t j : simplex.N) {
            if (j == e) continue;
            _A[i][j] = simplex.A[i][j] - simplex.A[i][e]*_A[e][j];
        }
        _A[i][l] = - simplex.A[i][e]*_A[e][l];
    }

    // Compute the objective function
    auto _v = simplex.v + simplex.c[e]*_b[e];
    for (size_t j : simplex.N) {
        if (j == e) continue;
        _c[j] = simplex.c[j] - simplex.c[e] * _A[e][j];
    }
    _c[l] = - simplex.c[e] * _A[e][l];

    //Compute new sets for basic and non-basic variables.
    unordered_set<size_t> ss = simplex.N;
    ss.erase(ss.find(e));
    auto _N(ss);
    auto _B(ss);
    _N.emplace(l);
    _B.emplace(e);

    return {_N, _B, _A, _b, _c, _v};
}

Simplex initialize_simplex(matrix<double> A, vector<double> b, vector<double> c) {
    return Simplex();
}

vector<double> simplex(matrix<double> A, vector<double> b, vector<double> c) {
    Simplex simplex = initialize_simplex(A, b, c);
    int n = c.size();
    vector<double> delta(n, 0);

    return vector<double>();
}


int main()
{
    cout << "Hello World!" << endl;
    return 0;
}

