#include <iostream>
#include <vector>
#include <iterator>

const int INF = 1 << 20;

void solve(std::vector<std::vector<int>> & a, std::vector<int> & u, std::vector<int> & v, int m, int n) {
    std::vector<int> p (m+1), way (m+1);
    for (int i = 1; i <= n; ++i) {
        p[0] = i;
        int j0 = 0;
        std::vector<int> minv (m+1, INF);
        std::vector<char> used (m+1, false);
        do {
            used[j0] = true;
            int i0 = p[j0],
                    delta = INF,
                    j1;
            for (int j = 1; j <= m; ++j)
                if (!used[j]) {
                    int cur = a[i0][j]-u[i0]-v[j];
                    if (cur < minv[j])
                        minv[j] = cur,  way[j] = j0;
                    if (minv[j] < delta)
                        delta = minv[j],  j1 = j;
                }
            for (int j = 0; j <= m; ++j)
                if (used[j])
                    u[p[j]] += delta,  v[j] -= delta;
                else
                    minv[j] -= delta;
            j0 = j1;
        } while (p[j0] != 0);
        do {
            int j1 = way[j0];
            p[j0] = p[j1];
            j0 = j1;
        } while (j0);
    }
}

int main() {
    int n = 0;
    std::cin >> n;

    std::vector<int> u(n + 1);
    std::vector<int> v(n + 1);
    std::vector<std::vector<int>> a(n + 1, std::vector<int>(n + 1));

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            int aij = 0;
            std::cin >> aij;
            a[i + 1][j + 1] = -aij;
        }
    }

    solve(a, u, v, n, n);

    for (size_t i = 1; i < u.size(); ++i) {
        std::cout << -u[i] << " ";
    }
    std::cout << std::endl;

    for (size_t i = 1; i < v.size(); ++i) {
        std::cout << -v[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}
