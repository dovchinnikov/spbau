#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::vector;

vector<int> result_holder;

struct AvlNode {

    AvlNode(int val) : data(val), height(0), children_count(0),
        parent(0), left(0), right(0) {}

    void update() {
        if (left != 0 && right != 0) {
            if (left->height > right->height) {
                height = left->height + 1;
            } else {
                height = right->height + 1;
            }
        } else if(left != 0) {
            height = left->height + 1;
        } else if(right != 0) {
            height = right->height + 1;
        } else {
            height = 0;
        }
        children_count = (left ? left->children_count : 0)
                + (right ? right->children_count : 0) + 1;
    }

    int getBalance() {
        AvlNode * n = this;
        if (n->left != 0 && n->right != 0) {
            return n->left->height - n->right->height;
        } else if (n->left != 0) {
            return n->left->height + 1;
        } else if (n->right != 0) {
            return (-1) * (n->right->height + 1);
        } else {
            return 0;
        }
    }

    AvlNode* setLeft(AvlNode* new_left) {
        if(new_left != 0) new_left->parent = this;
        left = new_left;
        update();
        return left;
    }

    AvlNode* setRight(AvlNode* new_right) {
        if(new_right != 0) new_right->parent = this;
        right = new_right;
        update();
        return right;
    }

    int data;
    int height;
    int children_count;
    AvlNode * parent;
    AvlNode * left;
    AvlNode * right;
};

struct AVLTree {

    AVLTree():root(0) {}

    void insert(int val) {
        AvlNode* new_node = new AvlNode(val);
        int rank = 0;
        if(root == 0) {
            root = new_node;
        } else {
            AvlNode* current = root;

            while(true) {
                if(val < current->data) {
                    if(current->left == 0) {
                        current->setLeft(new_node);
                        break;
                    } else {
                        current = current->left;
                    }
                } else {
                    rank += current->left?current->left->children_count:0;
                    rank += 1;

                    if(current->right == 0) {
                        current->setRight(new_node);
                        break;
                    } else {
                        current = current->right;
                    }
                }
            }

            current = new_node;
            while (current != 0) {
                current->update();
                doBalance(current);
                current = current->parent;
            }
        }
        result_holder[rank]++;
    }

    void doBalance(AvlNode* n) {
        int bal = n->getBalance();
        if (bal > 1) {
            if(n->left->getBalance() < 0) {
                rotateLeft(n->left);
            }
            rotateRight(n);
        } else if (bal < -1) {
            if(n->right->getBalance() > 0) {
                rotateRight(n->right);
            }
            rotateLeft(n);
        }
    }

    void rotateLeft(AvlNode * n) {
        AvlNode * parent = n->parent;
        enum {left, right} side;
        if (parent != 0) {
            if (parent->left == n) {
                side = left;
            } else {
                side = right;
            }
        }
        AvlNode * temp = n->right;
        n->setRight(temp->left);
        temp->setLeft(n);

        if (parent != 0) {
            if (side == left) {
                parent->setLeft(temp);
            }
            if (side == right) {
                parent->setRight(temp);
            }
        } else {
            setRoot(temp);
        }
    }

    void rotateRight(AvlNode * n) {
        AvlNode * parent = n->parent;
        enum {left, right} side;
        if(parent != 0) {
            if(parent->left == n) {
                side = left;
            } else {
                side = right;
            }
        }
        AvlNode * temp = n->left;
        n->setLeft(temp->right);
        temp->setRight(n);

        if(parent != 0) {
            if(side == left) {
                parent->setLeft(temp);
            }
            if(side == right) {
                parent->setRight(temp);
            }
        } else {
            setRoot(temp);
        }
    }

    void setRoot(AvlNode* n) {
        root = n;
        if(root != 0) root->parent = (AvlNode*)0;
    }

    AvlNode * root;

};

int main() {

    int n, x, y;
    AVLTree tree;

    cin >> n;
    result_holder.assign(n, 0);

    for (int i = 0; i < n; i++) {
        cin >> x >> y;
        tree.insert(x);
    }

    for (int i = 0; i < n; i++) {
        cout << result_holder[i] << endl;
    }

    return 0;
}

