#include <iostream>
#include <vector>

using namespace std;

template<class T>
class RangedTree {

private:
    struct RTNode {

        RTNode(T value, int index)
            : left(0), right(0),
              start(index), end(index),
              max(value), add(0) {
        }

        RTNode(RTNode *left, RTNode *right)
            : left(left), right(right), start(left->start), end(right->end) {
            this->max = left->max > right->max ? left->max : right->max;
            this->add = 0;
        }

        ~RTNode(){
            delete left;
            delete right;
        }

        T maximum(int start, int end) {
            if (start == this->start && this->end == end) {
                return this->add + this->max;
            } else {
                if (end <= left->end) {                 //entire range in the left subtree
                    return left->maximum(start, end) + this->add;
                } else if (right->start <= start) {     //entire range in the right subtree
                    return right->maximum(start, end) + this->add;
                } else {
                    T left_max = left->maximum(start, left->end);
                    T right_max = right->maximum(right->start, end);
                    return this->add + (left_max > right_max ? left_max : right_max);
                }
            }
        }

        T adddd(int start, int end, T a) {
            if (this->start == start && this->end <= end) {
                this->add += a;
            } else {
                T left_max, right_max;
                if (end <= left->end) {
                    left_max = left->adddd(start, end, a);
                    right_max = right->max + right->add;
                } else if (right->start <= start) {
                    left_max = left->max + left->add;
                    right_max = right->adddd(start, end, a);
                } else {
                    left_max = left->adddd(start, left->end, a);
                    right_max = right->adddd(right->start, end, a);
                }
                this->max = left_max > right_max ? left_max : right_max;
            }
            return this->max + this->add;
        }

        RTNode *left;
        RTNode *right;

        int start;
        int end;

        T max;
        T add;
    };

public:
    RangedTree(const vector<T> & arr) {
        this->root = buildTree(arr, 0,  arr.size() - 1);
    }
    ~RangedTree(){
        delete root;
    }

    void add(int start, int end, T a) {
        root->adddd(start, end, a);
    }

    T max(int start, int end) {
        return root->maximum(start, end);
    }

private:
    RTNode *buildTree(const vector<T>& arr, int start, int end) {
        if (start > end) {
            return 0;
        } else if (start == end) {
            return new RTNode(arr[start], start);
        } else {
            RTNode * l = buildTree(arr, start, (start + end) / 2);
            RTNode * r = buildTree(arr, (start + end) / 2 + 1, end);
            return new RTNode(l, r);
        }
    }

    RTNode* root;
};

int main()
{
    int n, q;
    cin >> n >> q;
    vector<int> arr(n);

    for (int i=0; i< n; i++) {
        cin >> arr[i];
    }

    RangedTree<int> rt(arr);

    string action;
    for (int i = 0; i < q; i++) {
        cin >> action;
        if (action == "max") {
            int l, r;
            cin >> l >> r;
            cout << rt.max(l - 1, r - 1) << endl;
        } else {
            int l, r, a;
            cin >> l >> r >> a;
            rt.add(l - 1, r - 1, a);
        }
    }

    return 0;
}

