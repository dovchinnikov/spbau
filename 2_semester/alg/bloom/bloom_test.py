#!/usr/bin/python3

__author__ = 'dany'
ITERATIONS = 1
PERCENTS = range(100, 401, 25)

from subprocess import call
import time


def run(hs, size, dictionary):
    command = './bloom %s %s %s text > /dev/null' % (hs, size, dictionary)
    start_time = time.time()
    call(command, shell=True)
    return time.time() - start_time


def main():
    for d in ['01', '02', '03', '04']:
        with open('%s.plot' % d, 'w') as plot:
            plot.write('#!/usr/bin/gnuplot -p\n'
                       'set grid\n'
                       'set title "Dictionary %s\n' % d)
            plot.write('set xlabel "Filter size/dictionary size, %"\n'
                       'set ylabel "Running time, s"\n'
                       'plot')
            for h in ['murmur', 'murmur3', 'md5']:
                data_file_name = '%s %s' % (h, d)
                plot.write(' "%s"  using 1:2 with lines,' % data_file_name)
                with open(data_file_name, 'w') as f:
                    for s in PERCENTS:
                        runs = [run(h, s, d) for i in range(ITERATIONS)]
                        f.write('%f, %f\n' % (s, sum(runs) / float(ITERATIONS)))
            plot.write(' %f' % run('none', 0, d))
        call('./%s.plot' % d, shell=True)


if __name__ == "__main__":
    main()