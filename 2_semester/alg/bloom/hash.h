#ifndef HASH_H
#define HASH_H

#include <string>
#include <openssl/md5.h>
#include "MurmurHash1.h"
#include "MurmurHash3.h"

using std::string;

namespace hashes {

    struct hash {
        virtual size_t operator()(const string & word) = 0;
    };

    struct murmur : hash {
        size_t seed;
        murmur(size_t seed) : seed(seed) {}
        size_t operator()(const string & word) {
            return MurmurHash1(word.c_str(), word.size(), seed);
        }
    };

    struct murmur3 : hash {
        size_t seed;
        murmur3(size_t seed) : seed(seed) {}
        size_t operator()(const string & word) {
            size_t out[128 / 8 / sizeof(size_t)];
            MurmurHash3_x86_128(word.c_str(), word.size(), seed, out);
            size_t hash = 0;
            for (size_t s : out) {
                hash ^= s;
            }
            return hash;
        }
    };

    struct md5 : hash {
        size_t seed;
        md5(size_t seed) : seed(seed) {}
        size_t operator() (const string & word) {
            size_t md[MD5_DIGEST_LENGTH * sizeof(unsigned char) / sizeof(size_t)];
            MD5((const unsigned char *)word.c_str(), word.size(), (unsigned char *) md);
            size_t hash = 0;
            for (size_t s : md) {
                hash *= seed;
                hash ^= s;
            }
            return hash;
        }
    };
}

#endif // HASH_H
