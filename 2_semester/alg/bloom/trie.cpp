#include "trie.h"

void Trie::put (const string & word, TrieNode & root, size_t level) {
    if (level >= word.size()) {
        return;
    }
    const char key = word[level];
    if (root.children.find(key) == root.children.end()) {
        root.children[key] = TrieNode();
    }
    if (word.size() - 1 == level) {
       root.children[key].terminal = true;
    }
    put(word, root.children[key], level + 1);
}

bool Trie::check(const string & word, const TrieNode & root, size_t level) const {
    if (level >= word.size()) {
        return false;
    }
    const char key = word[level];
    const auto c = root.children.find(key);
    if (c == root.children.end()) {
        return false;
    } else if (word.size() - 1 == level) {
        return c->second.terminal;
    } else {
        return check(word, c->second, level + 1);
    }
}
