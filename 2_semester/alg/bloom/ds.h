#ifndef DATA_STRUCTURE_H
#define DATA_STRUCTURE_H

#include <string>

class DS {
public:
    virtual void put(const std::string &) = 0;
    virtual bool check(const std::string &) const = 0;
};

#endif // DATA_STRUCTURE_H
