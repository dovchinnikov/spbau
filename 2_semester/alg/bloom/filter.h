#ifndef FILTER_H
#define FILTER_H

#include <string>
#include <vector>
#include <cmath>
#include <memory>
#include "ds.h"
#include <unistd.h>

using std::string;
using std::vector;
using std::shared_ptr;

template<typename H>
class Filter : public DS {
public:
    Filter(DS * underlying, const size_t dict_size, const size_t filter_size_percents = 100)
        : underlying(underlying)
        , dict_size(dict_size)
        , filter_size(filter_size_percents * dict_size / 100)
        , bitmap(vector<bool>(this->filter_size))
    {
        size_t k = ceil(log(2) * filter_size / dict_size);
        for (size_t i = 0; i < k; ++i) {
            hashes.push_back(H(i + 1));
        }
    }

    void put(const string & word) {
        for (auto h : hashes) {
            bitmap[h(word) % filter_size] = true;
        }
        underlying->put(word);
    }

    bool check(const string & word) const {
        for (auto h : hashes) {
            if (!bitmap[h(word) % filter_size]) {
                return false;
            }
        }
        return underlying->check(word);
    }
private:
    shared_ptr<DS> underlying;
    size_t dict_size;
    size_t filter_size;
    vector<bool> bitmap;
    vector<H> hashes;
};

#endif // FILTER_H
