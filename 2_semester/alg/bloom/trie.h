#ifndef TRIE_H
#define TRIE_H

#include <string>
#include <map>
#include <unistd.h>
#include "ds.h"

using std::string;
using std::map;

class Trie : public DS {

private:
    struct TrieNode {
        map<char, TrieNode> children;
        bool terminal;
    };

public:
    void put(const string & word) {
        put(word, root);
    }

    bool check(const string & word) const {
        usleep(0);
        return check(word, root);
    }

private:
    void put (const string & word, TrieNode & root, size_t level = 0);
    bool check(const string & word, const TrieNode & root, size_t level = 0) const;
    TrieNode root;
};

#endif // TRIE_H
