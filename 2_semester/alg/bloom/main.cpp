#include <iostream>
#include <fstream>
#include "trie.h"
#include "filter.h"
#include "hash.h"

using namespace std;

int main(const int argc, const char *argv[]) {
    if (argc < 5) {
        cout << "Usage: bloom hash size dict text" << endl;
        return 0;
    }

    ifstream dict_file(argv[3]);
    if (!dict_file) {
        cout << "Cannot open dictionary " << argv[3] << endl;
        return 0;
    }

    ifstream text_file(argv[4]);
    if (!text_file) {
        cout << "Cannot open text " << argv[4] << endl;
        return 0;
    }

    size_t dict_words_count;
    dict_file >> dict_words_count;

    DS *dict;
    {
        int filter_size_percents = atoi(argv[2]);
        string h = argv[1];
        if (h == "murmur") {
            dict = new Filter<hashes::murmur>(new Trie(), dict_words_count, filter_size_percents);
        } else if (h == "murmur3") {
            dict = new Filter<hashes::murmur3>(new Trie(), dict_words_count, filter_size_percents);
        } else if (h == "md5") {
            dict = new Filter<hashes::md5>(new Trie(), dict_words_count, filter_size_percents);
        } else {
            dict = new Trie();
        }
    }

    {
        string word;
        for (size_t i = 0; i < dict_words_count; ++i) {
            dict_file >> word;
            dict->put(word);
        }
    }

    {
        string word;
        size_t words_count;
        text_file >> words_count;
        for (size_t i = 0; i < words_count; ++i) {
            text_file >> word;
            cout << dict->check(word);
        }
    }

    cout << endl;
    return 0;
}

