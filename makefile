clean_tex:
	rm -f *.aux *.log *.out

clean: clean_tex
	rm -f *.pdf
